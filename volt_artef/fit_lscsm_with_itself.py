resultspath='/DATA/Margot/ownCloud/MargotUNIC/LSCSM_data/LSCSM_performance_data/'
from lscsm.checkpointing import loadParams, saveResults, loadVec
import numpy as np
from testArtefVolt import *
from lscsm.LSCSM import LSCSM
from lscsm_adapt.visualizationMgt import plotLSCSM
import matplotlib.pyplot as plt
from lscsm.fitting_temp import fitLSCSM # /!\/!\/!\/!\/!\/!\/!\/!\/!\
from random import sample, seed as rseed
from numpy.random import seed as nseed, normal
from lscsm_adapt.manipulateLSCSMparams import *


#fileprefix='blank'
fileprefix='testSmooth'
resultname='testSmooth'
#nCellsPerPlot=25
nOut=20
nPix=100
nbins=2000
compCorr=True
subfracs=[0.0001,0.0005,0.001,0.005,0.01,0.05,0.1,0.5,1]
nrep=5
#subnouts=[1,10,50,100,500]
subnouts=[1]
#testseeds=np.arange(1,21)
#testseeds=np.arange(1,4)
testseeds=range(5)+range(10,20)
constrained_params=['center_weight', 'x_pos', 'surround_weight', 'size_center', 'y_pos', 'size_surround']
noise_relampl=0
fmintnc_numepochs=60
SGD_numepochs=5000
#batchsizes=[1,10,100,1000]
batchsizes=[5000]
#learning_rates=[10**-5,10**-4,10**-3]
learning_rates=[10**-5,10**-4,10**-3]
#epochsizes=[5,50,100]
epochsizes=[50]
#epochsizes=[1000,500,100,50]
totnrep=600000
changethr=np.zeros(20); #changethr[[15,10,13]]-=10
equalize=True


model={}
fit={}
prefit={}
model['outthresh']=True
fit['outthresh']=True
model['LGNthresh']=False
fit['LGNthresh']=False
model['powerlaw']='SmoothLin'
fit['powerlaw']='SmoothLin'
model['seed']=0
fit['seed']=5
model['second_layer']=True
fit['second_layer']=True
prefit['outthresh']=True
prefit['LGNthresh']=False
prefit['powerlaw']='SmoothLin'
prefit['seed']=1
prefit['second_layer']=False

suppl_params = {
 'epochSize': 1000,
 'numEpochs': 10,
 'seed': fit['seed'],
 'stimsize': np.int(np.sqrt(nPix)),
 'val_frac': 0.2,
# 'batchSize': 100,
# 'learningRate': 0.001,
# 'SGDseed': 0,
 'fittingMode': 'fmin_tnc',
 'nOut': nOut}
        

def createLSCSM(paramfile,outthresh,powerlaw,LGNthresh,seed,second_layer,subout=None):
    meta_params, suppl_params = loadParams(paramfile)
    lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
    K=lscsm.create_random_parametrization(seed)
    if not(outthresh):
        K=setLSCSMparams(lscsm,K,['output_layer_threshold'],[-1000])
    if powerlaw=='SmoothLin':
        meta_params['v1of']='LogisticLoss'
    elif powerlaw==1:
        meta_params['v1of']='SquareRect'
    elif powerlaw==2:
        meta_params['v1of']='SquareRect'
    elif powerlaw==3:
        meta_params['v1of']='Pow3Rect'
    elif powerlaw=='fullsq':
        meta_params['v1of']='Square'
    lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params) 
    
    if LGNthresh:
        meta_params['LGN_treshold']=True
        meta_params['lgnof']=meta_params['v1of']
        lscsm=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
        tempK=lscsm.create_random_parametrization(0)
        i,t=lscsm.free_params['lgn_threshold']
        K=K[:i]+tempK[i:i+t]+K[i:]
        
    if not(second_layer):
        meta_params['second_layer']=False
        lscsmbis=LSCSM(np.zeros((2,nPix)),np.zeros((2,nOut)),**meta_params)
        tempK=lscsmbis.create_random_parametrization(0)
        paramstocopy=lscsmbis.free_params.keys()
        paramstocopy.remove('output_weights')
        K=copyLSCSMparams(lscsm,lscsmbis,K,tempK,paramstocopy)
        lscsm=lscsmbis 
        
    if subout!=None:
        outweights=lscsm.getParam(K,'output_weights').eval()[:,subout]
        outthresh=np.array(lscsm.getParam(K,'output_layer_threshold'))[subout]
        lscsmbis=LSCSM(np.zeros((2,nPix)),np.zeros((2,len(subout))),**meta_params)
        tempK=lscsmbis.create_random_parametrization(0)
        paramstocopy=lscsmbis.free_params.keys()
        paramstocopy.remove('output_weights')
        paramstocopy.remove('output_layer_threshold')
        K=copyLSCSMparams(lscsm,lscsmbis,K,tempK,paramstocopy)
        K=setLSCSMparams(lscsmbis,K,['output_weights','output_layer_threshold'],[outweights,outthresh])
        lscsm=lscsmbis
        
    return lscsm, K             


def multifit(expltype,model,fit,suppl_params,prefit=None,savemodel=False):
    SN=np.concatenate((generate_stim(stimsize=nPix, nbins=nbins, stimtype='Snoise'),np.zeros((1,nPix))),axis=0)
    DN=np.concatenate((generate_stim(stimsize=nPix, nbins=nbins, stimtype='Dnoise'),np.zeros((1,nPix))),axis=0)
    
    paramfilename=resultspath+fileprefix+'_metaparams'
    ntraining=np.int(np.round((1-suppl_params['val_frac'])*SN.shape[0])) 
    subsets=(np.array(subfracs)*ntraining).astype(int)
    
    model['lscsm'], model['K']=createLSCSM(paramfilename,**model)
    fit['lscsm'], fitK0=createLSCSM(paramfilename,**fit)    
    fit['K0']={'initial': fitK0}
    fit['K']={}
    fit['tcorr']={}
    fit['vcorr']={}
    resps={}
    #stims={'SN':SN, 'DN':DN}
    stims={'DN':DN}
    
    if savemodel:
        saveResults(model['lscsm'],{'seed': model['seed'], 'stimsize': np.sqrt(nPix), 'nOut': nOut},K=model['K'],prefix=resultspath+resultname+'_model_s'+str(model['seed'])+'nOut'+str(nOut))    
    
    if model['outthresh']:
        r0=np.zeros((1,nOut))
    else:    
        r0=model['lscsm'].response(np.zeros((1,nPix)),model['K'])

        
    if expltype=='simple':
        for stimtype in stims.keys():
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])
            fit['K'][stimtype],terr,verr,fit['tcorr'][stimtype],fit['vcorr'][stimtype]=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)
        
        
    elif expltype=='prefit':
        prefit['lscsm'], prefit['K0']=createLSCSM(paramfilename,**prefit)
        prefit['K']={}
        prefit['tcorr']={}
        prefit['vcorr']={}
        
        for stimtype in stims.keys():
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            prefit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            prefit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
        
            prefit['K'][stimtype],terr,verr,prefit['tcorr'][stimtype],prefit['vcorr'][stimtype]=fitLSCSM(prefit['lscsm'],list(prefit['K0'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(prefit['seed'])+'_prefit',compCorr=compCorr)
            fit['K0'][stimtype]=copyLSCSMparams(prefit['lscsm'],fit['lscsm'],prefit['K'][stimtype],fit['K0']['initial'],constrained_params)
            fit['K'][stimtype],terr,verr,fit['tcorr'][stimtype],fit['vcorr'][stimtype]=fitLSCSM(fit['lscsm'],list(fit['K0'][stimtype])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_withprefit',compCorr=compCorr)
            fit['K'][stimtype],terr,verr,fit['tcorr'][stimtype],fit['vcorr'][stimtype]=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_default',compCorr=compCorr)
    
    
    elif expltype=='datalen':  
        for stimtype in stims.keys():
            rseed(a=0)  
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0   
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={} 
            for sublen in subsets:
                fit['K'][stimtype][sublen]=np.zeros((len(model['K']),nrep))
                fit['tcorr'][stimtype][sublen]=np.zeros((suppl_params['numEpochs'],nrep))
                fit['vcorr'][stimtype][sublen]=np.zeros((suppl_params['numEpochs'],nrep))
                for irep in range(nrep):
                    choice=sample(np.arange(ntraining),sublen)
                    fit['lscsm'].X.set_value(np.mat(stims[stimtype][:,choice]))
                    fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:,choice]))
                    fit['K'][stimtype][sublen],terr,verr,fit['tcorr'][stimtype][sublen],fit['vcorr'][stimtype][sublen]=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][choice,:]),np.mat(resps[stimtype][choice,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_'+str(sublen)+'_'+str(irep+1),compCorr=compCorr)
                 
                 
    elif expltype=='constrained':
        suppl_params_c=suppl_params.copy()
        suppl_params_c['param_mask']=setLSCSMparams(fit['lscsm'],np.ones(len(model['K'])),constrained_params,np.zeros(len(constrained_params)))
        fit['K0']['constrained']=copyLSCSMparams(model['lscsm'],fit['lscsm'],list(model['K'])[:],list(fit['K0']['initial'])[:],constrained_params)
        for stimtype in stims.keys():
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            fit['K'][stimtype],terr,verr,fit['tcorr'][stimtype],fit['vcorr'][stimtype]=fitLSCSM(fit['lscsm'],list(fit['K0']['constrained'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_k0constrained',compCorr=compCorr)
            fit['K'][stimtype],terr,verr,fit['tcorr'][stimtype],fit['vcorr'][stimtype]=fitLSCSM(fit['lscsm'],list(fit['K0']['constrained'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params_c.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_fullyconstrained',compCorr=compCorr)
            fit['K'][stimtype],terr,verr,fit['tcorr'][stimtype],fit['vcorr'][stimtype]=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_default',compCorr=compCorr)
     
     
    elif expltype=='nOut':
        for stimtype in stims.keys(): 
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0 
            if noise_relampl>0:
                nseed(0)
                noise=normal(np.zeros(resps[stimtype].shape),np.repeat(resps[stimtype].std(axis=0).reshape(1,-1)*noise_relampl,resps[stimtype].shape[0],axis=0))
                resps[stimtype]+=noise
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={}
            for subnout in subnouts:
                fit['tcorr'][stimtype][subnout]=np.zeros((suppl_params['numEpochs'],nrep))
                fit['vcorr'][stimtype][subnout]=np.zeros((suppl_params['numEpochs'],nrep))
                rseed(a=0)
                for irep in range(nrep):
                    choice=np.array(sample(np.arange(nOut),subnout))
                    print 'choice: ', choice
                    print ''
                    fittemp=dict([(keyname,fit[keyname]) for keyname in ['outthresh','powerlaw','LGNthresh','seed','second_layer']])
                    fit['lscsm'], fit['K0']['initial']=createLSCSM(paramfilename,subout=choice,**fittemp)
                    if irep==0:
                        fit['K'][stimtype][subnout]=np.zeros((len(fit['K0']['initial']),nrep))
                    fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
                    fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,choice]))
                    #filename=resultspath+resultname+stimtype+'_nOut'+str(subnout)+'_'+str(irep+1)
                    filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_noise'+str(noise_relampl)+'_nOut'+str(subnout)+'_'+str(irep+1)
                    fit['K'][stimtype][subnout],terr,verr,fit['tcorr'][stimtype][subnout],fit['vcorr'][stimtype][subnout]=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,choice]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,choice]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)
                  
                  
    elif expltype=='fitseed':
        for stimtype in stims.keys():
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0 
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={}
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            for seedval in testseeds:
                fit['seed']=seedval
                K0=fit['lscsm'].create_random_parametrization(seedval) # Rq: ne faudrait-il pas passer par la fonction createLSCSM (si je me souviens bien, sert a faire en sorte qu'un max de parametres initiaux reste identique entre les differentes architectures possibles)
                fit['K'][stimtype][seedval],terr,verr,fit['tcorr'][stimtype][seedval],fit['vcorr'][stimtype][seedval]=fitLSCSM(fit['lscsm'],list(K0)[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(seedval)+'_datalen'+str(nbins)+'_epochsize'+str(suppl_params['epochSize']),compCorr=compCorr)

                
    elif expltype=='modelseed':
        for stimtype in stims.keys():
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={}
            for seedval in testseeds:
                model['seed']=seedval
                #model['lscsm'], model['K']=createLSCSM(paramfilename,**model)
                model['K']=model['lscsm'].create_random_parametrization(seedval)
                resp=model['lscsm'].response(stims[stimtype],model['K'])-r0
                if equalize:
                    resp=(resp-resp.mean(axis=0))*1./resp.std(axis=0)
                    resp-=resp.min()  
                fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
                fit['lscsm'].Y.set_value(np.mat(resp[:ntraining,:]))
                filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_datalen'+str(nbins)+'_epochsize'+str(suppl_params['epochSize'])
                fit['K'][stimtype][seedval],terr,verr,fit['tcorr'][stimtype][seedval],fit['vcorr'][stimtype][seedval]=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resp[:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resp[ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)


    elif expltype=='lowfiringrates':
        for stimtype in stims.keys():
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0 
            normresp=((resps[stimtype]-resps[stimtype].mean(axis=0))*1./resps[stimtype].std(axis=0)).copy()
            normresp-=normresp.min() 
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={}
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_datalen'+str(nbins)+'_epochsize'+str(suppl_params['epochSize'])
            fit['K'][stimtype]['raw'],terr,verr,fit['tcorr'][stimtype]['raw'],fit['vcorr'][stimtype]['raw']=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)
            filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_datalen'+str(nbins)+'_epochsize'+str(suppl_params['epochSize'])+'_normalized'
            fit['K'][stimtype]['normalized'],terr,verr,fit['tcorr'][stimtype]['normalized'],fit['vcorr'][stimtype]['normalized']=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(normresp[:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(normresp[ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)
            rawcellcorr=computeCorr(fit['lscsm'].response(stims[stimtype][ntraining:,:],fit['K'][stimtype]['raw']),model['lscsm'].response(stims[stimtype][ntraining:,:],model))
            subout=np.where(rawcellcorr<0.5)[0]
            sublscsm, subK0=createLSCSM(paramfilename,subout=subout,**fit)
            filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_datalen'+str(nbins)+'_epochsize'+str(suppl_params['epochSize'])+'_lowcorronly'
            fit['K'][stimtype]['lowcorronly'],terr,verr,fit['tcorr'][stimtype]['lowcorronly'],fit['vcorr'][stimtype]['lowcorronly']=fitLSCSM(sublscsm,subK0[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,subout]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,subout]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)

        
    elif expltype=='SGDparams':
        for stimtype in stims.keys():
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0 
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={}
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            for seedval in testseeds:
                fit['seed']=seedval
                K0=fit['lscsm'].create_random_parametrization(seedval) # Rq: ne faudrait-il pas passer par la fonction createLSCSM (si je me souviens bien, sert a faire en sorte qu'un max de parametres initiaux reste identique entre les differentes architectures possibles)               
                suppl_params['fittingMode']='fmin_tnc'
                suppl_params['numEpochs']=fmintnc_numepochs
                filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(seedval)+'_fmintnc'              
                #fit['K'][stimtype][seedval],terr,verr,tcorr,vcorr=fitLSCSM(fit['lscsm'],list(K0)[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)                
                suppl_params['fittingMode']='SGD'                
                for batchsize in batchsizes:
                    suppl_params['batchSize']=batchsize
                    for lrate in learning_rates:
                        suppl_params['learningRate']=lrate
                        for SGDseed in testseeds:
                            suppl_params['SGDseed']=SGDseed
                            suppl_params['numEpochs']=SGD_numepochs
                            filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(seedval)+'_SGDbs'+str(batchsize)+'lr'+str(lrate)+'ss'+str(SGDseed)              
                            fit['K'][stimtype][seedval],terr,verr,tcorr,vcorr=fitLSCSM(fit['lscsm'],list(K0)[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)
                            
    elif expltype=='epochSize':
        for stimtype in stims.keys():
            if np.any(changethr!=0):
                thresh=model['lscsm'].getParam(model['K'],'output_layer_threshold')+changethr
                model['K']=setLSCSMparams(model['lscsm'],model['K'],'output_layer_threshold',thresh)  
            resps[stimtype]=model['lscsm'].response(stims[stimtype],model['K'])-r0 
            if equalize:
                resps[stimtype]=resps[stimtype]*1./resps[stimtype].mean(axis=0)
            fit['K'][stimtype]={}  
            fit['tcorr'][stimtype]={} 
            fit['vcorr'][stimtype]={}
            fit['lscsm'].X.set_value(np.mat(stims[stimtype][:ntraining,:]))
            fit['lscsm'].Y.set_value(np.mat(resps[stimtype][:ntraining,:]))
            for esize in epochsizes:
                suppl_params['epochSize']=esize
                suppl_params['numEpochs']=int(np.ceil(totnrep*1./esize))
                filename=resultspath+resultname+stimtype+'_m'+str(model['seed'])+'f'+str(fit['seed'])+'_nbins'+str(nbins)+'_epochsize'+str(esize)+'_equalized'              
                fit['K'][stimtype][esize],terr,verr,tcorr,vcorr=fitLSCSM(fit['lscsm'],list(fit['K0']['initial'])[:],np.mat(stims[stimtype][:ntraining,:]),np.mat(resps[stimtype][:ntraining,:]),np.mat(stims[stimtype][ntraining:,:]),np.mat(resps[stimtype][ntraining:,:]),fit_params=suppl_params.copy(),checkpointName=filename,compCorr=compCorr)
        


def restoreAndComp(modelprefix,fileexts):
    filenames=[modelprefix+'_model']+[modelprefix+ext for ext in fileexts]
    lscsmObjects={}
    K={}
    error={'training':{}, 'validation':{}}
    corr=error.copy()
    
    for filename in filenames:     
        meta_params, suppl_params = loadParams(resultspath+filename+'_metaparams')
        if filename==filenames[0]:
            nOut=suppl_params['nOut']
        K[filename]=loadVec(resultspath+filename+'_K')
        lscsmObjects[filename]=LSCSM(np.zeros((2,suppl_params['stimsize']**2)),np.zeros((2,nOut)),**meta_params)
    return lscsmObjects, K    
                       
#multifit('lowfiringrates',model.copy(),fit.copy(),suppl_params.copy())
#modelprefix='testSmooth'    
#fileexts=['DN_m0f1_nbins2000_epochsize500_thrchanged','DN_m0f1_nbins2000_epochsize1000_thrchanged','DN_m0f1_nbins2000_epochsize500_equalized','DN_m0f1_nbins2000_epochsize1000_equalized']
#lscsmObjects,K=restoreAndComp(modelprefix,fileexts)                   