import itertools
import numpy as np
import matplotlib.pyplot as plt
from lscsm.checkpointing import loadParams, loadVec
from lscsm.LSCSM import LSCSM
from lscsm_adapt.simulations import generate_stim
from lscsm.visualization import computeCorr
resultspath='/DATA/Margot/ownCloud/MargotUNIC/LSCSM_data/LSCSM_performance_data/'

def loadLSCSMs(namesegments,specvals):
    assert len(namesegments) in [len(specvals),len(specvals)+1]
    lengths=[len(vec) for vec in specvals]
    indices=itertools.product(*[range(l) for l in lengths])
    lscsms={}
    Ks={}
    
    for ifile in range(np.prod(lengths)):
        currindice=indices.next()
        filename=''.join([namesegments[i]+str(specvals[i][currindice[i]]) for i in range(len(specvals))]+namesegments[len(specvals):len(namesegments)])
        print filename
        meta_params, suppl_params = loadParams(filename+'_metaparams')
        lscsms[currindice]=LSCSM(np.zeros((2,int(suppl_params['stimsize']**2))),np.zeros((2,suppl_params['nOut'])),**meta_params) 
        Ks[currindice]=loadVec(filename+'_K')
        
    return lscsms, Ks   


def computeCorrs(lscsms,Ks,theolscsms,theoKs,stim,nparamvals):
    specinds=lscsms.keys()
    corrs=np.zeros((list(nparamvals)+[lscsms[specinds[0]].num_neurons]))
    theoresps=np.zeros((list(nparamvals)+[stim.shape[0],lscsms[specinds[0]].num_neurons]))
    resps=np.zeros((list(nparamvals)+[stim.shape[0],lscsms[specinds[0]].num_neurons]))
    
    for ind in specinds:
        theoresps[ind]=theolscsms[ind].response(stim,theoKs[ind])
        resps[ind]=lscsms[ind].response(stim,Ks[ind])
        corrs[ind]=computeCorr(resps[ind],theoresps[ind])
        
    return corrs, resps, theoresps
        


    
#prefix='testSmooth'
#stimtype='DN'
#nbins=2000
#epochsize=1000
#modelseeds=range(5)+range(10,20)
#fitseeds=range(5,10)
#nPix=100
#nbins=2000
#stimnames={'DN':'Dnoise','SN':'Snoise'}
#val_frac=0.2
#ntraining=np.int(np.round((1-val_frac)*nbins)) 
#    
#namesegments=[resultspath+prefix+stimtype+'_m','f','_datalen'+str(nbins)+'_epochsize'+str(epochsize)]
#specvals=[modelseeds,fitseeds]    
#lscsms,Ks=loadLSCSMs(namesegments,specvals)
#theolscsms=lscsms.copy()
#theoKs={}
#for imseed in range(len(modelseeds)):
#    for ifseed in range(len(fitseeds)):
#        theoKs[(imseed,ifseed)]=theolscsms[(imseed,ifseed)].create_random_parametrization(modelseeds[imseed])
#        
#stim=np.concatenate((generate_stim(stimsize=nPix, nbins=nbins, stimtype=stimnames[stimtype]),np.zeros((1,nPix))),axis=0)        
#corrs,resps,theoresps=computeCorrs(lscsms,Ks,theolscsms,theoKs,stim[ntraining:,:],[len(l) for l in specvals])      
    
fontsize=25
framewidth=3

#plt.figure()
#ax=plt.hist(corrs.reshape(-1),arange(-0.05,1.1,0.1)) 
#tick_params(axis='x', labelsize=fontsize, width=framewidth, size=framewidth*2)
#tick_params(axis='y', labelsize=fontsize, width=framewidth, size=framewidth*2)  
#plt.xlabel('Performance (correlation) on HSM-generated data', fontsize=fontsize)     
#plt.ylabel('Number of HSM units', fontsize=fontsize)  
#for pos in ['top','right','left','bottom']:
#    plt.axes().spines[pos].set_linewidth(framewidth) 

plt.figure()
predicted=corrs>0.5
meanresps=theoresps.mean(axis=2).squeeze()
plt.hist(log(meanresps[~predicted]),color='r',normed=True)
plt.hist(log(meanresps[predicted]),color='b',normed=True)
leg=plt.legend(['<0.5','>0.5'],title='HSM performance',fontsize=fontsize,loc='upper left')
leg.get_frame().set_linewidth(framewidth)
plt.setp(leg.get_title(),fontsize=fontsize)
plt.xlabel('Log of mean response', fontsize=fontsize)
plt.ylabel('Normalized number of units', fontsize=fontsize)
tick_params(axis='x', labelsize=fontsize, width=framewidth, size=framewidth*2)
tick_params(axis='y', labelsize=fontsize, width=framewidth, size=framewidth*2) 
for pos in ['top','right','left','bottom']:
    plt.axes().spines[pos].set_linewidth(framewidth) 