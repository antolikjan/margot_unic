import numpy as np
from lscsm_adapt.simulations import gen_grat_seq
import matplotlib.pyplot as plt
import itertools


def compute_ori_resp(modelFunc,period_vals,ori_vals,phaserat_vals,stimSide=10):
    stims=gen_grat_seq(periods=period_vals,oris=ori_vals,phase_ratios=phaserat_vals,sidesize=stimSide)
    resps=modelFunc(stims.reshape(-1,stimSide**2))-modelFunc(np.zeros((1,stimSide**2)))
    ncells=resps.shape[-1]    
    return resps.reshape(len(period_vals),len(ori_vals),len(phaserat_vals),ncells)


def split_by_respsign(resps):
    ncells=resps.shape[3]
    resp_mean=resps.reshape(-1,ncells).mean(axis=0)
    return resps[:,:,:,resp_mean>0], resps[:,:,:,resp_mean<0]
    
    
def rectify_negresps(resps):
    ncells=resps.shape[3]
    respsign=resps.reshape(-1,ncells).mean(axis=0)
    respsign[respsign==0]=1
    return resps*respsign  


def compute_modulation_ratio(resps,phasedim=2):
    resps_meanphase=np.abs(resps.mean(axis=phasedim))
    resps_modphase=(resps.max(axis=phasedim)-resps.min(axis=phasedim))*0.5
    return resps_modphase*1./resps_meanphase
    
    
def plot_ori_select(resps,period_vals,ori_vals,phaserat_vals,resps_modindex=None,subset=None,mode='max',figname=None):
    ncells=resps.shape[3]
    nrows=np.int(np.ceil(np.sqrt(ncells)))
    ncols=np.int(np.ceil(ncells*1./nrows))
    if mode=='max':
        resps_bestphase=resps.max(axis=2)
    elif mode=='mean':    
        resps_bestphase=np.abs(resps.mean(axis=2))
    elif mode=='F1':
        resps_bestphase=(resps.max(axis=2)-resps.min(axis=2))*0.5
    plt.figure()
    for icell in range(ncells):
        plt.subplot(nrows,ncols,icell+1)
        plt.pcolor(resps_bestphase[:,:,icell],cmap='YlOrRd')
#        plt.xlabel('Orientation (degrees)',fontsize=5)
#        plt.ylabel('Spatial period (pixels)',fontsize=5)
#        xlegends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in ori_vals]))
#        plt.xticks(np.arange(len(xlegends))*0.5,xlegends,fontsize=5)
#        ylegends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in period_vals]))
#        plt.yticks(np.arange(len(ylegends))*0.5,ylegends,fontsize=5)
        #plt.axis('tight')
        plt.axis('off')
        plt.title(str(icell+1),fontsize=8)
    
    plt.figure()
    for icell in range(ncells):
        best_indices=np.array(np.where(resps_bestphase[:,:,icell]==resps_bestphase[:,:,icell].max()))
        plt.subplot(nrows,ncols,icell+1)
        plt.plot(phaserat_vals,resps[best_indices[0],best_indices[1],:,icell].squeeze())
#        plt.xlabel('Phase/spatial period')
#        plt.ylabel('Cell response')
        plt.axis('off')
        plt.title(str(icell+1),fontsize=8)
        
    if resps_modindex!=None:
            mapmed=np.median(resps_modindex.reshape(-1,ncells),axis=0)
            plt.figure()
            for icell in range(ncells):
                plt.subplot(nrows,ncols,icell+1)
                plt.pcolor(resps_modindex[:,:,icell],vmin=0,vmax=mapmed[icell]*3,cmap='YlOrRd')
                plt.axis('off')
                plt.title(str(icell+1),fontsize=8)
                
                
    if subset!=None:
        _,_,best_indices_mean=compute_ori_pref(resps,period_vals,ori_vals,mode='mean')
        _,_,best_indices_F1=compute_ori_pref(resps,period_vals,ori_vals,mode='F1')
        for icell in subset:
            fig=plt.figure()
            count=1
            respmax=np.abs(resps[:,:,:,icell]).max()
            respmean=resps[:,:,:,icell].mean(axis=2)
            for i in range(len(period_vals)):
                for j in range(len(ori_vals)):
                    ax=plt.subplot(len(period_vals),len(ori_vals),count)
                    if np.all([i,j]==best_indices_F1[:,icell]):
                        plt.plot(phaserat_vals,resps[i,j,:,icell],'g',linewidth=3)
                    else:    
                        plt.plot(phaserat_vals,resps[i,j,:,icell],'g')
                    if np.all([i,j]==best_indices_mean[:,icell]):
                        plt.plot([0,1],[respmean[i,j],respmean[i,j]],'r',linewidth=3)
                    else:
                        plt.plot([0,1],[respmean[i,j],respmean[i,j]],'r')
                    plt.plot([0,1],[0,0],'k')
                    plt.axis([0,1,-respmax,respmax])
                    ax.set_xticks([])
                    ax.set_yticks([])
                    if i==len(period_vals)-1:
                        plt.xlabel(str(ori_vals[j]))
                    if j==0:
                        plt.ylabel(str(period_vals[i]))    
                    count+=1
            if figname!=None:        
                fig.savefig(figname+'_'+str(icell+1)+'.png')
                plt.close(fig)                    
        

def compute_circvar(resps,period_vals,ori_vals,mode='mean'):
    rad_ori_vals=np.array(ori_vals)*np.pi/180
    ncells=resps.shape[3]
    _,resps_bestphase,best_indices=compute_ori_pref(resps,period_vals,ori_vals,mode=mode)  
    ori_vects=np.repeat(np.concatenate([np.cos(2*rad_ori_vals),np.sin(2*rad_ori_vals)],axis=0).reshape(2,-1,1),ncells,axis=2)    
    respsum=np.zeros(ncells)    
    for icell in range(ncells):
        ori_vects[:,:,icell]=ori_vects[:,:,icell]*resps_bestphase[best_indices[0,icell],:,icell] 
        respsum[icell]=np.abs(resps_bestphase[best_indices[0,icell],:,icell]).sum()
    vect_result=ori_vects.sum(axis=1)
    vect_result_norm=np.sqrt(vect_result[0,:]**2+vect_result[1,:]**2)*1./respsum 
    
    return 1-vect_result_norm   
    
    
def compute_ori_pref(resps,period_vals,ori_vals,mode='mean'):
    if mode=='max':
        resps_bestphase=resps.max(axis=2)
    elif mode=='mean':    
        resps_bestphase=resps.mean(axis=2)
    elif mode=='F1':
        resps_bestphase=(resps.max(axis=2)-resps.min(axis=2))*0.5
        
    best_indices=np.array(np.where(resps_bestphase==resps_bestphase.max(axis=1).max(axis=0)))
    best_indices=best_indices[:-1,np.argsort(best_indices[-1,:])]    
        
    return np.concatenate([np.array(period_vals)[best_indices[0,:]],np.array(ori_vals)[best_indices[1,:]]],axis=0), resps_bestphase, best_indices   
        
    
#    best_indices_meanphase=np.array(np.where(resps_meanphase==resps_meanphase.max(axis=1).max(axis=0)))
#    assert best_indices_meanphase[-1,:]==np.arange(ncells)
#    best_indices_meanphase=best_indices_meanphase[:-1,:]
    
#    respmin=0
#    respmax=resps_meanphase.max()
#    for icell in range(ncells):
#        plt.figure();
#        plt.pcolor(resps_meanphase[:,:,icell],vmin=respmin,vmax=respmax,cmap='YlOrRd')
#        plt.xlabel('Period (pixels)')
#        plt.ylabel('Orientation (degrees)')
#        
#        cellBI=best_indices_meanphase[:,icell]
#        plt.figure();
#        plt.plot(phaserat_vals,resps[cellBI[0],cellBI[1],:,icell],'b',linewidth=2)
#        plt.xlabel('Phase/period')
#        plt.ylabel('Response amplitude')
        

 
    

