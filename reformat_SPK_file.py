import glob
import numpy as np
from scipy.io import loadmat, savemat


def reformat_key(key,entries):
    key_parts=[]
    length=0
    for entry in entries[1:]:
        key_parts.append(key[length:].split(entry)[0])
        length+=len(key_parts[-1])
    key_parts.append(key[length:])  
    return '_'.join(key_parts)


#prefix='/home/margot/Bureau/Jonathan/*/*_SPK.mat'
prefix='/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/*_SPK.mat'
filenames=sorted(glob.glob(prefix))

for f in filenames:
    old_data=loadmat(f)
    keys=[k for k in old_data.keys() if k[:2]!='__']
    new_data={}
    for k in keys:
        new_data[reformat_key(k,['Ep','Vspk','Vu'])]=old_data[k]
    savemat(f[:-4]+'_reformatted.mat',new_data)    
    
    
def reformat_key(key,entries):
    key_parts=[]
    length=0
    for entry in entries[1:]:
        key_parts.append(key[length:].split(entry)[0])
        length+=len(key_parts[-1])
    key_parts.append(key[length:])  
    return '_'.join(key_parts)