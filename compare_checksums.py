from os.path import basename
from utils.handlebinaryfiles import load_info_table

file_names = ['/home/margot/Bureau/peter_server_checksums.txt',
              '/media/margot/backup_mgt/DATA/extracell_data/raw_elphy_data/sha256_checksums.txt']



def compare_checksums(names1, names2, checksums1, checksums2, rm_path=True):
    
    if rm_path:
        names1 = [basename(name) for name in names1]
        names2 = [basename(name) for name in names2]

    common = sorted(list(set(names1).intersection(names2)))
    same_checksum = [checksums1[names1.index(name)] == checksums2[names2.index(name)] for name in common]
    for i in range(len(common)):
        print common[i] + ' : ' + str(same_checksum[i])
    
    return common, same_checksum    


cs_dicts = [load_info_table(fname,sep='  ') for fname in file_names]
compare_checksums(cs_dicts[0]['filename'], cs_dicts[1]['filename'], cs_dicts[0]['checksum'], cs_dicts[1]['checksum'])