import numpy as np
import matplotlib.pyplot as plt 
from elphy_utils.elphy_misc_utils import load_klusta_binary, elphy_visualize, detect_repeated_samples, elphy_vtags
from data_analysis.extracell_funcs_NEW import load_vtags_matfmt, checkVtags
from miscellaneous.fileListToCsv_NEW import fileListToCsv, elphy_database
from collections import OrderedDict
from elphy_utils.elphy_reader import ElphyFile, elphy_to_klusta
from elphy_utils.elphy_missing_samples import ElphyFileMissingData

elphy_name='/media/margot/backup_mgt/DATA/raw_data/MANIP_2018/MANIP_2018_17/1718_CXRIGHT/1718_CXRIGHT_TUN2.DAT'
#elphy_name='/media/margot/backup_mgt/DATA/raw_data/MANIP_2016/MANIP_2016_46/4616_CXRIGHT/4616_CXRIGHT_NBR1.DAT'
vtag_name='/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/2817_CXLEFT_TUN21_Vtags.mat'
klusta_name='/home/margot/Bureau/0918_CXRIGHT_TUN3_5ep/0918_CXRIGHT_TUN3_5ep.dat'
txt_name='/home/margot/Bureau/0918_CXRIGHT_TUN3_5ep/0918_CXRIGHT_TUN3_5ep_eplengths.txt'
elphy_dir='/media/margot/backup_mgt/DATA/raw_data/MANIP_2015'
database_dir='/media/margot/DATA/Margot/ownCloud/DATA/manips_database/test2'
n_ep=10

#print 'loading elphy file'
ef=ElphyFile(elphy_name)
#test=ef.episodes[1].get_data()
#print 'loading new vtags'
#new_vtags=elphy_vtags(ef)
#checkVtags(new_vtags[0])
#print 'loading old vtags'
#old_vtags=load_vtags_matfmt(vtag_name)
#checkVtags(old_vtags[0])
#np.where(np.array([len(v) for v in new_vtags[0]])!=np.array([len(v) for v in old_vtags[0]]))
#set(np.array([len(v) for v in new_vtags[0]])-np.array([len(v) for v in old_vtags[0]]))
#np.any([np.any(np.abs(np.diff(new_vtags[0][iep]-old_vtags[0][iep]))>0.000001) for iep in range(len(old_vtags))])
#elphy_visualize(ef,ep_num=100)
elphy_to_klusta(elphy_name,ep_nums=range(n_ep)+range(ef.n_episodes/2-n_ep/2,ef.n_episodes/2+n_ep/2)+range(ef.n_episodes-n_ep,ef.n_episodes),junction_len=150,ep_len_file=True)
#elphy_to_klusta(elphy_name,ep_nums='all',junction_len=150,ep_len_file=True)
#fileListToCsv(dirname)
#elphy_database(elphy_dir,database_dir)
#test=load_klusta_binary(klusta_name,txt_name)
#
#for iep in range(len(test)):
#    reshaped_ep=(test[iep]-test[iep].mean(axis=0))*0.3/test[iep].std(axis=0)+np.arange(test[iep].shape[1])
#    plt.figure();
#    plt.plot(reshaped_ep)


#plt.figure();
#for iep in range(len(repeated)):
#    plt.plot(repeated[iep],[iep]*len(repeated[iep]),'o')

#repeated=[detect_repeated_samples(ep) for ep in ef.episodes]
#holes_mat=np.zeros([ef.n_episodes,ef.episodes[0].channels[0].nsamp])
#for iep in range(ef.n_episodes):
#    holes_mat[iep]+=iep%2
#    holes_mat[iep,repeated[iep]]=10
#plt.figure();
#plt.imshow(holes_mat,interpolation='none',aspect=100)    


#maxNmov = 4000
#key = OrderedDict([('Titre', ('str', 30)), ('NbParam', 'int'), ('actif', ('bool', 10)),
#       ('NbTest', ('int', 10)),
#       ('ValeurMin', ('ext', 10)), ('ValeurMax', ('ext', 10)),
#       ('x1', 'ext'), ('y1', 'ext'), ('dx1', 'ext'), ('dy1', 'ext'),
#       ('theta1', 'ext'), ('ShapeStim', 'int'), ('TypeStim', 'int'),
#       ('dtOn1', 'ext'), ('dtOff1', 'ext'),
#       ('nbCycle1', 'int'), ('size1', 'ext'), ('DoBlank', 'bool'),
#       ('TypeStimGG', 'int'),
#       ('valeurs', ('ext', (10, 20))), ('TabFixvalue', ('ext', (10, 24))),
#       ('TabFixvalueMov', ('ext', (10, maxNmov))),
#       ('FrameOrder', 'int'), ('EyeMvtNI', 'int'),
#       ])
#print(ef.file_info)
#ef.decode_file_info(key)
#print(ef.file_info)

#times,units=ef.episodes[5].spikes.get_data(do_waves=False)
#print times[0]/(1000.*ef.episodes[5].Dxu)