from glob import glob
from os import rename
from os.path import basename
from os.path import join as path_join

base_dir='/home/margot/Cluster/HSM/Explore_params/to_run'
done_dir='/home/margot/Cluster/HSM/Explore_params/done'


done_files=glob(path_join(base_dir,'*_BEST_*'))
done_files_bis=[''.join(fname.split('_BEST')) for fname in done_files]

for fname in done_files+done_files_bis:
    rename(fname,path_join(done_dir,basename(fname)))