import numpy as np
from utils.simulations import generate_gratingSeq, circular_mask
from miscellaneous.convertVideo import saveMatToTex
import itertools

#mov_name='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_24_120pix_stdmax_20180416_NEW/grating_pairs_stdmax_'
#mov_name='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_24_195pix_stdmax_20180416/bar_pairs_stdmax_'
mov_name='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/gratings_108_120pix_stdmax_20180424/gratings_stdmax_'
#mov_name='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_freq06_stdmax_20180423/grating_pairs_stdmax_'
#stiminfo_file='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_24_120pix_stdmax_20180416_NEW/grating_pairs_24_120pix_stdmax_20180416_stiminfo.txt'
stiminfo_file='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/gratings_108_120pix_stdmax_20180424/gratings_108_120pix_stdmax_20180424_stiminfo.txt'
#stiminfo_file='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_freq06_stdmax_20180423/grating_pairs_144_120pix_freq06_stdmax_20180423_stiminfo.txt'

stimbank_id=80000
ext='.tex'
n_zeros=4
offset=104
second_pos=48
stim_dur=12
#stim_dur=60
stim_mean=127.5
stim_scale=127.5
side_npix=120
#side_npix=195
stim_type='sin'
#stim_type='bar'
mask_inner_radius=side_npix/2.*2/7.5
mask_outer_radius=side_npix/2.*2/7.5*3./2



#params={
#'dton' : [[12], [12]],
#'ISI' : [[6,12,24,36]],
#'period' : [[1/0.6*120/7.5], [1/0.6*120/7.5]],
#'orientation' : [[0,45,90,145], [0]],
#'phase' : [[0,1./3,2./3],[0,1./3,2./3]],
#'color' : [[np.nan],[np.nan]]}

#params=[
#{'dton' : [[12], [12]],
#'ISI' : [[3,6,12,18,24,36]],
#'period' : [[1/0.6*120/7.5], [1/0.6*120/7.5]],
#'orientation' : [[0], [0]],
#'phase' : [[ph],[ph]],
#'color' : [[np.nan],[np.nan]]}
#for ph in [0,0.25,0.5,0.75]]
    
#params=[
#{'dton' : [[12], [12]],
#'ISI' : [[6,12,24,36]],
#'period' : [[44], [44]],
#'orientation' : [[0], [0]],
#'phase' : [[ph],[ph]],
#'color' : [[col],[col]]}
#for col, ph in itertools.product([1,-1],[-0.25,0,0.25])]   

#params={
#'dton' : [[12]],
#'ISI' : [[np.nan]],
#'period' : [[1/0.5*120/7.5,1/0.6*120/7.5,1/0.7*120/7.5,1/0.8*120/7.5]],
#'orientation' : [[0,30,60,90,120,150]],
#'phase' : [[0,0.25,0.5,0.75]],
#'color' : [[np.nan]]}  

params={
'dton' : [[12]],
'ISI' : [[np.nan]],
'period' : [[1/0.6*120/7.5]],
'orientation' : [[0]],
'phase' : [[0,0.25,0.5,0.75]],
'color' : [[np.nan]]} 
    
#params={
#'dton' : [[12]],
#'ISI' : [[np.nan]],
#'period' : [[1/0.6*120/7.5]],
#'orientation' : [[0,90]],
#'phase' : [[0,0.5]],
#'color' : [[np.nan]]}

if isinstance(params,dict):
    params=[params]
n_combinations=0
bar_length=2*mask_inner_radius if (mask_inner_radius is not None) else None
for iparam in range(len(params)):
    pm=params[iparam]
    offset+=n_combinations
    
    param_order=['period','orientation','phase','color','dton']
    param_numtype=['float','float','float','float','int']
    n_grating_types=np.prod(np.array([[len(l) for l in pm[key]] for key in set(pm.keys())-set(['ISI'])]),axis=0)
    movie_ngrat=len(n_grating_types)
    #combinations=list(itertools.product(*[pm['ISI'][0]]+[pm[key][0] for key in param_order]+[pm[key][1] for key in param_order]))
    combinations=list(itertools.product(*pm['ISI']+list(itertools.chain(*[[pm[key][igrat] for key in param_order] for igrat in range(movie_ngrat)]))))
    n_combinations=len(combinations)
    print 'Number of pairs :', n_combinations, 'Expected duration :', n_combinations*(stim_dur*1./60+7), 's'
    gratings=[[]]*movie_ngrat
    
    
    for igrat in range(len(gratings)):
        factors=np.array([1 if np.isnan(col) else col for col in pm['color'][igrat]])
        gratings[igrat]=generate_gratingSeq(periods=pm['period'][igrat],oris=pm['orientation'][igrat],phase_ratios=pm['phase'][igrat],factors=factors,sidesize=side_npix,functype=stim_type,bar_length=bar_length)
#        gratings[igrat]=gratings[igrat].reshape(gratings[igrat].shape[:-1]+(1,side_npix**2)).repeat(len(pm['color'][igrat]))
#        for icolor in range(len(pm['color'][igrat])):
#           gratings[igrat] 
        gratings[igrat]=gratings[igrat].reshape([np.prod(gratings[igrat].shape[:-1]),1,side_npix**2])
        
    if movie_ngrat==2:    
        pairs=np.zeros([len(pm['ISI'][0])]+list(n_grating_types)+[stim_dur,side_npix,side_npix])
        mask=circular_mask([side_npix,side_npix],inner_radius=mask_inner_radius,outer_radius=mask_outer_radius) if ((mask_inner_radius is not None) and (mask_outer_radius is not None) and (stim_type=='sin')) else 1.
        count=1
        for iISI in range(len(pm['ISI'][0])):
            ISI=pm['ISI'][0][iISI]
            for igrat1 in range(gratings[0].shape[0]):
                for idton1 in range(len(pm['dton'][0])):
                    dton1=pm['dton'][0][idton1]
                    for igrat2 in range(gratings[1].shape[0]):
                        for idton2 in range(len(pm['dton'][1])):
                            dton2=pm['dton'][1][idton2]
                            init_dur=second_pos-ISI-dton1
                            assert init_dur>=0
                            final_dur=stim_dur-second_pos-dton2
                            assert final_dur>=0
                            movie=np.concatenate([np.zeros([init_dur,side_npix**2]),np.repeat(gratings[0][igrat1],dton1,axis=-2),np.zeros([ISI,side_npix**2]),np.repeat(gratings[1][igrat2],dton2,axis=-2),np.zeros([final_dur,side_npix**2])])
                            movie=movie.reshape(-1,side_npix,side_npix)*mask*stim_scale+stim_mean
                            pairs[iISI,igrat1*len(pm['dton'][0])+idton1][igrat2*len(pm['dton'][1])+idton2]=movie
                            print mov_name+str(count+offset).zfill(n_zeros)+ext
                            saveMatToTex(movie,mov_name+str(count+offset).zfill(n_zeros)+ext)
                            count+=1
                            
                            
    elif movie_ngrat==1:
        movs=np.zeros(list(n_grating_types)+[stim_dur,side_npix,side_npix])
        mask=circular_mask([side_npix,side_npix],inner_radius=mask_inner_radius,outer_radius=mask_outer_radius) if ((mask_inner_radius is not None) and (mask_outer_radius is not None) and (stim_type=='sin')) else 1.
        count=1
        for igrat in range(gratings[0].shape[0]):
            for idton in range(len(pm['dton'][0])):
                dton=pm['dton'][0][idton]
                final_dur=stim_dur-dton
                assert final_dur>=0
                movie=np.concatenate([np.repeat(gratings[0][igrat],dton,axis=-2),np.zeros([final_dur,side_npix**2])])
                movie=movie.reshape(-1,side_npix,side_npix)*mask*stim_scale+stim_mean
                movs[igrat*len(pm['dton'][0])+idton]=movie
                saveMatToTex(movie,mov_name+str(count+offset).zfill(n_zeros)+ext)
                count+=1
                            
    
    mask_size=np.nan if ((mask_inner_radius is None) or (mask_outer_radius==None)) else 2*mask_inner_radius
    if offset>0:
        open_mode='a'
    else:
        open_mode='w'    
    with open(stiminfo_file,open_mode) as f:
        if offset==0:
            f.write(' '.join(['stim_id','stim_type','pixel_size','ISI']+[p+str(1) for p in param_order]+[p+str(2) for p in param_order]+['theo_nframes','mask_size'])+'\n')
            f.write(' '.join(['int','string','int','int']+param_numtype*2+['int','float'])+'\n')
        else:
            f.write('\n')
        if movie_ngrat==2: 
            stimtype_name=('bar_pair' if stim_type=='bar' else 'grating_pair')
            f.write('\n'.join([' '.join([str(i+1+offset+stimbank_id),stimtype_name,'1']+[str(num) for num in combinations[i]]+[str(stim_dur),str(mask_size)]) for i in range(n_combinations)]))
        elif movie_ngrat==1:  
            stimtype_name=('bar' if stim_type=='bar' else 'grating')
            f.write('\n'.join([' '.join([str(i+1+offset+stimbank_id),stimtype_name,'1']+[str(num) for num in combinations[i]]+['nan']*(len(combinations[i])-1)+[str(stim_dur),str(mask_size)]) for i in range(n_combinations)]))    
