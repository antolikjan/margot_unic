import numpy as np
from miscellaneous.convertVideo import loadMovieList
from collections import OrderedDict

stim_list_file='/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/2217_CXRIGHT_TUN14.DAT_order.txt'
stim_corresp_file='/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/2217_CXRIGHT_TUN14.DAT_corr.txt'
stim_info_file='/home/margot/Bureau/natmov_800_stretched_stimtypes_temp.txt'
out_name='/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/2217_CXRIGHT_TUN14_stiminfo.txt'
header_nlines=2
to_do=['create_stiminfo','convert']
#stim_types=OrderedDict([ (('id','int'), [np.arange(120)+1,np.array([121])]), (('type','str'), ['natmov','blank']) ])
stim_types=OrderedDict([ (('id','int'), [np.arange(120)+1]) ])


if 'create_stiminfo' in to_do:
    
    infos=stim_types.keys()
    n_stim_types=len(stim_types[infos[0]])
    
    with open(stim_info_file,'w') as f:
        f.write( '\n'.join([' '.join([str(tup[i]) for tup in infos]) for i in range(len(infos[0]))])+'\n' )
        for i_type in range(n_stim_types):
            n_stims=len(stim_types[infos[0]][i_type])
            for i_stim in range(n_stims):
                f.write(' '.join([str(stim_types[infos[0]][i_type][i_stim])]+[str(stim_types[infos[i]][i_type]) for i in range(1,len(infos))])+'\n')


if 'convert' in to_do:
    
    stim_list=np.array(loadMovieList(stim_list_file,lookfor='STIM: ',convert_to=int))
    if bool(stim_corresp_file):
        stim_corresp=np.array(loadMovieList(stim_corresp_file,lookfor='SP',convert_to=float)).astype(int)
    else:
        stim_corresp=np.arange(stim_list.max())+1    
    with open(stim_info_file,'r') as f:
        stim_infos_lines=f.readlines()
    
    out_lines=[stim_infos_lines[i] for i in range(header_nlines)+list(np.array(stim_corresp[stim_list-1])+header_nlines-1)]
    with open(out_name,'w') as f:
        for line in out_lines:
            f.write(line)