import numpy as np

def histeq(im,nbr_bins=256):
#From http://www.janeriksolem.net/histogram-equalization-with-python-and.html
   #get image histogram
   imhist,bins = np.histogram(im.flatten(),nbr_bins,normed=True)
   cdf = imhist.cumsum() #cumulative distribution function

   #use linear interpolation of cdf to find new pixel values
   im2 = np.interp(im.flatten(),bins[:-1],cdf)

   return im2.reshape(im.shape), cdf
  
import glob
import matplotlib.pyplot as plt
import os
from miscellaneous.convertVideo import loadFrame
from utils.handlebinaryfiles import *

im_fmt='B'
im_endian='<'
#im_shape=np.array([1024,1536])
im_shape=np.array([300,300])
   
im_dir='/DATA/Margot/tex_stimuli/van_hateren/vanhateren_imc300_images'
im_ext=''
n_col=5
offset=100
max_nim=20
image_names=sorted(glob.glob(os.path.join(im_dir,'*'+im_ext)))

for i_im in range(min(len(image_names),max_nim)):
    if im_fmt=='image':
        im=loadFrame(image_names[i_im+offset])
    else:
        im=loadVec(image_names[i_im],fmt=im_fmt,endian=im_endian).reshape(im_shape)
    if i_im%n_col==0:
        if i_im>0:
            plt.tight_layout()
        plt.figure(figsize=[2*n_col,2])     
    plt.subplot(2,n_col,i_im%n_col+1)
    plt.imshow(im,interpolation='none',cmap='gray')
    plt.axis('off')
    plt.subplot(2,n_col,n_col+i_im%n_col+1)
    plt.imshow(histeq(im)[0],interpolation='none',cmap='gray')
    plt.axis('off')