import numpy as np
import glob
from miscellaneous.convertVideo import loadMovieList, readTexHeader
from collections import OrderedDict
from utils.handlebinaryfiles import load_info_table, save_info_table


stim_list_file=None
stim_corresp_file=None
stim_info_file='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/stimbankinfo_mixedtypes_120_sd60_NEW.txt'
out_name='/home/margot/Bureau/0918_metadata/0918_CXLEFT_CXRIGHT_TUN3_stiminfo.txt'
header_nlines=2
to_do=['create_stiminfo','concatenate_gratings']

#grating_files={'filename':['/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_1440_std60_201801/grating_pairs_1440_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_stdmax_20180225/grating_pairs_144_120pix_stdmax_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_32_195pix_stdmax_20180225/bar_pairs_32_195pix_stdmax_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_432_120pix_stdmax_20180302/grating_pairs_432_120pix_stdmax_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_24_120pix_stdmax_20180416/grating_pairs_24_120pix_stdmax_20180416_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_24_195pix_stdmax_20180416/bar_pairs_24_195pix_stdmax_20180416_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_freq06_stdmax_20180423/grating_pairs_144_120pix_freq06_stdmax_20180423_stiminfo.txt',
#                           '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/gratings_108_120pix_stdmax_20180424/gratings_108_120pix_stdmax_20180424_stiminfo.txt'],
#               'offset': range(10000,90000,10000),
#               'n_movies': [1440,144,32,432,24,24,144,108],
#               'type': ['grating_pair','grating_pair','bar_pair','grating_pair','grating_pair','bar_pair','grating_pair','grating']}            
#n_gratings=len(grating_files['filename'])

grating_files = ['/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_1440_std60_201801/grating_pairs_1440_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_stdmax_20180225/grating_pairs_144_120pix_stdmax_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_32_195pix_stdmax_20180225/bar_pairs_32_195pix_stdmax_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_432_120pix_stdmax_20180302/grating_pairs_432_120pix_stdmax_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_24_120pix_stdmax_20180416/grating_pairs_24_120pix_stdmax_20180416_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/bar_pairs_24_195pix_stdmax_20180416/bar_pairs_24_195pix_stdmax_20180416_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_144_120pix_freq06_stdmax_20180423/grating_pairs_144_120pix_freq06_stdmax_20180423_stiminfo.txt',
                 '/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/gratings_108_120pix_stdmax_20180424/gratings_108_120pix_stdmax_20180424_stiminfo.txt']
n_gratings=len(grating_files)


#stim_types=OrderedDict([(('stim_id','int'), [np.arange(i*300,(i+1)*300)+1 for i in range(6)]),
#                        (('stim_type','str'), ['natmov','natmov_randphase','natmov_randframe','natmov_randspatpix','natmov_randallpix','natmov_randframe3']),
#                        (('stim_npix','int'), [20]*6),
#                        (('pixel_size','int'), [1]*6),
#                        (('theo_nframes','int'), [600]*6),
#                        (('mask_npix','float'), [20*2./3]*6),
#                        (('ISI','float'), [np.nan]*6)])
#for i in [1,2]:
#    for tup in [('period','float'),('orientation','float'),('phase','float'),('dton','float'),('color','float')]:
#        stim_types[(tup[0]+str(i), tup[1])] = [np.nan]*6     


stim_types=OrderedDict([(('stim_id','int'), [np.arange(i*100,(i+1)*100)+1 for i in range(6)]+[np.arange(i*100,(i+2)*100)+1 for i in [6,8,10]]+[np.arange(i*100,(i+3)*100)+1 for i in [12,15]]+[np.arange(1801,2001)]),
                        (('stim_type','str'), ['vanhateren','vanhateren_randphase','natmov','natmov_randframe','densenoise','vanhateren','vanhateren','natmov','densenoise','densenoise_halfdensity','densenoise_densercenter','natmov_randframe']),
                        (('stim_npix','int'), [120]*12),
                        (('pixel_size','int'), [1,1,1,1,6,6,1,1,6,6,6,1]),
                        (('theo_nframes','int'), [600]*12),
                        (('mask_npix','float'), [120*2./3]*12),
                        (('ISI','float'), [np.nan]*12)])
for i in [1,2]:
    for tup in [('period','float'),('orientation','float'),('phase','float'),('dton','float'),('color','float')]:
        stim_types[(tup[0]+str(i), tup[1])] = [np.nan]*12                           
                                        
#stim_types=OrderedDict([(('stim_id','int'), [np.arange(i*300,(i+1)*300)+1 for i in range(6)]+[grating_files['offset'][i]+np.arange(grating_files['n_movies'][i])+1 for i in range(n_gratings)]),
#                        (('stim_type','str'), ['natmov','natmov_randphase','natmov_randframe','natmov_randspatpix','natmov_randallpix','natmov_randframe3']+grating_files['type']),
#                        (('pixel_size','int'), [1]*6+[1]*n_gratings)]
#                        (('theo_nframes','int'), []))
#stim_types=OrderedDict([(('id','int'), [np.arange(i*100,(i+1)*100)+1 for i in range(6)]+[np.arange(i*100,(i+2)*100)+1 for i in [6,8,10]]+[np.arange(i*100,(i+3)*100)+1 for i in [12,15]]+[np.arange(1801,2001)]+[grating_files['offset'][i]+np.arange(grating_files['n_movies'][i])+1 for i in range(n_gratings)]),
#                        (('type','str'), ['vanhateren','vanhateren_randphase','natmov','natmov_randframe','densenoise','vanhateren','vanhateren','natmov','densenoise','densenoise_halfdensity','densenoise_densercenter','natmov_randframe']+grating_files['type']),
#                        (('pixel_size','int'), [1,1,1,1,6,6,1,1,6,6,6,1]+[1]*n_gratings)])
#stim_types=OrderedDict([(('id','int'), [np.arange(i*100,(i+1)*100)+1 for i in range(6)]+[np.arange(i*100,(i+2)*100)+1 for i in [6,8,10]]+[np.arange(i*100,(i+3)*100)+1 for i in [12,15]]+[np.arange(1801,2001)]),
#                        (('type','str'), ['vanhateren','vanhateren_randphase','natmov','natmov_randframe','densenoise','vanhateren','vanhateren','natmov','densenoise','densenoise_halfdensity','densenoise_densercenter','natmov_randframe']),
#                        (('pixel_size','int'), [1,1,1,1,6,6,1,1,6,6,6,1])])
#stim_types=OrderedDict([(('id','int'), [np.arange(i*50,(i+1)*50)+1 for i in range(4)]),
#                        (('type','str'), ['vanhateren','vanhateren_randphase','densenoise','densenoise']),
#                        (('pixel_size','int'), [1,1,6,3])])


if 'create_stiminfo' in to_do:
    
    infos=stim_types.keys()
    n_stim_types=len(stim_types[infos[0]])
    
    with open(stim_info_file,'w') as f:
        f.write( '\n'.join([' '.join([str(tup[i]) for tup in infos]) for i in range(len(infos[0]))])+'\n' )
        for i_type in range(n_stim_types):
            n_stims=len(stim_types[infos[0]][i_type])
            for i_stim in range(n_stims):
                f.write(' '.join([str(stim_types[infos[0]][i_type][i_stim])]+[str(stim_types[infos[i]][i_type]) for i in range(1,len(infos))])+'\n')


if 'add_keys' in to_do:
    
    target_keys=['stim_id','stim_type','ISI','period1','orientation1','phase1','dton1','color1','period2','orientation2','phase2','dton2','color2']
    for i_gf in range(len(grating_files)):
        grating_info=load_info_table(grating_files['filename'][i_gf])
        grating_info=OrderedDict([(k,grating_info[k] if k in grating_info.keys() else [np.nan]*len(grating_info.values()[0])) for k in target_keys])
        save_info_table(grating_info,grating_files['filename'][i_gf].split('.')[0]+'_TEST.txt')
    


if 'add_gratings_old' in to_do:
    
    stim_info=load_info_table(stim_info_file)
    offsets=[0]+sorted(grating_files['offset'])+[np.inf]
    stim_cat=np.zeros(len(stim_info['id'])).astype(int)
    for icat in range(len(offsets)-1):
        stim_cat[(stim_info['id']>=offsets[icat]) & (stim_info['id']<offsets[icat+1])]=icat    
    grating_info=[None]+[load_info_table(grating_files['filename'][icat]) for icat in np.argsort(grating_files['offset'])]
    assert np.all([set(gi.keys())==set(grating_info[1].keys()) for gi in grating_info[1:]])
    
    for key in grating_info[1].keys()[1:]:
        stim_info[key]=[]
        for i_stim in range(len(stim_info['id'])):
            if stim_cat[i_stim]>0:
                gi=grating_info[stim_cat[i_stim]]
                stim_info[key].append(gi[key][list(gi['id']).index(stim_info['id'][i_stim]-offsets[stim_cat[i_stim]])])
            else:
                stim_info[key].append(np.NaN)
    save_info_table(stim_info,stim_info_file.split('.')[0]+'_grat.txt')    



if 'concatenate_gratings' in to_do:

    stim_info=load_info_table(stim_info_file)
    for gfile in grating_files:
        grat_info=load_info_table(gfile)
        n_mov=len(grat_info.values()[0])
        for key in stim_info.keys():
            if key in grat_info.keys():
                stim_info[key]=np.array(list(stim_info[key])+list(grat_info[key]))
            else:
                stim_info[key]=np.array(list(stim_info[key])+[np.nan]*n_mov)
    save_info_table(stim_info,stim_info_file.split('.')[0]+'_grat.txt')             



if 'convert' in to_do:
    
    stim_list=np.array(loadMovieList(stim_list_file,lookfor='STIM: ',convert_to=int))
    if bool(stim_corresp_file):
        stim_corresp=np.array(loadMovieList(stim_corresp_file,lookfor='SP',convert_to=float)).astype(int)
    else:
        stim_corresp=np.arange(stim_list.max())+1    
    with open(stim_info_file,'r') as f:
        stim_infos_lines=f.readlines()
    stim_info=load_info_table(stim_info_file)    
    
    #out_lines=[stim_infos_lines[i] for i in range(header_nlines)+list(np.array(stim_corresp[stim_list-1])+header_nlines-1)]
    out_lines=[stim_infos_lines[i] for i in range(header_nlines)+list(np.array([list(stim_info['id']).index(j) for j in stim_corresp[stim_list-1]])+header_nlines)]
    with open(out_name,'w') as f:
        for line in out_lines:
            f.write(line)
            

if 'add_info_bis' in to_do: 
           
    dir_name='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/gratings_and_bars/grating_pairs_432_120pix_stdmax_20180302'
    offset=40000
    stim_type='grating_pairs'
    mask_size=2./3
    
    
    info_name=glob.glob(dir_name+'/*.txt')[0]
    tex_name=glob.glob(dir_name+'/*.tex')[0]
    info=load_info_table(info_name)
    n_stim=len(info.values()[0])
    tex_info=readTexHeader(tex_name)
    new_info=OrderedDict()
    new_info['stim_id']=info['id']+offset
    new_info['stim_type']=np.array([stim_type]*n_stim)
    new_info['pixel_size']=np.ones(n_stim,dtype=int)
    for key in ['ISI','period1','orientation1','phase1','dton1','color1','period2','orientation2','phase2','dton2','color2']:
        new_info[key]=info[key]
    new_info['theo_nframes']=np.array([tex_info['num_frames']]*n_stim)
    new_info['mask_size']=np.array([tex_info['frame_shape'][0]*mask_size]*n_stim)
    
    save_info_table(new_info,info_name[:-4]+'_NEW.txt')            