import numpy as np
import glob
from utils.handlebinaryfiles import *
from miscellaneous.convertVideo import playMovieFromMat, compressHistTail, initializeTex, loadFrame, loadSubTexMovie, saveMatToTex, loadTexMovie, rescale_with_saturation
from utils.various_tools import downsample
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat
import os
from utils.fourier_utils import randomize_phases


colors='rgbycmk'
to_do=['upsample']
#to_do=['frame_infos','crop_frames','merge_to_texmovs']
im_dir='/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_800_stretched'
framesinfo_file='/DATA/Margot/tex_stimuli/natural_movies/frames_infos_temporal_tail3.mat'
framesexamples_file='/DATA/Margot/tex_stimuli/van_hateren/frames_examples_temporal_tail3.mat'
#im_shape=np.array([1920,2560]) # np.array([1024,1536])
im_shape=np.array([600,800,800])
downsample_fact=1
righttail_pos=97
lefttail_pos=3
std_threshold=25
temporal_std_threshold=25
red_std_margin=30
redstd_threshold=25
temporal_redstd_threshold=25
normmin_threshold=-2.965
normmax_threshold=2.965
tail_max_percent=0.2
offset=0
#cuts=np.array([[0,300,0,300],[0,300,300,600],[0,300,600,900],[0,300,900,1200],[300,600,0,300],[300,600,300,600],[300,600,600,900],[300,600,900,1200],[600,900,0,300],[600,900,300,600],[600,900,600,900],[600,900,900,1200]])
#cuts=np.array([[0,600,400,520,400,520],[0,600,600,720,600,720]])
cuts='most_possible'
frame_shape=np.array([600,120,120])
#cuts=np.array([[0,500,0,500],[0,500,500,1000],[0,500,1000,1500],[500,1000,0,500],[500,1000,500,1000],[500,1000,1000,1500]])
#ext='.imc'
#ext='.imc'
ext='.tex'
im_fmt='tex_movie'
#im_fmt='image' #'tex_movie' # 'H'
im_endian='>'    
im_margin=2
n_examples=15
frame_by_frame=True

corr_imshape=im_shape.copy()
corr_imshape[1:]=np.ceil((im_shape[1:]-2*im_margin)*1./downsample_fact).astype(int)
if cuts=='most_possible':
    true_cuts=[[i*frame_shape[0],(i+1)*frame_shape[0],j*frame_shape[1],(j+1)*frame_shape[1],k*frame_shape[2],(k+1)*frame_shape[2]] for i in range(int(corr_imshape[0]/frame_shape[0])) for j in range(int(corr_imshape[1]/frame_shape[1])) for k in range(int(corr_imshape[2]/frame_shape[2]))]
else:
    true_cuts=cuts
max_ncuts=len(true_cuts)


if 'frame_infos' in to_do:
        
    print 'Computing frames infos from '+im_dir    
    n_histbins=100
    hist_max=5  
    std_centile=2.5    
    images=sorted(glob.glob(os.path.join(im_dir,'*'+ext)))
    n_im=len(images)
    
    frame_examples=np.zeros([n_examples,np.prod(frame_shape)])
    example_inds=np.array([np.random.permutation(n_im)[:n_examples],np.random.permutation(max_ncuts)[:n_examples]]).T
    frame_info={}
    frame_info['frame_hists']=np.zeros([n_im,max_ncuts,n_histbins+2],dtype='uint32')
    frame_info['frame_stds']=np.zeros([n_im,max_ncuts])
    frame_info['frame_temporal_stds']=np.zeros([n_im,max_ncuts])
    frame_info['frame_means']=np.zeros([n_im,max_ncuts])
    frame_info['frame_redstds']=np.zeros([n_im,max_ncuts])
    frame_info['frame_temporal_redstds']=np.zeros([n_im,max_ncuts])
    frame_info['frame_normmins']=np.zeros([n_im,max_ncuts])
    frame_info['frame_normmaxs']=np.zeros([n_im,max_ncuts])
    frame_info['almost_blank']=np.zeros([n_im,max_ncuts],dtype=bool)
    frame_info['hist_bins']=np.linspace(-hist_max,hist_max,n_histbins+1).flatten()
    frame_info['corrected_frame_hists']=np.zeros([n_im,max_ncuts,n_histbins+2],dtype='uint32')
    frame_info['corrected_frame_normmins']=np.zeros([n_im,max_ncuts])
    frame_info['corrected_frame_normmaxs']=np.zeros([n_im,max_ncuts])

    for i_im in range(n_im):
        print os.path.basename(images[i_im+offset])
        
        if frame_by_frame:
            frame_groups=[np.array([i]) for i in range(max_ncuts)]
        else:
            frame_groups=[np.arange(max_ncuts)]
            if im_fmt=='image':
                im=loadFrame(images[i_im+offset]).reshape(im_shape)
            elif im_fmt=='tex_movie':
                im=loadTexMovie(images[i_im+offset])   
            else:    
                im=loadVec(images[i_im+offset],fmt=im_fmt,endian=im_endian).reshape(im_shape)
            im=im[:,im_margin:im.shape[0]-im_margin,im_margin:im.shape[1]-im_margin]
            if downsample_fact>1:
                im=downsample(downsample(im,2,downsample_fact),1,downsample_fact)
            frames=np.array([im[cut[0]:cut[1],cut[2]:cut[3],cut[4]:cut[5]] for cut in true_cuts])
            
        for fg in frame_groups:
            group_nframes=len(fg)
            if frame_by_frame:
                cut=true_cuts[fg[0]]
                if im_fmt=='tex_movie':
                    frames=loadSubTexMovie(images[i_im+offset],crop_range=np.array(cut[2:]).reshape(2,2),frames_tokeep=np.arange(cut[0],cut[1]))  
                if downsample_fact>1:
                    frames=downsample(downsample(frames,2,downsample_fact),1,downsample_fact)
                frames=frames.reshape([1]+list(frame_shape))    
            inf_centiles=np.percentile(frames.reshape(group_nframes,-1),std_centile,axis=1).reshape(-1,1)
            sup_centiles=np.percentile(frames.reshape(group_nframes,-1),100-std_centile,axis=1).reshape(-1,1)            
            nanframes=frames.copy()
            for iframe in range(group_nframes):
                nanframes[iframe,nanframes[iframe]<inf_centiles[iframe]]=np.nan
                nanframes[iframe,nanframes[iframe]>sup_centiles[iframe]]=np.nan
            frame_info['frame_temporal_stds'][i_im,fg]=np.nanmean(np.nanstd(nanframes,axis=1).reshape(group_nframes,-1),axis=1)
            frame_info['frame_temporal_redstds'][i_im,fg]=np.nanmean(np.nanstd(nanframes,axis=1)[:,red_std_margin:frame_shape[1]-red_std_margin,red_std_margin:frame_shape[2]-red_std_margin].reshape(group_nframes,-1),axis=1)
            frame_info['almost_blank'][i_im,fg]=(sup_centiles<=inf_centiles).flatten()
            frame_info['frame_stds'][i_im,fg]=np.nanstd(nanframes.reshape(group_nframes,-1),axis=1)
            frame_info['frame_redstds'][i_im,fg]=np.nanstd(nanframes[:,:,red_std_margin:frame_shape[1]-red_std_margin,red_std_margin:frame_shape[2]-red_std_margin].reshape(group_nframes,-1),axis=1)
            frame_info['frame_means'][i_im,fg]=np.nanmean(nanframes.reshape(group_nframes,-1),axis=1)
            frames=(frames.reshape(group_nframes,-1)-frame_info['frame_means'][i_im,fg].reshape(-1,1))*1./frame_info['frame_stds'][i_im,fg].reshape(-1,1)
            frame_info['frame_hists'][i_im,fg]=np.array([[(fr<-hist_max).sum()]+list(np.histogram(fr,frame_info['hist_bins'])[0])+[(fr>hist_max).sum()] for fr in frames]).astype('uint32')        
            frame_info['frame_normmins'][i_im,fg]=frames.min(axis=1)
            frame_info['frame_normmaxs'][i_im,fg]=frames.max(axis=1)
            
            if i_im in example_inds[:,0]:
                ex_pos=np.where(example_inds[:,0]==i_im)[0]
                if example_inds[ex_pos,1] in fg:
                    ex_pos_fg=np.where(fg==example_inds[ex_pos,1])[0]
                    frame_examples[ex_pos]=frames[ex_pos_fg]
                
            right_tails=np.array([np.percentile(fr,righttail_pos) for fr in frames]) 
            left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in frames])
            frames=np.array([compressHistTail(frames[ifr],right_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),right_tails[ifr]) for ifr in range(len(left_tails))])   
            frames=-1*np.array([compressHistTail(-frames[ifr],-left_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),-left_tails[ifr]) for ifr in range(len(left_tails))])
            frames=(frames-frames.mean(axis=1).reshape(-1,1))*1./frames.std(axis=1).reshape(-1,1)
            frame_info['corrected_frame_hists'][i_im,fg]=np.array([[(fr<-hist_max).sum()]+list(np.histogram(fr,frame_info['hist_bins'])[0])+[(fr>hist_max).sum()] for fr in frames]).astype('uint32')        
            frame_info['corrected_frame_normmins'][i_im,fg]=frames.min(axis=1)
            frame_info['corrected_frame_normmaxs'][i_im,fg]=frames.max(axis=1)
            
    frame_info['corrected_parameters']={'righttail_pos':righttail_pos,'lefttail_pos':lefttail_pos, 'tail_max_percent':tail_max_percent}
    savemat(framesinfo_file,frame_info)
    savemat(framesexamples_file,{'frames':frame_examples, 'indices': example_inds})
   
   
if 'plot_infos' in to_do:
    plt.figure()
    plt.hist(frame_info['frame_stds'].flatten(),1000)
    plt.title('STD values')
    plt.figure()
    plt.hist(frame_info['frame_redstds'].flatten(),1000)
    plt.title('STD values on center area')
    
    extreme_hists=(frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[:,0]+frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[:,-1])>0 
    plt.figure()
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[extreme_hists][:30].T)
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[extreme_hists].mean(axis=0),'k',linewidth=3)
    plt.figure()
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[~extreme_hists][:30].T)
    plt.plot([-hist_max]+list(0.5*(frame_info['hist_bins'][:-1]+frame_info['hist_bins'][1:]))+[hist_max],frame_info['frame_hists'].reshape(n_im*max_ncuts,-1)[~extreme_hists].mean(axis=0),'k',linewidth=3)
        
    
if 'predicted_statistics' in to_do:  
    frame_info=loadmat(framesinfo_file)
    n_im,n_cuts=frame_info['frame_means'].shape
    new_mins=np.zeros([n_im,n_cuts])
    new_maxs=np.zeros([n_im,n_cuts])
    frame_info['hist_bins']=frame_info['hist_bins'].flatten()
    frame_hist_bins=np.array([np.nan]+list(frame_info['hist_bins'])+[np.nan])
    new_centers=np.zeros([n_im,n_cuts,len(frame_info['hist_bins'])+1])
    new_histbins=np.zeros([n_im,n_cuts,len(frame_info['hist_bins'])+2])
        
    for i_im in range(n_im):
        for i_frame in range(n_cuts):
            frame_hist_bins[0]=min(frame_info['frame_normmins'][i_im][i_frame],frame_hist_bins[1])
            frame_hist_bins[-1]=max(frame_info['frame_normmaxs'][i_im][i_frame],frame_hist_bins[-2])
            frame_bin_centers=0.5*(frame_hist_bins[:-1]+frame_hist_bins[1:])
            left_tail=frame_bin_centers[np.cumsum(frame_info['frame_hists'][i_im,i_frame])*100./frame_info['frame_hists'][i_im,i_frame].sum()>=lefttail_pos][0]
            right_tail=frame_bin_centers[np.cumsum(frame_info['frame_hists'][i_im,i_frame])*100./frame_info['frame_hists'][i_im,i_frame].sum()>=righttail_pos][0]
            left_thresh=left_tail-tail_max_percent*(right_tail-left_tail)
            right_thresh=right_tail+tail_max_percent*(right_tail-left_tail)
            new_centers[i_im,i_frame]=compressHistTail(-1*compressHistTail(-frame_bin_centers,-left_thresh,-left_tail),right_thresh,right_tail)
            new_histbins[i_im,i_frame]=compressHistTail(-1*compressHistTail(-frame_hist_bins,-left_thresh,-left_tail),right_thresh,right_tail)
            temp_mean=(new_centers[i_im,i_frame].reshape(1,-1)*frame_info['frame_hists'][i_im,i_frame]).sum()*1./frame_info['frame_hists'][i_im,i_frame].sum()
            temp_std=np.sqrt((((new_centers[i_im,i_frame]-temp_mean)**2)*frame_info['frame_hists'][i_im,i_frame]).sum()*1./(frame_info['frame_hists'][i_im,i_frame].sum()-1))
            new_centers[i_im,i_frame]=(new_centers[i_im,i_frame]-temp_mean)*1./temp_std
            new_mins[i_im,i_frame]=new_histbins[i_im,i_frame][:-1][frame_info['frame_hists'][i_im,i_frame]>0][0]
            new_maxs[i_im,i_frame]=new_histbins[i_im,i_frame][1:][frame_info['frame_hists'][i_im,i_frame]>0][-1]
   
    valid_frames=(frame_info['almost_blank']==False) & (frame_info['frame_stds']>=std_threshold) & (frame_info['frame_redstds']>=redstd_threshold) & (frame_info['frame_temporal_stds']>=temporal_std_threshold) & (frame_info['frame_temporal_redstds']>=temporal_redstd_threshold) & (new_mins>=normmin_threshold) & (new_maxs<=normmax_threshold)      
    stim_extremums=[new_mins[valid_frames].min(),new_maxs[valid_frames].max()]
    expected_mean=stim_extremums[0]*255./(stim_extremums[0]-stim_extremums[1])
    expected_std=255./(stim_extremums[1]-stim_extremums[0])
    plt.figure()
    plt.hist(new_mins.flatten(),200)
    plt.title('Minimum values after normalization')
    plt.figure()
    plt.hist(new_maxs.flatten(),200)
    plt.title('Maximum values after normalization')
    #rand_order=np.random.permutation(n_im*n_cuts)
    examples=loadmat(framesexamples_file)
    ex_right_tails=np.array([np.percentile(fr,righttail_pos) for fr in examples['frames']]) 
    ex_left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in examples['frames']])
    new_exframes=np.array([compressHistTail(examples['frames'][ifr],ex_right_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),ex_right_tails[ifr]) for ifr in range(len(ex_left_tails))])   
    new_exframes=-1*np.array([compressHistTail(-new_exframes[ifr],-ex_left_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),-ex_left_tails[ifr]) for ifr in range(len(ex_left_tails))])
    new_exframes=(new_exframes-new_exframes.mean(axis=1).reshape(-1,1))*1./new_exframes.std(axis=1).reshape(-1,1)  
    n_examples=examples['indices'].shape[0]
    n_plots=25
    n_figs=int(np.ceil(n_examples*1./n_plots))
    for ifig in range(n_figs):
        plt.figure()    
        for ifr in range(min(n_plots,n_examples-ifig*n_plots)):
            #frame_hist_bins[0]=min(frame_info['frame_normmins'].flatten()[rand_order[ifr+n_plots*ifig]],frame_hist_bins[1])
            #frame_hist_bins[-1]=max(frame_info['frame_normmaxs'].flatten()[rand_order[ifr+n_plots*ifig]],frame_hist_bins[-2])
            fr_inds=examples['indices'][ifr+n_plots*ifig]
            frame_hist_bins[0]=min(frame_info['frame_normmins'][fr_inds[0],fr_inds[1]],frame_hist_bins[1])
            frame_hist_bins[-1]=max(frame_info['frame_normmaxs'][fr_inds[0],fr_inds[1]],frame_hist_bins[-2])
            frame_bin_centers=0.5*(frame_hist_bins[:-1]+frame_hist_bins[1:])
            #frame_new_centers=new_centers.reshape(n_im*n_cuts,-1)[rand_order[ifr+n_plots*ifig]]
            #hist_vals=frame_info['frame_hists'].reshape(n_im*n_cuts,-1)[rand_order[ifr+n_plots*ifig]]
            frame_new_centers=new_centers[fr_inds[0],fr_inds[1]]
            hist_vals=frame_info['frame_hists'][fr_inds[0],fr_inds[1]]
            new_frame=[]
            for ibin in range(len(frame_new_centers)):
                new_frame+=[frame_new_centers[ibin]]*hist_vals[ibin]
            corr_hist,corr_bins=np.histogram(new_frame,frame_hist_bins)
            plt.subplot(5,5,ifr+1)
            plt.plot(0.5*(corr_bins[:-1]+corr_bins[1:]),corr_hist,colors[ifr%len(colors)],linewidth=2)
            plt.plot(frame_bin_centers,hist_vals,colors[ifr%len(colors)]+'--',linewidth=2) 
            plt.axis('off')
    #        plt.plot(new_centers.reshape(n_im*n_cuts,-1)[rand_order[ifr]],frame_info['frame_hists'].reshape(n_im*n_cuts,-1)[rand_order[ifr]],colors[ifr%len(colors)])#*1./np.diff(new_histbins.reshape(n_im*n_cuts,-1)[ifr]))
    #        plt.plot(frame_bin_centers,frame_info['frame_hists'].reshape(n_im*n_cuts,-1)[rand_order[ifr]],colors[ifr%len(colors)]+'--') 
        if frame_shape[0]==1:    
            plt.figure()
            for ifr in range(min(n_plots,n_examples-ifig*n_plots)):  
                plt.subplot(5,5,ifr+1)
                plt.imshow(new_exframes[ifr+n_plots*ifig].reshape(frame_shape),vmin=stim_extremums[0],vmax=stim_extremums[1],interpolation='none',cmap='gray')
                plt.axis('off')
    
    print 'Nframes : '+str(valid_frames.sum())+'/'+str(valid_frames.size)
    print 'Expected mean : '+str(expected_mean)
    print 'Expected std : '+str(expected_std)       
    
    
if 'real_statistics' in to_do:   
    frame_info=loadmat(framesinfo_file)
    frame_hist_bins=np.array([np.nan]+list(frame_info['hist_bins'].flatten())+[np.nan])
    corr_hist_bins=frame_hist_bins.copy()
    valid_frames=(frame_info['almost_blank']==False) & (frame_info['frame_stds']>=std_threshold) & (frame_info['frame_redstds']>=redstd_threshold) & (frame_info['frame_temporal_stds']>=temporal_std_threshold) & (frame_info['frame_temporal_redstds']>=temporal_redstd_threshold) & (frame_info['corrected_frame_normmins']>=normmin_threshold) & (frame_info['corrected_frame_normmaxs']<=normmax_threshold)      
    stim_extremums=[frame_info['corrected_frame_normmins'][valid_frames].min(),frame_info['corrected_frame_normmaxs'][valid_frames].max()]
    expected_mean=stim_extremums[0]*255./(stim_extremums[0]-stim_extremums[1])
    expected_std=255./(stim_extremums[1]-stim_extremums[0])
    plt.figure()
    plt.hist(frame_info['corrected_frame_normmins'].flatten(),200)
    plt.title('Minimum values after normalization')
    plt.figure()
    plt.hist(frame_info['corrected_frame_normmaxs'].flatten(),200)
    plt.title('Maximum values after normalization')
    #rand_order=np.random.permutation(n_im*n_cuts)
    examples=loadmat(framesexamples_file)
    ex_right_tails=np.array([np.percentile(fr,righttail_pos) for fr in examples['frames']]) 
    ex_left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in examples['frames']])
    new_exframes=np.array([compressHistTail(examples['frames'][ifr],ex_right_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),ex_right_tails[ifr]) for ifr in range(len(ex_left_tails))])   
    new_exframes=-1*np.array([compressHistTail(-new_exframes[ifr],-ex_left_tails[ifr]+tail_max_percent*(ex_right_tails[ifr]-ex_left_tails[ifr]),-ex_left_tails[ifr]) for ifr in range(len(ex_left_tails))])
    new_exframes=(new_exframes-new_exframes.mean(axis=1).reshape(-1,1))*1./new_exframes.std(axis=1).reshape(-1,1)  
    n_examples=examples['indices'].shape[0]
    n_plots=25
    n_figs=int(np.ceil(n_examples*1./n_plots))
    hist_labels=(frame_info['hist_bins'].flatten()[:-1]+frame_info['hist_bins'].flatten()[1:])*0.5
    for ifig in range(n_figs):
        plt.figure()    
        for ifr in range(min(n_plots,n_examples-ifig*n_plots)):
            fr_inds=examples['indices'][ifr+n_plots*ifig]
            frame_hist_bins[0]=min(frame_info['frame_normmins'][fr_inds[0],fr_inds[1]],frame_hist_bins[1])
            frame_hist_bins[-1]=max(frame_info['frame_normmaxs'][fr_inds[0],fr_inds[1]],frame_hist_bins[-2])
            frame_bin_centers=0.5*(frame_hist_bins[:-1]+frame_hist_bins[1:])
            corr_hist_bins[0]=min(frame_info['corrected_frame_normmins'][fr_inds[0],fr_inds[1]],frame_hist_bins[1])
            corr_hist_bins[-1]=max(frame_info['corrected_frame_normmaxs'][fr_inds[0],fr_inds[1]],frame_hist_bins[-2])
            corr_bin_centers=0.5*(corr_hist_bins[:-1]+corr_hist_bins[1:])
            plt.subplot(5,5,ifr+1)
            plt.plot(frame_bin_centers,frame_info['corrected_frame_hists'][fr_inds[0],fr_inds[1]],colors[ifr%len(colors)],linewidth=2)
            plt.plot(corr_bin_centers,frame_info['frame_hists'][fr_inds[0],fr_inds[1]],colors[ifr%len(colors)]+'--',linewidth=2) 
            plt.axis('off')
        
        if frame_shape[0]==1:
            plt.figure()
            for ifr in range(min(n_plots,n_examples-ifig*n_plots)):  
                plt.subplot(5,5,ifr+1)
                plt.imshow(new_exframes[ifr+n_plots*ifig].reshape(frame_shape)[1:],vmin=stim_extremums[0],vmax=stim_extremums[1],interpolation='none',cmap='gray')
                plt.axis('off')
                
    if frame_shape[0]>1:
        playMovieFromMat(new_exframes[0].reshape(frame_shape),vmin=stim_extremums[0],vmax=stim_extremums[1])
    
    print 'Nframes : '+str(valid_frames.sum())+'/'+str(valid_frames.size)
    print 'Expected mean : '+str(expected_mean)
    print 'Expected std : '+str(expected_std)        


if 'crop_frames' in to_do:
    
    newim_prefix='/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120/naturalmovie120_'
    nzeros=6
    frame_info=loadmat(framesinfo_file)
    images=sorted(glob.glob(os.path.join(im_dir,'*'+ext)))
    n_im=len(images)
    frames_info=loadmat(framesinfo_file)
    assert frames_info['frame_means'].shape[0]==n_im
    valid_frames=(frames_info['almost_blank']==False) & (frames_info['frame_stds']>=std_threshold) & (frames_info['frame_redstds']>=redstd_threshold) & (frame_info['frame_temporal_stds']>=temporal_std_threshold) & (frame_info['frame_temporal_redstds']>=temporal_redstd_threshold) & (frame_info['corrected_frame_normmins']>=normmin_threshold) & (frame_info['corrected_frame_normmaxs']<=normmax_threshold)
    stim_extremums=[frame_info['corrected_frame_normmins'][valid_frames].min(),frame_info['corrected_frame_normmaxs'][valid_frames].max()]    
    frame_count=0
    
    for i_im in range(n_im):
        if frame_by_frame:
            frame_groups=[np.array([i]) for i in range(max_ncuts)]
        else:
            frame_groups=[np.arange(max_ncuts)]
            if im_fmt=='image':
                im=loadFrame(images[i_im+offset]).reshape(im_shape)
            elif im_fmt=='tex_movie':
                im=loadTexMovie(images[i_im+offset])   
            else:    
                im=loadVec(images[i_im+offset],fmt=im_fmt,endian=im_endian).reshape(im_shape)
            im=im[:,im_margin:im.shape[0]-im_margin,im_margin:im.shape[1]-im_margin]
            if downsample_fact>1:
                im=downsample(downsample(im,2,downsample_fact),1,downsample_fact)
            frames=np.array([im[cut[0]:cut[1],cut[2]:cut[3],cut[4]:cut[5]] for cut in true_cuts[valid_frames[i_im]]]).reshape(1,-1)
        
        for fg in frame_groups:
            if np.any(valid_frames[i_im,fg]):
                group_nframes=len(fg)
                if frame_by_frame:
                    cut=true_cuts[fg[0]]
                    if im_fmt=='tex_movie':
                        frames=loadSubTexMovie(images[i_im+offset],crop_range=np.array(cut[2:]).reshape(2,2),frames_tokeep=np.arange(cut[0],cut[1]))  
                    if downsample_fact>1:
                        frames=downsample(downsample(frames,2,downsample_fact),1,downsample_fact)
                    frames=frames.reshape(1,-1)                
                right_tails=np.array([np.percentile(fr,righttail_pos) for fr in frames]) 
                left_tails=np.array([np.percentile(fr,lefttail_pos) for fr in frames])
                frames=np.array([compressHistTail(frames[ifr],right_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),right_tails[ifr]) for ifr in range(len(left_tails))])   
                frames=-1*np.array([compressHistTail(-frames[ifr],-left_tails[ifr]+tail_max_percent*(right_tails[ifr]-left_tails[ifr]),-left_tails[ifr]) for ifr in range(len(left_tails))])
                frames=(frames-frames.mean(axis=1).reshape(-1,1))*1./frames.std(axis=1).reshape(-1,1)
                frames=(frames-stim_extremums[0])*255./(stim_extremums[1]-stim_extremums[0])
                frames=np.round(frames).astype(int)
                assert np.all((frames>=0) & (frames<=255))       
                for iframe in range(len(frames)):      
                    frame_count+=1
                    if frame_shape[0]==1:
                        saveMat(frames[iframe],newim_prefix+str(frame_count).zfill(nzeros),fmt='B')
                    else:
                        saveMatToTex(frames[iframe].reshape(frame_shape),newim_prefix+str(frame_count).zfill(nzeros)+'.tex',fmt='B')
                
                
if 'randomize_phase' in to_do:

    bin_im_dir='/DATA/Margot/natural_movies/vanhateren_imc300_images/'
    rand_im_dir='/DATA/Margot/natural_movies/vanhateren_imc300_randphase_images/'
    nzeros=5
    ext=''
    bin_im_shape=np.array([300,300])
    mov_nframes=600
    
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    for i_im in range(n_im):
        im=loadVec(images[i_im],fmt='B').reshape(bin_im_shape)
        rand_im=randomize_phases(im)
        rand_im=(rand_im-rand_im.mean())*im.std()/rand_im.std()+im.mean()
        rand_im[rand_im<0]=0
        rand_im[rand_im>255]=255
        saveMat(rand_im,images[i_im][:-nzeros-len(ext)]+'randphase_'+images[i_im][-nzeros-len(ext):],fmt='B')   


if 'randomize_phase_mov' in to_do:

    in_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/ds6_antialias3/natmov'
    out_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/ds6_antialias3/randphase'
    nzeros=3
    ext='.tex'
    target_mean=127.5
    target_std=60
    
    movies=sorted(glob.glob(os.path.join(in_dir,'*'+ext)))
    n_movs=len(movies)
    for i_mov in range(n_movs):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
        mov=randomize_phases(mov)
        mov=(mov-mov.mean())*target_std/mov.std()+target_mean
        print 'Saturated pixels :', np.sum(mov<0)+np.sum(mov>255),'/',np.prod(mov.shape)
        mov=rescale_with_saturation(mov,[0,255],target_mean,target_std)
        print 'mean :', np.round(mov.mean(),3), ', std :', np.round(mov.std(),3)
        saveMatToTex(mov,os.path.join(out_dir,os.path.basename(movies[i_mov])[:-nzeros-len(ext)]+'randphase_'+movies[i_mov][-nzeros-len(ext):])) 
        

if 'randomize_pixels' in to_do:

    in_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/ds6/natmov'
    out_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/ds6/randallpix'
    randomization_file=''
    nzeros=3
    ext='.tex'
    spatial_only=False
    
    movies=sorted(glob.glob(os.path.join(in_dir,'*'+ext)))
    n_movs=len(movies)
    np.random.seed(0)
    for i_mov in range(n_movs):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
        mov_shape=mov.shape
        if spatial_only:
            randomization=np.random.permutation(np.prod(mov_shape[1:]))
            mov=mov.reshape(mov_shape[0],-1)[:,randomization].reshape(mov_shape)
            label='randspatpix'
        else:    
            randomization=np.random.permutation(np.prod(mov_shape))
            mov=mov.flatten()[randomization].reshape(mov_shape)
            label='randallpix'
        saveMatToTex(mov,os.path.join(out_dir,os.path.basename(movies[i_mov])[:-nzeros-len(ext)]+label+'_'+movies[i_mov][-nzeros-len(ext):]))         


if 'randomize_frames' in to_do:
    
    bin_mov_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218'
    rand_mov_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/temp'
    randomization_file='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/randomization6_order.txt'
    new_rand=True
    n_adjacent=3
    nzeros=4
    ext='.tex'
 
    movies=sorted(glob.glob(os.path.join(bin_mov_dir,'*'+ext)))
    n_mov=300#len(movies)
    for i_mov in range(n_mov):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
        
        if i_mov==0:
            mov_nframes=mov.shape[0]
            assert mov_nframes%n_adjacent==0
            if new_rand:
                randomization=np.array([((np.random.permutation(int(mov_nframes)/n_adjacent).reshape(-1,1).repeat(n_adjacent,axis=1)*n_adjacent)+np.arange(n_adjacent).reshape(1,-1)).flatten() for i in range(n_mov)]) 
                with open(randomization_file,'w') as f:
                    for line in range(n_mov):
                        f.write(' '.join([str(num) for num in randomization[line]])+'\n')                
            else:            
                randomization=np.zeros((n_movs,mov_nframes)).astype(int)
                with open(randomization_file,'r') as f:
                    lines=f.read().split('\n')
                for i_line in range(n_movs):
                    randomization[i_line]=np.array(lines[i_line].split(' ')).astype(int)
     
        #saveMatToTex(mov[randomization[i_mov]],rand_mov_dir+movies[i_mov][len(bin_mov_dir):-nzeros-len(ext)]+'randframe'+str(n_adjacent)+'_'+movies[i_mov][-nzeros-len(ext):])
        saveMatToTex(mov[randomization[i_mov]],rand_mov_dir+movies[i_mov][len(bin_mov_dir):-nzeros-len(ext)]+str(i_mov+1501).zfill(nzeros)+ext)


if 'merge_to_texmovs' in to_do:

    bin_im_dir='/DATA/Margot/natural_movies/vanhateren_imc300_randphase_images/'
    tex_prefix='/DATA/Margot/natural_movies/vanhateren_imc300_randphase_tex600/vanhateren_imc300_randphase_'
    randomization_file='/DATA/Margot/natural_movies/vanhateren_imc300_randphase_tex600/randomization_order.txt'
    nzeros=3
    ext=''
    bin_im_shape=np.array([300,300])
    mov_nframes=600
    new_rand=False
    
    images=sorted(glob.glob(os.path.join(bin_im_dir,'*'+ext)))
    n_im=len(images)
    n_movs=int(n_im/mov_nframes)
    
    if new_rand:
        randomization=np.random.permutation(np.arange(n_movs*mov_nframes)).reshape(n_movs,-1)    
        with open(randomization_file,'w') as f:
            for line in range(n_movs):
                f.write(' '.join([str(num) for num in randomization[line]])+'\n')
                
    else:            
        randomization=np.zeros((n_movs,mov_nframes)).astype(int)
        with open(randomization_file,'r') as f:
            lines=f.read().split('\n')
        for i_line in range(n_movs):
            randomization[i_line]=np.array(lines[i_line].split(' ')).astype(int)
            
    initializeTex('temptex',bin_im_shape,fmt='B',close=True)
    for imov in range(n_movs):
        mov_name=tex_prefix+str(imov+1).zfill(nzeros)+'.tex'
        os.system('cat temptex '+' '.join([images[randomization[imov,i_im]] for i_im in range(mov_nframes)])+' >'+mov_name)  
        
        
if 'change_nzeros' in to_do:
    prefix='/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120/naturalmovie120_'
    ext=''
    nzeros_out=4
    
    filenames=sorted(glob.glob(prefix+'*'+ext))
    for f in filenames:
        print f
        print prefix+str(int(f[len(prefix):len(f)-len(ext)])).zfill(nzeros_out)+ext
        os.rename(f,prefix+str(int(f[len(prefix):len(f)-len(ext)])).zfill(nzeros_out)+'.tex')        
        
        
if 'randomize_fileorder' in to_do:
    prefix='/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120/naturalmovie120_'  
    order_textfile='/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_120/file_order.txt'  
    ext='.tex'
    filenames=sorted(glob.glob(prefix+'*'+ext))
    file_order=np.random.permutation(len(filenames))
    nzeros_in=len(filenames[0])-len(prefix)-len(ext)
    with open(order_textfile,'w') as f:
        for i_file in range(len(filenames)):
            print filenames[file_order[i_file]]
            print prefix+str(i_file+1).zfill(nzeros_in+1)+ext
            f.write(filenames[file_order[i_file]]+'\n')
            os.rename(filenames[file_order[i_file]],prefix+str(i_file+1).zfill(nzeros_in+1)+ext)
            
            
if 'downsample' in to_do:

    in_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/natural_movies/tenseconds_tex_120_std60_randframes_subselected'
    out_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/mixedstims_tex_20pix_std60_090218/ds6_antialias3/natmov_randframes'
    out_name='tenseconds_tex_20_std60_randframes_'
    nzeros=3
    down_factor=6
    ext='.tex'
    target_mean=127.5
    target_std=60
    mode='blur'
    anti_alias=3
    
    movies=sorted(glob.glob(os.path.join(in_dir,'*'+ext)))
    n_movs=len(movies)
    for i_mov in range(n_movs):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
        old_size=np.array(mov.shape)
        assert np.all(old_size[1:]%down_factor==0)
#        new_size=old_size.copy()
#        new_size[1:]=(old_size[1:]/down_factor).astype(int)
#        mov=mov.reshape([new_size[0],new_size[1],down_factor,new_size[2],down_factor]).mean(axis=4).mean(axis=2)
        mov=downsample(downsample(mov,axis=2,factor=down_factor,mode=mode,anti_alias=anti_alias),axis=1,factor=down_factor,mode=mode,anti_alias=anti_alias)
        mov=rescale_with_saturation(mov,[0,255],target_mean,target_std)
        print 'mean :', np.round(mov.mean(),3), ', std :', np.round(mov.std(),3)
        saveMatToTex(mov,os.path.join(out_dir,out_name+movies[i_mov][-nzeros-len(ext):]))     
        
        
if 'upsample' in to_do:

    in_dir='/home/margot/Bureau/temp_withmask'
    out_dir='/home/margot/Bureau/temp_withmask_x6'
    out_name='mixedtypes_fr120_'
    nzeros=4
    up_factor=6
    ext='.tex'
    
    movies=sorted(glob.glob(os.path.join(in_dir,'*'+ext)))
    n_movs=len(movies)
    for i_mov in range(n_movs):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
        saveMatToTex(np.repeat(np.repeat(mov,up_factor,axis=1),up_factor,axis=2),os.path.join(out_dir,out_name+movies[i_mov][-nzeros-len(ext):]))


if 'add_corners' in to_do:

    in_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/grating_pairs/bar_pairs_32_375pix_stdmax_20180225'
    out_dir='/media/margot/DATA/Margot/ownCloud/STIMULI/tex_stimuli/grating_pairs/bar_pairs_32_375pix_stdmax_20180225_CORNERS2'
    out_name='bar_pairs_stdmax_'
    nzeros=4
    ext='.tex'
    movsize=375
    movlen=48
    mov_mean=127.5
    margin=5
    movmax=255
    
    mask=np.zeros([movlen,movsize,movsize]).astype(bool)
    for i in range(movsize):
        for j in range(movsize):
            if (i-(movsize-1)/2.)**2+(j-(movsize-1)/2.)**2<=(movsize/2.+margin)**2:
                mask[:,i,j]=True
    
    movies=sorted(glob.glob(os.path.join(in_dir,'*'+ext)))
    n_movs=len(movies)
    for i_mov in range(n_movs):
        print movies[i_mov]
        mov=loadTexMovie(movies[i_mov])
#        corner_val=(mov_mean*np.prod(mov.shape)-mov[mask].sum())*1./np.sum(~mask)
#        mov[~mask]=int(np.round(corner_val))
        corner_sum=(mov_mean*np.prod(mov.shape)-mov[mask].sum())
        n_max=int(np.round(corner_sum*1./movmax))
        val_list=np.zeros(np.sum(~mask))
        val_list[:n_max]=movmax
        np.random.shuffle(val_list)
        mov[~mask]=val_list
        print 'mean :', mov.mean(), 'std :', mov.std()
        saveMatToTex(mov,os.path.join(out_dir,out_name+movies[i_mov][-nzeros-len(ext):]))             
 

#from miscellaneous.convertVideo import appendToBinary
#im_dir='/DATA/Margot/natural_movies/vanhateren_imc500_images/'
#mov_name='/DATA/Margot/natural_movies/vanhateren_imc500_ds2_tex50.tex'
#ext=''
#im_shape=np.array([500,500])
#crop_range=(np.array([[200,300],[200,300]]))
#downsample_fact=2
#final_shape=np.array([50,50])
#im_fmt='B'
#im_endian='<'
#
#images=sorted(glob.glob(im_dir+'*'+ext))
#n_im=len(images)
#mov_nframes=n_im
#n_movs=1
#randomization=np.random.permutation(np.arange(mov_nframes))
#        
#with initializeTex(mov_name,final_shape,fmt='B',close=False) as f:
#    for i_im in randomization:
#        im=loadVec(images[i_im],fmt=im_fmt,endian=im_endian).reshape(im_shape)[crop_range[0,0]:crop_range[0,1],crop_range[1,0]:crop_range[1,1]]
#        im=downsample(downsample(im,1,downsample_fact),0,downsample_fact)
#        appendToBinary(f,im,fmt=im_fmt)
        