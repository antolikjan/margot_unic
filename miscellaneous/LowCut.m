function out = LowCut (in, cut_off_period, filter_type, boundary)

% low cut filter for one-dimensional data (N x 1).
% DC will not be subtracted. 
% filter_type: 
%   if this arg is not specified, or specified as '',
%   filter cuts sharply at the cut_off period,
%   so ripples may happen.
%   if this is specified as 'gaussian',
%   this filter cuts with a gaussian window,
%   so ripples will be suppressed.
% boundary:
%   if this is not specified or set as 0, 
%   some artifacts can happen around the boundaries,
%   when the first and last values in the data are very different,
%   because FFT assumes periodic boundary condition.
%   if this is set as 1, this problem will be managed
%   by making a time-course as periodic. 
%
% examples
%   LowCut (in, cut_off_period)
%       low-cut sharply at cut_off_period
%   LowCut (in, cut_off_period, 'gaussian')
%       low-cut by gaussian around cut_off_period
%   LowCut (in, cut_off_period, '', 1)
%       low-cut sharply at cut_off_period, considering boundaries
%   LowCut (in, cut_off_period, 'gaussian', 1)
%       low-cut by gaussian around cut_off_period, considering boundaries
%
%
%   Kenichi Ohki  2004. 9. 8. 

if nargin <= 2
    filter_type = '';
end

if nargin <= 3
    boundary =0;
end

N = length(in);
filter_size = N/cut_off_period;

if boundary == 1
    N=N*2;
    filter_size = filter_size *2;
    in=[in; flipud(in)];
end

switch lower(filter_type)
    case ('gaussian')
        filter=fftshift(1-gausswin(N+1, N/(filter_size*2)));
        filter=[1; filter(2:N)];
    otherwise
        filter=ones(N,1);
        filter_size=round(filter_size);
        filter([2:2+filter_size-1,N-filter_size+1:N])=0;
end

k=fft(in);
k=k.*filter;

out=real(ifft(k));

if boundary == 1
    out = out(1:N/2);
end







