from utils.simulations import generate_visualnoise
from miscellaneous.convertVideo import initializeTex, appendToBinary
import numpy as np
from warnings import warn

mov_nsquares=[20,20]
npix_per_square=6
mov_nbins=600
n_movs=300
noise_type='Dnoise'
freqs=[1.02/6, 1.02/6]
mov_name='/media/margot/DATA/Margot/tex_stimuli/noise/dense_noise_fr120_sq6_std60_freq017/densenoise_fr120_sq6_std60_freq017'
n_zeros=4
frame_mean=127.5
frame_std=60.
ext='.tex'
starting_num=0

for imov in np.arange(n_movs)+starting_num:
    mov=generate_visualnoise(stimsize=np.prod(mov_nsquares), nbins=mov_nbins, stimtype=noise_type, seedval=imov, freqs=freqs)
    mov=mov.reshape([-1]+mov_nsquares).repeat(npix_per_square,axis=1).repeat(npix_per_square,axis=2).reshape([-1,np.prod(mov_nsquares)*npix_per_square**2])
    mov=(mov-mov.mean())*frame_std*1./mov.std()+frame_mean
    n_satur=np.sum(mov<0)+np.sum(mov>255)
    if n_satur>0:
        warn('Warning: '+str(n_satur)+' over '+str(np.prod(mov.shape))+' pixels saturate')
        mov[mov<0]=0
        mov[mov>255]=255
    with initializeTex(mov_name+'_'+str(imov+1).zfill(n_zeros)+ext,np.array(mov_nsquares)*npix_per_square,fmt='B',close=False) as f:
        for iframe in range(mov_nbins):
            appendToBinary(f,mov[iframe],fmt='B')