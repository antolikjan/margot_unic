from datetime import datetime

text_file='/media/margot/DATA/Margot/ownCloud/NOTES/Fichiers courants/Jdb these.txt'
notebook_dir='/home/margot/Bureau/test_redmachin/jdb/'
date_fmt='%d/%m/%Y'


def clean_date(line_string):
    return line_string.replace('\n','').replace('\r','').replace(' ','')
    
def clean_line(line_string):
    return line_string.replace("'","''")    


with open(text_file,'r') as f:
    lines=f.readlines()

current_date=None
#current_year=None
current_month=None
current_file=None

for l in lines:
    #print l
    try:    
        new_date=datetime.strptime(clean_date(l),date_fmt)
    except:
        new_date=None

    if new_date is not None:
        print 'new date :', str(new_date)
        current_date=new_date
        if current_file is not None:
            current_file.write("    '}\n") 
        if current_month!=new_date.month:
            current_month=new_date.month
            notebook_name=str(new_date.year)+'-'+str(current_month).zfill(2)+'.txt'
            print 'new file :', notebook_name
            if current_file is not None:
                current_file.close()            
            current_file=open(notebook_dir+notebook_name,'w')
        current_file.write(str(new_date.day)+": {text: '")  
    elif current_date is not None:
        current_file.write('    '+clean_line(l))   
        
current_file.write("    '}\n") 
current_file.close()   


