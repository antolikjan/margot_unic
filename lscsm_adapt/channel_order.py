import numpy as np


def restore_recsite_order(resp):
# Takes neuronal response variable (Nt*Ncells), and rearranges columns (cells) in recording site order (they are originally aranged in elphy input order)
# /!\/!\ TO BE TESTED: is the order right? Haven't recording site and elphy order been inverted?
    channel_order=range(32)
    recsite_order=np.array([3,2,4,1,7,6,8,5,11,10,12,9,15,14,16,13,19,18,20,17,23,22,24,21,27,26,28,25,31,30,32,29])-1
    #recsite_order=range(32)
                
    nchan=len(channel_order)
    elec_corresp=np.zeros((nchan,nchan))
    for i in range(nchan):
        elec_corresp[channel_order[i],recsite_order[i]]=1
        
    return np.dot(resp,elec_corresp)
    

def coltorow(mat,nrow,ncol):
# Utility function for plotting: when using matplotlib.pyplot.subplot, the subplot grid is ran through line by line.
# To place a succession of plots column by column, one applies this function to the matrix containing the data to plot (each column = one plot)
# -> columns are rearranged in desired order
    neworder=np.arange(nrow*ncol).reshape((nrow,ncol)).T.reshape(-1)
    convmat=np.zeros((nrow*ncol,nrow*ncol))
    for i in range(nrow*ncol):
        convmat[i,neworder[i]]=1
    return np.dot(mat,convmat)


#def rowtocol(mat,nrow,ncol):
#    neworder=np.arange(nrow*ncol).reshape((nrow,ncol)).T.reshape(-1)
#    convmat=np.zeros((nrow*ncol,nrow*ncol))
#    for i in range(nrow*ncol):
#        convmat[neworder[i],i]=1
#    return np.dot(mat,convmat)    