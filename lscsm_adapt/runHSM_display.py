from loadandsave import restoreLSCSM_mgt, loadParams
from lscsm.checkpointing import loadErrCorr, saveResults
from lscsm.visualization import computeCorr
from visualizationMgt import retrieve_LSCSM_params, plotLSCSM_network, plotLSCSM_params, plotWeights
import numpy as np
import param
from utils.param_gui_tools import param_dialog, param_list_string
from utils.handlebinaryfiles import saveMat
import sys
import matplotlib.pyplot as plt
from HSM_pruning import prune_HSM, paramdict_to_Kvector
from utils.various_tools import generate_cmap
from scipy.io import savemat, loadmat
from os.path import basename, dirname, abspath
from lscsm.LSCSM import LSCSM
from data_analysis.extracell_funcs import max_possible_crosscorr


"""
PARAMETERS
"""

class exploreHSM_params(param.Parameterized):
    param_file=param.Filename(default=None,allow_None=True,search_paths=['/media/margot/DATA/Margot/ownCloud/Data/LSCSM_data/FittingResults','/Users/margot/ownCloud/MargotUNIC/Data/LSCSM_data/FittingResults'],doc='Parameter file of the fit')
    data_path=param.Foldername(default=None,allow_None=True,search_paths=['/media/margot/DATA/Margot/Extracell_data/HSM_format','/media/margot/DATA/Margot/ownCloud/Data/Extracellular_elphy_data','/Users/margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data'],doc='Path where to search for data files')    
    load_HSM=param.Boolean(default=True, doc='Load fitting dataset and final HSM parameters')    
    to_plot=param.ListSelector(default=[], objects=['model_network','pruned_network','model_params','model_weights','error','layer_activity','adaptation_effect','generate_adapted_responses','generate_preadapt_K','datahist_to_outhist'], doc="List of things to plot")
    add_STA=param.Boolean(default=False, doc='Draw STA on the HSM model plot')
    add_corr=param.Boolean(default=False, doc='Add validation performance (predictive power) on to of each cell in the HSM model plot')
    ROI=param.Array(default=np.array([]), doc='Spatial area to which the displayed RF will be restricted')
    ROI_downsplfact=param.Integer(default=1, doc='Downsampling factor for the stimulus ROI used for RF computation')   
    cells_tokeep=param.Array(default=np.array([]), doc='List of cells to plot (default:all)')
    keep_previous_lines=param.Boolean(default=False, doc='When drawling lines to represent weights, keep the previous ones when drawing new ones')
    line_width=param.Number(default=3., doc='Width of weight representing lines')    
    binary_weights=param.Boolean(default=False, doc='Whether to draw all non-zero connections with the same width and alpha')
    normalize_RFs=param.Dict(default={'LGN':False,'hidden':False,'out':False}, doc='Whether to normalize RFs of each layer for display')
    scale_by_cell=param.Boolean(default=False, doc='Whether to normalize RFs when plotting for better visualization')
    display_temporal_lgn=param.Boolean(default=False, doc='Whether to plot the temporal kernels of the LGN layer')    
    display_temporal_hidden=param.Boolean(default=False, doc='Whether to plot the temporal kernels of the hidden layer')
    prun_simil_thresh=param.Number(default=0.95, doc='Threshold of similarity (between 0 and 1) between responses of two units above which they will be merged')
    prun_contrib_thresh=param.Number(default=0.3, doc='Threshold of relative contribution (normalized by expected contribution = 1/number_of_inputs) under which a synapic weight will be set to 0')
    save_pruning=param.Boolean(default=False, doc='Whether to save pruned structure (K mask)')
    HSMparam_ncol=param.Integer(default=5, doc='Number of columns in the HSM parameters figure')
    
display_order={'PARAMETERS': ['param_file','data_path','load_HSM','to_plot','add_STA','add_corr','ROI','ROI_downsplfact','cells_tokeep','keep_previous_lines','line_width','binary_weights','normalize_RFs','scale_by_cell','display_temporal_lgn','display_temporal_hidden','prun_simil_thresh','prun_contrib_thresh','save_pruning','HSMparam_ncol']}


try: colors
except NameError: colors=generate_cmap(10)

try: pm
except NameError: pm=exploreHSM_params()
if (len(sys.argv)<2) or (sys.argv[1]!='skip_dialog'):    
    pm,validated=param_dialog(pm,display_order)
else:
    validated=True    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'    
   
    """
    RUN
    """    
    if pm.load_HSM:
        lscsm,K,suppl_params,training_inputs,training_set,validation_inputs,validation_set=restoreLSCSM_mgt(pm.param_file[:-11],resetK=False,data_path=pm.data_path)
        if len(pm.cells_tokeep)==0:
            pm.cells_tokeep=np.arange(training_set.shape[1])+1
        
        # A mettre a jour: corr deja calculee de base
        if pm.add_STA:
            stim_for_STA=training_inputs.astype('float32')
        else:
            stim_for_STA=None
        if pm.add_corr:
            stim_for_corr=validation_inputs.astype('float32')
            resp_for_corr=validation_set.astype('float32')
        else:
            stim_for_corr=resp_for_corr=None
        
        HSM_train_resp=lscsm.response(training_inputs,K,optional_Y=training_set)
        HSM_val_resp=lscsm.response(validation_inputs,K,optional_Y=validation_set)
        train_corr=computeCorr(HSM_train_resp,training_set)  
        val_corr=computeCorr(HSM_val_resp,validation_set)          
        HSM_params,HSM_bounds=retrieve_LSCSM_params(lscsm,K,stim_for_STA)     

        
    if 'model_network' in pm.to_plot:           
        plotLSCSM_network(HSM_params,subRegion=pm.ROI,subSet=pm.cells_tokeep-1,line_width=pm.line_width,keep_previous=pm.keep_previous_lines,normalize_RFs=pm.normalize_RFs,scale_by_cell=pm.scale_by_cell,temporal_lgn=pm.display_temporal_lgn,binary_mode=pm.binary_weights)

        
    if 'pruned_network' in pm.to_plot: 
        pruned_params,pruned_pmask=prune_HSM(HSM_params,training_inputs,bounds=HSM_bounds,similarity_thresh=pm.prun_simil_thresh,wcontrib_thresh=pm.prun_contrib_thresh)          
        pruned_K=paramdict_to_Kvector(lscsm,pruned_params)
        pruned_Kmask=paramdict_to_Kvector(lscsm,pruned_pmask)
        if pm.save_pruning :
            saveMat(pruned_Kmask,pm.param_file[:-11]+'_Kmask'+str(int(pm.prun_simil_thresh*100)).zfill(2)+'-'+str(int(pm.prun_contrib_thresh*100)).zfill(2),fmt='?')       
        pruned_train_resp=lscsm.response(training_inputs,pruned_K)
        pruned_val_resp=lscsm.response(validation_inputs,pruned_K)
        pruned_similarity=computeCorr(lscsm.response(training_inputs,K),pruned_train_resp)
        pruned_train_corr=computeCorr(training_set,pruned_train_resp)
        pruned_val_corr=computeCorr(validation_set,pruned_val_resp)
        plt.figure();
        plt.subplot(1,3,1)
        plt.hist(pruned_similarity,30)
        plt.title('Pruned-unpruned similarity')
        plt.subplot(1,3,2)
        plt.plot([0,1],[0,1],'k')
        plt.plot(train_corr,pruned_train_corr,'o')
        plt.title('Pruned vs unpruned train corr')
        plt.axis('equal')
        plt.subplot(1,3,3)
        plt.plot([0,1],[0,1],'k')
        plt.plot(val_corr,pruned_val_corr,'o')
        plt.title('Pruned vs unpruned val corr')
        plt.axis('equal')
        plotWeights(pruned_params,nbins=30,yaxis=None)
        plotLSCSM_network(pruned_params,subRegion=pm.ROI,subSet=pm.cells_tokeep-1,line_width=pm.line_width,keep_previous=pm.keep_previous_lines,normalize_RFs=pm.normalize_RFs,scale_by_cell=pm.scale_by_cell,temporal_lgn=pm.display_temporal_lgn,binary_mode=pm.binary_weights)
        print '\nNumber of pruned params :', np.sum(pruned_Kmask==0), '/', len(pruned_K)
        print 'Validation corr :', pruned_val_corr.mean(), 'vs', val_corr.mean()
        
        
    if 'model_params' in pm.to_plot:
        plotLSCSM_params(HSM_params,subRegion=pm.ROI,same_scale=False,normalize_lgn=False,n_col=pm.HSMparam_ncol,plot_cleaning=['ticks','frame'],temporal_lgn=pm.display_temporal_lgn,temporal_hidden=pm.display_temporal_hidden,weights=('model_weights' in pm.to_plot))
        

    if ('model_weights' in pm.to_plot) and not('model_params' in pm.to_plot):
        plotWeights(HSM_params,nbins=30,yaxis=None)
     
     
    if 'error' in pm.to_plot:
        metaparams,supplparams=loadParams(pm.param_file)
        terr,verr=loadErrCorr(pm.param_file[:-11]+'_error')
        fontsize=25
        linewidth=3        
        t=np.arange(len(terr))*supplparams['epochSize']
        plt.figure()
        plt.plot(t,terr,'b--',linewidth=linewidth)
        plt.plot(t,verr,'b',linewidth=linewidth)
        plt.xlabel("Nombre d'iterations",fontsize=fontsize)
        plt.ylabel('Erreur',color='b',fontsize=fontsize)
        plt.tick_params(axis='y', colors='b',labelsize=fontsize)
        plt.tick_params(axis='x',labelsize=fontsize)
        [i.set_linewidth(3) for i in plt.gca().spines.itervalues()]
        
        try:
            tcorr,vcorr=loadErrCorr(pm.param_file[:-11]+'_corr')
            plt.twinx()
            plt.plot(t,tcorr,'r--',linewidth=linewidth)
            plt.plot(t,vcorr,'r',linewidth=linewidth)
            plt.xlabel("Nombre d'iterations",fontsize=fontsize)
            plt.ylabel('Correlation entre reponse predite et mesuree',color='r',fontsize=fontsize)
            plt.tick_params(axis='y', colors='r',labelsize=fontsize)
            plt.tick_params(axis='x',labelsize=fontsize)
        except:
            pass
        
        
    if 'layer_activity' in pm.to_plot:
        layer_resps_func=lscsm.inner_resps_func()
        layer_resps={'train': layer_resps_func(training_inputs,K), 'reg':layer_resps_func(validation_inputs,K)}
        resps_list=layer_resps['train'].keys()
        n_var=len(resps_list)
        for dset in layer_resps.keys():
            fig=plt.figure();
            for ivar in range(n_var):
                plt.subplot(n_var,1,ivar+1)
                var=layer_resps[dset][resps_list[ivar]]
                var=var.reshape(var.shape[0],-1)
                for icell in range(var.shape[1]):
                    plt.plot(var[:,icell],color=colors[icell%len(colors)])
                #plt.legend(range(var.shape[1]))    
                plt.title(resps_list[ivar])
            plt.suptitle(dset)
            
            
    if 'adaptation_effect' in pm.to_plot: 
        logloss=lambda inpt: np.log(1+np.exp(inpt))
        layer_resps_func=lscsm.inner_resps_func()
        layer_resps={'train': layer_resps_func(training_inputs,K), 'reg':layer_resps_func(validation_inputs,K)}
        thresh_medians=np.median(layer_resps['reg']['adapted_output_thresh'],axis=0)
        noadapt_resps=logloss(layer_resps['reg']['output_layer_input']-thresh_medians)
        deterministic_resps=logloss(layer_resps['reg']['output_layer_input']-layer_resps['reg']['adapted_output_thresh'])
        adapt_corr=computeCorr(noadapt_resps,deterministic_resps)
        plot_max=deterministic_resps.max(axis=0)
        plt.figure()
        plt.plot(noadapt_resps,deterministic_resps,'o')
        plt.plot([0,plot_max.max()],[0,plot_max.max()],'k')
        plt.axis('equal')
        plt.figure()
        plt.hist(adapt_corr,50)
        plt.figure()
        for icell in range(lscsm.num_neurons):
            plt.subplot(1,lscsm.num_neurons,icell+1)
            plt.plot(noadapt_resps[:,icell],deterministic_resps[:,icell],'o')
            plt.plot([0,plot_max[icell]],[0,plot_max[icell]],'k')
            plt.title(np.round(adapt_corr[icell],3))
        
            
    if 'generate_adapted_responses' in pm.to_plot :
        adapt_type='rate'
        adapt_loc='output'
        history_loc='output'
        data=loadmat(pm.data_path+'/'+basename(suppl_params['datafilename']))
        n_cut=data['stim'].shape[0]-training_inputs.shape[0]
        to_be_cut=np.zeros([n_cut,training_set.shape[1]])
        HSM_kwargs=dict(lscsm.get_param_values())
#        HSM_kwargs.update({'rate_plasticity_layers': [],
#                           'rate_plasticity_shape': 'Recursive',
#                           'rate_history_location': 'output',
#                           'rate_plasticity_sep_params': False,
#                           'synaptic_plasticity_layers': ['hidden'],
#                           'synaptic_plasticity_shape': 'Recursive',
#                           'synaptic_history_location': 'input',
#                           'synaptic_plasticity_sep_params': (False,False),
#                           'plasticity_maxtau': 100,
#                           'plasticity_coeffs_bounds': (0,100),
#                           'plasticity_initstim_len':500,
#                           'poisson_output':False})
        HSM_kwargs.update({adapt_type+'_plasticity_layers': [adapt_loc],
                           adapt_type+'_plasticity_shape': 'Recursive',
                           adapt_type+'_history_location': history_loc,
                           'rate_plasticity_sep_params': False,
                           'synaptic_plasticity_sep_params': (False,False),
                           'synaptic_plasticity_effect': 'Multiplicative',
                           'plasticity_maxtau': 100,
                           'plasticity_coeffs_bounds': (0,100),
                           'plasticity_initstim_len':500,
                           'poisson_output':True})
        adapted_lscsm=LSCSM(training_inputs,training_set,**HSM_kwargs)
        saveResults(adapted_lscsm,suppl_params,prefix=basename(pm.param_file[:-11])+'_simulateddata')
        adapted_resps_func=adapted_lscsm.inner_resps_func()
        HSM_kwargs.update({'poisson_output':False})
        deterministic_lscsm=LSCSM(training_inputs,training_set,**HSM_kwargs)
        deterministic_resps_func=deterministic_lscsm.inner_resps_func()
        tau_vals=[50]
        amplitude_vals=[5]
        n_rep=1
        n_saved=0
        to_display=0
        updated_K={}
        adapted_resps={}
        noisy_resps={}
        updated_HSM_params=HSM_params.copy()
        for tau in tau_vals:
            updated_K[tau]={}
            adapted_resps[tau]={}
            #noisy_resps[tau]={}
            for ampl in amplitude_vals:
                updated_HSM_params.update({adapt_loc+'_'+adapt_type+'_plasticity_timeconstant':np.array([tau]), adapt_loc+'_'+adapt_type+'_plasticity_valueatzero':np.array([ampl])})
                updated_K[tau][ampl]=paramdict_to_Kvector(adapted_lscsm,updated_HSM_params)
                #adapted_resps[tau][ampl]={'train': adapted_resps_func(training_inputs,updated_K[tau][ampl]), 'reg':adapted_resps_func(validation_inputs,updated_K[tau][ampl])}                for irep in range(n_rep):
                #temp_resps={'train':np.zeros([n_rep,training_set.shape[0],adapted_lscsm.num_neurons]),'reg':np.zeros([n_rep,validation_set.shape[0],adapted_lscsm.num_neurons])} # /!\/!\/!\ To prevent memory errors
                temp_resps={'reg':np.zeros([n_rep,validation_set.shape[0],adapted_lscsm.num_neurons])}
                adapted_resps[tau][ampl]={'mean': {'train': np.zeros([training_set.shape[0],adapted_lscsm.num_neurons]), 'reg':np.zeros([validation_set.shape[0],adapted_lscsm.num_neurons])}}
                for irep in range(n_rep):
                    train=adapted_resps_func(training_inputs,updated_K[tau][ampl])
                    reg=adapted_resps_func(validation_inputs,updated_K[tau][ampl])
                    #temp_resps['train'][irep]=train['model_output'] # /!\/!\/!\ To prevent memory errors
                    temp_resps['reg'][irep]=reg['model_output']
                    adapted_resps[tau][ampl]['mean']['train']+=train['model_output']*1./n_rep
                    adapted_resps[tau][ampl]['mean']['reg']+=reg['model_output']*1./n_rep
                    if irep<n_saved:
                        adapted_resps[tau][ampl][irep]={'train':train,'reg':reg}
                    #adapted_resps[tau][ampl][irep]={'train': adapted_resps_func(training_inputs,updated_K[tau][ampl]), 'reg':adapted_resps_func(validation_inputs,updated_K[tau][ampl])}
                resps_list=train.keys()
                #adapted_resps[tau][ampl][0]={k: np.random.poisson(adapted_resps[tau][ampl][0][k]['model_output']) for k in ['train','reg']}
                #noisy_resps[tau][ampl]={k: np.random.poisson(adapted_resps[tau][ampl][k]['model_output']) for k in ['train','reg']}
                #adapted_resps[tau][ampl]['mean']={k: {'model_output':temp_resps[k].mean(axis=0)} for k in ['train','reg']}
                adapted_resps[tau][ampl]['nopoisson']={'train': deterministic_resps_func(training_inputs,updated_K[tau][ampl]), 'reg':deterministic_resps_func(validation_inputs,updated_K[tau][ampl])}
                if n_rep>1:
                    #max_corr={k:max_possible_crosscorr(temp_resps[k]) for k in ['train','reg']} # /!\/!\/!\ To prevent memory errors
                    max_corr={'reg':max_possible_crosscorr(temp_resps['reg']), 'train':None}
                else:
                    max_corr={k:None for k in ['train','reg']}
                for irep in range(n_saved):                   
                    #max_corr={k: np.array([np.corrcoef(adapted_resps[tau][ampl][irep][k]['model_output'][:,i],adapted_resps[tau][ampl]['mean'][k]['model_output'][:,i])[0,1] for i in range(training_set.shape[1])]) for k in ['train','reg']}
                    to_save={'stim': data['stim'],
                             'reg_stim': data['reg_stim'],
                             'resps': np.concatenate([to_be_cut,adapted_resps[tau][ampl][irep]['train']['model_output']],axis=0),
                             #'resps': np.concatenate([to_be_cut,noisy_resps[tau][ampl]['train']],axis=0),
                             'reg_resps': np.concatenate([to_be_cut,adapted_resps[tau][ampl]['mean']['reg']],axis=0),
                             #'reg_resps': np.concatenate([to_be_cut,adapted_resps[tau][ampl]['mean']['reg']['model_output']],axis=0),
                             #'reg_resps': np.concatenate([to_be_cut,adapted_resps[tau][ampl]['reg']['model_output']],axis=0),
                             'deterministic_train_resps': adapted_resps[tau][ampl]['mean']['train'],
                             #'deterministic_train_resps': adapted_resps[tau][ampl]['mean']['train']['model_output'],
                             #'deterministic_train_resps': adapted_resps[tau][ampl]['train']['model_output'],
                             'poisson_reg_resps': adapted_resps[tau][ampl][irep]['reg']['model_output'],
                             'nopoisson_train_resps':adapted_resps[tau][ampl]['nopoisson']['train']['model_output'],
                             'nopoisson_reg_resps':adapted_resps[tau][ampl]['nopoisson']['reg']['model_output'],
                             #'max_train_corr': max_corr['train'],
                             'max_reg_corr': max_corr['reg']}
                    savemat(basename(pm.param_file[:-11])+'_simulatedpoissondata_ptau'+str(tau)+'_pampl'+str(ampl)+'_'+str(irep+1),to_save)
                
                #resps_list=adapted_resps[tau][ampl][to_display]['train'].keys()
                n_var=len(resps_list)
                fig=plt.figure();
                for ivar in range(n_var):
                    plt.subplot(n_var,1,ivar+1)
                    var=adapted_resps[tau][ampl][to_display]['train'][resps_list[ivar]]
                    var=var.reshape(var.shape[0],-1)
                    for icell in range(var.shape[1]):
                        plt.plot(var[:,icell],color=colors[icell%len(colors)])
                    plt.title(resps_list[ivar])
                plt.suptitle('tau: '+str(tau)+' ampl: '+str(ampl)+' train')
        del data 
        
        
    if 'generate_preadapt_K' in pm.to_plot :
        tau_val=0
        ampl_val=0.001
        HSM_kwargs=dict(lscsm.get_param_values())
        HSM_kwargs.update({'rate_plasticity_layers': ['output'],
                           'synaptic_plasticity_layers': [],
                           'rate_plasticity_shape': 'Custom',
                           'rate_history_location': 'output',
                           'rate_plasticity_sep_params': True,
                           'plasticity_maxtau': 500,
                           'plasticity_coeffs_bounds': (-10,10),
                           'plasticity_nbins':2,
                           'plasticity_initstim_len':500,
                           'recurrent_connections': True})    
#        HSM_kwargs.update({'rate_plasticity_layers': [],
#                           'synaptic_plasticity_layers': ['hidden'],
#                           'synaptic_plasticity_shape': 'Custom',
#                           'synaptic_history_location': 'output',
#                           'synaptic_plasticity_sep_params': syn_sep,
#                           'plasticity_nbins':n_bins,
#                           'plasticity_coeffs_bounds': (0,1),
#                           'plasticity_initstim_len':500})      
        updated_HSM_params=HSM_params.copy()
        updated_sp=suppl_params.copy()
        updated_sp.update({'epochSize':1000})
        #name=lscsm.name+'_rateadapt_recursive_outhist'
        #name='rateadapt_custom2_sepparams_outhist_refitfromnoadapt'
        #name='synadapt_multiplicative_custom2_sepparamsin_outhist_refitfromnoadapt'
        name='20validcells_recurrence_nbins5_refitfromnoadapt_initampl0001'
        HSM_kwargs['name']=name
        adapted_lscsm=LSCSM(training_inputs,training_set,**HSM_kwargs)  
        updated_HSM_params.update({'output_rate_plasticity_timeconstant':np.array([tau_val]),
                                   'output_rate_plasticity_valueatzero':np.array([ampl_val]),
                                   'output_rate_plasticity_coefficients':np.array([ampl_val]),
                                   'hidden_synaptic_plasticity_timeconstant':np.array([tau_val]),
                                   'hidden_synaptic_plasticity_valueatzero':np.array([ampl_val]),
                                   'hidden_synaptic_plasticity_coefficients':np.array([ampl_val])})
        updated_K=paramdict_to_Kvector(adapted_lscsm,updated_HSM_params)
        saveResults(adapted_lscsm,updated_sp,K=updated_K,prefix=pm.param_file[:-11]+'_'+name)             
                
                
    if 'datahist_to_outhist' in pm.to_plot:
        sim_data=loadmat(pm.data_path+'/'+basename(suppl_params['datafilename']))
        validation_trials=None#loadmat('/media/margot/DATA/Margot/Extracell_data/HSM_format/2817_CXLEFT_TUN21_natmov_seed8_nostimrand_BEST_simulatedpoissondata_ptau50_pampl5_500valrep.mat')['reg_resps']
        data={'stim': {'train':training_inputs, 'reg':validation_inputs}, 'resps': {'train':training_set, 'reg':validation_set, 'nonpoisson_train':sim_data['nopoisson_train_resps'], 'nonpoisson_reg':sim_data['nopoisson_val_resps']}}
        hsm_versions={'datahist':None, 'outhist':None, 'outhist_poisson':None, 'no_adapt':None}
        hsm_Ks={version: K for version in ['datahist','outhist','outhist_poisson']}
        HSM_kwargs=dict(lscsm.get_param_values())
        HSM_kwargs.update({'rate_history_location':'data',
                           'plasticity_initstim_len':0,
                           'poisson_output': False})
        hsm_versions['datahist']=LSCSM(training_inputs,training_set,**HSM_kwargs)                
        HSM_kwargs.update({'rate_history_location':'output',
                           'plasticity_initstim_len':500,
                           'poisson_output': False})
        hsm_versions['outhist']=LSCSM(training_inputs,training_set,**HSM_kwargs) 
        HSM_kwargs.update({'poisson_output':True})
        hsm_versions['outhist_poisson']=LSCSM(training_inputs,training_set,**HSM_kwargs)
        HSM_kwargs.update({'rate_plasticity_layers':[],
                           'poisson_output':False})
        hsm_versions['no_adapt']=LSCSM(training_inputs,training_set,**HSM_kwargs)
        hsm_Ks['no_adapt']=paramdict_to_Kvector(hsm_versions['no_adapt'],HSM_params)                
        resp_funcs={version: hsm_versions[version].inner_resps_func() for version in hsm_versions.keys()}
        hsm_resps={version: {datatype: resp_funcs[version](data['stim'][datatype],hsm_Ks[version]) for datatype in data['stim'].keys()} for version in ['outhist','outhist_poisson','no_adapt']}
        hsm_resps['datahist']={datatype: resp_funcs['datahist'](data['stim'][datatype],hsm_Ks['datahist'],data['resps'][datatype]) for datatype in data['stim'].keys()}
        if validation_trials!=None:
            trialbytrial_resps=[resp_funcs['datahist'](data['stim']['reg'],hsm_Ks['datahist'],validation_trials[irep].astype('float32')) for irep in range(validation_trials.shape[0])]
            hsm_resps['datahist_trialbytrial']={'reg': {output_type: np.array([trialbytrial_resps[irep][output_type] for irep in range(len(trialbytrial_resps))]).mean(axis=0) for output_type in trialbytrial_resps[0].keys()}}
        #hsm_corrs={version: {datatype: computeCorr(hsm_resps[version][datatype]['model_output'],data['resps'][datatype]) if hsm_resps[version].has_key(datatype) else None for datatype in data['stim'].keys()} for version in hsm_resps.keys()}
        hsm_corrs={'to_estim_gndtruth':{version: {datatype: computeCorr(hsm_resps[version][datatype]['model_output'],data['resps'][datatype]) if hsm_resps[version].has_key(datatype) else None for datatype in data['stim'].keys()} for version in hsm_resps.keys()}}
        hsm_corrs['to_nonpoisson_sim']={version: {datatype: computeCorr(hsm_resps[version][datatype]['model_output'],data['resps']['nonpoisson_'+datatype]) if hsm_resps[version].has_key(datatype) else None for datatype in data['stim'].keys()} for version in hsm_resps.keys()}
        mean_hsm_corrs={ctype: {k1: {k2: hsm_corrs[ctype][k1][k2].mean() if hsm_corrs[ctype][k1][k2]!=None else None for k2 in data['stim'].keys()} for k1 in hsm_corrs[ctype].keys()} for ctype in hsm_corrs.keys()}
        for ctype in hsm_corrs.keys():
            print '\n'+ctype+'\n'
            print '\n'.join([k1+' :\t\t\t'+'\t'.join([k2+' : '+str(mean_hsm_corrs[ctype][k1][k2]) for k2 in mean_hsm_corrs[ctype][k1].keys()]) for k1 in mean_hsm_corrs[ctype].keys()])
        resps_list=hsm_resps['datahist']['train'].keys()
        n_var=len(resps_list)
        fig=plt.figure()
        for ivar in range(n_var):
            plt.subplot(n_var,1,ivar+1)
            #for icell in range(hsm_resps['datahist']['train'][resps_list[ivar]].shape[1]):
            for icell in range(3):    
                plt.plot(hsm_resps['datahist']['train'][resps_list[ivar]][:,icell],color=colors[icell%len(colors)],alpha=0.5)
                plt.plot(hsm_resps['outhist']['train'][resps_list[ivar]][:,icell],'--',color=colors[icell%len(colors)])
                plt.plot(hsm_resps['outhist_poisson']['train'][resps_list[ivar]][:,icell],color=colors[icell%len(colors)])
            plt.title(resps_list[ivar])