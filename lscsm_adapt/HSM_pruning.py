import numpy as np
from copy import deepcopy
from loadandsave import enforce_bounds
from runHSM_funcs import paramdict_to_Kvector


def HSM_layer_resps(HSM_params,stim):
    lgn_resps=np.dot(stim,HSM_params['lgn_rf'])
    if HSM_params.has_key('lgn_outfunc'):
        lgn_resps=HSM_params['lgn_outfunc'](lgn_resps-HSM_params['lgn_threshold'].reshape(1,-1))
    hidden_resps=np.dot(lgn_resps,HSM_params['hidden_weights'])  
    hidden_resps=HSM_params['v1_outfunc'](hidden_resps-HSM_params['hidden_layer_threshold'].reshape(1,-1))
    return lgn_resps, hidden_resps
    
def merge_similar_units(resps,out_weights,threshold=0.95):
    resps=resps.copy()
    out_weights=out_weights.copy()
    done=False
    while not(done):
        #corr_mat=np.corrcoef(resps.T)
        prop_mat=np.triu(proportionality_coef(resps.T),1) # proportionalite plutot que correlation: (car correlation = 1 implique seulement relation affine)
        prop_max=prop_mat.max()
        if prop_max>=threshold:
            to_merge=np.array(np.where(prop_mat==prop_max)).T[0]
            total_contribs=np.abs(resps).sum(axis=0)*np.abs(out_weights).sum(axis=1)
            to_delete,to_keep=to_merge[np.argsort(total_contribs[to_merge])]
            prop_coef=np.mean(resps[:,to_delete]*1./resps[:,to_keep])                           
            out_weights[to_keep]+=out_weights[to_delete]*prop_coef
            out_weights[to_delete]=0                           
            resps[:,to_delete]=0
        else:
            done=True
    return out_weights        
            
 
def relative_input_contribs(resps,out_weights):
    n_in,n_out=out_weights.shape
    synapse_resps=np.abs(resps.reshape(resps.shape+(1,)).repeat(n_out,axis=2)*out_weights)
    return np.nanmean(synapse_resps*1./synapse_resps.sum(axis=1).reshape(-1,1,n_out),axis=0)   


def delete_minor_weights(resps,out_weights,relative_thresh=0.3):
    n_units=resps.shape[1]
    input_contribs=relative_input_contribs(resps,out_weights)
    threshold=relative_thresh*1./n_units   
    to_delete=input_contribs<threshold   
    out_weights=out_weights.copy()
    out_weights[to_delete]=0
    return out_weights
        
def proportionality_coef(data_mat):
    log_mat=np.log(data_mat)
    return 2*np.cov(log_mat)*1./np.array([[np.var(row1)+np.var(row2) for row1 in log_mat] for row2 in log_mat])
    
    
def repercute_deletions(valid_units,valid_weights):
    n_layers=len(valid_weights)
    updated_weights=[w.copy().astype(bool) for w in valid_weights]
    updated_units=[u.copy().astype(bool) for u in valid_units]
    
    for i_layer in range(n_layers)[::-1]:
        updated_units[i_layer]=updated_units[i_layer].astype(bool) & np.any(updated_weights[i_layer], axis=1)
        if i_layer>0:
            updated_weights[i_layer-1][:,~updated_units[i_layer]]=0

    return updated_units, updated_weights

    
def cancel_unused_params(HSM_params,valid_units,valid_weights,prevent_nans=False):
    HSM_params=deepcopy(HSM_params)
    HSM_params['hidden_weights'][~valid_weights[0].astype(bool)]=0
    HSM_params['output_weights'][~valid_weights[1].astype(bool)]=0
    lgn_plist=['LGN_temporal_'+p for p in ['K','c1','c2','n1','n2','t1','t2']]+['center_weight','surround_weight','x_pos','y_pos','lgn_threshold']
    if not(prevent_nans):
        lgn_plist+=['size_center','size_surround']
    for pname in lgn_plist:
        if HSM_params.has_key(pname):
            HSM_params[pname][~valid_units[0].astype(bool)]=0
    HSM_params['hidden_layer_threshold'][~valid_units[1].astype(bool)]=0
    if len(valid_units)>2:
        HSM_params['output_layer_threshold'][~valid_units[2].astype(bool)]=0

    return HSM_params
            
     
def prune_HSM(HSM_params,stim,bounds=None,wcontrib_thresh=0.3,similarity_thresh=0.95):
    n_lgn=HSM_params['n_lgn']
    n_hid=HSM_params['n_hidden']
    n_out=HSM_params['n_neurons']
    pruned_params=deepcopy(HSM_params)
    resps={}
    resps['lgn'],resps['hidden']=HSM_layer_resps(pruned_params,stim)
    pruned_params['hidden_weights']=merge_similar_units(resps['lgn'],pruned_params['hidden_weights'],threshold=similarity_thresh)
    pruned_params['hidden_weights']=delete_minor_weights(resps['lgn'],pruned_params['hidden_weights'],relative_thresh=wcontrib_thresh)
    pruned_params['output_weights']=merge_similar_units(resps['hidden'],pruned_params['output_weights'],threshold=similarity_thresh)
    pruned_params['output_weights']=delete_minor_weights(resps['hidden'],pruned_params['output_weights'],relative_thresh=wcontrib_thresh)
    valid_units,valid_weights=repercute_deletions([np.ones(n_lgn),np.ones(n_hid),np.ones(n_out)],[pruned_params['hidden_weights']!=0,pruned_params['output_weights']!=0])
    pruned_params=cancel_unused_params(pruned_params,valid_units,valid_weights,prevent_nans=True)
    if bounds!=None:
        in_bounds={key: enforce_bounds(pruned_params[key].flatten(),bounds[key])[2] for key in bounds.keys()}
        corrected_bounds={key: list(set(enforce_bounds(pruned_params[key].flatten(),bounds[key])[1])) for key in bounds.keys()}
        corrected_bounds={key: (min([tup[0] for tup in corrected_bounds[key]]),max([tup[1] for tup in corrected_bounds[key]])) for key in bounds.keys()}
        exceeded_bounds=[key  for key in bounds.keys() if not(np.all(in_bounds[key]))]
        if len(exceeded_bounds)>0:
            print 'Warning : some bounds have been exceeded :\n'
            print '\n'.join([key+' : '+str(corrected_bounds[key]) for key in exceeded_bounds])+'\n'
        #corrected_bounds={key: enforce_bounds(pruned_params[key],bounds(key))[0] for key in bounds.keys()}
    param_mask={key : np.ones(pruned_params[key].shape).astype(bool) for key in pruned_params.keys() if isinstance(pruned_params[key],np.ndarray)}
    param_mask=cancel_unused_params(param_mask,valid_units,valid_weights,prevent_nans=False)
    return pruned_params, param_mask