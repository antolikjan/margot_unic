import numpy as np

def setLSCSMparams(lscsm,K,param_names,new_values):
    newK=np.array(K[:])
    if isinstance(param_names,str):
        param_names=[param_names]
        new_values=[new_values]
    for iparam in range(len(param_names)):    
        (i,t) = lscsm.free_params[param_names[iparam]]
        newK[i:i+np.prod(t)]=np.array([new_values[iparam]]).reshape(-1)
    return list(newK)
    
    
def copyLSCSMparams(lscsmIn,lscsmOut,KIn,KOut,param_names):
    newK=np.array(KOut[:])
    if isinstance(param_names,str):
        param_names=[param_names]
    for iparam in range(len(param_names)):    
        (i_in,t_in) = lscsmIn.free_params[param_names[iparam]]
        (i_out,t_out) = lscsmOut.free_params[param_names[iparam]]
        newK[i_out:i_out+np.prod(t_out)]=KIn[i_in:i_in+np.prod(t_in)]
    return list(newK)