from explore_metaparams_funcs import *
import param
from utils.param_gui_tools import *
from os.path import dirname, isfile
from scipy.io import loadmat, savemat
from loadandsave import restoreLSCSM_mgt
from traceback import format_exc
base_directory='/media/margot/DATA/Margot/ownCloud'

class exploreMTP_params(param.Parameterized):
    param_file=param.Filename(default=None,allow_None=True,search_paths=[base_directory+'/ANALYSIS/HSM_analysis/HSM_fitting_files',base_directory+'/ANALYSIS/HSM_analysis/HSM_exploration_files'],doc='Default parameter file for the exploration (for which values of the unexplored parameters will be taken)')
    param_ranges_file=param.Filename(default=None,allow_None=True,search_paths=[base_directory+'/ANALYSIS/HSM_analysis/HSM_fitting_files',base_directory+'/ANALYSIS/HSM_analysis/HSM_exploration_files'],doc='File containing lists of parameter values to explore')
    data_file=param.Filename(default=None,allow_None=True,search_paths=[base_directory+'/DATA/extracell_data/python_data',base_directory+'/DATA/extracell_data/python_data/excluded_from_owncloud'],doc='Data file on which to fit')
    exploration_name=param.String(default='', doc='Exploration name (to incorporate into the names of resulting files)')
    params_in_name=param.Boolean(default=False, doc='Whether to incorporate parameter values into file names')
    parallel_mode=param.ObjectSelector(default='none', objects=['slurm','multicore','none'], doc="Which parallelization mode to use")    
    slurm_command=param.String(default='srun -p intel -o %j.txt /home/margot/.virtualenvs/theanoenv/bin/python',doc='If parallelization mode is slurm, detail of the slurm + python command to use')
    to_do=param.ObjectSelector(default='create_pfiles', objects=['create_pfiles','run_fit','brute_force_fit'], doc="Whether to just create parameter files or actually run the fitting procedures") 
    offset=param.Integer(default=0, doc="Amount by which to shift file numbers")
    
display_order={'PARAMETERS': ['param_file','param_ranges_file','data_file','exploration_name','params_in_name','parallel_mode','slurm_command','to_do','offset']}
 
 
try: pm
except NameError: pm=exploreMTP_params()
if (len(sys.argv)<2):    
    pm,validated=param_dialog(pm,display_order)
else:
    if sys.argv[1]!='previous_params':
        if '=' in sys.argv[1]:
            pm=exploreMTP_params(**correct_inputs(dict([inpt.split('=') for inpt in sys.argv[1:]]),pm))
        else:    
            pm=exploreMTP_params(**correct_inputs(retrieve_params(sys.argv[1],raise_error=True),pm))
    validated=True    
    
    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'


    if pm.parallel_mode=='slurm':
        parallel_mode=pm.slurm_command
    else:
        parallel_mode=pm.parallel_mode
    
    if pm.to_do in ['run_fit','create_pfiles']:    
        mpranges,spranges=loadParamRange(pm.param_ranges_file)
        update_mp,update_sp=createCombinations([mpranges,spranges],pm.exploration_name,params_in_name=pm.params_in_name,offset=pm.offset)
        if (not(update_sp[0].has_key('datafilename')) and bool(pm.data_file)):
            for ifit in range (len(update_sp)):
                update_sp[ifit]['datafilename']=pm.data_file
            
    if pm.to_do=='run_fit':        
        runExploratoryFits(pm.param_file[:-11],update_mp=update_mp,update_sp=update_sp,parallel_mode=parallel_mode)
    elif pm.to_do=='create_pfiles':
        createExploratoryPfiles(pm.param_file,update_mp=update_mp,update_sp=update_sp)

        
    elif pm.to_do=='brute_force_fit':
        lscsm,K,suppl_params,training_inputs,training_set,validation_inputs,validation_set=restoreLSCSM_mgt(pm.param_file[:-11],resetK=True,data_path=dirname(pm.data_file))
        func=lscsm.func()
        filename=pm.param_file[:-11]+'_bruteforcefit.mat'
        if isfile(filename):
            best=loadmat(filename)
        else:  
            best={'Ks':np.zeros([100,len(K)]), 'errors':np.zeros(100)+np.inf}
        best_count=0
        whole_count=0
        np.random.seed(None)
        try:
            while True:
                whole_count+=1
                test_K=lscsm.create_random_parametrization(False)
                test_error=func(test_K)
                pos=np.where(test_error<best['errors'])[0]
                if len(pos)>0:
                    insert_among_list(best['Ks'],test_K,pos[0])
                    insert_among_list(best['errors'],test_error,pos[0])
                    best_count+=1
                    print best_count, test_error
                    if (best_count%10==0) | (whole_count%100==0):
                        savemat(filename,best)                        
        except:
            print format_exc()
        finally:
            savemat(filename,best)