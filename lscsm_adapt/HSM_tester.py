import numpy as np
import param
from lscsm.LSCSM import LSCSM

            
class LSCSM_tester(LSCSM):

    def test_of(self,inn,of):
            """
            A function providing implementation of commonly used output functions.
            """
            if of == 'Linear':
               return inn
            if of == 'Exp':
               return np.exp(inn)
            elif of == 'Sigmoid':
               return 5.0 / (1.0 + np.exp(-inn))
            elif of == 'SoftSign':
               return inn / (1 + np.abs_(inn)) 
            elif of == 'Square':
               return inn**2
            elif of == 'ExpExp':
               return np.exp(np.exp(inn))         
            elif of == 'ExpSquare':
               return np.exp(inn**2)
            elif of == 'LogisticLoss':
               return self.log_loss_coefficient*np.log(1+np.exp(self.log_loss_coefficient*inn))
            elif of == 'Rectification':
                return np.max(inn,0)
            elif of == 'SquaredRectif':
                return np.max(inn,0)**2
            elif of == 'Pow3Rectif':
                return np.max(inn,0)**3   
            
    def adaptive_threshold(self,inn,baseline_thresh,history_pos,history_shape):
        if mode=='Recursive':
            # Recursive mode : bias(t) = param1 x bias(t-1) + param2 x signal(t-1)
            # signal = input if adapt_after_func=False, out_func(input,bias) if adapt_after_func=True
            if history_location=='output':
                iteration_func = lambda previous_in,previous_bias : np.exp(-np.ones(1)/adapt_coeffs[0])*previous_bias + adapt_coeffs[1]*out_func(previous_in,previous_bias)
            elif history_location=='input':
                iteration_func = lambda previous_in,previous_bias : T.exp(-T.ones(1)/adapt_coeffs[0])*previous_bias + adapt_coeffs[1]*previous_in
            bias = theano.scan(iteration_func, outputs_info={'initial':T.zeros(output_shape, dtype=theano.config.floatX), 'taps':[-1]}, sequences=T.concatenate([T.zeros(reshaped_inputs[0:1].shape,dtype=theano.config.floatX),reshaped_inputs],axis=0))[0][:-1]
            return out_func(reshaped_inputs,bias)
            
        else:
            # Non-recursive modes : bias(t) = sum_over_n( param(n) x signal(t-n) )
            if mode=='Custom':
                # Custom mode : param = independent temporal coefficients
                history_coeffs=adapt_coeffs
            elif mode=='ExpDecay':
                # Exponential mode : param(n) = A*exp(-(n-1)/tau)
                #time=theano.shared(numpy.repeat([numpy.arange(adapt_coeffs_shape[0],dtype=theano.config.floatX)],adapt_coeffs_shape[1],axis=0).T,broadcastable=(False,adapt_coeffs_shape[1]==1))
                time=T.zeros_like(adapt_coeffs[0],dtype=theano.config.floatX)+T.arange(adapt_coeffs_shape[0],dtype=theano.config.floatX).reshape([adapt_coeffs_shape[0]]+[1]*(len(adapt_coeffs_shape)-1))
                history_coeffs=adapt_coeffs[1]*T.exp(-time/adapt_coeffs[0])
            else:
                raise NotImplementedError('Adaptive mode not implemented : '+str(mode))
            
            if history_location=='output': 
                # signal = output = out_func(input,bias)
                output_at_t = lambda input_t,*past_output: out_func(input_t, T.sum(history_coeffs*T.as_tensor(past_output),axis=0))
                #output_at_t = lambda input_t,*past_output: out_func(input_t, T.sum(history_coeffs*(T.as_tensor(past_output)[0,0]*T.zeros(T.as_tensor(past_output).shape)),axis=0))
                #output_at_t = lambda input_t,*past_output: T.zeros(self.n_hidden)+T.as_tensor
                #output_at_t = lambda input_t,*past_output: out_func(input_t, T.sum(history_coeffs*T.zeros(T.as_tensor(past_output).shape),axis=0))
                return theano.scan(output_at_t, outputs_info={'initial':T.zeros([adapt_coeffs_shape[0]]+output_shape, dtype=theano.config.floatX), 'taps':range(-1,-adapt_coeffs_shape[0]-1,-1)}, sequences=reshaped_inputs)[0]
            elif history_location=='input':
                # signal = input
                # output = out_func(input,bias)
                bias_func = lambda t: T.sum(history_coeffs[:T.min([adapt_coeffs_shape[0],t])]*reshaped_inputs[T.max([t-adapt_coeffs_shape[0],0]):t],axis=0)# if t>0 else T.zeros([1,n_cells],dtype=theano.config.floatX)
                bias  = theano.scan(bias_func, sequences=T.arange(1,inputs.shape[0]))[0]
                bias =  T.concatenate([T.zeros([1]+list(bias.shape[1:]),dtype=theano.config.floatX),bias],axis=0)
                return out_func(reshaped_inputs, bias)   

    def test_response(self,K_vec,stim):        
        pm=retrieve_LSCSM_params(self,K_vec)
        
        lgn_resp=np.dot(pm['lgn_rf'],stim)        
        if self.LGN_threshold:
            if ('lgn' in self.rate_plasticity_layers) & (self.plasticity_nbins>0):
                
            else: 
                lgn_resp = self.test_of(lgn_resp-pm['lgn_threshold'],self.lgnof)
            lgn_resp = self.construct_adaptive_output(lgn_output,'lgn','rate')
        else:
            lgn_resp = self.test_of(lgn_resp,self.lgnof)
        
        if self.second_layer:
           # LGN layer output to hidden layer subthreshold activity
           output = self.construct_adaptive_output(output,'hidden','synaptic')               
           # Hidden layer subthreshold activity to hidden layer output
           output = self.construct_adaptive_output(output,'hidden','rate')
        
        # Hidden layer output to output layer subthreshold activity 
        output = self.construct_adaptive_output(output,'output','synaptic')          
        # Output layer subthreshold activity to output layer output
        self.model_output = self.construct_adaptive_output(output,'output','rate')
        
        return self.model_output
        
          
          
        
        
