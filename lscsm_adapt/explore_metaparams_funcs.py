import itertools
from runHSM_funcs import runHSM
import numpy as np
from visualizationMgt import computeCorr
from loadandsave import restoreLSCSM_mgt
from lscsm.checkpointing import loadParams, loadErrCorr, restoreLSCSM
import matplotlib.pyplot as plt
from pprint import pformat
from collections import OrderedDict
from multiprocessing import Pool as mp_pool, cpu_count
from subprocess import call as sp_call
import glob
from os.path import isfile, dirname, basename
# This module allows to explore a large number of different combinations values for the metaparameters of the LSCSM on a given dataset
# For each combination, the corresponding LSCSM model is adjusted to the data, then its performance is computed.


def saveParamRange(pranges,datafile,explname,resultspath):
# Saves prange = the dict containing the names of the LSCSM params to be explored and the values to be tested for each of these
# ( pranges={'param_1': [value_1,...,value_i], ..., 'param_n': [value_1,...,value_j]} )    
     with open(resultspath+datafile[:-4]+'_'+explname+'_paramrange','w') as f:
            #f.write('pranges = {\n'+pformat(dict(pranges), width=1)[1:]+'\n\n')
            f.write('pranges = {\n'+pformat(dict(pranges))[1:]+'\n\n')

            
#def loadParamRange(datafile,explname,resultspath):
## Loads prange    
#     with open(resultspath+datafile[:-4]+'_'+explname+'_paramrange','r') as f:
#            exec(f.read())
#     return pranges 


def loadParamRange(file_name):
# Loads prange    
     with open(file_name,'r') as f:
            exec(f.read())
     return mpranges, spranges          


def alphaOrderDict(rand_dict):
# Transforms a dictionnary into an ordered dictionnary with keys in alphabetical order    
    keys=sorted(rand_dict.keys())
    return OrderedDict(zip(keys,[rand_dict[k] for k in keys]))
    
    
def split_dicts(merged_dict,dict_keys):
    return([{key: merged_dict[key] for key in dk} for dk in dict_keys])    


#def createCombinations(pranges,explname,params_in_name=False):
## Creates a list of all possible combinations from a list of wanted values for each parameter to explore ( pranges={'param_1': [value_1,...,value_i], ..., 'param_n': [value_1,...,value_j]} )    
## Explname is the identifier (string) chosen for this exploration (which will be added to subsequent file names)
#    pranges=alphaOrderDict(pranges)
#    update_p=[]
#    nfits=0
#    for items in itertools.product(*pranges.itervalues()):
#        nfits+=1 
#        if params_in_name:
#            filename='_'.join([str(pranges.keys()[i])+str(items[i]) for i in range(len(items))])
#            if len(explname)>0:
#                filename=explname+'_'+filename
#        else:
#            filename=explname+str(nfits)            
#        update_p.append(dict(zip(pranges.keys()+['name'],list(items)+[filename])))
#    return update_p
    

def createCombinations(pranges,explname,params_in_name=False,offset=0,n_zeros=None):
# Creates a list of all possible combinations from a list of wanted values for each parameter to explore ( pranges={'param_1': [value_1,...,value_i], ..., 'param_n': [value_1,...,value_j]} )    
# Explname is the identifier (string) chosen for this exploration (which will be added to subsequent file names)
    if isinstance(pranges,dict):
        merged_pranges=pranges
        pranges=[pranges]
    else:
        merged_pranges=pranges[0].copy()
        for idict in range(1,len(pranges)):
            merged_pranges.update(pranges[idict])
    merged_pranges=alphaOrderDict(merged_pranges)
    update_p=[]
    nfits=0
    if n_zeros is None:
        n_zeros=len(str(np.prod([len(l) for l in merged_pranges.values()])))
    for items in itertools.product(*merged_pranges.itervalues()):
        comb_dict=dict(zip(merged_pranges.keys(),list(items)))
        if params_in_name:
            filename='_'.join([str(key).translate(None,'_')+str(comb_dict[key]) for key in sorted(comb_dict.keys())])
            if len(explname)>0:
                filename=explname+'_'+filename
        else:
            filename=explname+'_'+str(offset+nfits).zfill(n_zeros)
        splitted_dicts=split_dicts(comb_dict,[d.keys() for d in pranges])
        for idict in range(len(pranges)):
            if nfits==0:
                update_p.append([])
            update_p[idict].append(splitted_dicts[idict])
        update_p[0][-1]['name']=filename
        nfits+=1
    if len(update_p)==1:
        update_p=update_p[0]
    return update_p    


def createExploratoryPfiles(defaultpfile,update_mp=[],update_sp=[]):
    assert (len(update_mp)==0) or (len(update_sp)==0) or (len(update_mp)==len(update_sp))
    if len(update_mp)==0:
        update_mp=[{}]*len(update_sp)
    if len(update_sp)==0:
        update_sp=[{}]*len(update_mp)        
    mp,sp=loadParams(defaultpfile)    
            
    for ifit in range(len(update_mp)):
        if not(update_sp[ifit].has_key('datafilename')):
            update_sp[ifit]['datafilename']=sp['datafilename']
        update_sp[ifit]['datafilename']=basename(update_sp[ifit]['datafilename'])   
        filename=dirname(defaultpfile)+'/'+update_sp[ifit]['datafilename'].split('.')[0]+'_'+update_mp[ifit]['name']+'_metaparams'
        print filename
        mp.update(update_mp[ifit])
        sp.update(update_sp[ifit])
        with open(filename,'w') as f:
            f.write('meta_params = {\n'+pformat(mp)[1:]+'\n\n')
            f.write('suppl_params = {\n'+pformat(sp)[1:]+'\n\n')
 

def runExploratoryFits(defaultpfile,update_mp=[],update_sp=[],startfrom=1,parallel_mode=None):
# Takes the list of combinations created by createCombinations, runs the following LSCSM fits and saves the results  
    assert (len(update_mp)==0) or (len(update_sp==0)) or (len(update_mp)==len(update_sp))
    if len(update_mp)==0:
        update_mp=[{}]*len(update_sp)
    if len(update_sp)==0:
        update_sp=[{}]*len(update_mp) 
        
    mp,sp=loadParams(defaultpfile+'_metaparams') 
    if not(update_sp[0].has_key('datafilename')):
        for ifit in range(len(update_sp)):
            update_sp[ifit]['datafilename']=sp['datafilename']
        
    if parallel_mode in ['none', 'None', None]:        
        for ifit in range(startfrom-1,len(update_mp)):
            print dirname(defaultpfile)+'/'+basename(update_sp[ifit]['datafilename'].split('.')[0])+'_'+update_mp[ifit]['name']+'_metaparams'
            if not isfile(dirname(defaultpfile)+'/'+basename(update_sp[ifit]['datafilename'].split('.')[0])+'_'+update_mp[ifit]['name']+'_metaparams'):
                print 'Running '+update_mp[ifit]['name']
                K,lscsm,terr,verr,tcorr,vcorr=runHSM(prevrunfile=defaultpfile,resetK=True,meta_params=update_mp[ifit],suppl_params=update_sp[ifit],compCorr=False)
    
    elif parallel_mode=='multicore': 
        try:
            pool=mp_pool(cpu_count())
            pool.map(runLSCSM_wrapper,[{'prevrunfile':defaultpfile,'resetK':True,'meta_params':update_mp[ifit],'suppl_params':update_sp[ifit],'compCorr':False} for ifit in range(startfrom-1,len(update_mp))])
        except:
            pool.terminate()
        finally:    
            pool.close()
            pool.join()
            
    else:
        for ifit in np.arange(startfrom-1,len(update_mp)):
            if not isfile(dirname(defaultpfile)+'/'+basename(update_sp['datafilename'][ifit].split('.')[0])+'_'+update_mp[ifit]['name']+'_metaparams'):
                sp_call(parallel_mode+' runHSM_funcs "'+defaultpfile+'" "True" "'+str(update_mp[ifit])+'" "'+str(update_sp[ifit])+'" "False"')   


def runLSCSM_wrapper(args):
    mp,sp=loadParams(args['prevrunfile']+'_metaparams')
    if not isfile(dirname(args['prevrunfile'])+'/'+basename(args['suppl_params']['datafilename'].split('.')[0])+'_'+args['meta_params']['name']+'_metaparams'):
        print 'Running '+args['meta_params']['name']
        _=runHSM(**args)


#def computeExploratoryMat(pranges,datafile,explname,resultspath,datapath,measure='corr'):
## Computes validation performance (correlation between predicted and recorded response: measure='corr', or log-likelihood: measure='loglkl') for each LSCSM model resulting from a given parameter exploration
## Returns all values in a N_param_1 x ... x N_param_i array
#    pranges=alphaOrderDict(pranges)     
#    lengths=[len(pranges[pranges.keys()[i]]) for i in range(len(pranges.keys()))]
#    #update_mp=createCombinations(pranges,explname)
#    vmat=np.zeros(lengths)
#    indices=itertools.product(*[range(l) for l in lengths])
#    
#    if measure=='corr':
#        for ifit in range(np.prod(lengths)):  
#        #for items in itertools.product(*pranges.itervalues()):        
#            meta_params,suppl_params=loadParams(datafile[:-4]+'_'+explname+str(ifit+1)+'_metaparams',filepath=resultspath)
#            if ifit==0:
#                training_inputs,training_set,validation_inputs,validation_set = loadData(suppl_params['datafilename'],minStimSize=suppl_params['stimsize']**2,val_frac=suppl_params['val_frac'],tau=suppl_params['tau'],filepath=datapath)
#            lscsm=LSCSM(training_inputs,training_set,**meta_params)
#            K=loadVec(datafile[:-4]+'_'+explname+str(ifit+1)+'_K',filepath=resultspath)
#            val_c=computeCorr(lscsm.response(validation_inputs,K),validation_set)        
#            vmat[indices.next()]=np.mean(val_c)
#            
#    elif measure=='loglkl':
#         for ifit in range(np.prod(lengths)):
#             terr,verr=loadErrCorr(datafile[:-4]+'_'+explname+str(ifit+1)+'_error',filepath=resultspath)
#             vmat[indices.next()]=verr[-1]
#             
#    return vmat 


#def compute_perfmat(expl_names,data_path='',measure='corr'):
def compute_perfmat(pranges,name_mat,data_path='',measure='correlation'):
    #pranges, name_mat=reconstruct_pranges(expl_names)
    assert not(pranges.has_key('datafilename'))
    perf_mat=np.zeros(name_mat.shape)+np.nan
    indices=itertools.product(*[range(l) for l in name_mat.shape])
    
    if measure=='correlation':
        for ifit in range(np.prod(name_mat.shape)):  
            inds=indices.next()
            if len(name_mat[inds])>0:
                print name_mat[inds]
                try :
                    tcorr,vcorr=loadErrCorr(name_mat[inds][:-11]+'_corr')
                    if np.isnan(vcorr[-1]):
                        raise ValueError('Is NaN')
                    perf_mat[inds]=vcorr[-1]
                except :    
                    lscsm,K,suppl_params,training_inputs,training_set,validation_inputs,validation_set=restoreLSCSM_mgt(name_mat[inds][:-11],data_path=data_path)
                    vcorr=computeCorr(lscsm.response(validation_inputs,K),validation_set)        
                    perf_mat[inds]=np.mean(vcorr)
            
    elif measure=='log-likelihood':
         for ifit in range(np.prod(name_mat.shape)):
            inds=indices.next()
            if len(name_mat[inds])>0:
                terr,verr=loadErrCorr(name_mat[inds][:-11]+'_error')
                perf_mat[inds]=verr[-1]
             
    return perf_mat#, pranges   
    
    
def reconstruct_pranges(expl_names):
    expl_files=list(itertools.chain(*[glob.glob(expl_names[i]+'*_BEST_metaparams') for i in range(len(expl_names))]))
    ms_params={}
    for filename in expl_files:
        mp,sp=loadParams(filename)
        for dict_var in [mp,sp]:
            for key in ['date','n_rep','name']:
                try: del dict_var[key]
                except: pass
        mp.update(sp)
        ms_params[filename]=mp
   
    #pranges=alphaOrderDict({key: sorted(list(set([ms_params[name][key] for name in ms_params.keys()]))) for key in ms_params[expl_files[0]].keys()})
    pranges=alphaOrderDict({key: [elem for elem,_ in itertools.groupby(sorted([ms_params[name][key] for name in ms_params.keys()]))] for key in ms_params[expl_files[0]].keys()})        
    if 'datafilename' in pranges.keys():
        if len(set(pranges['datafilename']))>1:
            print 'Warning : more than one data file'
            print '\n'.join(list(set(pranges['datafilename'])))+'\n\n'
        del pranges['datafilename']
    pnames=np.array(pranges.keys())
    lengths=np.array([len(pranges[key]) for key in pnames])
    pranges=OrderedDict([pranges.items()[i] for i in range(len(lengths)) if lengths[i]>1])
    pnames=pnames[lengths>1]
    lengths=lengths[lengths>1]
    names_mat=np.zeros(lengths).astype('|S300')
    names_mat[:]=''
    indices=itertools.product(*[range(l) for l in lengths])
    #param_list=[]
    
    existing_params={name: {key: ms_params[name][key] for key in pnames} for name in ms_params.keys()}
    for ifit in range(np.prod(lengths)):
        inds=indices.next()
        pdict={pnames[i]:pranges[pnames[i]][inds[i]] for i in range(len(inds))}
        if pdict in existing_params.values():
            #param_list.append(pdict)
            names_mat[inds]=existing_params.keys()[existing_params.values().index(pdict)]
    #return OrderedDict([pranges.items()[i] for i in range(len(lengths)) if lengths[i]>1]), names_mat.squeeze()#, param_list   
    return pranges, names_mat 
    

def compute_partial_perfs(explmat,pranges,to_ignore=['seed'],collapse_func=np.nanmean):
    pranges=alphaOrderDict(pranges.copy()) 
    for pname in to_ignore:
        if pname in pranges:
            explmat=collapse_func(explmat,axis=pranges.keys().index(pname))
            del pranges[pname]
        
    matshape=explmat.shape
    nparams=len(matshape)
    redmats=[]

    for comb in itertools.combinations(range(nparams),2):
        comb=sorted(list(comb))
        tempmat=np.rollaxis(np.rollaxis(explmat,comb[0]),comb[1],1)
        redmats.append(collapse_func(np.reshape(tempmat,(tempmat.shape[0],tempmat.shape[1],-1)),axis=2).copy())

    return redmats, pranges


def partialperf_colormap(redmats,red_pranges,forcescale=None):
    nparams=int(0.5*(1+np.sqrt(8.*len(redmats)+1.)))
    assert len(redmats)==0.5*nparams*(nparams-1)
    if forcescale:
        vmin,vmax=forcescale
    else:    
        vmax=np.nanmax([np.nanmax(r) for r in redmats])
        vmin=np.nanmin([np.nanmin(r) for r in redmats])
    combs=[sorted(list(comb)) for comb in itertools.combinations(range(nparams),2)]    
    keys=red_pranges.keys()
          
    fig=plt.figure()
    for iplot in range(len(combs)):       
        plt.subplot(nparams-1,nparams-1,combs[iplot][0]*(nparams-1)+combs[iplot][1])
        subp=plt.pcolor(redmats[iplot],vmin=vmin,vmax=vmax,cmap='YlOrRd')
        
        if combs[iplot][1]==combs[iplot][0]+1:
            plt.xlabel(keys[combs[iplot][1]],fontsize=20)
            plt.ylabel(keys[combs[iplot][0]],fontsize=20)
            tempxleg=red_pranges[keys[combs[iplot][1]]]
            xlegends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in tempxleg]))
 
            plt.xticks(np.arange(len(xlegends))*0.5,xlegends,fontsize=18)
            tempyleg=red_pranges[keys[combs[iplot][0]]]
            ylegends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in tempyleg]))
            plt.yticks(np.arange(len(ylegends))*0.5,ylegends,fontsize=18)
            plt.axis('tight')
            
        else:
            plt.axis('off')
            
    cax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
    cb=fig.colorbar(subp, cax=cax)
    for t in cb.ax.get_yticklabels():
        t.set_fontsize(18)
    
 
#def plotParamExploration(explmat,pranges,forcescale=None,legends=None,to_ignore=['seed'],collapse_func=np.nanmean):
## Plots the performance array returned by computeExploratoryMat:
## Does 2-among-number-of-explored-params color plots: in each of them, two of the params are varied, and the performance values plotted correspond to the mean performance across all values taken by the other params during the exploration.
#    pranges=alphaOrderDict(pranges.copy()) 
#    for pname in to_ignore:
#        if pname in pranges:
#            explmat=collapse_func(explmat,axis=pranges.keys().index(pname))
#            del pranges[pname]
#        
#    matshape=explmat.shape
#    nparams=len(matshape)
#    keys=pranges.keys()
#    if legends is None:
#        legends=keys
#    redmats=[]
#    combs=[]
#    if forcescale:
#        vmin,vmax=forcescale
#    else:    
#        vmax=0
#        vmin=10
#
#    for comb in itertools.combinations(range(nparams),2):
#        comb=list(comb)
#        comb.sort()
#        combs.append(comb)
#        tempmat=np.rollaxis(np.rollaxis(explmat,comb[0]),comb[1],1)
#        redmats.append(collapse_func(np.reshape(tempmat,(tempmat.shape[0],tempmat.shape[1],-1)),axis=2).copy())
#        #redmat=np.reshape(tempmat,(tempmat.shape[0],tempmat.shape[1],-1)).mean(axis=2)
#        if not(forcescale):
#            if redmats[-1].max()>vmax:
#                vmax=redmats[-1].max()
#            if redmats[-1].min()<vmin:
#                vmin=redmats[-1].min()    
#                
#    fig=plt.figure()
#    for iplot in range(len(combs)):
#       
#        plt.subplot(nparams-1,nparams-1,combs[iplot][0]*(nparams-1)+combs[iplot][1])
#        #pcolor(-redverr,vmin=0,vmax=errmax,cmap='RdBu')
#        subp=plt.pcolor(redmats[iplot],vmin=vmin,vmax=vmax,cmap='YlOrRd')
#        #plt.pcolor(redmat,vmin=0,vmax=vmax,cmap='YlOrRd')
#        #plt.pcolor(redmat,cmap='YlOrRd')
#        
#        if combs[iplot][1]==combs[iplot][0]+1:
#            plt.xlabel(legends[combs[iplot][1]],fontsize=20)
#            plt.ylabel(legends[combs[iplot][0]],fontsize=20)
#            tempxleg=pranges[keys[combs[iplot][1]]]
#            xlegends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in tempxleg]))
#            #xlegends=['']+[str(leg) for leg in pranges[legends[comb[1]]]]
#            #plt.xticks(range(len(xlegends)),xlegends)
#            plt.xticks(np.arange(len(xlegends))*0.5,xlegends,fontsize=18)
#            tempyleg=pranges[keys[combs[iplot][0]]]
#            ylegends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in tempyleg]))
#            #ylegends=['']+[str(leg) for leg in pranges[legends[comb[0]]]]
#            plt.yticks(np.arange(len(ylegends))*0.5,ylegends,fontsize=18)
#            plt.axis('tight')
#            
#        else:
#            plt.axis('off')
#            
#    cax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
#    cb=fig.colorbar(subp, cax=cax)
#    for t in cb.ax.get_yticklabels():
#        t.set_fontsize(18)   


def bestParamSets(update_mp,explmat,order=1):
# Sorts the list of param combinations of a given exploration (update_mp) in increasing (order=1) or decreasing (order=-1) order of performance (using performance values stored in explmat)
# Also returns the corresponding performance values
    assert order in [-1,1]
    explvec=np.reshape(explmat,-1)
    update_mp=np.array(update_mp)[~np.isnan(explvec)]
    explvec=explvec[~np.isnan(explvec)]
    sortorder=np.argsort(explvec*order)
    return list(update_mp[sortorder]), explvec[sortorder]


def insert_among_list(full_list,new_value,indice):
    full_list[indice+1:]=full_list[indice:-1]
    full_list[indice]=new_value