import scipy.io
import numpy as np
from lscsm.checkpointing import loadParams, restoreLSCSM
from utils.volterra_kernels import reshape_data_forRF
from data_analysis.extracell_funcs import pythondata_to_rfdata
import os.path
from lscsm.LSCSM import LSCSM

# Functions to load and save data, fitting parameters and fitting results


#def rmFiles(filenames,filepath=''):
## Delete files listed in filenames
#    for fname in filenames:
#        if os.path.isfile(filepath+fname):
#            os.remove(filepath+fname)


def removeBlanks(stim,resp):
# Remove alls bins which correspond to blank stimuli from stimulus and response variables    
    validbins=[]
    for ibin in range(stim.shape[0]):
        if any(stim[ibin,:]):
            validbins.append(ibin)
            
    return stim[validbins,:], resp[validbins,:]


def loadData(filename,reg_frac=0.2,val_frac=0,tau=0,n_tau=1,seed_val=None,normalize_stim=False,ROI=None,ceiling_val=1.,stim_types=[]):
# Load stimulus and response data and reshape it if necessary
# Outputs: - training_input (stimuli used for training): nbins_training x npixels
#          - training_set (responses used for training): nbins_training x ncells   
#          - validation_input (stimuli used for validation): nbins_validation x npixels
#          - validation_set (responses used for validation): nbins_validation x ncells  
    
    if stim_types:
        # New data format
        stim_name=','.join(stim_types)
        stim_list={'name':[stim_name],'fraction':[1],'selection_seed':[0],'stimuli':[stim_name]}
        stim_dict,spikes_dict,_=pythondata_to_rfdata(filename,ep_info_file=filename.split('.')[0]+'_epinfo.txt',stim_categories=stim_list,file_type='new')
        if ROI is not None :
            nPix=(ROI[1]-ROI[0])*(ROI[3]-ROI[2])
            t_stim=stim_dict[stim_name]['raw_train'][:,:,ROI[0]:ROI[1],ROI[2]:ROI[3]].reshape(-1,nPix)
            r_stim=stim_dict[stim_name]['raw_reg'][:,:,ROI[0]:ROI[1],ROI[2]:ROI[3]].reshape(-1,nPix)
            v_stim=stim_dict[stim_name]['raw_val'][:,:,ROI[0]:ROI[1],ROI[2]:ROI[3]].reshape(-1,nPix)
        else :
            nPix=np.prod(stim_dict[stim_name]['raw_train'].shape[2:])
            t_stim=stim_dict[stim_name]['raw_train'].reshape(-1,nPix)
            r_stim=stim_dict[stim_name]['raw_reg'].reshape(-1,nPix)
            v_stim=stim_dict[stim_name]['raw_val'].reshape(-1,nPix)
        nCells=spikes_dict[stim_name]['raw_train'].shape[-1]    
        t_spikes=spikes_dict[stim_name]['raw_train'].reshape(-1,nCells)  
        r_spikes=spikes_dict[stim_name]['raw_reg'].reshape(-1,nCells)
        v_spikes=spikes_dict[stim_name]['raw_val'].reshape(-1,nCells)
        [t_stim,r_stim,_],[t_spikes,r_spikes,_],_=reshape_data_forRF([t_stim,r_stim,v_stim],[t_spikes,r_spikes,v_spikes],normalize_stim=normalize_stim,normalize_resp=False,tau=tau,n_tau=n_tau,stim_seed=seed_val,sub_data_fracs=[1])
        
    else:
        # Old data formats
        data=scipy.io.loadmat(filename)
        if data.has_key('stim1'):    
            (nBins,nUnits,nEp)=np.shape(data['spikes'])
            (nDivX,nDivY,nBins)=np.shape(data['stim1'])
            
            spikes=np.reshape(np.rollaxis(data['spikes'],2,0),(nBins*nEp,nUnits)).astype(float)
            stim=np.zeros((nBins*nEp,nDivX*nDivY))
            for iEp in range(nEp):
                stim[iEp*nBins:(iEp+1)*nBins,:]=np.reshape(data['stim'+str(iEp+1)],(nDivX*nDivY,nBins)).T
                
        elif data.has_key('stim'):
            (nBins,nDivX,nDivY)=data['stim'].shape
            stim=data['stim'].reshape(data['stim'].shape[0],-1) 
            spikes=data['resps']
              
        else:
            raise ValueError("Data file doesn't contain expected entries")

        if ROI is not None :
            stim=stim.reshape(nBins,nDivX,nDivY)[:,ROI[0]:ROI[1],ROI[2]:ROI[3]].reshape(nBins,-1)    
        else:
            ROI=(0,nDivX,0,nDivY)
        
        if data.has_key('reg_stim') and data.has_key('reg_resps'):
            r_stim=data['reg_stim'][:,ROI[0]:ROI[1],ROI[2]:ROI[3]].reshape(data['reg_stim'].shape[0],-1)
            r_spikes=data['reg_resps']
            if data.has_key('val_stim') and data.has_key('val_resps'):
                v_stim=data['val_stim'][:,ROI[0]:ROI[1],ROI[2]:ROI[3]].reshape(data['val_stim'].shape[0],-1)
                v_spikes=data['val_resps']
            else:
                v_stim=np.zeros((0,r_stim.shape[1]))
                v_spikes=np.zeros((0,r_spikes.shape[1]))
    #        if normalize_stim:
    #            stim_mean=np.concatenate([stim,r_stim,v_stim],axis=0).mean()
    #            stim_std=np.concatenate([stim,r_stim,v_stim],axis=0).std()
    #            stim=(stim-stim_mean)*1./stim_std
    #            r_stim=(r_stim-stim_mean)*1./stim_std
    #        [t_stim],[t_spikes]=reshape_data_forRF(stim,spikes,normalize_stim=False,normalize_resp=False,tau=tau,n_tau=n_tau,stim_seed=seed_val,sub_data_fracs=[1])
    #        [r_stim],[r_spikes]=reshape_data_forRF(r_stim,r_spikes,normalize_stim=False,normalize_resp=False,tau=tau,n_tau=n_tau,stim_seed=seed_val,sub_data_fracs=[1])
            [t_stim,r_stim,_],[t_spikes,r_spikes,_],_=reshape_data_forRF([stim,r_stim,v_stim],[spikes,r_spikes,v_spikes],normalize_stim=normalize_stim,normalize_resp=False,tau=tau,n_tau=n_tau,stim_seed=seed_val,sub_data_fracs=[1])            
      
        else:    
            [t_stim,r_stim,_],[t_spikes,r_spikes,_],_=reshape_data_forRF(stim,spikes,normalize_stim=normalize_stim,normalize_resp=False,tau=tau,n_tau=n_tau,stim_seed=seed_val,sub_data_fracs=[1-reg_frac-val_frac,reg_frac,val_frac])
    
    np.random.seed(int(np.round(ceiling_val*100)))
    ceiling_inds=np.array(sorted(np.random.choice(t_stim.shape[0],int(np.round(t_stim.shape[0]*1./ceiling_val)), replace=False)))      
    return t_stim[ceiling_inds], t_spikes[ceiling_inds], r_stim, r_spikes
    

#def loadData(filename,minStimSize=1,val_frac=0.2,tau=0,conv_func=None,remove_bl=False,seed_val=None):
## Load stimulus and response data and reshape it if necessary
## Outputs: - training_input (stimuli used for training): nbins_training x npixels
##          - training_set (responses used for training): nbins_training x ncells   
##          - validation_input (stimuli used for validation): nbins_validation x npixels
##          - validation_set (responses used for validation): nbins_validation x ncells  
# 
#    
#    data=scipy.io.loadmat(filename)
#    if data.has_key('stim1'):    
#        (nBins,nUnits,nEp)=np.shape(data['spikes'])
#        (nDivX,nDivY,nBins)=np.shape(data['stim1'])
#        nBinsTot=nBins*nEp
#        multFactor=np.ceil(np.sqrt(minStimSize*1.0/(nDivX*nDivY)))
#        stimSize=nDivX*nDivY*multFactor**2
#        
#        spikes=np.reshape(np.rollaxis(data['spikes'],2,0),(nBinsTot,nUnits)).astype(float)
#        stims=np.zeros((nBinsTot,stimSize))
#        for iEp in range(nEp):
#            tempstim=np.repeat(np.repeat(data['stim'+str(iEp+1)],multFactor,0),multFactor,1)
#            stims[iEp*nBins:(iEp+1)*nBins,:]=np.reshape(tempstim,(stimSize,nBins)).T
#            
#    elif data.has_key('stim'):
#        (nBinsTot,nDivX,nDivY)=np.shape(data['stim'])
#        multFactor=np.ceil(np.sqrt(minStimSize*1.0/(nDivX*nDivY)))
#        stims=data['stim'].repeat(multFactor,1).repeat(multFactor,2).reshape(nBinsTot,-1) 
#        spikes=data['resps']
#    else:
#        raise ValueError("Data file doesn't contain expected entries")        
#    
#    if remove_bl:
#        # Remove bins which correspond to blank stimuli
#        stims,spikes=removeBlanks(stims,spikes)
#        nBinsTot=stims.shape[0]
#    
#    if tau!=0:
#        # Shift stimulus and response from tau bins, so as to align bin i of the stimulus with bin i+tau of the response
#        stims=stims[:nBinsTot-tau,:]
#        spikes=spikes[tau:,:]
#   
#    # Convolve stim with vector conv_func
#    # /!\ Option not tested yet    
#    elif conv_func<>None:
#        for ipix in range(stimSize):
#            stims[:,ipix]=convolve(reshape(stims[:,ipix],nBinsTot),conv_func,'same').copy()
#            
#    # Randomize  
#    if seed_val<>None:
#         np.random.seed(seed_val)
#         random_range=np.random.permutation(np.arange(nBinsTot-tau))
#         stims=stims[random_range,:]
#         spikes=spikes[random_range,:]
#    
#    nBinsTrain=round((nBinsTot-tau)*(1-val_frac))   
#    return stims[:nBinsTrain,:], spikes[:nBinsTrain,:], stims[nBinsTrain:,:], spikes[nBinsTrain:,:] # training_input, training_set, validation_input, validation_set
    

def loadBinarySignals(file_name,n_ep,n_elec,data_format='klusta'):
    if data_format=='klusta':
        data=np.fromfile(file_name,dtype='h').reshape(n_ep,-1,n_elec)
    elif data_format=='elphy':
        data=np.fromfile(file_name,dtype='h').reshape(n_ep,n_elec,-1)
        data=np.rollaxis(data,2,1)
    return data   

    
def enforce_bounds(K,bounds):
    correct_K=K.copy()
    correct_bounds=bounds[:]
    for i_param in range(len(K)):
        if K[i_param]<bounds[i_param][0]:
            correct_K[i_param]=bounds[i_param][0]
            correct_bounds[i_param]=(K[i_param],correct_bounds[i_param][1])
        elif K[i_param]>bounds[i_param][1]:   
            correct_K[i_param]=bounds[i_param][1]
            correct_bounds[i_param]=(correct_bounds[i_param][0],K[i_param])
    return correct_K, correct_bounds, correct_K==K        


def bounds_homogenization(bounds,param_list):
    bounds_dict={}
    correct_bounds=np.array(bounds).copy()
    for pname in param_list.keys():
        indices=np.arange(np.prod(param_list[pname][1]))+param_list[pname][0]
        inf_bounds=[bounds[i][0] for i in indices]
        sup_bounds=[bounds[i][1] for i in indices]
        bounds_dict[pname]=(np.floor(min(inf_bounds)),np.ceil(max(sup_bounds)))
        correct_bounds[indices]=[bounds_dict[pname]]*len(indices)
    return correct_bounds, bounds_dict    
        


def restoreLSCSM_mgt(fileprefix,resetK=False,update_mp={},update_sp={},data_path=''):
# Use a meta-parameter file to recreate a lcscm object identical to the one used for a previous fit
# (with or whithout setting initial parameters K0 to the final parameter values obtained for this previous fit)
# Return the lscsm object, the initial parameter values and the data (stimuli and responses)
    meta_params, suppl_params = loadParams(fileprefix+'_metaparams')
    ori_mp=meta_params.copy()
    ori_sp=suppl_params.copy()
    suppl_params.update(update_sp)
    meta_params.update(update_mp)
    if len(data_path)>0:
        suppl_params['datafilename']=os.path.join(data_path,os.path.basename(suppl_params['datafilename']))
    training_inputs,training_set,validation_inputs,validation_set = loadData(suppl_params['datafilename'],val_frac=suppl_params['val_frac'],reg_frac=suppl_params['reg_frac'],tau=suppl_params['tau'],n_tau=meta_params['n_tau'],seed_val=suppl_params['stim_seed'],normalize_stim=suppl_params['norm_stim'],ROI=suppl_params['stim_ROI'],ceiling_val=suppl_params['noise_ceiling_val'],stim_types=suppl_params['stim_types'])
    if suppl_params['cell_nums'] in ['all',[]]:
        suppl_params['cell_nums']=range(training_set.shape[1])
    lscsm, K, sp= restoreLSCSM(fileprefix, training_inputs, training_set[:,suppl_params['cell_nums']], update_mp=update_mp, update_sp=update_sp)
    if resetK: final_K = np.array(lscsm.create_random_parametrization(suppl_params['seed']))
    else: #assert len(K)==len(lscsm.create_random_parametrization(suppl_params['seed']))    
        ori_lscsm=LSCSM(training_inputs,training_set[:,ori_sp['cell_nums']],**ori_mp)
        if lscsm.free_params!=ori_lscsm.free_params:
            assert np.all([lscsm.free_params[key][1]==ori_lscsm.free_params[key][1] for key in set(lscsm.free_params.keys()) & set(ori_lscsm.free_params.keys())]) # Works only when number of cells is kept constant
            print "\n/!\ restoreLSCSM_mgt warning: network architecture has been changed by metaparams updating. Updating K accordingly. Unspecified parameters will be chosen randomly.\n"
            final_K=lscsm.create_random_parametrization(suppl_params['seed'])
            final_K=lscsm.setParam(final_K,**{key : ori_lscsm.getParam(K,key) for key in ori_lscsm.free_params.keys()})
        else:
            final_K=K
            
    return lscsm, final_K, suppl_params, training_inputs, training_set[:,suppl_params['cell_nums']], validation_inputs, validation_set[:,suppl_params['cell_nums']]    
    
#def importDataBis(filename,minStimSize,nEp):
#    data=scipy.io.loadmat(filename)
#    (nBins,nUnits,truc)=np.shape(data['spikes'])
#    (nDivX,nDivY,nBins)=np.shape(data['stim1'])
#    multFactor=np.ceil(np.sqrt(minStimSize*1.0/(nDivX*nDivY)))
#    stimSize=nDivX*nDivY*multFactor**2
#    
#    spikes=np.reshape(np.rollaxis(data['spikes'],2,0),(nBins*truc,nUnits))
#    spikes=spikes[:nBins*nEp,:]
#    stims=np.zeros((nBins*nEp,stimSize))
#    for iEp in range(nEp):
#        tempstim=np.repeat(np.repeat(data['stim'+str(iEp+1)],multFactor,0),multFactor,1)
#        stims[iEp*nBins:(iEp+1)*nBins,:]=np.reshape(tempstim,(stimSize,nBins)).T
#    
#    return stims[:100,:], spikes[:100,:], stims[100:200,:], spikes[100:200,:]    