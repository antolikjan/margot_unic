from runLSCSMfit import runLSCSM, setLSCSMparam
from loadandsave import restoreLSCSM_mgt
import numpy as np

templateStim='DN'
fittedStim='SN'
#templateParams=['x_pos','y_pos','center_weight','surround_weight','size_center','size_surround']
templateParams=['hidden_weights','output_weights','hidden_layer_threshold','output_layer_threshold']

prefix={};
lscsm={}
K={}

prefix['SN']='0212RIGHT_M22_DTon_unsorted_workswell_tau2'
prefix['DN']='0212RIGHT_M25_DTon_unsorted_workswell_tau2'
#pfile='params_posthresh'
update_mp={'name': 'nonlgnfixedtoDN'}
numEpochs=100

lscsm['template'], K['template'], _, _, _, _, _ =restoreLSCSM_mgt(prefix[templateStim],resetK=False,datapath=datapath,resultspath=resultspath)

customK0={}
for paramname in templateParams:
    if paramname in ['hidden_weights', 'output_weights']:
        customK0[paramname]=lscsm['template'].getParam(K['template'],paramname).eval()
    else:
        customK0[paramname]=lscsm['template'].getParam(K['template'],paramname)
param_mask=setLSCSMparam(lscsm['template'],np.ones(len(K['template'])),customK0.keys(),np.zeros(len(customK0)))    
update_sp={'param_mask': param_mask, 'customK0': customK0, 'numEpochs':100}    


K['fitted'],lscsm['fitted'],terr,verr,tcorr,vcorr=runLSCSM(prevrunfile=prefix[fittedStim],resetK=True,meta_params=update_mp,suppl_params=update_sp,compCorr=True,visualization=False,datapath=datapath,resultspath=resultspath)