from lscsm.LSCSM import LSCSM
from loadandsave import restoreLSCSM_mgt, loadData, enforce_bounds
from lscsm.fitting import fitLSCSM
import numpy as np
import os
from utils.handlebinaryfiles import loadVec
import param
import sys
from user_specific_paths import data_paths


#def setHSMparam(lscsm,K,param_name,new_value):
#    newK=np.array(K[:])
#    if isinstance(param_name,str):
#        param_name=[param_name]
#        new_value=[new_value]
#    for iparam in range(len(param_name)):    
#        (i,t) = lscsm.free_params[param_name[iparam]]
#        newK[i:i+np.prod(t)]=np.array([new_value[iparam]]).reshape(-1)
#    return list(newK)


def paramdict_to_Kvector(lscsm,param_dict,K=None):
    pnames=sorted(list( set(lscsm.free_params.keys()) & set(param_dict.keys()) ))
    positions=np.array([lscsm.free_params[p][0] for p in pnames])
    shapes=[lscsm.free_params[p][1] for p in pnames]
    if K is None:
        K=np.zeros(lscsm.free_param_count)
        if len(pnames)<len(lscsm.free_params):
            print "paramdict_to_Kvector warning : some parameters haven't been provided and will be set to 0."
    for i_par in range(len(pnames)):
        K[positions[i_par]:positions[i_par]+np.prod(shapes[i_par])]=np.array(param_dict[pnames[i_par]]).flatten() # param_dict[pnames[i_par]]: list, array or scalar
    return K


class HSM_suppl_params(param.Parameterized):
    datafilename=param.Filename(default=None,allow_None=True,search_paths=data_paths)
    epochSize=param.Integer(default=1000)
    norm_stim=param.Boolean(default=True)
    numEpochs=param.Integer(default=1)
    seed=param.Integer(default=0)
    stim_seed=param.Integer(default=0,allow_None=True)
    tau=param.Integer(default=0)
    reg_frac=param.Number(default=0.2)
    val_frac=param.Number(default=0)
    cell_nums=param.List(default=[], doc="List of cells to include (default: all)")   
    pruned=param.Boolean(default=False)
    K_mask=param.Filename(default=None,allow_None=True)
    masked_params=param.List(default=[])
    invert_mask=param.Boolean(default=False)
    update_init_params=param.Dict(default={})
    n_stuck_iter=param.Integer(default=None, allow_None=True)
    n_rep=param.Integer(default=0,doc="Number of already runned iterations (for cases were K is not reset)")
    stim_ROI=param.NumericTuple(default=None,length=4,allow_None=True)#,allow_None=True)
    noise_ceiling_val=param.Number(default=1.,doc="Inverse fraction of training data to keep for fitting")
    stim_types=param.List(default=[],doc="List of stimulus types to use (new data format)")



def runHSM(prevrunfile=None,resetK=False,meta_params={},suppl_params={},compCorr=False):
# Runs the whole LSCSM fitting procedure

    # If meta-parameter file provided: recreate corresponding lscsm object and load data.
    # If a fitting results file (Kfile) is also provided, initial lscsm parameters (K0) will be set to these values
    if prevrunfile is not None:
        lscsm, K0, suppl_params, training_inputs, training_set, validation_inputs, validation_set = restoreLSCSM_mgt(prevrunfile,resetK=resetK,update_mp=meta_params,update_sp=suppl_params)
        results_path=os.path.dirname(prevrunfile)+'/'
    # Else create a lscsm object with default parameters (except the ones overridden by update_mp and update_sp) and load data
    # Initialize K0 to random values 
    else:
        results_path=''
        training_inputs,training_set,validation_inputs,validation_set=loadData(suppl_params['datafilename'],tau=suppl_params['tau'],n_tau=meta_params['n_tau'],reg_frac=suppl_params['reg_frac'],val_frac=suppl_params['val_frac'],seed_val=suppl_params['stim_seed'],normalize_stim=suppl_params['norm_stim'],ROI=suppl_params['stim_ROI'],ceiling_val=suppl_params['noise_ceiling_val'])
        if suppl_params['cell_nums']=='all':
            suppl_params['cell_nums']=range(training_set.shape[1])
        training_set=training_set[:,suppl_params['cell_nums']]  
        validation_set=validation_set[:,suppl_params['cell_nums']]   
        lscsm=LSCSM(training_inputs,training_set[:,suppl_params['cell_nums']],mask=K_mask,**meta_params)
        if not(meta_params.has_key('name')): lscsm.name=''
        K0=lscsm.create_random_parametrization(suppl_params['seed']) # meta_params['seed'] before, but doesnt exist, does it ?!?
    
#    if suppl_params.has_key('customK0') and bool(suppl_params['customK0']):
#        K0=setLSCSMparam(lscsm,K0,suppl_params['customK0'].keys(),suppl_params['customK0'].values())
    if suppl_params.has_key('K_mask') and bool(suppl_params['K_mask']):
        K_mask=loadVec(suppl_params['K_mask'],fmt='?',keep_fmt=True)
    else:
        K_mask=np.ones(len(K0)).astype(bool)  
    if suppl_params.has_key('masked_params') and len(suppl_params['masked_params'])>0:
        mask_dict={key : np.zeros(lscsm.getParam(K0,key).shape) for key in suppl_params['masked_params']}
        K_mask=paramdict_to_Kvector(lscsm,mask_dict,K_mask).astype(bool)
    if suppl_params.has_key('invert_mask') and suppl_params['invert_mask']:
        K_mask=~K_mask
    if np.all(K_mask):
        K_mask=None
        
    K0=np.array(K0)  
    if suppl_params.has_key('update_init_params') and len(suppl_params['update_init_params'])>0:
        K0=paramdict_to_Kvector(lscsm,suppl_params['update_init_params'],K0)
    if (K_mask is not None) and suppl_params.has_key('pruned') and suppl_params['pruned']:
        K0[~K_mask]=0
        K0=enforce_bounds(K0,lscsm.bounds)[0]  
        
    suppl_params['datafilename']=os.path.basename(suppl_params['datafilename'])

    # If no previous fit results have been provided, reset number of repetitions (of optimization process) to 0
    if (prevrunfile is None) | resetK: suppl_params['n_rep']=0 
    if suppl_params.has_key('stim_types') and suppl_params['stim_types']:
        prefix = '_' + ','.join(suppl_params['stim_types']) + '_'
    else :
        prefix = '_'
    prefix = results_path + suppl_params['datafilename'].split('.')[0] + prefix + lscsm.name
    
    # Do the optimization    
    K,terr,verr,tcorr,vcorr,error_msg=fitLSCSM(lscsm,K0,np.mat(training_inputs),np.mat(training_set),np.mat(validation_inputs),np.mat(validation_set),fit_params=suppl_params,checkpointName=prefix,compCorr=compCorr,K_mask=K_mask)
    lscsm.X.set_value([[]]) # To free GPU memory
    lscsm.Y.set_value([[]])
    return K, lscsm, terr, verr, tcorr, vcorr,error_msg
    
   
   
if __name__=='__main__':
    if len(sys.argv)==1:
        raise ValueError('Program needs at least one command line input')
    else:    
        results=runHSM(*sys.argv[1:])