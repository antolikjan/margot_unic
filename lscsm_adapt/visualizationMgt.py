import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from channel_order import restore_recsite_order
import scipy.stats
from lscsm.visualization import computeCorr
from utils.various_tools import downsample
from utils.volterra_kernels import STA_LR_3D
from theano import function as theano_function
from theano.tensor import matrix as Tmatrix
from copy import deepcopy
from utils.various_tools import clean_plot
from matplotlib.gridspec import GridSpec
# Plotting functions for the LSCSM model, and function to compute correlation


def onclick(event,fig,max_ncells,offset,hid_axes,alphas_out,colors_out,lgn_axes,alphas_hid,colors_hid,plotsize,keep_previous,line_width=3):
# Function used by plotLSCSM to handle mouse clicks on the LSCSM plot   
    if event.inaxes is not None:
        plotnum=event.inaxes.get_subplotspec().num1
        col=['b','r']
        
        if plotnum<=3*max_ncells:
            if plotnum<=max_ncells:
                iUp=plotnum-offset[2]
                dw_axes=hid_axes
                alphas=alphas_out
                colors=colors_out
                up_horiz_pos=0
            else:
                iUp=plotnum-2*max_ncells-offset[1]
                dw_axes=lgn_axes
                alphas=alphas_hid
                colors=colors_hid
                up_horiz_pos=plotsize[0]
            
            if keep_previous:
                lines=fig.lines
            else:    
                lines=[]               
            transFigure = fig.transFigure.inverted()
            upCoord = transFigure.transform(event.inaxes.transData.transform([plotsize[1]/2,up_horiz_pos]))    
            for iDw in range(len(dw_axes)):
                dwCoord = transFigure.transform(dw_axes[iDw].transData.transform([plotsize[1]/2,0]))        
                lines.append(Line2D((upCoord[0],dwCoord[0]),(upCoord[1],dwCoord[1]),transform=fig.transFigure, alpha=alphas[iDw,iUp], color=col[colors[iDw,iUp]], linewidth=alphas[iDw,iUp]*line_width))
            fig.lines = lines
            event.canvas.draw()
            
    else:
        fig.lines=[]
        event.canvas.draw()            

            
#def onclick(event,fig,max_ncells,offset,hid_axes,alphas_out,colors_out,lgn_axes,alphas_hid,colors_hid,plotsize):
#    if event.inaxes is not None:
#        plotnum=event.inaxes.get_subplotspec().num1
#        col=['b','r']
#        
#        if plotnum<=3*max_ncells:
#            if plotnum<=max_ncells:
#                iUp=plotnum-offset[2]
#                dw_axes=hid_axes
#                alphas=alphas_out
#                colors=colors_out
#            else:
#                iUp=plotnum-2*max_ncells-offset[1]
#                dw_axes=lgn_axes
#                alphas=alphas_hid
#                colors=colors_hid
#    
#            lines=[]               
#            transFigure = fig.transFigure.inverted()
#            upCoord = transFigure.transform(event.inaxes.transData.transform([plotsize/2,0]))    
#            for iDw in range(len(dw_axes)):
#                dwCoord = transFigure.transform(dw_axes[iDw].transData.transform([plotsize/2,plotsize]))        
#                lines.append(Line2D((upCoord[0],dwCoord[0]),(upCoord[1],dwCoord[1]),transform=fig.transFigure, alpha=alphas[iDw,iUp], color=col[colors[iDw,iUp]], linewidth=alphas[iDw,iUp]*3))
#            fig.lines = lines
#            event.canvas.draw()            


def plotLSCSM_network_OLD(lscsm,K,inputsForSTA=None,inputsForCorr=None,respForCorr=None,subSet=None,hidSubSet=None,lgnSubSet=None,restoreChOrder=False,subRegion=None,plot_cleaning=['ticks'],keep_previous=False,normalize_RFs={'LGN':False,'hidden':False,'out':False},scale_by_cell=False,downsample_STA=1,line_width=3,temporal_lgn=False):
# KERNEL TEMPOREL A REPRENDRE
# Draw the structure of a given LSCSM model: RFs for each layer, weights between layers    
    # A faire: gestion des echelles pour le plot des CR, normaliser les CR LGN?
    # Et verifier que les histoires de subsets sont bien prises en compte partout
    K=np.array(K)    
    n_neurons=lscsm.num_neurons
    n_lgn=lscsm.num_lgn
    n_hidden=lscsm.num_hidden
    if subSet is None:
        subSet=np.arange(n_neurons)
    else:
        subSet=np.array(subSet)
    n_toplot=len(subSet)
    if restoreChOrder: 
        cellorder=restore_recsite_order(np.arange(n_neurons))[subSet]
    else:
        cellorder=subSet
        
    if hidSubSet is None:
        hidSubSet=np.arange(n_hidden)
    else:
        hidSubSet=np.array(hidSubSet)
        
    if lgnSubSet is None:
        lgnSubSet=np.arange(n_lgn)
    else:
        lgnSubSet=np.array(lgnSubSet)    
        
    if (subRegion is None) or (len(subRegion)==0):
        subRegion=[0,lscsm.size,0,lscsm.size]
    
    x=lscsm.getParam(K,'x_pos')[lgnSubSet]
    y=lscsm.getParam(K,'y_pos')[lgnSubSet]
    sc=lscsm.getParam(K,'size_center')[lgnSubSet]
    ss=lscsm.getParam(K,'size_surround')[lgnSubSet]
    hidden_w = lscsm.getParam(K,"hidden_weights").eval()[lgnSubSet[:,None],hidSubSet]
    #hidden_w=concatenate((diag(range(10)),ones((10,10))*5),axis=1)
    output_w = lscsm.getParam(K,"output_weights").eval()[hidSubSet[:,None],cellorder]
    #output_w=concatenate((diag(range(17)),ones((3,17))*5),axis=0)
    if not lscsm.balanced_LGN:
        rc=lscsm.getParam(K,'center_weight')[lgnSubSet]
        rs=lscsm.getParam(K,'surround_weight')[lgnSubSet]
    
    
    lgn_rf=np.zeros((len(lgnSubSet),lscsm.size**2))
    xx = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).T.flatten()   
    yy = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).flatten()
    
    if lscsm.balanced_LGN:
        for iCell in range(len(lgnSubSet)):
            lgn_rf[iCell,:] = (np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/sc[iCell])/ (2*sc[iCell]*np.pi) - np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/ss[iCell])/ (2*ss[iCell]*np.pi)).copy()
    else:
        for iCell in range(len(lgnSubSet)):
            lgn_rf[iCell,:] = (rc[iCell]*(np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/sc[iCell]).T/ (2*sc[iCell]*np.pi)) - rs[iCell]*(np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/(sc[iCell]+ss[iCell])).T/ (2*(sc[iCell]+ss[iCell])*np.pi))).copy()
    
    if normalize_RFs['LGN']:
        norm_factor=np.sqrt((lgn_rf**2).mean(axis=1)).reshape(-1,1)
        lgn_rf=lgn_rf*1./norm_factor
        hidden_w=hidden_w*norm_factor  
        
    hidden_rf=np.dot(lgn_rf.T,hidden_w)
    if normalize_RFs['hidden']:
        hidden_rf=hidden_rf*1./np.sqrt((hidden_rf**2).mean(axis=1)).reshape(-1,1)
        
    if temporal_lgn and lscsm.n_tau>1:
        time=np.arange(0,lscsm.n_tau-0.9,0.1)
        lgn_temporal_rf=np.zeros((len(lgnSubSet),len(time)))        
        if lscsm.temporal_kernel=='GammaDiff':
            lgn_t={}
            for p in ['LGN_temporal_K','LGN_temporal_c1','LGN_temporal_c2','LGN_temporal_t1','LGN_temporal_t2','LGN_temporal_n1','LGN_temporal_n2']:
                lgn_t[p]=lscsm.getParam(K,'LGN_temporal_'+p)[lgnSubSet]
            gamma_func=lambda K,c,t,n,time: K * ((c*(time-t))**n) * np.exp(-c*(time-t)) * 1./((n**n)*np.exp(-n))
            for iCell in range(len(lgnSubSet)):
                lgn_temporal_rf[iCell]=gamma_func(lgn_t['LGN_temporal_K'][iCell],lgn_t['LGN_temporal_c1'][iCell],lgn_t['LGN_temporal_t1'][iCell],lgn_t['LGN_temporal_n1'][iCell],time)-gamma_func(1,lgn_t['LGN_temporal_c2'][iCell],lgn_t['LGN_temporal_t2'][iCell],lgn_t['LGN_temporal_n2'][iCell],time)
   
    if not(inputsForSTA in [None,[]]):
        pred_respT=lscsm.response(inputsForSTA,K)-lscsm.response(np.zeros((1,lscsm.size**2),dtype='float32'),K)
        inputsForSTA=inputsForSTA.reshape(-1,lscsm.size,lscsm.size)[:,subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]]
        if downsample_STA>1:
            inputsForSTA=downsample(downsample(inputsForSTA,2,downsample_STA),1,downsample_STA)
        STAshape=inputsForSTA.shape
        inputsForSTA=inputsForSTA.reshape(STAshape[0],-1)
        modelSTA=np.array(STA_LR(np.mat(inputsForSTA),np.mat(pred_respT),0.0001))
        if normalize_RFs['out']:
            modelSTA=modelSTA*1./np.sqrt((modelSTA**2).mean(axis=1)).reshape(-1,1)
        #STA_max=abs(modelSTA).max()/2 * np.ones((1,n_neurons))
        STA_max=abs(modelSTA).max()/2 * np.ones(n_neurons)        
        #STA_max=abs(modelSTA).max(axis=0)
    else:
        modelSTA=None
        
    if (not(inputsForCorr in [None,[]])) & (not(respForCorr in [None,[]])):
        pred_respV=lscsm.response(inputsForCorr,K)
        corr=computeCorr(pred_respV,respForCorr)
            
    
    max_ncells=max([len(lgnSubSet),len(hidSubSet),n_toplot])
    offset=(max_ncells-np.array([len(lgnSubSet),len(hidSubSet),n_toplot]))/2
    if scale_by_cell:
        lgn_max=abs(lgn_rf).max(axis=0)
        hid_max=abs(hidden_rf).max(axis=0)
    else:
        lgn_max=np.ones(n_lgn)*abs(lgn_rf).max()
        hid_max=np.ones(n_hidden)*abs(hidden_rf).max()
    lgn_axes=[]
    hid_axes=[]    
    n_lines=5
    
    if temporal_lgn and lscsm.n_tau>1:
        n_lines=6
        lgn_temporal_axes=[]
        lgn_t_bounds=[lgn_temporal_rf.min(),lgn_temporal_rf.max()]
    fig=plt.figure(figsize=(max_ncells*2,n_lines*2))
    for iCell in range(len(lgnSubSet)):
        lgn_axes.append(plt.subplot(n_lines,max_ncells,4*max_ncells+offset[0]+iCell+1))
        #plt.pcolor(-np.reshape(lgn_rf[iCell,:],(lscsm.size,lscsm.size))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-lgn_max,vmax=lgn_max,cmap='RdBu')
        plt.imshow(-np.reshape(lgn_rf[iCell,:],(lscsm.size,lscsm.size))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-lgn_max[iCell],vmax=lgn_max[iCell],cmap='RdBu',interpolation='nearest')
#        plt.axis('off')
#        lgn_axes[-1].set_aspect('equal')
        clean_plot(lgn_axes[-1],to_delete=plot_cleaning)
        
        if temporal_lgn and lscsm.n_tau>1:
            lgn_temporal_axes.append(plt.subplot(n_lines,max_ncells,5*max_ncells+offset[0]+iCell+1))
            plt.plot(time,lgn_temporal_rf[iCell],'k',linewidth=2)
            plt.axis([0,lscsm.n_tau]+lgn_t_bounds)
            clean_plot(lgn_temporal_axes[-1],to_delete=plot_cleaning,square=False)
            
     
    for iCell in range(len(hidSubSet)):
        hid_axes.append(plt.subplot(5,max_ncells,2*max_ncells+offset[1]+iCell+1))
        #plt.pcolor(-np.reshape(hidden_rf[:,iCell],(lscsm.size,lscsm.size))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-hid_max,vmax=hid_max,cmap='RdBu')
        plt.imshow(-np.reshape(hidden_rf[:,iCell],(lscsm.size,lscsm.size))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-hid_max[iCell],vmax=hid_max[iCell],cmap='RdBu',interpolation='nearest')
#        plt.axis('off')
#        hid_axes[-1].set_aspect('equal')
        clean_plot(hid_axes[-1],to_delete=plot_cleaning)
    
    
    for iCell in range(n_toplot):
        
        ax=plt.subplot(5,max_ncells,offset[2]+iCell+1)
        if inputsForSTA in [None,[]]:
            circle=plt.Circle((lscsm.size/2,lscsm.size/2),lscsm.size/2-1,color='k',fill=False,linewidth=2)
            ax.add_artist(circle)
            plt.axis([0,lscsm.size,0,lscsm.size])
        else:
            #plt.pcolor(-np.reshape(modelSTA[:,iCell],(lscsm.size,lscsm.size)),vmin=-STA_max,vmax=STA_max,cmap='RdBu')
            #plt.pcolor(-np.reshape(modelSTA[:,cellorder[iCell]],STAshape[1:]),vmin=-STA_max[cellorder[iCell]],vmax=STA_max[cellorder[iCell]],cmap='RdBu')
            plt.imshow(-np.reshape(modelSTA[:,cellorder[iCell]],STAshape[1:]),vmin=-STA_max[cellorder[iCell]],vmax=STA_max[cellorder[iCell]],cmap='RdBu',interpolation='nearest')
            #plt.pcolor(-np.reshape(modelSTA[:,cellorder[iCell]],(lscsm.size,lscsm.size)),vmin=-STA_max,vmax=STA_max,cmap='RdBu')
#        plt.axis('off')
#        ax.set_aspect('equal')
        clean_plot(ax,to_delete=plot_cleaning)
        if (not(inputsForCorr in [None,[]])) & (not(respForCorr in [None,[]])):
            #ax.set_title(str(iCell+1)+': c='+str(corr[iCell])[:6],{'fontsize':8})
            #ax.set_title(str(int(cellorder[iCell]+1))+': c='+str(corr[cellorder[iCell]])[:6],{'fontsize':8})
            ax.set_title(str(int(subSet[iCell]+1))+': c='+str(corr[cellorder[iCell]])[:6],{'fontsize':8})
        else:
            #ax.set_title(str(iCell+1))
            #ax.set_title(str(int(cellorder[iCell]+1)))
            ax.set_title(str(int(subSet[iCell]+1)))
       
    fig.tight_layout()
    #fig.subplots_adjust(wspace=0.8,hspace=0.8)   
    if restoreChOrder:
        output_w=restore_recsite_order(output_w)
    alphas_hid=abs(hidden_w)*1./abs(hidden_w).max()
    alphas_out=abs(output_w)*1./abs(output_w).max()
    colors_hid=hidden_w.copy(); colors_hid[colors_hid>=0]=1; colors_hid[colors_hid<0]=0; colors_hid=colors_hid.astype(int)
    colors_out=output_w.copy(); colors_out[colors_out>=0]=1; colors_out[colors_out<0]=0; colors_out=colors_out.astype(int)

    cid = fig.canvas.mpl_connect('button_press_event',lambda event: onclick(event,fig,max_ncells,offset,hid_axes,alphas_out,colors_out,lgn_axes,alphas_hid,colors_hid,[subRegion[1]-subRegion[0],subRegion[3]-subRegion[2]],keep_previous,line_width))

    return lgn_rf, hidden_rf, modelSTA
 
 
def plotLSCSM_network(pm,subSet=None,hidSubSet=None,lgnSubSet=None,subRegion=None,plot_cleaning=['ticks'],keep_previous=False,normalize_RFs={'LGN':False,'hidden':False,'out':False},scale_by_cell=False,line_width=3,temporal_lgn=False,inputsForSTA=None,inputsForCorr=None,respForCorr=None,binary_mode=False):
# KERNEL TEMPOREL A REPRENDRE
# Draw the structure of a given LSCSM model: RFs for each layer, weights between layers    
    # A faire: gestion des echelles pour le plot des CR, normaliser les CR LGN?
    # Et verifier que les histoires de subsets sont bien prises en compte partout
    pm=deepcopy(pm)

    if subSet is None:
        subSet=np.arange(pm['n_neurons'])
    else:
        subSet=np.array(subSet)
    n_toplot=len(subSet)
        
    if hidSubSet is None:
        hidSubSet=np.arange(pm['n_hidden'])
    else:
        hidSubSet=np.array(hidSubSet)
        
    if lgnSubSet is None:
        lgnSubSet=np.arange(pm['n_lgn'])
    else:
        lgnSubSet=np.array(lgnSubSet)    
        
    if (subRegion is None) or (len(subRegion)==0):
        subRegion=[0,pm['size'],0,pm['size']]
    
    if normalize_RFs['LGN']:
        norm_factor=np.sqrt((pm['lgn_spatial_rf']**2).mean(axis=0)).reshape(-1,1)
        pm['lgn_spatial_rf']=pm['lgn_spatial_rf']*1./norm_factor
        pm['hidden_weights']=pm['hidden_weights']*norm_factor  
        
    if normalize_RFs['hidden']:
        pm['hidden_spatial_rf']=pm['hidden_spatial_rf']*1./np.sqrt((pm['hidden_spatial_rf']**2).mean(axis=0)).reshape(-1,1)
   
    if pm['model_STA'] is not None:
       if normalize_RFs['out']:
            pm['model_spatial_STA']=pm['model_spatial_STA']*1./np.sqrt((pm['model_spatial_STA']**2).mean(axis=0)).reshape(-1,1) 
       STA_max=abs(pm['model_spatial_STA']).max()/2 * np.ones(pm['n_neurons'])        
        #STA_max=abs(pm['model_spatial_STA']).max(axis=0)
          
          
    max_ncells=max([len(lgnSubSet),len(hidSubSet),n_toplot])
    offset=(max_ncells-np.array([len(lgnSubSet),len(hidSubSet),n_toplot]))/2
    if scale_by_cell:
        lgn_max=abs(pm['lgn_spatial_rf']).max(axis=0)
        hid_max=abs(pm['hidden_spatial_rf']).max(axis=0)
    else:
        lgn_max=np.ones(pm['n_lgn'])*abs(pm['lgn_spatial_rf']).max()
        hid_max=np.ones(pm['n_hidden'])*abs(pm['hidden_spatial_rf']).max()
    lgn_axes=[]
    hid_axes=[]    
    n_lines=5
    
    if temporal_lgn and pm['n_tau']>1:
        n_lines=6
        lgn_temporal_axes=[]
        lgn_t_bounds=[pm['lgn_temporal_rf'].min(),pm['lgn_temporal_rf'].max()]
    fig=plt.figure(figsize=(max_ncells*2,n_lines*2))
    for iCell in range(len(lgnSubSet)):
        lgn_axes.append(plt.subplot(n_lines,max_ncells,4*max_ncells+offset[0]+iCell+1))
        plt.imshow(-np.reshape(pm['lgn_spatial_rf'][:,iCell],(pm['size'],pm['size']))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-lgn_max[iCell],vmax=lgn_max[iCell],cmap='RdBu',interpolation='nearest')
        clean_plot(lgn_axes[-1],to_delete=plot_cleaning)
        
        if temporal_lgn and pm['n_tau']>1:
            lgn_temporal_axes.append(plt.subplot(n_lines,max_ncells,5*max_ncells+offset[0]+iCell+1))
            plt.plot(pm['precise_rf_time'],pm['lgn_temporal_rf'][:,iCell],'k',linewidth=2)
            plt.axis([0,pm['n_tau']]+lgn_t_bounds)
            clean_plot(lgn_temporal_axes[-1],to_delete=plot_cleaning,square=False)
            
     
    for iCell in range(len(hidSubSet)):
        hid_axes.append(plt.subplot(5,max_ncells,2*max_ncells+offset[1]+iCell+1))
        plt.imshow(-np.reshape(pm['hidden_spatial_rf'][:,iCell],(pm['size'],pm['size']))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-hid_max[iCell],vmax=hid_max[iCell],cmap='RdBu',interpolation='nearest')
        clean_plot(hid_axes[-1],to_delete=plot_cleaning)
    
    
    for iCell in range(n_toplot):
        
        ax=plt.subplot(5,max_ncells,offset[2]+iCell+1)
        if inputsForSTA in [None,[]]:
            circle=plt.Circle((pm['size']/2,pm['size']/2),pm['size']/2-1,color='k',fill=False,linewidth=2)
            ax.add_artist(circle)
            plt.axis([0,pm['size'],0,pm['size']])
        else:
            plt.imshow(-np.reshape(modelSTA[:,cellorder[iCell]],STAshape[1:]),vmin=-STA_max[cellorder[iCell]],vmax=STA_max[cellorder[iCell]],cmap='RdBu',interpolation='nearest')
        clean_plot(ax,to_delete=plot_cleaning)
        if (not(inputsForCorr in [None,[]])) & (not(respForCorr in [None,[]])):
            ax.set_title(str(int(subSet[iCell]+1))+': c='+str(corr[cellorder[iCell]])[:6],{'fontsize':8})
        else:
            ax.set_title(str(int(subSet[iCell]+1)))
       
    #fig.tight_layout()
    #fig.subplots_adjust(wspace=0.8,hspace=0.8)   
    if binary_mode:
        for wtype in ['hidden_weights','output_weights']:
            pm[wtype][pm[wtype]>0]=1
            pm[wtype][pm[wtype]<0]=-1
    alphas_hid=abs(pm['hidden_weights'])*1./abs(pm['hidden_weights']).max()
    alphas_out=abs(pm['output_weights'])*1./abs(pm['output_weights']).max()
    colors_hid=pm['hidden_weights'].copy(); colors_hid[colors_hid>=0]=1; colors_hid[colors_hid<0]=0; colors_hid=colors_hid.astype(int)
    colors_out=pm['output_weights'].copy(); colors_out[colors_out>=0]=1; colors_out[colors_out<0]=0; colors_out=colors_out.astype(int)

    cid = fig.canvas.mpl_connect('button_press_event',lambda event: onclick(event,fig,max_ncells,offset,hid_axes,alphas_out,colors_out,lgn_axes,alphas_hid,colors_hid,[subRegion[1]-subRegion[0],subRegion[3]-subRegion[2]],keep_previous,line_width))

    return 
    
    
def plotLSCSM_params(pm,subRegion=None,same_scale=False,normalize_lgn=False,n_col=5,plot_cleaning=['ticks'],temporal_lgn=True,temporal_hidden=False,weights=False):
    if temporal_hidden:
        n_col=max(n_col,pm['n_tau'])
    
    if (subRegion is None) or (len(subRegion)==0):
        subRegion=[0,pm['size'],0,pm['size']] 
        
    if normalize_lgn:
        norm_factor=np.sqrt((pm['lgn_spatial_rf']**2).mean(axis=0)).reshape(-1,1)
        pm['lgn_spatial_rf']=pm['lgn_spatial_rf']*1./norm_factor
        pm['hidden_weights']=pm['hidden_weights']*norm_factor  
    
    if temporal_hidden and pm['n_tau']>1:
        hidden_tokeep='hidden_rf'
    else:
        hidden_tokeep='hidden_spatial_rf'
    
    lgn_max=abs(pm['lgn_spatial_rf']).max()
    hid_max=abs(pm[hidden_tokeep]).max()
    lgn_axes=[]
    hid_axes=[]
    n_lgn_rows=int(np.ceil(pm['n_lgn']*1./n_col))
    if temporal_hidden and pm['n_tau']>1:
        n_hidden_rows=pm['n_hidden']
    else:    
        n_hidden_rows=int(np.ceil(pm['n_hidden']*1./n_col))
    if temporal_lgn and pm['n_tau']>1:
        n_lgn_rows=n_lgn_rows*2
    fig=plt.figure(figsize=(n_col*2,(n_lgn_rows+n_hidden_rows+5)*2))
    fig_grid=GridSpec(n_lgn_rows+n_hidden_rows+2+(2 if weights else 0),n_col)
    
    for iCell in range(pm['n_lgn']):
#        if temporal_lgn and pm['n_tau']>1:
#            pos=int(iCell/n_col)*2*n_col+n_col+iCell%n_col
#        else:
#            pos=iCell
#        lgn_axes.append(plt.subplot(n_lgn_rows+n_hidden_rows+2,n_col,pos+1))
        if temporal_lgn and pm['n_tau']>1:
            row=int(iCell/n_col)*2+1
        else:
            row=int(iCell/n_col)
        lgn_axes.append(plt.subplot(fig_grid[row,iCell%n_col]))        
        if not(same_scale):
            lgn_max=np.abs(np.reshape(pm['lgn_spatial_rf'][:,iCell],(pm['size'],pm['size']))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]]).max()
            
        plt.imshow(-np.reshape(pm['lgn_spatial_rf'][:,iCell],(pm['size'],pm['size']))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-lgn_max,vmax=lgn_max,cmap='RdBu',interpolation='nearest')
        clean_plot(lgn_axes[-1],to_delete=plot_cleaning)
        if temporal_lgn and pm['n_tau']>1:
            if not(same_scale):
                temporal_rf_max=np.abs(pm['lgn_temporal_rf'][:,iCell]).max()
            #lgn_axes.append(plt.subplot(n_lgn_rows+n_hidden_rows+2,n_col,int(iCell/n_col)*2*n_col+iCell%n_col+1))
            lgn_axes.append(plt.subplot(fig_grid[int(iCell/n_col)*2,iCell%n_col]))
            plt.plot(pm['precise_rf_time'],pm['lgn_temporal_rf'][:,iCell],'k',linewidth=2)
            plt.axis([-0.1,pm['n_tau']+0.1]+[-temporal_rf_max*1.1,temporal_rf_max*1.1])
            clean_plot(lgn_axes[-1],to_delete=plot_cleaning,square=False)
     
    for iCell in range(pm['n_hidden']):
        if temporal_hidden:
            if not(same_scale):
                hid_max=np.abs(np.reshape(pm['hidden_rf'][:,iCell],(pm['n_tau'],pm['size'],pm['size']))[:,subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]]).max()
            reshaped_rf=pm['hidden_rf'][:,iCell].reshape(pm['n_tau'],pm['size'],pm['size'])[:,subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]]    
            for tau in range(pm['n_tau']):
                #hid_axes.append(plt.subplot(n_lgn_rows+n_hidden_rows+2,n_col,(n_lgn_rows+1)*n_col+iCell*n_col+tau+1))
                hid_axes.append(plt.subplot(fig_grid[n_lgn_rows+1+iCell,tau]))
                plt.imshow(-reshaped_rf[tau],vmin=-hid_max,vmax=hid_max,cmap='RdBu',interpolation='nearest')
                clean_plot(hid_axes[-1],to_delete=plot_cleaning)
        else:    
            #hid_axes.append(plt.subplot(n_lgn_rows+n_hidden_rows+2,n_col,(n_lgn_rows+1)*n_col+iCell+1))
            hid_axes.append(plt.subplot(fig_grid[n_lgn_rows+1+int(iCell/n_col),iCell%n_col]))
            if not(same_scale):
                hid_max=np.abs(np.reshape(pm['hidden_spatial_rf'][:,iCell],(pm['size'],pm['size']))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]]).max()
            plt.imshow(-np.reshape(pm['hidden_spatial_rf'][:,iCell],(pm['size'],pm['size']))[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]],vmin=-hid_max,vmax=hid_max,cmap='RdBu',interpolation='nearest')
            clean_plot(hid_axes[-1],to_delete=plot_cleaning)
    
    if weights:
        w_max=np.abs(pm['output_weights']).max()
        ax=plt.subplot(fig_grid[n_lgn_rows+n_hidden_rows+2,:n_col])
        plt.imshow(-pm['output_weights'],vmin=-w_max,vmax=w_max,cmap='RdBu',interpolation='nearest',aspect='auto')
        clean_plot(ax,to_delete=plot_cleaning,square=False)
        ax=plt.subplot(fig_grid[n_lgn_rows+n_hidden_rows+3,:n_col])
        plt.hist(pm['output_weights'].flatten(),bins=30,range=[-w_max,w_max],color='k')
        clean_plot(ax,to_delete=plot_cleaning,square=False)
        
    fig.tight_layout()

    return    
 

def retrieve_LSCSM_params(lscsm,Kvec,inputsForSTA=None):

    theano_mat=Tmatrix(dtype='float64')
    Kvec=np.array(Kvec) 
    bounds={key : lscsm.getParam(lscsm.bounds,key) for key in lscsm.free_params.keys()}
    pm={key : lscsm.getParam(Kvec,key) for key in lscsm.free_params.keys()}
    pm['n_neurons']=lscsm.num_neurons
    pm['n_lgn']=lscsm.num_lgn
    pm['n_hidden']=lscsm.num_hidden
    pm['size']=lscsm.size
    pm['n_tau']=lscsm.n_tau

#    for pname in lscsm.free_params.keys():
#        if isinstance(lscsm.free_params[pname][1],tuple):
#            pm[pname]=lscsm.getParam(Kvec,pname).eval()
#        else:  
#            pm[pname]=lscsm.getParam(Kvec,pname)
    
    pm['v1_outfunc'] = theano_function(inputs=[theano_mat], outputs=lscsm.construct_of(theano_mat,lscsm.v1of))
    if lscsm.balanced_LGN:
        pm['center_weight']=pm['surround_weight']=np.ones(pm['n_lgn'])
    if lscsm.n_tau==1:    
        pm['LGN_temporal_K']=np.ones(pm['n_lgn'])*2    
        for pname in ['LGN_temporal_c1','LGN_temporal_c2','LGN_temporal_t1','LGN_temporal_t2','LGN_temporal_n1','LGN_temporal_n2']:
            pm[pname]=np.zeros(pm['n_lgn'])
    if lscsm.LGN_threshold: 
        pm['lgn_outfunc'] = theano_function(inputs=[theano_mat], outputs=lscsm.construct_of(theano_mat,lscsm.lgnof))
       
    pm['lgn_rf']=np.zeros((lscsm.n_tau*lscsm.size**2,pm['n_lgn']))
    pm['lgn_spatial_rf']=np.zeros((lscsm.size**2,pm['n_lgn']))
    if lscsm.temporal_kernel=='GammaDiff':
        pm['precise_rf_time']=np.arange(0,lscsm.n_tau-0.9,0.1)
        pm['lgn_temporal_rf']=np.zeros([len(pm['precise_rf_time']),pm['n_lgn']])
    elif lscsm.temporal_kernel=='Custom':
        pm['precise_rf_time']=np.arange(lscsm.n_tau)
        pm['lgn_temporal_rf']=pm['LGN_temporal_kernel'].T
        
    tt = np.repeat([np.arange(0,lscsm.n_tau,1)],lscsm.size**2,axis=0).T.flatten()
    #xx = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).T.flatten()
    xx = np.repeat([np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).T],lscsm.n_tau,axis=0).flatten()
    #yy = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).flatten()
    yy = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size*lscsm.n_tau,axis=0).flatten()   
    
    lgn_spatial_kernel = lambda i,x,y,sc,ss,rc,rs,xpix,ypix: rc[i]*(np.exp(-((xpix - x[i])**2 + (ypix - y[i])**2)/2/sc[i]).T/ (2*sc[i]*np.pi)) - rs[i]*(np.exp(-((xpix - x[i])**2 + (ypix - y[i])**2)/2/(sc[i]+ss[i])).T/ (2*(sc[i]+ss[i])*np.pi))
    if lscsm.temporal_kernel=='GammaDiff':
        gamma_func=lambda K,c,t,n,time: K * ((c*(time-t))**n) * np.exp(-c*(time-t)) * 1./((n**n)*np.exp(-n))
        lgn_temporal_kernel = lambda i,K,c1,c2,t1,t2,n1,n2,time: gamma_func(K[i],c1[i],t1[i],n1[i],time)-gamma_func(1,c2[i],t2[i],n2[i],time)
        
    for iCell in range(pm['n_lgn']):  
        spatial_rf = lgn_spatial_kernel(iCell,pm['x_pos'],pm['y_pos'],pm['size_center'],pm['size_surround'],pm['center_weight'],pm['surround_weight'],xx,yy)            
        if lscsm.temporal_kernel=='GammaDiff':
            pm['lgn_rf'][:,iCell] = lgn_temporal_kernel(iCell,pm['LGN_temporal_K'],pm['LGN_temporal_c1'],pm['LGN_temporal_c2'],pm['LGN_temporal_t1'],pm['LGN_temporal_t2'],pm['LGN_temporal_n1'],pm['LGN_temporal_n2'],tt)*spatial_rf
            pm['lgn_temporal_rf'][:,iCell]= lgn_temporal_kernel(iCell,pm['LGN_temporal_K'],pm['LGN_temporal_c1'],pm['LGN_temporal_c2'],pm['LGN_temporal_t1'],pm['LGN_temporal_t2'],pm['LGN_temporal_n1'],pm['LGN_temporal_n2'],pm['precise_rf_time'])
        elif lscsm.temporal_kernel=='Custom':
            pm['lgn_rf'][:,iCell] = pm['lgn_temporal_rf'][tt.astype(int),iCell]*spatial_rf
        pm['lgn_spatial_rf'][:,iCell] = spatial_rf.reshape(lscsm.n_tau,-1)[0]
                
    pm['hidden_rf']=np.dot(pm['lgn_rf'],pm['hidden_weights'])
    reshaped_rf=pm['hidden_rf'].reshape(pm['n_tau'],-1,pm['n_hidden'])
    pm['hidden_max_tau']=(reshaped_rf**2).mean(axis=1).argmax(axis=0)
    pm['hidden_spatial_rf']=np.array([reshaped_rf[pm['hidden_max_tau'][icell],:,icell] for icell in range(pm['n_hidden'])]).T
   
    if inputsForSTA is not None:
        pred_resp=lscsm.response(inputsForSTA,Kvec)-lscsm.response(np.zeros((1,lscsm.size**2),dtype='float32'),Kvec)
        pm['model_STA']=STA_LR_3D(inputsForSTA,pred_resp,[pm['n_tau'],lscsm.size,lscsm.size],0.0001)
        reshaped_sta=pm['model_STA'].reshape(pm['n_tau'],-1,pm['n_neurons'])
        pm['STA_max_tau']=(reshaped_sta**2).mean(axis=1).argmax(axis=0)
        pm['model_spatial_STA']=np.array([reshaped_sta[pm['STA_max_tau'][icell],:,icell] for icell in range(pm['n_neurons'])]).T
    else:
        pm['model_STA']=None

    return pm, bounds 
    
    
def plotWeights(pm,plotrange=None,nbins=10,yaxis=None,savename=None):
# Do a colorplot and a histogram of synaptic weight values       
    
    if plotrange is None:    
        max_h=np.max([np.abs(pm['hidden_weights']).max(),np.abs(pm['output_weights']).max()])   
        plotrange=[-max_h,max_h]

    figs=[]    
    figs.append(plt.figure())
    plt.imshow(pm['hidden_weights']!=0,vmin=0,vmax=1,cmap='gray',interpolation='nearest')
    ax=plt.imshow(-pm['hidden_weights'],vmin=-plotrange[1],vmax=-plotrange[0],cmap='RdBu',interpolation='nearest',alpha=0.7)
    
    #plt.axis('equal')
    plt.axis('tight')   
    plt.ylabel('LGN cell number')
    plt.xlabel('Hidden cell number')
    plt.colorbar(ax)
    
    figs.append(plt.figure());
    plt.imshow(pm['output_weights']!=0,vmin=0,vmax=1,cmap='gray',interpolation='nearest')
    ax=plt.imshow(-pm['output_weights'],vmin=-plotrange[1],vmax=-plotrange[0],cmap='RdBu',interpolation='nearest',alpha=0.7)
    #plt.axis('equal')
    plt.axis('tight')
    plt.ylabel('Hidden cell number')
    plt.xlabel('Output cell number')
    plt.colorbar(ax)
    
    figs.append(plt.figure());
    plt.hist(np.reshape(pm['hidden_weights'],-1),bins=nbins,range=plotrange,color='k')
    if yaxis is None:
        y=list(plt.axis())[2:]
    else:
        y=list(yaxis)        
    plt.axis(list(plotrange)+y)
    plt.title('Distribution of hidden weights')
    plt.xlabel('Synaptic weight')
    plt.ylabel('Number of occurrences')
    
    figs.append(plt.figure());
    plt.hist(np.reshape(pm['output_weights'],-1),bins=nbins,range=plotrange,color='k')
    if yaxis is None:
        y=list(plt.axis())[2:]
    else:
        y=list(yaxis)        
    plt.axis(list(plotrange)+y)
    plt.title('Distribution of output weights')
    plt.xlabel('Synaptic weight')
    plt.ylabel('Number of occurrences')
    
    if savename is not None:
        figs[0].savefig(savename+'_hidweightsmat.png')
        figs[1].savefig(savename+'_outweightsmat.png')
        figs[2].savefig(savename+'_hidweightshist.png')
        figs[3].savefig(savename+'_outweightshist.png')
        
    return figs


def plotRFampl(lscsm,K,inputsForSTA=None,plotrange=None,yaxis=None,nbins=10,savename=None): 
# Compute and plot (histograms) RF amplitudes ("energy": sum of squares)    
    n_neurons=lscsm.num_neurons
    n_lgn=lscsm.num_lgn
    n_hidden=lscsm.num_hidden
    
    x=lscsm.getParam(K,'x_pos')
    y=lscsm.getParam(K,'y_pos')
    sc=lscsm.getParam(K,'size_center')
    ss=lscsm.getParam(K,'size_surround')
    hidden_w = lscsm.getParam(K,"hidden_weights").eval()
    output_w = lscsm.getParam(K,"output_weights").eval()
    if not lscsm.balanced_LGN:
        rc=lscsm.getParam(K,'center_weight')
        rs=lscsm.getParam(K,'surround_weight')
        
    lgn_rf=np.zeros((lscsm.size**2,n_lgn))
    xx = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).T.flatten()   
    yy = np.repeat([np.arange(0,lscsm.size,1)],lscsm.size,axis=0).flatten()
    
    if lscsm.balanced_LGN:
        for iCell in range(n_lgn):
            lgn_rf[:,iCell] = (np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/sc[iCell])/ (2*sc[iCell]*np.pi) - np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/ss[iCell])/ (2*ss[iCell]*np.pi)).copy()
    else:
        for iCell in range(n_lgn):
            lgn_rf[:,iCell] = (rc[iCell]*(np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/sc[iCell]).T/ (2*sc[iCell]*np.pi)) - rs[iCell]*(np.exp(-((xx - x[iCell])**2 + (yy - y[iCell])**2)/2/(sc[iCell]+ss[iCell])).T/ (2*(sc[iCell]+ss[iCell])*np.pi))).copy()
            
    hidden_rf=np.dot(lgn_rf,hidden_w)

    lgn_energy=np.sqrt(np.sum(lgn_rf**2,axis=0))
    hidden_energy=np.sqrt(np.sum(hidden_rf**2,axis=0))
    if not(inputsForSTA in [None,[]]):
        pred_respT=lscsm.response(inputsForSTA,K)
        modelSTA=np.array(STA_LR(np.mat(inputsForSTA),np.mat(pred_respT),0.0001))
        out_energy=np.sqrt(np.sum(modelSTA**2,axis=0))
    else: out_energy=np.zeros((1,n_neurons)) 
    
    if plotrange is None:    
        max_e=np.max([lgn_energy.max(),hidden_energy.max(),out_energy.max()])   
        plotrange=[0,max_e]
    
    figs=[]
    figs.append(plt.figure());
    plt.hist(lgn_energy,bins=nbins,range=plotrange)
    if yaxis is None:
        y=list(plt.axis())[2:]
    else:
        y=list(yaxis)        
    plt.axis(list(plotrange)+y)
    plt.title('Distribution of LGN RF energy')
    plt.xlabel('RF energy')
    plt.ylabel('Number of cells')    
    
    figs.append(plt.figure());
    plt.hist(hidden_energy,bins=nbins,range=plotrange)
    if yaxis is None:
        y=list(plt.axis())[2:]
    else:
        y=list(yaxis)       
    plt.axis(list(plotrange)+y)
    plt.title('Distribution of hidden RF energy')
    plt.xlabel('RF energy')
    plt.ylabel('Number of cells')    

    if not(inputsForSTA in [None,[]]):
        figs.append(plt.figure());
        plt.hist(out_energy,bins=nbins,range=plotrange)
        if yaxis is None:
            y=list(plt.axis())[2:]
        else:
            y=list(yaxis)        
        plt.axis(list(plotrange)+y)
        plt.title('Distribution of output RF energy')
        plt.xlabel('RF energy')
        plt.ylabel('Number of cells') 
    
    if savename is not None:
        figs[0].savefig(savename+'_lgnRFampl.png')
        figs[1].savefig(savename+'_hidRFampl.png')
        
        if not(inputsForSTA in [None,[]]):
            figs[2].savefig(savename+'_outRFampl.png')
    
    return figs
    
    

def plotThresholds(lscsm,K,plotrange=None,yaxis=None,nbins=10,savename=None):
# Plot the distribution of firing thresholds    
    hid_thresh=np.array(lscsm.getParam(K,'hidden_layer_threshold'))
    out_thresh=np.array(lscsm.getParam(K,'output_layer_threshold'))
    
    max_t=np.max([hid_thresh.max(),out_thresh.max()])
    min_t=np.min([hid_thresh.min(),out_thresh.min()])
    print [max_t,min_t]
    
    if plotrange is None:    
        max_t=np.max([hid_thresh.max(),out_thresh.max()])
        min_t=np.min([hid_thresh.min(),out_thresh.min()])
        plotrange=[min_t,max_t]
    
    figs=[]
    figs.append(plt.figure());
    plt.hist(hid_thresh,bins=nbins,range=plotrange)
    if yaxis is None:
        y=list(plt.axis())[2:]
    else:
        y=list(yaxis)        
    plt.axis(list(plotrange)+y)
    plt.title('Distribution of hidden layer firing thresholds')
    plt.xlabel('Firing threshold')
    plt.ylabel('Number of cells')    
    
    figs.append(plt.figure());
    plt.hist(out_thresh,bins=nbins,range=plotrange)
    if yaxis is None:
        y=list(plt.axis())[2:]
    else:
        y=list(yaxis)       
    plt.axis(list(plotrange)+y)
    plt.title('Distribution of output layer firing thresholds')
    plt.xlabel('Firing threshold')
    plt.ylabel('Number of cells')    

    
    if savename is not None:
        figs[0].savefig(savename+'_hidThresh.png')
        figs[1].savefig(savename+'_outThresh.png')
          
    return figs
    
 
    
 
    
 