import numpy as np
import matplotlib.pyplot as plt 
from elphy_utils.elphy_misc_utils import load_klusta_binary, elphy_visualize, detect_repeated_samples, elphy_vtags
from data_analysis.extracell_funcs_NEW import load_vtags_matfmt, checkVtags
from miscellaneous.fileListToCsv_NEW import fileListToCsv, elphy_database
from collections import OrderedDict
from elphy_utils.elphy_reader import ElphyFile, elphy_to_klusta
from elphy_utils.elphy_missing_samples import ElphyFileMissingData
import glob

elphy_dir='/media/margot/backup_mgt/DATA/raw_data/MANIP_2016/MANIP_2016_38/3816_CXRIGHT'

elphy_files=sorted(glob.glob(elphy_dir+'/*.DAT'))
valid_files={}
dxu={}

for elphy_name in elphy_files:
    valid_files[elphy_name]=0   
    try:
        ef=ElphyFile(elphy_name)
        valid_files[elphy_name]=1
        dxu[elphy_name]=ef.episodes[0].channels[0].dxu
        test=ef.episodes[0].get_data()
        valid_files[elphy_name]=2
    except:
        pass
