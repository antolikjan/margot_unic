from miscellaneous.convertVideo import loadFrame, saveToImfile
import matplotlib.pyplot as plt
from utils.various_tools import downsample
import numpy as np

im_name='/Users/margot/Desktop/cateye.png'
NB_name='/Users/margot/Desktop/cateye_NB.png'
down_fact=3
up_fact=20
csv_name='/Users/margot/Desktop/cateye.csv'
sep=' '
n_zeros=3

im=loadFrame(im_name)
margins=(im.shape[0]-im.shape[0]%down_fact,im.shape[1]-im.shape[1]%down_fact)
dim=downsample(downsample(im[:margins[0],:margins[1]],axis=0,factor=down_fact,mode='blur'),axis=1,factor=down_fact,mode='blur')
plt.figure(); plt.imshow(dim,cmap='gray',interpolation='none')

with open(csv_name,'w') as f:
    for line in dim:
        f.write(sep.join([str(int(i)).zfill(n_zeros) for i in line])+'\n')
saveToImfile(np.repeat(np.repeat(dim,up_fact,axis=0),up_fact,axis=1),NB_name,imagetype='.jpg')        