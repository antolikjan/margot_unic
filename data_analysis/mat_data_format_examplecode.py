from collections import OrderedDict
from scipy.io import loadmat
import numpy as np

mat_name='1718_CXRIGHT_TUN2_spksort180523.mat'
unitinfo_name='1718_CXRIGHT_TUN2_spksort180523_unitinfo.txt'
epinfo_name='1718_CXRIGHT_TUN2_spksort180523_epinfo.txt'


def load_info_table(filename,sep=' '):
    with open(filename,'r') as f:
        lines=f.read().split('\n')
    keys=lines[0].split(sep)
    types=lines[1].split(sep)
    info_dict=OrderedDict()
    for ikey in range(len(keys)):
        if types[ikey]=='bool':
            info_dict[keys[ikey]]=np.array([l.split(sep)[ikey] in ['True','true','1'] for l in lines[2:] if len(l)>0])
        else:    
            info_dict[keys[ikey]]=np.array([l.split(sep)[ikey] for l in lines[2:] if len(l)>0]).astype(types[ikey])
    return info_dict  

# Load data
data=loadmat(mat_name)
stimulus=list(data['stimulus'].flatten())
responses=list(data['responses'].flatten())
n_episodes=len(responses)
n_units=responses[0].shape[1]
del data
episode_info=load_info_table(epinfo_name)
unit_info=load_info_table(unitinfo_name)

# Subselect non-grating stimuli, validation set and spikesorted units
episode_selection={'data_splitting': ['val'],
                   'stim_type': list(set(episode_info['stim_type'])-set(['grating_pair']))}
unit_selection={'spikesorting': ['isolated','sorted_multiunit']}

valid_episodes=[ep for ep in range(n_episodes) if np.all([episode_info[p][ep] in episode_selection[p] for p in episode_selection.keys()])]
valid_units=[i for i in range(n_units) if np.all([unit_info[p][i] in unit_selection[p] for p in unit_selection.keys()])]
subselected_stim=np.array([stimulus[ep] for ep in valid_episodes]) # n_ep x n_bins x n_pix x n_pix array
subselected_resps=np.array([responses[ep] for ep in valid_episodes])[:,:,valid_units] # n_ep x n_bins x n_units array

# Average together repetitions of the same stimulus
unique_stims=sorted(list(set(episode_info['stim_id'][valid_episodes])))
subselected_stim=np.array([subselected_stim[episode_info['stim_id'][valid_episodes]==sid].mean(axis=0) for sid in unique_stims]) 
subselected_resps=np.array([subselected_resps[episode_info['stim_id'][valid_episodes]==sid].mean(axis=0) for sid in unique_stims]) 

# -> Then to compute a spatiotemporal RF, one has to reshape the stim to add a temporal dimension