analyzeExtracellData.py
Main program to load neuronal data (raw and/or spikesorted), visualize it, preprocess it and converted to a lighter format used for RF computation

extracell_funcs.py
Library of functions used by analyzeExtracellData

computeRFs.py
Main program to fit various RF models (except HSM) to preprocessed neuronal data
