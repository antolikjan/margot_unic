from scipy.io import loadmat, savemat
import numpy as np
from miscellaneous.convertVideo import loadStimInfo

spike_file='/home/margot/Bureau/0917_CXLEFT_TUN23_matlab/0917_CXLEFT_TUN23_SPK.mat'
vtag_file='/home/margot/Bureau/0917_CXLEFT_TUN23_matlab/0917_CXLEFT_TUN23_Vtags.mat'
stimorder_file='/home/margot/Bureau/0917_CXLEFT_TUN23_matlab/0917_CXLEFT_TUN23_stimorder_cleaned.txt'
out_file='/home/margot/Bureau/0917_CXLEFT_TUN23_matlab/0917_CXLEFT_TUN23_lea.mat'
n_elec=64
n_ep=330

spikes_raw=loadmat(spike_file)
vtag_raw=loadmat(vtag_file)
stim_info=loadStimInfo(stimorder_file)

spikes=np.zeros([n_elec,n_ep],dtype=object)
for ielec in range(n_elec):
    for iep in range(n_ep):
        key='Ep'+str(iep+1)+'Vspk'+str(ielec+1)+'Vu0'
        if spikes_raw.has_key(key):
            spikes[ielec,iep]=spikes_raw[key].flatten()
        else:
            spikes[ielec,iep]=np.array([])
            
            
stim_t0=np.zeros(n_ep)    
for iep in range(n_ep):
    key='Ep'+str(iep+1)+'_Vtag1'
    assert vtag_raw.has_key(key)      
    assert vtag_raw[key].shape==(1,1)
    stim_t0[iep]=vtag_raw[key][0,0]
    
    
savemat(out_file,{'spikes': spikes, 'stim_t0': stim_t0, 'stim_infos': stim_info})    