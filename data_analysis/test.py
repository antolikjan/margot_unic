plot_params=['orientation1','dton1','ISI']
param_vals=[sorted(list(set(stim_info[plot_params[i]][~np.isnan(stim_info[plot_params[i]])]))) for i in range(len(plot_params))]
fig_grid=GridSpec(*[len(param_vals[i]) for i in range(2)])

for phase1 in set(stim_info['phase1'][~np.isnan(stim_info['phase1'])]):
    for phase2 in set(stim_info['phase2'][~np.isnan(stim_info['phase2'])]):       
        for icell in [cells_tokeep]:#list(cells_tokeep)+[cells_tokeep]:
            plt.figure()
            for i0 in range(len(param_vals[0])):
                for i1 in range(len(param_vals[1])):
                    ax=plt.subplot(fig_grid[i0,i1])
                    plt.suptitle('phase1 : '+str(phase1)+', phase2 : '+str(phase2))
                    if i0==0:
                        plt.title(plot_params[1]+' : '+str(param_vals[1][i1]))
                    if i1==0:
                        plt.ylabel(plot_params[0]+' : '+str(param_vals[0][i0]))
                    leg_objects=[]    
                    for i2 in range(len(param_vals[2])):
                        indices=np.where((stim_info['type']=='grating_pair') & (stim_info[plot_params[0]]==param_vals[0][i0]) & (stim_info[plot_params[1]]==param_vals[1][i1]) & (stim_info[plot_params[2]]==param_vals[2][i2]) & (stim_info['phase1']==phase1) & (stim_info['phase2']==phase2))[0]
                        assert np.all(stim_info['display_time1'][indices]==stim_info['display_time1'][indices][0]) and np.all(stim_info['display_time2'][indices]==stim_info['display_time2'][indices][0])
                        sub_rates=np.array([rates[iep][:,icell] for iep in indices])
                        if not isinstance(icell,int):
                            sub_rates=np.rollaxis(sub_rates,2,1).reshape(-1,sub_rates.shape[1])
                                                        
                        pos=np.array(list(stim_info['display_time1'][indices][0])+list(stim_info['display_time2'][indices][0]))
                        pos=init_nbins[indices[0]]*pm.bin_width+pos*stim_dton
                        pos_col=[colors[i2]]*2+['k']*2
                        pos_style=['-','--']*2
                        for ipos in range(4):
                            plt.axvline(pos[ipos],color=pos_col[ipos],linestyle=pos_style[ipos])  
                            
                        leg_objects.append(plt.errorbar(np.arange(sub_rates.shape[1])*pm.bin_width,sub_rates.mean(axis=0),yerr=2.*sub_rates.std(axis=0)/np.sqrt(sub_rates.shape[0]),color=colors[i2])[0])
        #                        for pos in list(stim_info['display_time1'][indices][0])+list(stim_info['display_time2'][indices][0]):
        #                            plt.axvline(init_nbins[indices[0]]*pm.bin_width+pos*stim_dton,color=colors[i2])
        
                    plt.axis([0.99,1.9,0,0.8]) 
                    ax.set_yticks([])
                    if i0==len(param_vals[0])-1:
                        plt.xlabel('Time (s)')
                        ax.set_xticks(np.arange(1,1.9,0.1))
                    else:
                        ax.set_xticks([])
                    if (i0==0) and (i1==0):
                        #plt.legend(leg_objects,param_vals[2],title=plot_params[2])
                        plt.legend(leg_objects,[50,100,150,200,400],title='ISI (ms)')