from twophoton_funcs import *
import param
from utils.param_gui_tools import param_dialog, param_list_string
from utils.volterra_kernels import lsq_kernel, plotk1_multifig
from scipy.io import loadmat
import sys
from matplotlib.patches import Patch


"""
PARAMETERS
"""
class analyze2photon_params(param.Parameterized):    
    # STEPS TO EXECUTE:
    #changeparams=param.Boolean(default=True, doc='Change parameters?')
    load=param.Boolean(default=True, doc='Reload data and stimuli?')
    preprocess=param.Boolean(default=True, doc='Redo the preprocessing?')
    to_plot=param.List(['timeseries'], doc="MULTICHOICE('repeats','timeseries','timeseries_color','volterra','mean_timecourse','corr','dir_tuning','dir_resps','ori_tuning_pop') - List of things to plot")
    cells_to_plot=param.List([], doc='List of cells to plot individually')  
    to_include=param.List(['uncorrected','corrected','neuropil'], doc="MULTICHOICE('uncorrected','corrected','neuropil') - Signals on which to perform preprocessing and plotting")
    
    # DATA PATHS:
    respfile=param.Filename(default='160617_37-2/data_group1 - signals.mat',search_paths=['/media/partage/eqbrice_rawdata/ANALYSIS/Margot/'],doc='Neuronal responses file')
    imagelistfile=param.Filename(default='image_list - 1x1700 - 7x100 - randomized.txt',search_paths=['/DATA/Margot/ownCloud/MargotUNIC/Data/Mice_data/NaturalImagesJan/'],doc='Text file with list of image file names')
    imagepath=param.Foldername(default='/DATA/Margot/ownCloud/MargotUNIC/Data/Mice_data/NaturalImagesJan/',search_paths=['/DATA/Margot/ownCloud/MargotUNIC/Data/Mice_data/NaturalImagesJan/'],doc='Stimulus images folder')

    # PROCESSING PARAMETERS:
    protocol=param.String(default='images', doc="SINGLECHOICE('images','gratings','retinotopy') - Type of stimulation protocol")
    include_blanks=param.Boolean(default=False, doc='Include blank responses when computing the peak/mean responses array (and add corresponding blank frames to stimulus array)')
    stim_downsplfact=param.Integer(default=15, doc='Downsampling factor for the stimulus (by which number of pixels along one dimension will be divided)')
    normalize_stim=param.Boolean(default=True, doc="Normalize the stimulus array (by default: map 0 - 255 luminance values to -1 - 1, doesn't imply the stimulus will be centered if its mean wasn't 128)")
    baseline_mode=param.Tuple(default=('min',3), doc="('min', n): baseline for deltaF/F is sliding minimum over n episodes. ('blank',n): it is mean over the initial blank of duration n (frames) for each episode")
    lowpass_tau=param.Number(default=0., doc='Cut period (in seconds) for low pass filtering. If tau=None: no filtering')
    deconv_tau=param.Number(default=0., doc='Value of GCamp6 time constant for deconvolution, in seconds. If tau=None: no deconvolution')
    peak_loc=param.Array(default=np.array([0.1,0.7]), doc='Where to look for the peak of the onset response, in seconds')
    peak_width=param.Number(default=0.3, doc='Interval around the peak to average in order to get peak response')
    averaging_loc=param.Array(default=np.array([1.8,3.]), doc='Interval on which to compute an average response')
    resp_measure=param.String(default='average', doc="SINGLECHOICE('average','peak') - Way to compute a single response value to each image for each neuron")
    substract_blank=param.Boolean(default=True, doc='Substract blank response (average over the blank period preceding the image) from each computed peak/average response')
    subep_dur=param.Number(default=2.5, doc='Interval between two image onsets, in seconds (for natural images, image presentation duration is in fact 0.5)')
    ep_iblank=param.Number(default=2, doc='Duration of the initial blank before the first image presentation of each episode, in seconds')
    ep_fblank=param.Number(default=2, doc='Duration of the final blank after the last image of each episod, in seconds')
    subep_iblank=param.Number(default=0, doc='Duration of the blank preceding each image, in seconds')
    subep_fblank=param.Number(default=2, doc='Duration of the blank following each image, in seconds')
    n_im=param.Integer(default=20, doc='Number of image presentations per episode')
    #eps_to_analyze=param.Array(default=np.array([0,1000]), doc='Range of episodes to which the analysis is restricted')
    expcond_seqs=param.Dict(default={}, doc='Ranges of episodes corresponding to different experimental conditions (with their respective names), for separate analysis and comparison')


display_order={'1/ TO RUN':['load','preprocess','to_plot','cells_to_plot','to_include'],
               '2/ PROTOCOL INFOS':['protocol','ep_iblank','ep_fblank','n_im','subep_dur','subep_iblank','subep_fblank'],
               '3/ NEURONAL DATA PREPROCESSING': ['respfile','expcond_seqs','include_blanks','lowpass_tau','baseline_mode','deconv_tau','peak_loc','peak_width','averaging_loc','resp_measure','substract_blank'],
               '4/ STIMULUS (natural images protocol only)': ['imagelistfile','imagepath','stim_downsplfact','normalize_stim']}

colors='rgbycmk'


try: pm
except NameError: pm=analyze2photon_params()
if (len(sys.argv)<2) or (sys.argv[1]!='skip_dialog'):    
    pm,validated=param_dialog(pm,display_order)
else:
    validated=True    
if validated:
    print 'PARAMETERS : \n\n'+param_list_string(pm,display_order)+'\n\n'
    ep_dur=pm.subep_dur*pm.n_im+pm.ep_iblank+pm.ep_fblank
       
    
    """
    RUN
    """
    # LOAD DATA AND STIM
    if pm.load:
        # Load calcium data         
        data=loadmat(pm.respfile)
        n_bins,n_cells,n_eps=data['signals'].shape # Data['signals'] contains the neuronal calcium traces
        if pm.expcond_seqs=={}:
            pm.expcond_seqs={'':[0,n_eps]}
        expconds=sorted(pm.expcond_seqs.keys())    
#        if pm.eps_to_analyze[1]>n_eps:
#            pm.eps_to_analyze[1]=n_eps
#        if np.any(pm.eps_to_analyze!=[0,n_eps]):
#            data['signals']=data['signals'][:,:,pm.eps_to_analyze[0]:pm.eps_to_analyze[1]]
#            data['localneuropil']=data['localneuropil'][:,:,pm.eps_to_analyze[0]:pm.eps_to_analyze[1]]
#            n_eps=data['signals'].shape[2]
        dt=data['dt'][0,0] # Frame duration
         # Compute durations of the different episode stages in number of frames (initially provided in seconds)
        n_initbins, n_finalbins, nbins_per_im=compute_nbins(n_bins,pm.ep_iblank,pm.subep_dur,pm.n_im,dt)
        # Sometimes theoretical duration based on provided parameters and real duration (number of frames x frame duration) don't match exactly
        if n_bins!=np.round(ep_dur*1./dt): 
            print "Warning: theoretical and real durations don't match: "+str(ep_dur)+'s vs '+str(n_bins*dt)+'s'
        
        try: raw_signals
        except NameError: raw_signals = {}
        raw_signals['uncorrected']=data['signals'] # Raw calcium traces
        raw_signals['corrected'],nan_neuropil_cells=neuropilCorrection(data['signals'],data['localneuropil']) # Raw calcium traces with neuropil correction
        validcells=list(set(range(n_cells))-set(nan_neuropil_cells)) # Subset of cells for which neuropil data is available. Others are discarded in the corrected data
        raw_signals['corrected']=raw_signals['corrected'][:,validcells,:]
        raw_signals['neuropil']=data['localneuropil'][:,validcells,:] # Raw neuropil traces
        
        # Load and downsample stimulus (final shape time x pixels, frameshape gives number of raws and columns)
        if pm.protocol=='images':    
            stim=loadImageMat(pm.imagelistfile,imagepath=pm.imagepath,imrange=[0,pm.n_im*n_eps])  
            stim,frameshape=reshapeStim(stim,downsplfact=pm.stim_downsplfact,normalize=pm.normalize_stim,include_blanks=pm.include_blanks)
            
        elif pm.protocol=='gratings':
            conds=data['conds'].reshape(-1)-1
            isblank=np.array(data['xpar']['table'][0][0]['VstimType'],dtype=np.uint8).reshape(-1)==7
            dir_table=np.array(data['xpar']['table'][0][0]['VOri'],dtype=np.float).reshape(-1)
            dir_table[isblank]=np.nan
            dir_vals=dir_table[conds]
            
        del data    
            
    
    # PREPROCESS DATA      
    if pm.preprocess:
        try: preproc_signals
        except NameError: preproc_signals = {}
        try: point_resp
        except NameError: point_resp = {'peak':{},'average':{},'blank':{}} 
        
        for signaltype in pm.to_include:
            # Preprocess all signals listed in to_include
            preproc_signals[signaltype]=preProcessSignal(raw_signals[signaltype],dt,lowpass_tau=pm.lowpass_tau,baseline_mode=pm.baseline_mode,deconv_tau=pm.deconv_tau)
            # Reshape the signal to separate the successive image presentation
            reshaped_signal=separate_episodes(preproc_signals[signaltype],pm.ep_iblank,pm.subep_dur,pm.n_im,dt)
            time_courses=reshaped_signal.mean(axis=3).mean(axis=0) # Mean response to all images for each cell
            # Compute peak value of onset response to each image for each cell
            point_resp['peak'][signaltype]=restrictToRange(np.rollaxis(reshaped_signal,0,4),findPeaks(time_courses[range(*(pm.peak_loc*1./dt).astype(int)),:]),interval_width=int(pm.peak_width*1./dt))
            point_resp['peak'][signaltype]=point_resp['peak'][signaltype].mean(axis=0).reshape(-1,pm.n_im*n_eps).T
            #peaks2=np.rollaxis(reshaped_signal[:,range(*(peak_loc*1./dt).astype(int)),:,:].max(axis=1),2).reshape(-1,n_cells)
            #peaks[signaltype]-=np.rollaxis(reshaped_signal[:,0,:,:],2).reshape(-1,n_cells)
            point_resp['average'][signaltype]=np.rollaxis(reshaped_signal,3,0)[:,:,range(*(np.round(pm.averaging_loc*1./dt)).astype(int)),:].mean(axis=2).reshape(pm.n_im*n_eps,-1)
            point_resp['blank'][signaltype]=np.rollaxis(reshaped_signal,3,0)[:,:,:max(int(np.round(pm.subep_iblank*1./dt)),1),:].mean(axis=2).reshape(pm.n_im*n_eps,-1)
            if pm.substract_blank:
                point_resp['peak'][signaltype]-=point_resp['blank'][signaltype]
                point_resp['average'][signaltype]-=point_resp['blank'][signaltype]
  
  
    # PLOT
    if len(pm.to_plot)>0: 
        try: sliders
        except NameError: sliders = {}
        try: axhandles
        except NameError: axhandles = {}
        try: figcount
        except NameError: figcount = 0
        
        for signaltype in pm.to_include:       
            if 'repeats' in pm.to_plot:
                # Colorplot of peak responses to each image for each cell, repetitions of the same image being grouped by columns
                figcount+=1
                if pm.protocol=='images':
                    stimrepeats,nonrepeated=listRepeats(loadImageList(pm.imagelistfile)[:pm.n_im*n_eps])
                elif pm.protocol=='gratings':  
                    stimrepeats,nonrepeated=listRepeats(dir_vals)
                orderedresps,rep_boundaries,seq_boundaries=classByRepeats(point_resp[pm.resp_measure][signaltype],stimrepeats,sep_seqs=[range(*np.array(pm.expcond_seqs[key])*pm.n_im) for key in expconds])
                xleg={'names':sorted(stimrepeats.keys()), 'pos':0.5*(np.array([0]+list(rep_boundaries))+np.array(list(rep_boundaries)+[orderedresps.shape[0]]))}               
                if len(pm.expcond_seqs)==1:
                    stamp_lists=[rep_boundaries]
                else:
                    stamp_lists=[seq_boundaries,rep_boundaries]                    
                sliders[figcount],axhandles[figcount]=plotTimeSeries(orderedresps,1,400,stamp_lists=stamp_lists,plottype='colormap',norm_fact=0.03,title='Repeats - '+signaltype,xleg=xleg)
                if len(pm.expcond_seqs)>1:
                    axhandles[figcount].set_xlabel('Condition ('+' / '.join(sorted(pm.expcond_seqs.keys()))+') x Stimulus identity')
                else:    
                    axhandles[figcount].set_xlabel('Stimulus identity')   
                axhandles[figcount].set_ylabel('Cell number') 
                
            if 'timeseries' in pm.to_plot:
                # Plot the whole preprocessed signal for each cell
                figcount+=1
                #sliders[figcount],axhandles[figcount]=plotSignals(preproc_signals[signaltype],n_im,subep_dur,ep_iblank,dt,comparison_signals=preproc_signals['neuropil'])
                sliders[figcount],axhandles[figcount]=plotSignals(preproc_signals[signaltype],pm.n_im,pm.subep_dur,pm.ep_iblank,pm.subep_iblank,pm.subep_fblank,dt,norm_fact=0.1,expcond_seqs=pm.expcond_seqs,title='Signals across time - '+signaltype)
                axhandles[figcount].set_xlabel('Time (s)')   
                axhandles[figcount].set_ylabel('Cell number / deltaF/F') 
                
            if 'timeseries_color' in pm.to_plot:
                # Plot the whole preprocessed signal for each cell
                figcount+=1
                sliders[figcount],axhandles[figcount]=plotSignals(preproc_signals[signaltype],pm.n_im,pm.subep_dur,pm.ep_iblank,pm.subep_iblank,pm.subep_fblank,dt,plottype='colormap',norm_fact=0.03,expcond_seqs=pm.expcond_seqs,title='Signals across time (colorplot) - '+signaltype)
                axhandles[figcount].set_xlabel('Time (s)')   
                axhandles[figcount].set_ylabel('Cell number') 
                
            if ('volterra' in pm.to_plot) & (pm.protocol=='images'):  
                # Compute and plot receptive fields (first order Volterra)
                for cond in expconds:
                    cond_inds=range(*np.array(pm.expcond_seqs[cond])*pm.n_im)
                    volt0,volt1=lsq_kernel(stim[cond_inds,:],point_resp[pm.resp_measure][signaltype][cond_inds,:],order0=True,order2=False)
                    plotk1_multifig(volt1,ntau=1,ncellsperfig=49,scalemax='bycell',smooth=False,griddim=None,bycol=False,subRegion=None,stimshape=frameshape,title='Volterra 1 - '+signaltype+' - '+cond)
                    figcount+=int(np.ceil(n_cells/49))
                
            if 'mean_timecourse' in pm.to_plot:
                # Plot mean response to all images for each cell
                reshaped_signal=separate_episodes(preproc_signals[signaltype],pm.ep_iblank,pm.subep_dur,pm.n_im,dt)
                fig=plt.figure()
                figcount+=1
                leghandles=[[]]*len(expconds)
                for icond in range(len(expconds)):
                    meansignals=reshaped_signal[:,:,:,range(*pm.expcond_seqs[expconds[icond]])].mean(axis=3).mean(axis=0)
                    plt.plot(np.arange(nbins_per_im)*dt,meansignals,color=colors[icond],alpha=0.15)
                    plt.plot(np.arange(nbins_per_im)*dt,meansignals.mean(axis=1),color='k',linewidth=4)[0]
                    leghandles[icond]=plt.plot(np.arange(nbins_per_im)*dt,meansignals.mean(axis=1),color=colors[icond],linewidth=4,alpha=0.6)[0]
                if len(expconds)>1:
                    plt.legend(leghandles,expconds)
                axhandles[figcount]=plt.gca()
                fig.suptitle( 'Mean response over all stimuli - '+signaltype)           
                axhandles[figcount].set_xlabel('Time (s)')   
                axhandles[figcount].set_ylabel('Mean deltaF/F (over all stimuli)') 
                        
#            if ('dir_tuning' in pm.to_plot) & (pm.protocol=='gratings'):          
#                sorted_dir=sorted(dir_table[~np.isnan(dir_table)])
#                tuning_curves=np.zeros((len(pm.expcond_seqs),len(sorted_dir),n_cells,2))
#                         
#                for icond in range(len(expconds)):
#                    eprange=range(*pm.expcond_seqs[expconds[icond]])
#                    blank_resp=point_resp[pm.resp_measure][signaltype][eprange,:][np.isnan(dir_vals[eprange]),:].mean(axis=0)
#                    for idir in range(len(sorted_dir)):
#                        ov_resps=point_resp[pm.resp_measure][signaltype][eprange,:][dir_vals[eprange]==sorted_dir[idir],:]
#                        tuning_curves[icond,idir,:,0]=ov_resps.mean(axis=0)-blank_resp
#                        tuning_curves[icond,idir,:,1]=ov_resps.std(axis=0)    
#                
#                ncols=15;
#                nrows=int(np.ceil((n_cells+1)*1./ncols))
#                figcount+=1 
#                axhandles[figcount]=plt.figure(figsize=(ncols,nrows))
#                axhandles[figcount].suptitle('Tuning curves - '+signaltype)
#                plt.subplot(nrows,ncols,1)
#                plt.legend(handles=[Patch(color=colors[icond],label=expconds[icond]) for icond in range(len(expconds))],loc='center',fontsize='x-small')#,mode='expand')
#                plt.axis('off')
#                for icell in range(n_cells):
#                    plt.subplot(nrows,ncols,icell+2)
#                    plt.axvline(x=0.5*(sorted_dir[0]+sorted_dir[-1]),color='k',linewidth=0.5)
#                    plt.axhline(y=0,color='k',linewidth=0.5)
#                    for icond in range(len(expconds)):                         
#                        plt.errorbar(sorted_dir,tuning_curves[icond,:,icell,0],yerr=tuning_curves[icond,:,icell,1],color=colors[icond])
#                    plt.title(str(icell+1),fontsize='x-small')
#                    plt.axis('off')
#                #plt.subplots_adjust(left=1, bottom=1, right=2, top=2, wspace=0, hspace=0)
#                #plt.tight_layout(pad=10)  
                    
            if ('dir_tuning' in pm.to_plot) & (pm.protocol=='gratings'):          
                sorted_dir=sorted(dir_table[~np.isnan(dir_table)])
                sorted_ori=sorted(list(set(np.array(sorted_dir)%180)))
                ori_vals=np.array(dir_vals)%180
                dir_tuning=np.zeros((len(pm.expcond_seqs),len(sorted_dir),n_cells,2))    
                ori_tuning=np.zeros((len(pm.expcond_seqs),len(sorted_ori),n_cells,2))  
                circ_vars=np.zeros((len(expconds),n_cells))                  
                for icond in range(len(expconds)):
                    eprange=range(*pm.expcond_seqs[expconds[icond]])
                    dir_tuning[icond,:,:,:]=tuningCurve(dir_vals[eprange],point_resp[pm.resp_measure][signaltype][eprange,:])
                    ori_tuning[icond,:,:,:]=tuningCurve(ori_vals[eprange],point_resp[pm.resp_measure][signaltype][eprange,:])
                    circ_vars[icond,:]=circVar(sorted_ori,ori_tuning[icond,:,:,0])
                    
                tuning_order=np.argsort(circ_vars.mean(axis=0))    
                ncols=15;
                nrows=int(np.ceil((n_cells+1)*1./ncols))
                figcount+=1 
                axhandles[figcount]=plt.figure(figsize=(ncols,nrows))
                axhandles[figcount].suptitle('Tuning curves - '+signaltype)
                plt.subplot(nrows,ncols,1)
                plt.legend(handles=[Patch(color=colors[icond],label=expconds[icond]) for icond in range(len(expconds))],loc='center',fontsize='x-small')#,mode='expand')
                plt.axis('off')
                for icell in range(n_cells):
                    plt.subplot(nrows,ncols,icell+2)
                    plt.axvline(x=0.5*(sorted_dir[0]+sorted_dir[-1]),color='k',linewidth=0.5)
                    plt.axhline(y=0,color='k',linewidth=0.5)
                    for icond in range(len(expconds)):                         
                        plt.errorbar(sorted_dir,dir_tuning[icond,:,tuning_order[icell],0],yerr=dir_tuning[icond,:,tuning_order[icell],1],color=colors[icond])
                    plt.title(str(tuning_order[icell]+1),fontsize='x-small')
                    plt.axis('off')
                #plt.subplots_adjust(left=1, bottom=1, right=2, top=2, wspace=0, hspace=0)
                #plt.tight_layout(pad=10)                 
                        
            if ('dir_resps' in pm.to_plot) & (pm.protocol=='gratings'):
                reshaped_signal=separate_episodes(preproc_signals[signaltype],pm.ep_iblank,pm.subep_dur,pm.n_im,dt)
                sorted_dir=sorted(dir_table[~np.isnan(dir_table)])
                figcount+=1
                axhandles[figcount]=plt.figure(figsize=(2.5*len(sorted_dir),2*len(pm.cells_to_plot)))
                axhandles[figcount].suptitle('Single responses - '+signaltype)
                for icell in range(len(pm.cells_to_plot)):
                    for idir in range(len(sorted_dir)):
                        dir_eps=dir_vals==sorted_dir[idir]
                        plt.subplot(len(pm.cells_to_plot),len(sorted_dir),idir+icell*len(sorted_dir)+1)
                        for icond in range(len(expconds)):                           
                            seq=range(*pm.expcond_seqs[expconds[icond]])  
                            signals=reshaped_signal[:,:,pm.cells_to_plot[icell]-1,seq][:,:,dir_vals[seq]==sorted_dir[idir]].reshape(np.prod(reshaped_signal.shape[:2]),-1)
                            plt.plot(np.arange(nbins_per_im)*dt,signals,color=colors[icond],alpha=0.8)
                        plt.axis([0,nbins_per_im*dt,preproc_signals[signaltype][:,pm.cells_to_plot[icell]-1,:].min(),preproc_signals[signaltype][:,pm.cells_to_plot[icell]-1,:].max()])
                        if icell==len(pm.cells_to_plot)-1:                            
                            plt.xlabel('dir='+str(sorted_dir[idir]))  
                        if idir==0:
                            plt.ylabel('Cell '+str(pm.cells_to_plot[icell]))
                            if icell==0:
                                plt.legend(handles=[Patch(color=colors[icond],label=expconds[icond]) for icond in range(len(expconds))],fontsize='x-small')  

            if ('ori_tuning_pop' in pm.to_plot) & (pm.protocol=='gratings'):
                ori_vals=np.array(dir_vals)%180
                sorted_ori=sorted(list(set(ori_vals[~np.isnan(ori_vals)]))) 
                ori_tuning=np.zeros((len(pm.expcond_seqs),len(sorted_ori),n_cells))  
                circ_vars=np.zeros((len(expconds),n_cells))                  
                for icond in range(len(expconds)):
                    eprange=range(*pm.expcond_seqs[expconds[icond]])
                    ori_tuning[icond,:,:]=tuningCurve(ori_vals[eprange],point_resp[pm.resp_measure][signaltype][eprange,:])[:,:,0]
                    circ_vars[icond,:]=circVar(sorted_ori,ori_tuning[icond,:,:])
                figcount+=1 
                axhandles[figcount]=plt.figure()
                axhandles[figcount].suptitle('Circular variance - '+signaltype)
                for icond in range(len(expconds)):
                    plt.hist(circ_vars[icond,:],bins=np.linspace(0,1,21),color=colors[icond],alpha=0.5)
                plt.legend(expconds,loc='upper left') 
                plt.xlabel('Circular variance')
                plt.ylabel('Number of cells')
               
                
        if 'corr' in pm.to_plot: 
            # Plot correlation matrix of peak responses between neurons and neuropile
            for cond in expconds:
                seq=range(*pm.expcond_seqs[cond])
                corrmat=np.corrcoef(point_resp[pm.resp_measure]['uncorrected'][seq,:].T,point_resp[pm.resp_measure]['neuropil'][seq,:].T)
                fig=plt.figure()
                figcount+=1
                axhandles[figcount]=plt.imshow(-corrmat,interpolation='none',vmin=-1,vmax=1,cmap='RdBu')
                fig.suptitle('Response correlation matrix - uncorrected and neuropil - '+cond)        
                corrmat=np.corrcoef(point_resp[pm.resp_measure]['corrected'][seq,:].T,point_resp[pm.resp_measure]['neuropil'][seq,:].T)
                fig=plt.figure()
                figcount+=1
                axhandles[figcount]=plt.imshow(-corrmat,interpolation='none',vmin=-1,vmax=1,cmap='RdBu')
                fig.suptitle('Response correlation matrix - corrected and neuropil - '+cond)
                