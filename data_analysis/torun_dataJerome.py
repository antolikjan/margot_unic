from computeReceptiveFields import *
import scipy
from matplotlib.pyplot import *
from volterra_kernels import *


def loadDataJerome(filename,bl_duration=1):
# Load stimulus and response data and reshape it if necessary
# Outputs: - training_input (stimuli used for training): nbins_training x npixels
#          - training_set (responses used for training): nbins_training x ncells   
#          - validation_input (stimuli used for validation): nbins_validation x npixels
#          - validation_set (responses used for validation): nbins_validation x ncells  
 
    respdata=scipy.io.loadmat(filename+'_Data_file.mat')
    stimdata=scipy.io.loadmat(filename+'_Stimulus_related_info.mat')
    stims=stimdata['Info'].astype(float)
    spikes=respdata['spike_times']
    nBlank=np.int(np.round(bl_duration*1./stimdata['frameDuration'][0,0]))
    trialDuration=stimdata['nFramesPerTrial'][0,0]*stimdata['frameDuration'][0,0]+bl_duration
    
    (nDivX,nDivY,nFrames)=np.shape(stims)
    stims[stims==1]=-1
    stims[stims==255]=1   
    stims=np.concatenate([stims.reshape(nDivX*nDivY,stimdata['nTrial'][0,0],-1), np.zeros((nDivX*nDivY,stimdata['nTrial'][0,0],nBlank))], axis=2)
    stims=stims.reshape(nDivX*nDivY,-1).T
    
    wherevalid=(spikes>=0)
    spikes=spikes*1./respdata['samplingFreq'][0,0]+np.arange(spikes.shape[0]).reshape(-1,1)*trialDuration
    spikes=np.histogram(spikes[wherevalid],np.arange(0,trialDuration*stimdata['nTrial'][0,0]+stimdata['frameDuration'][0,0],stimdata['frameDuration'][0,0]))[0]
    spikes=spikes.reshape(-1,1)
    
    return stims, spikes


def divideTrainAndVal(data,val_frac=0.2):
    nBinsTrain=round((data.shape[0])*(1-val_frac))
    return data[:nBinsTrain], data[nBinsTrain:]


def tauShift(stim,resp,tau):
    return stim[:stim.shape[0]-tau,:], resp[tau:,:]  
    


ntau=5
tau=0
smooth=False
npix=100
scalemax='bycell'
filepath='/DATA/Margot/ownCloud/MargotUNIC/data_Jerome/'
#filename='rfxyt018a04_2B-rfxyt018a03_2B/rfxyt018a04_2B_SN'
#filename='rfxyt018a04_2B-rfxyt018a03_2B/rfxyt018a03_2B_DN'
#filename='rfxyt019a01_2A-rfxyt019a02_2B/rfxyt019a01_2A_SN'
#filename='rfxyt019a01_2A-rfxyt019a02_2B/rfxyt019a02_2B_DN'
filenames=['rfxyt018a04_2B-rfxyt018a03_2B/rfxyt018a04_2B_SN','rfxyt018a04_2B-rfxyt018a03_2B/rfxyt018a03_2B_DN','rfxyt019a01_2A-rfxyt019a02_2B/rfxyt019a01_2A_SN','rfxyt019a01_2A-rfxyt019a02_2B/rfxyt019a02_2B_DN']
celllabels=['018a04_2B_SN','018a03_2B_DN','019a01_2A_SN','019a02_2B_DN']

#k0=np.zeros((1,len(filenames)))
#k1=np.zeros((npix*ntau,len(filenames)))
#k2diag=np.zeros((npix*ntau,len(filenames)))
#for ifile in range(len(filenames)):
#    rawstim,rawspikes=loadDataJerome(filepath+filenames[ifile]) 
#    stim,spikes=add_temporal_dim(rawstim,rawspikes,ntau)
#    stim,spikes=tauShift(stim,spikes,tau)  
#    k0[:,ifile],k1[:,ifile:ifile+1],k2diag[:,ifile:ifile+1]=lsq_kernel(stim,spikes,order0=True,order2='diag')
    
    
#k0,k1,k2full=lsq_kernel(stim,spikes,order0=True,order2='full')
#k2=diagterms(k2full)
plot_kernel1(k1,ntau=ntau,firstcell=1,scalemax=scalemax,smooth=smooth,celllabels=celllabels,binwidth=50)
plot_kernel1(k2diag,ntau=ntau,firstcell=1,scalemax=scalemax,smooth=smooth,celllabels=celllabels,binwidth=50)


#ntau=1
#tau=2
#val_frac=0.2
#laplace_bias=0.0001
##toinclude=['lscsm','STA','volt1','volt2']
#toinclude=['lscsm','volt1','volt2diag','volt2']
##toinclude=['volt1','volt2diag','volt2']
##minStimSize=int(sys.argv[2])
#minStimSize=1
#reduced_volt2=[3,-4,4,-3]
#sigvolt2=list(ones(32)*2)

#rfs=computeReceptiveFields(filename,val_frac,ntau=ntau,tau=tau,minStimSize=minStimSize,STA_lb=None,volt1=False,volt2=False,volt2diag=True,datapath=datapath,outpath=RFpath)
#plotReceptiveFields(filename,ntau=ntau,tau=tau,RFpath=RFpath,STA=False,volt1=False,k2neig=0,volt2diag=True,outpath=RFpath,ncellsperfig='all')
#vcorr=compareRFperf(filename,val_frac=val_frac,ntau=ntau,tau=tau,minStimSize=minStimSize,toinclude=toinclude,datatype='v',lscsm_name=lscsm_name,reduced_volt2=reduced_volt2,sigvolt2=sigvolt2,datapath=datapath,resultspath=resultspath,RFpath=RFpath)
#tcorr=compareRFperf(filename,val_frac=val_frac,ntau=ntau,tau=tau,minStimSize=minStimSize,toinclude=toinclude,datatype='t',lscsm_name=lscsm_name,reduced_volt2=reduced_volt2,sigvolt2=sigvolt2,datapath=datapath,resultspath=resultspath,RFpath=RFpath)
#plotRFcorr(vcorr,toinclude,scale=[0.05,0.5])
#plotRFcorr(tcorr,toinclude,scale=[0.05,0.5])


    
    
   