import numpy as np
from miscellaneous.convertVideo import loadFrame, downsample
import matplotlib.pyplot as plt
from itertools import chain
from matplotlib.widgets import Slider
import os



def neuropilCorrection(neuronresp,neuropilresp,mode='vol_frac'):
    """
    Substracts the neuropil signal from each cell (coefficient=0.7)
    nan_cells: list of cells for which the neuropil signal was nan (left uncorrected) 
    """    
    if mode=='vol_frac':
        correctedresp=neuronresp.copy()-0.7*neuropilresp.copy()
    nan_cells=np.where(np.isnan(neuropilresp.copy()).sum(axis=2).sum(axis=0))[0]    
    correctedresp[:,nan_cells,:]=neuronresp[:,nan_cells,:].copy()
    return correctedresp,nan_cells
        

def slidingMin(signal,window=5):
    """
    Takes the neuronal signal (time x cells x episodes) and computes the minimum over a sliding window (window=5 means 5 episodes) for each episode
    """
    nbins,n_cells,n_eps=signal.shape
    epmin=signal.min(axis=0)
    baseline=np.zeros(epmin.shape)
    for iep in range(n_eps):
        eprange=range(max(np.floor(iep-window/2).astype(int),0), min(np.floor(iep+window/2).astype(int)+np.mod(window,2),n_eps))
        baseline[:,iep]=epmin[:,eprange].min(axis=1)
    return baseline # shape = (cells x episodes)
    
    
def computeBlankResps(resps,blank_duration):
    """    
    Takes the mean over the blank frames (number 0 to number blank_duration) for each episode
    resps: time x cells x episodes
    output: cells x episodes 
    """
    return resps[:blank_duration,:,:].mean(axis=0) 
    

def deltaFoverF(signal,baseline):
    return (signal-baseline)*1./baseline


def deconvolve(signal,dt,tau=2):
    """  
    deconvolves using r = dC/dt + C/tau
    signal: time x cells x episodes
    dt: frame duration in seconds
    output: same shape     
    """
    dsigdt=np.diff(signal,axis=0)*1./dt
    deconvsig=dsigdt+signal[1:,:,:]*1./tau
    return np.concatenate([deconvsig[0:1,:,:],deconvsig], axis=0)   
    
    
def loadImageList(filename):
    """
    Takes name of file containing the list of names of presented images, and returns the list of names as a list of strings
    """    
    with open(filename,'r') as f:
        imagelist=f.read().split('\n')[:-1]
    return imagelist


def loadImageMat(imagelistfile,imagepath='',imrange='all'):
    """
    Takes a list of image file names and returns these images in a time x ni x nj matrix
    """    
    imagelist=loadImageList(imagelistfile)
    nframes=len(imagelist)
    if imrange=='all':
        imrange=[0,nframes]
    for iframe in range(imrange[0],imrange[1]):
        im=loadFrame(os.path.join(imagepath,imagelist[iframe]))
        if iframe==0:
            imagemat=np.zeros((imrange[1]-imrange[0],)+im.shape)
        imagemat[iframe,:,:]=im
    return imagemat 
    
    
def addBlanks(stim,ep_length=None,nblanks=[1,1],blankval=0):
    if ep_length!=None:
        nframes,framesize=stim.shape    
        stim=stim.reshape(-1,ep_length,framesize)
    else:
        ep_nframes,nep,framesize=stim.shape
    stim=np.concatenate([np.zeros((stim.shape[0],nblanks[0],framesize))+blankval,stim,np.zeros((stim.shape[0],nblanks[1],framesize))+blankval],axis=1)
    return stim.reshape(-1,framesize)  


def compute_mean(resps,ep_iblank,subep_duration,n_im,include_blanks=True):
    n_bins,n_cells,n_eps=resps.shape
    n_initbins, n_finalbins, nbins_per_im=compute_nbins(n_bins,ep_iblank,subep_duration,n_im)
    reshapedresps=resps[n_initbins:-n_finalbins,:,:].reshape(n_im,nbins_per_im,n_cells,n_eps)
    meanresps=reshapedresps.mean(axis=1)
    if include_blanks:
        initb_resp=resps[:n_initbins,:,:]
        finalb_resp=resps[-n_finalbins:,:,:]
        meanresps=np.concatenate([initb_resp.mean(axis=0).reshape(1,n_cells,n_eps),meanresps,finalb_resp.mean(axis=0).reshape(1,n_cells,n_eps)],axis=0)
    return reshapedresps, np.rollaxis(meanresps,2).reshape(-1,n_cells)    
    
    
def separate_episodes(resps,ep_iblank,subep_duration,n_im,dt):
    """
    Reshapes resps from time x cells x episodes to image_number x time x cells x episodes (drops the blanks)
    """    
    n_bins,n_cells,n_eps=resps.shape
    n_initbins, n_finalbins, nbins_per_im=compute_nbins(n_bins,ep_iblank,subep_duration,n_im,dt)
    return resps[n_initbins:-n_finalbins,:,:].reshape(n_im,nbins_per_im,n_cells,n_eps)  
 

def compute_nbins(n_bins,ep_iblank,subep_duration,n_im,dt):      
    n_initbins=np.round(ep_iblank*1./dt).astype(int)
    nbins_per_im=np.round(subep_duration*1./dt).astype(int)
    n_finalbins=n_bins-n_initbins-n_im*nbins_per_im   
    return n_initbins, n_finalbins, nbins_per_im 
    

def normalizeMat(mat,mode='maptorange',params={'minval':0,'maxval':255}):
    """    
    mode='maptorange': shifts and scales an array such that values equal to minval become -1 and values equal to maxval become 1.
    mode='center_reduce': sets mean to 0 and std to 1
    """ 
    if mode=='maptorange':
        return (mat-(params['minval']+params['maxval'])*0.5)*2./(params['maxval']-params['minval'])
    elif mode=='center_reduce':
        return (mat-mat.mean())*1./mat.std()
    else:
        raise NotImplementedError    
    
    
def listRepeats(listobj):
    """
    Takes a list of items and returns a dict of repeated items and another dict of nonrepeated items with their occurences in the list
    """
    # Convert NaNs to strings for them to be handled properly by the set function
    strnan_list=list(listobj[:])
    for i in range(len(strnan_list)):
        if (type(strnan_list[i])==float) and (np.isnan(strnan_list[i])):
            strnan_list[i]='nan'
            
    diffitems=set(strnan_list)
    repeats={}
    nonrepeated={}
    for item in diffitems:
        occurrences=[i for i, x in enumerate(strnan_list) if x == item]
#        if item=='nan':
#            item=np.nan
        if len(occurrences)>1:
            repeats[item]=occurrences
        else:
            nonrepeated[item]=occurrences
            
    return repeats, nonrepeated


#def classByRepeats(meanresps,repeats):
#    """
#    Takes the responses (image_number x cell, no time dimension) and the dictionary of repeats generated by the listRepeats function.
#    Returns the subset of the response array that corresponds to repetitions, ordered such that repetitions of the same image follow each other.
#    Boundaries contains the indice of the first repetition of each image
#    """    
##     respsdict={}
##     for stim in repeats.keys():
##         respsdict[stim]=meanresps[repeats[stim],:]
#    indices=[repeats[key] for key in sorted(repeats.keys())]
#    boundaries=np.cumsum([len(l) for l in indices[:-1]])
#    orderedresps=meanresps[list(chain(*indices)),:]
#    #     return respsdict,orderedresps,boundaries   
#    return orderedresps,boundaries
    
    
def classByRepeats(meanresps,repeats,sep_seqs=None):
    """
    Takes the responses (image_number x cell, no time dimension) and the dictionary of repeats generated by the listRepeats function.
    Returns the subset of the response array that corresponds to repetitions, ordered such that repetitions of the same image follow each other.
    Boundaries contains the indice of the first repetition of each image
    """    
#     respsdict={}
#     for stim in repeats.keys():
#         respsdict[stim]=meanresps[repeats[stim],:]
    if sep_seqs!=None:
        valid_inds=list(chain(*sep_seqs))
    else:
        valid_inds=range(meanresps.shape[0])
    rep_indices=[[i for i in repeats[key] if i in valid_inds] for key in sorted(repeats.keys())]
    rep_boundaries=np.cumsum([len(l) for l in rep_indices[:-1]])
    if sep_seqs!=None:
        seq_indices=list(chain(*[[[i for i in seq if i in rep_group] for seq in sep_seqs] for rep_group in rep_indices]))
        seq_boundaries=np.cumsum([len(l) for l in seq_indices[:-1]])
        orderedresps=meanresps[list(chain(*seq_indices)),:]
    else :   
        orderedresps=meanresps[list(chain(*rep_indices)),:]
        seq_boundaries=[]
    #     return respsdict,orderedresps,boundaries   
    return orderedresps,rep_boundaries,seq_boundaries  
        
        
def addStamps(stampvals,axhandle=None,color='k',linewidth=1):
    """
    Draws vertical lines on the current/provided plot at the provided abscissae
    """
    for stamp in stampvals:
        if axhandle!=None:
            axhandle.axvline(x=stamp,color=color,linewidth=linewidth)
        else:
            plt.axvline(x=stamp,color=color,linewidth=linewidth)
            
            
def plotRepeatStats(orderedresps,boundaries):
    tot_nim,n_cells=orderedresps.shape
    n_diffim=len(boundaries)
    boundaries=[0]+list(boundaries)
    means=np.zeros((n_diffim,n_cells))
    stds=np.zeros((n_diffim,n_cells))
    for i_im in range(n_diffim):
        means[i_im,:]=orderedresps[boundaries[i_im]:boundaries[i_im+1],:].mean(axis=0)
        stds[i_im,:]=orderedresps[boundaries[i_im]:boundaries[i_im+1],:].std(axis=0)
    plt.figure()
    #axh=plt.errorbar(range(n_diffim),means,yerr=stds) 
    for icell in range(n_cells):
        axh=plt.errorbar(range(n_diffim),means[:,icell],yerr=stds[:,icell])
    return  axh
        
            
def plotTimeSeries(datamat,dt,visiblerange,superimpose=None,norm_fact=0.1,stamp_lists=[],plottype='lines',title='',xleg=None):
    """
    Plot the times series provided in datamat (time x cells) with a slider for better visualization
    dt: frame duration (to display the correct time axis)  
    stamps: positions of the vertical lines to draw (ie to indicate image presentations) 
    """
    colors='bgrcmyk'           
    plt.figure()
    plthandle=plt.subplot2grid((40,1),(0,0),rowspan=37,title=title)
    norm_mat=(datamat-datamat.mean(axis=0))
    norm_mat=norm_mat*norm_fact/norm_mat.std(axis=0) 
    nt,ns=norm_mat.shape
    if plottype=='lines':
        for i in range(ns):
            plt.plot(np.arange(nt)*dt,norm_mat[:,i]+i,colors[i%len(colors)])
        if superimpose!=None:  
            norm_supmat=(superimpose-superimpose.mean(axis=0))
            norm_supmat=norm_supmat*norm_fact/datamat.std(axis=0) 
            for i in range(ns):
                plt.plot(np.arange(nt)*dt,norm_supmat[:,i]+i+0.3,colors[i%len(colors)],alpha=0.5)
        stampcolor='k'
        stampwidth=1        
    elif plottype=='colormap':  
        plt.imshow(norm_mat.T,interpolation='none',vmin=-0.2,vmax=0.2,extent=[0,nt*dt,0,ns],aspect='auto')  
        #plt.xticks(np.arange(0,nt,100),np.arange(0,nt,100)*dt)
        stampcolor='k' 
        stampwidth=2
    for iSL in range(len(stamp_lists)):    
        addStamps(stamp_lists[iSL],color=stampcolor,linewidth=stampwidth*(1+iSL))
    plt.axis([0,min(visiblerange,nt*dt),-1,ns+1])
    if nt*dt>visiblerange:
        slax=plt.subplot2grid((40,1),(39,0))
        slider = Slider(slax, 'x', 0, nt*dt-visiblerange)    
        slider.on_changed(lambda val: sliderUpdate(val,plthandle,visiblerange))
    else:
        slider=None
    if xleg!=None:
        plthandle.set_xticks(xleg['pos'])
        plthandle.set_xticklabels(xleg['names'],rotation='vertical')
    return slider, plthandle   
    
    
def sliderUpdate(val,ax,visiblerange):
    ax.axis((val,val+visiblerange)+ax.axis()[2:4])
    
    
def grossSpikeDetection2d(signal,threshold):
    crossedthr=signal.copy()>threshold
    crossedthr = ( crossedthr & ~np.concatenate([np.zeros((1,signal.shape[1])).astype(bool), crossedthr[:-1,:]]) )
    return crossedthr
    
def pseudoFrate2d(calciumsig,threshold,imNbins):
    spikes=grossSpikeDetection2d(calciumsig,threshold)
    return spikes.reshape(-1,imNbins,spikes.shape[1]).sum(axis=1)
    

def findPeaks(mean_traces):
    """
    Returns position of the peak for each provided signal
    mean_traces: time x cells
    """    
    peakvals=mean_traces.max(axis=0)
    peakpos=np.where((mean_traces==peakvals).T)
    assert np.all(peakpos[0]==np.arange(mean_traces.shape[1]))
    return peakpos[1]
 
 
def restrictToRange(signal,interval_centers,interval_width=1):
    """ 
    Using peak positions for each cell, restrict the signal to a interval around the peak for each cell
    """
    assert len(interval_centers)==signal.shape[1]
    interval_centers=np.array(interval_centers).reshape(-1,1)
    intervals=np.concatenate([np.floor(interval_centers-interval_width/2).astype(int),np.floor(interval_centers+interval_width/2).astype(int)+np.mod(interval_width,2)],axis=1)
    under_zero=intervals[:,0]<0
    if any(under_zero):   
        intervals[under_zero,:]-=intervals[under_zero,0].reshape(-1,1)  
    over_max=intervals[:,1]>signal.shape[0]    
    if any(over_max):    
        intervals[over_max,:]-=(intervals[over_max,1]-signal.shape[0]).reshape(-1,1)
    restricted_signal=np.zeros((interval_width,)+signal.shape[1:])
    for i in range(len(interval_centers)):
        restricted_signal[:,i]=signal[intervals[i,0]:intervals[i,1],i]
    return restricted_signal    
    
    
def preProcessSignal(resps,dt,lowpass_tau=0,baseline_mode=('min',3),deconv_tau=0):
    """
    Preprocesses the raw signal: delta F over F and deconvolution
    """  
    if lowpass_tau>0:
        resps=gaussianLowpass(resps,int(np.round(lowpass_tau*1./dt)),dim=0)
    if baseline_mode[0]=='min':
        baseline=slidingMin(resps,window=baseline_mode[1])
    elif baseline_mode[0]=='blank':
        baseline=computeBlankResps(resps,baseline_mode[1])
    resps=deltaFoverF(resps,baseline)
    if deconv_tau>0:
        resps=deconvolve(resps,dt,tau=deconv_tau)
    return resps    
    
    
def reshapeStim(stim,downsplfact=1,normalize=False,include_blanks=False):
    """
    Takes a stimulus matrix with shape time x ni x ni, downsamples it in both spatial dimension
    (divides the number of pixels along one dimension by downsamplefact), and reshapes it into time x all_pixels
    """    
    if downsplfact!=1:
        stim=downsample(downsample(stim,2,downsplfact),1,downsplfact) 
    frameshape=stim.shape[1:]
    stim=stim.reshape(stim.shape[0],-1)     
    if normalize:
        stim=normalizeMat(stim)
    if include_blanks:
        stim=addBlanks(stim,include_blanks)    
    return stim, frameshape    
        
 
def plotSignals(signals,n_im,subep_duration,ep_iblank,subep_iblank,subep_fblank,dt,expcond_seqs={},comparison_signals=None,plottype='lines',norm_fact=1,title=''):
    n_bins,n_cells,n_eps=signals.shape 
    if plottype=='lines':
        n_plotted=min(6,n_eps*n_im)
    elif plottype=='colormap':
        n_plotted=min(20,n_eps*n_im)
    if comparison_signals!=None:
        comparison_signals=np.rollaxis(comparison_signals,2).reshape(-1,n_cells)
    im_stamps=np.array([0,subep_iblank,subep_duration-subep_fblank,subep_duration])
    subep_stamps=np.array([ep_iblank+im_stamps+subep_duration*i for i in range(n_im)]).reshape(-1)
    #subep_stamps=np.arange(n_im+1)*subep_duration+ep_iblank
    all_stamps=np.array([subep_stamps+n_bins*dt*i for i in range(n_eps)]).reshape(-1)
    epstamps=np.arange(0,n_bins*dt*n_eps,n_bins*dt)
    expcond_stamps=np.array(list(set(np.array([expcond_seqs[key] for key in sorted(expcond_seqs.keys())]).reshape(-1))))*n_bins*dt
    #slider,axh=plotTimeSeries(np.rollaxis(signals,2).reshape(-1,n_cells),dt,all_imstamps,range(n_cells),n_bins*dt*4,thick_stamps=epstamps,superimpose=comparison_signals,plottype=plottype,norm_fact=norm_fact)
    slider,axh=plotTimeSeries(np.rollaxis(signals,2).reshape(-1,n_cells),dt,subep_duration*n_plotted,stamp_lists=[all_stamps,epstamps,expcond_stamps],superimpose=comparison_signals,plottype=plottype,norm_fact=norm_fact,title=title)
    return slider, axh 
   
   
def circVar(deg_orivals,ori_resps):
    n_ori,n_cells=ori_resps.shape
    rad_orivals=np.array(deg_orivals)*np.pi/180
    ori_vects=np.repeat(np.array([np.cos(rad_orivals*2),np.sin(rad_orivals*2)]).T.reshape(n_ori,1,2),n_cells,axis=1)
    vect_sum=(ori_vects*ori_resps.reshape(ori_resps.shape+(1,))).sum(axis=0) # cell num x 2
    vect_norm=np.sqrt((vect_sum**2).sum(axis=1))
    return 1-vect_norm*1./np.abs(ori_resps).sum(axis=0) # Take absolute value of response for normalization -> negative responses are turned into positive responses to the orthogonal orientation
    
    
def tuningCurve(stim_vals,resps,blank_val=np.nan):
    n_cells=resps.shape[1]
    if np.isnan(blank_val):
        blank_pos=np.isnan(stim_vals)
    else:
        blank_pos=(stim_vals==blank_val)
    if len(blank_pos)>0:  
        blank_resp=resps[blank_pos,:].mean(axis=0)
    else:
        blank_resp=np.zeros(n_cells)
    sorted_vals=sorted(list(set(stim_vals[~blank_pos])))
    n_vals=len(sorted_vals)    
    tuning_curves=np.zeros((n_vals,n_cells,2))

    for ival in range(n_vals):
        val_resps=resps[stim_vals==sorted_vals[ival],:]
        tuning_curves[ival,:,0]=val_resps.mean(axis=0)-blank_resp
        tuning_curves[ival,:,1]=val_resps.std(axis=0)    
    
    return tuning_curves   
    