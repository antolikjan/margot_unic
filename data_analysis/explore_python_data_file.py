from scipy.io import loadmat
import numpy as np
from os.path import getsize
from utils.handlebinaryfiles import binary_to_dict

# Data file can be either .mat or binary
# The program will load it to a dictionary, and display all keys and information about their content

#data_file = '/home/margot/Bureau/temp_data/jan2_mai_2019/1718_CXRIGHT_TUN2_spksort180523.mat'
#data_file = '/home/margot/Bureau/temp_data/for_jan_sept2018/1718_CXRIGHT_TUN2/1718_CXRIGHT_TUN2_spksort180523.mat'
#data_file = '/home/margot/Bureau/temp_data/1718_CXRIGHT_TUN2_spksort180523.mat'
#data_file = '/media/margot/DATA/Margot/NON_SYNCED_FILES/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_all_raw.mat'
#data_file = '/media/margot/DATA/Margot/SYNCED_FILES/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_all.mat'
data_file = '/home/margot/Bureau/temp_data/jan2_mai_2019/1718_CXRIGHT_TUN2_spksort180523.dat'


print '\n' + data_file
print 'size : ' + str(np.round(getsize(data_file)/1000000.,3)) + ' Mo'


data_dict = loadmat(data_file) if (data_file.split('.')[-1]=='mat') else binary_to_dict(data_file,data_file.split('.')[0]+'.txt')
data_dict = {key : data_dict[key] for key in data_dict.keys() if ((len(key)<4) or ((key[:2]!='__') or key[-2:]!='__'))}

for key in sorted(data_dict.keys()) :
    print '\n' + key
    
    # if variable is a list/array of one single element, keep that element instead
    if (type(data_dict[key]) is np.ndarray) and np.all([num<=1 for num in data_dict[key].shape]) and np.any([num==1 for num in data_dict[key].shape]) :
       data_dict[key]=data_dict[key].flatten()[0]
    if (type(data_dict[key]) in [list, tuple]) and len(data_dict[key])==1:
       data_dict[key]=data_dict[key][0]  
       
    print type(data_dict[key])
    
    # print shape if variable is a list/tuple/array, print variable itself otherwise
    if type(data_dict[key]) is np.ndarray :
        print 'shape : ' + str(data_dict[key].shape)
    elif type(data_dict[key]) in [list, tuple] :
        print 'length : ' + str(len(data_dict[key]))
    else :
        print data_dict[key]
    
    # specific to neuronal data files
    if key in ['stimulus', 'responses'] :
        shapes=[s.shape for s in data_dict[key].flatten()]
        print 'episode shapes : ' + str(list(set(shapes)))