from scipy.io import loadmat
import numpy as np
import itertools, h5py, glob
from miscellaneous.convertVideo import loadTexMovie, downsample
import matplotlib.pyplot as plt
from warnings import warn
from utils.various_tools import list_repeats
from os.path import isfile
from utils.handlebinaryfiles import load_info_table, binary_to_dict


def load_elphyspikes_matfmt(filename,ep_tmax=None):
    assert (isinstance(filename,str) and (filename[-4:]=='.mat'))
    data=loadmat(filename)
    for key in ['__globals__', '__header__', '__version__']:
        del data[key]
    labels=extractLabels(data.keys(),['Ep','Vspk','Vu'],sep='_')
    units=np.array(list(set([tuple(pair) for pair in labels[:,1:]])))
    n_episodes=labels[:,0].max()
 
    spikelist={}
    for u in units:
        spikelist[tuple(u)]=[]
        for iep in range(n_episodes):
            name='Ep'+str(iep+1)+'_Vspk'+str(u[0])+'_Vu'+str(u[1])
            if name in data.keys():
                spikelist[tuple(u)]+=list(data[name].reshape(-1)+ep_tmax*iep)     
        spikelist[tuple(u)]=np.array(spikelist[tuple(u)])        
        
    return spikelist, n_episodes#, data, units, labels
    
    
def loadKlustaSpikes(filename):
    with h5py.File(filename,'r') as kf:
        allspikes=np.array(kf['channel_groups/0/spikes/time_samples'][:])
        cluster_labels=np.array(kf['channel_groups/0/spikes/clusters/main'][:])
        cluster_list=np.array(list(set(cluster_labels)))
        group_labels=np.array([kf['channel_groups/0/clusters/main/'+str(c)].attrs['cluster_group'] for c in cluster_list])
        group_dict={kf['channel_groups/0/cluster_groups/main/'+str(g)].attrs['name'][0]: cluster_list[group_labels==g] for g in group_labels}
        for expected_key in ['Good','MUA']:
            if not(group_dict.has_key(expected_key)):
                group_dict[expected_key]=np.array([])
    spike_list={c: allspikes[cluster_labels==c] for c in list(group_dict['Good'])+list(group_dict['MUA'])}
    return spike_list, group_dict  
    
    
def spikelist_to_frate(spikedict,ep_lengths,bin_width,vtag_list,valid_eps=None,unit_order=None,theo_lengths=None):
    n_ep=len(vtag_list)
    if isinstance(ep_lengths,int):
        ep_lengths=np.zeros(n_ep).astype(int)+ep_lengths
    if theo_lengths is None:
        theo_lengths=ep_lengths.copy()
    len_groups,temp=list_repeats(theo_lengths)    
    len_groups.update(temp)
    del temp
    unique_lengths=len_groups.keys()
    if valid_eps is None:
        valid_eps=np.ones(n_ep).astype(bool)
    init_blanks=np.array([vt[0] for vt in vtag_list])
    init_nbins_dict={l: int(init_blanks[len_groups[l]][valid_eps[len_groups[l]]].min()*1./bin_width) for l in unique_lengths}
    init_nbins_vec=np.array([init_nbins_dict[l] for l in theo_lengths])
    init_keptblank={l: init_nbins_dict[l]*bin_width for l in unique_lengths}
    vtag_list=[np.array(vtag_list[iep])-(init_blanks[iep]-init_keptblank[theo_lengths[iep]]) for iep in range(n_ep)]
    ep_nbins_dict={l: int((l-init_blanks[len_groups[l]][valid_eps[len_groups[l]]].max())*1./bin_width)+init_nbins_dict[l] for l in unique_lengths}
    ep_nbins_vec=np.array([ep_nbins_dict[l] for l in theo_lengths])
    if unit_order is None:
        unit_order=sorted(spikedict.keys())
    n_units=len(unit_order)
    rates=np.zeros((ep_nbins_vec.sum(),n_units))
    control_bins=np.zeros((ep_nbins_vec.sum()+1))
    ep_cuts=np.zeros((n_ep,2))  
    for iunit in range(n_units):
        new_spikelist=[]
        for iep in range(n_ep):
            # On coupe plus ou moins du debut de chaque episode de maniere a recaler tous les debuts de stim (au cas ou ils ne le seraient pas suite a bug dans elphy)
            # On coupe aussi eventuellement un petit bout a la fin pour que ca tombe juste       
            # Note : si l'ep est plus petit que sa longueur theorique, on complete avec des zeros a la fin du vecteur rates
            #belongs_toep=(spikedict[unit_order[iunit]]>=iep*ep_nbins*bin_width) & (spikedict[unit_order[iunit]]<(iep+1)*ep_nbins*bin_width)
            ep_cuts[iep,0]=init_blanks[iep]-init_keptblank[theo_lengths[iep]]
            ep_cuts[iep,1]=ep_lengths[iep]-ep_cuts[iep,0]-ep_nbins_vec[iep]*bin_width
            valid_cuts=[max(ep_cuts[iep,i],0) for i in range(2)]
            #assert (ep_cuts[iep,0]>=0) and (ep_cuts[iep,1]>=0)
            belongs_toep=(spikedict[unit_order[iunit]]>=ep_lengths[:iep].sum()+valid_cuts[0]) & (spikedict[unit_order[iunit]]<ep_lengths[:iep+1].sum()-valid_cuts[1])
            new_spikelist+=list(spikedict[unit_order[iunit]][belongs_toep]-(ep_lengths[:iep].sum()+ep_cuts[iep,0])+ep_nbins_vec[:iep].sum()*bin_width)
            #new_spikelist+=list(spikedict[unit_order[iunit]][belongs_toep]-(init_blanks[iep]-init_keptblank))
            #rate_mat[iep,:,iunit],control_bins[iep]=np.histogram(spikedict[unit_order[iunit]],bins=ep_nbins,range=(init_blanks[iep]-init_keptblank,ep_length*n_ep))
        spikedict[unit_order[iunit]]=np.array(new_spikelist)    
        rates[:,iunit],control_bins=np.histogram(spikedict[unit_order[iunit]],bins=ep_nbins_vec.sum(),range=(0,ep_nbins_vec.sum()*bin_width))

    rates=[rates[ep_nbins_vec[:iep].sum():ep_nbins_vec[:iep+1].sum()] for iep in range(n_ep)]
    for iep in np.where(ep_cuts[:,0]<0)[0]:
        rates[iep][:int(np.ceil(-ep_cuts[iep,0]/bin_width)),:]=np.nan
    for iep in np.where(ep_cuts[:,1]<0)[0]:
        rates[iep][-int(np.ceil(-ep_cuts[iep,1]/bin_width)):,:]=np.nan    
        
    return rates, spikedict, vtag_list, init_nbins_vec, ep_nbins_vec, ep_cuts
    
    
def which_singleunit(unitlist):
	return [i for i in range(len(unitlist)) if unitlist[i][1]!=1]
    
    
def extractLabels(datakeys,toextract,sep='_'):
    n_labels=len(toextract)
    labels=np.zeros((len(datakeys),n_labels)).astype(int)
    for ikey in range(len(datakeys)):
        chunks=datakeys[ikey].split(sep)
        assert len(chunks)==n_labels
        for ichunk in range(n_labels):
            labels[ikey,ichunk]=np.int(chunks[ichunk][len(toextract[ichunk]):])
    return labels  


def constructVecNames(labelvals,labelnames,sep='_'):
    assert labelvals.shape[1]==len(labelnames)
    vecnames=[]
    for ivec in range(labelvals.shape[0]):
        vecnames.append(sep.join([labelnames[i]+str(labelvals[ivec,i]) for i in range(len(labelnames))]))
    return vecnames     


def loadTexStim(movielist,downsample_fact=None,crop_range=[],movie_prefix='naturalmovie800_',ext='.tex',num_type='float64'):
    stim=[]
    full_indices=None
    if movie_prefix[-len(ext):]==ext :
        movie_prefix='_'.join(movie_prefix.split('_')[:-1])+'_'
    if (movielist=='all'):
        movielist=sorted(glob.glob(movie_prefix+'*'+ext)) 
    elif isinstance(movielist[0],int):
        unique_list=sorted(list(set(movielist)))
        full_indices=[unique_list.index(imovie) for imovie in movielist]
        first_movname=glob.glob(movie_prefix+'*'+ext)[0]
        n_zeros=len(first_movname.split('_')[-1])-len(ext)
        movielist=[]
        for imov in unique_list:
            moviename=first_movname[:-len(ext)-n_zeros]+str(imov).zfill(n_zeros)+ext
            movielist.append(moviename if isfile(moviename) else None)
        
    for imovie in range(len(movielist)):
        if movielist[imovie] is not None:
            print 'Loading '+movielist[imovie]
            movie=loadTexMovie(movielist[imovie])
            if len(crop_range)>0:
                movie=movie[:,crop_range[0,0]:crop_range[0,1],crop_range[1,0]:crop_range[1,1]]            
            if downsample_fact>1:
                movie=downsample(downsample(movie,2,downsample_fact),1,downsample_fact)
            if 'int' in str(num_type):
                movie=np.round(movie)
            stim.append(movie.astype(num_type))
        else:
            stim.append(None)
            
    if full_indices is not None:
        return [stim[ind] for ind in full_indices]
    else:
        return stim  
    
    
def loadNoiseMat(filename,eps_tokeep='all'):
 
    data=loadmat(filename)
    for key in data.keys():
        if key[:4]!='stim':
            del data[key]
    if eps_tokeep=='all':        
        labels=extractLabels(data.keys(),['stim'])    
        nEp=labels.max()
        eps_tokeep=np.arange(nEp)
    else:
        nEp=len(eps_tokeep)
    (nDivX,nDivY,nBins)=np.shape(data['stim1'])
    nBinsTot=nBins*nEp
    stimSize=nDivX*nDivY
    
    stims=np.zeros((nBinsTot,stimSize))
    for iEp in range(nEp):
        stims[iEp*nBins:(iEp+1)*nBins,:]=np.reshape(data['stim'+str(eps_tokeep[iEp]+1)],(stimSize,nBins)).T
    
    return stims


def duplicateSpikes(spikelist):
    units=sorted(spikelist.keys())
    n_units=len(units)
    dupl_mat=np.zeros((n_units,n_units))
 
    for iunit1 in range(n_units):
        for iunit2 in range(n_units):
            dupl_mat[iunit1,iunit2]=len(set(spikelist[units[iunit1]]).intersection(spikelist[units[iunit2]]))*1./len(set(spikelist[units[iunit1]]))
	
    return dupl_mat	
    
    
def duplicateSpikes_slowmethod(spikelist,tolerance=0,plotfig=True):
    units=sorted(spikelist.keys())
    n_units=len(units)
    dupl_mat=np.zeros((n_units,n_units))
 
    for iunit1 in range(n_units):
        for iunit2 in range(n_units):
            distmat=np.abs(np.repeat(np.array(spikelist[units[iunit1]]).reshape(-1,1),len(spikelist[units[iunit2]]),axis=1)-np.array(spikelist[units[iunit2]]).reshape(1,-1))
            dupl_mat[iunit1,iunit2]=np.any(distmat<=tolerance,axis=1).sum()*1./len(spikelist[units[iunit1]])
            
    plt.figure()
    plt.pcolor(dupl_mat)    
    plt.axes().set_aspect('equal')   
    legends=list(itertools.chain(*[['']]+[[str(leg),''] for leg in units]))
    plt.xticks(np.arange(len(legends))*0.5,legends)
    plt.yticks(np.arange(len(legends))*0.5,legends)
	
    return dupl_mat	    
	

def load_vtags_matfmt(filename,factor=30):
    # Vtags are assumed to be stored in ms.
    # By default (factor=30), they will be converted to 1./30 ms (= sample number since Vtags are assumed to be recorded at 30 kHz).
    data=loadmat(filename)
    for key in ['__globals__', '__header__', '__version__']:
        del data[key]
    labels=extractLabels(data.keys(),['Ep','Vtag'],sep='_')
    n_episodes,n_Vtags=labels.max(axis=0)    
    Vtags=[]   
    
    for iTag in range(n_Vtags):
    	Vtags.append([])        
        for ep in range(n_episodes):
            if [iTag+1,ep+1] in labels:
                Vtags[iTag].append(np.array(data['Ep'+str(ep+1)+'_Vtag'+str(iTag+1)]).reshape(-1)*factor)
            else: 
                Vtags[iTag].append([])
            
    return Vtags
	

def checkVtags(Vtag_list,time_unit=1./30000):
    nEp=len(Vtag_list)
    dt_errors=np.zeros(nEp).astype(bool)
    t0_errors=np.zeros(nEp).astype(bool)
    valid_dts=[]
    invalid_dts=[]
    
    for iEp in range(nEp):
        dts=np.diff(np.array(Vtag_list[iEp]).reshape(-1))
        if dts.max()>1.05*dts.min():
            dt_errors[iEp]=True
            invalid_dts+=list(dts)
        else:    
            valid_dts+=list(dts)
        
    if len(valid_dts)==0:
        dtON=np.median(invalid_dts)
    else:
        dtON=np.array(valid_dts).mean() 
        
    t0_list=np.array([Vtag_list[iEp][0] for iEp in range(nEp)])
    t0_theo=np.median(t0_list)
    t0_errors = (np.abs(t0_list-t0_theo)/t0_theo>0.3)

    if np.any(dt_errors):
        warn('Found irregular Vtags : '+','.join(np.where(dt_errors)[0].astype('str')))
        
    if np.any(t0_errors):
        warn('Found Vtags with anormal start times : '+','.join(np.where(t0_errors)[0].astype('str')))    
    
    lengths=list(set([len(v) for v in Vtag_list]))  
    if len(lengths)>1:
        warn('Length differs between Vtags: '+','.join(np.array(lengths).astype('str')))
                  
    return dtON*time_unit, dt_errors, t0_theo, t0_errors


def reconstructStim(stim_mat,Vtags,ep_bins,num_type='float64'):
    nEp=len(Vtags)
    bin_width=ep_bins[1]
    ep_nbins=len(ep_bins)-1
    new_mat=np.zeros((nEp,ep_nbins)+stim_mat.shape[2:],dtype=num_type)
    for iep in range(nEp):
        current_frame=0
        theo_dton=np.median(np.diff(Vtags[iep]))
        tags=np.array(list(Vtags[iep])+[Vtags[iep][-1]+theo_dton]) 
        for ibin in range(ep_nbins):
            frame_changes=tags[(tags>=ep_bins[ibin])&(tags<ep_bins[ibin+1])]
            proportions=np.diff(np.array([ep_bins[ibin]]+list(frame_changes)+[ep_bins[ibin+1]]))*1./bin_width
            new_mat[iep,ibin]=(stim_mat[iep,range(current_frame,current_frame+len(proportions))]*proportions.reshape((-1,)+(1,)*len(stim_mat.shape[2:]))).sum(axis=0)
            current_frame+=len(proportions)-1
    return new_mat            


def detectPeaks(signal,thresh):
    cross_inds=np.where((signal[1:]>=thresh) & (signal[-1]<thresh))[0]+1
    peak_vals=np.array([signal[cross_inds[i]:cross_inds[i+1]].max() for i in range(len(cross_inds)-1)]+[signal[cross_inds[-1:]].max()])   
    return cross_inds[peak_vals.argsort()]
    
    
def stimFourier(stim,ntau,frameshape):
    stim=stim[:].reshape((-1,ntau)+tuple(frameshape))
    return np.array([np.fft.fftshift(np.fft.fftn(stim[t,:,:])) for t in range(stim.shape[0])]).reshape(-1,ntau*np.prod(frameshape))
    
    
def plotby_cellandstim(val_means,val_errs=None,cell_labels=None,bold_cells=None,stim_labels=None,x=None,x_label='',ncells_per_fig=10,stim_colors='rgbycmk',cell_colors='rgbycmk',alpha=0.1,linewidth=2,same_scale=False,figsize=(5,10)):# stim, time, cell
    n_stims,n_x,n_cells=val_means.shape
    if same_scale:
        val_min=np.nanmin(val_means)
        val_max=np.nanmax(val_means)
    if x is None:
        x=range(n_x)
    if cell_labels is None:
        cell_labels=range(n_cells)
    if bold_cells is None:
        bold_cells=np.zeros(n_cells).astype(bool)
    if stim_labels is None:
        stim_labels=range(n_stims)    
  
    # One plot per cell, one curve per stim_type
    count=ncells_per_fig      
    for icell in range(n_cells):
        if count==ncells_per_fig:
            plt.figure(figsize=figsize)
            count=0
        ax=plt.subplot(ncells_per_fig,1,count+1)
        leg_items=[]
        for istim in range(n_stims):
            leg_items.append(plt.plot(x,val_means[istim,:,icell],color=stim_colors[istim%len(stim_colors)],linewidth=linewidth)[0])
            if val_errs is not None:
                plt.fill_between(x,val_means[istim,:,icell]-val_errs[istim,:,icell],val_means[istim,:,icell]+val_errs[istim,:,icell],alpha=alpha,facecolor=stim_colors[istim%len(stim_colors)])
        if same_scale:
            y_scale=(val_min-0.1*np.abs(val_min),val_max+0.1*np.abs(val_max))
        else:
            y_scale=(np.nanmin(val_means[:,:,icell])-0.1*np.abs(np.nanmin(val_means[:,:,icell])),np.nanmax(val_means[:,:,icell])+0.1*np.abs(np.nanmax(val_means[:,:,icell])))
        plt.axis((x[0],x[-1])+y_scale)
        plt.ylabel(cell_labels[icell],fontweight=('bold' if bold_cells[icell] else None),fontsize=6)
        ax.set_yticks([])
        if count==0:
            plt.legend(leg_items,stim_labels)
        if count==ncells_per_fig-1:
            plt.xlabel(x_label)
        else:
            ax.set_xticks([])
        count+=1  
         
    # One plot per stim_type, one curve per cell
    plt.figure(figsize=figsize)
    for istim in range(n_stims):
        ax=plt.subplot(n_stims,1,istim+1)
        leg_items=[]
        for icell in range(n_cells):
            leg_items.append(plt.plot(x,val_means[istim,:,icell],color=cell_colors[icell%len(cell_colors)],linewidth=linewidth)[0])
            if val_errs is not None:
                plt.fill_between(x,val_means[istim,:,icell]-val_errs[istim,:,icell],val_means[istim,:,icell]+val_errs[istim,:,icell],alpha=alpha,facecolor=cell_colors[icell%len(cell_colors)])
        if same_scale:
            y_scale=(val_min-0.1*np.abs(val_min),val_max+0.1*np.abs(val_max))
        else:
            y_scale=(np.nanmin(val_means[istim,:,:])-0.1*np.abs(np.nanmin(val_means[istim,:,:])),np.nanmax(val_means[istim,:,:])+0.1*np.abs(np.nanmax(val_means[istim,:,:])))
        plt.axis((x[0],x[-1])+y_scale)
        plt.ylabel(stim_labels[istim],fontsize=6)
        ax.set_yticks([])
        if count==0:
            plt.legend(leg_items,cell_labels)
        if count==n_stims:
            plt.xlabel(x_label)
        else:
            ax.set_xticks([])   
    
    # Single plot, average over cells, one curve per stim    
    plt.figure(figsize=figsize)
    plt.subplot(2,1,1)
    stim_means=np.nanmean(val_means,axis=2)
    stim_sems=np.nanstd(val_means,axis=2)*2./np.sqrt(n_cells)
    leg_items=[]
    for istim in range(n_stims):
        leg_items.append(plt.plot(x,stim_means[istim],color=stim_colors[istim%len(stim_colors)],linewidth=linewidth)[0])
        plt.fill_between(x,stim_means[istim]-stim_sems[istim],stim_means[istim]+stim_sems[istim],alpha=alpha,facecolor=stim_colors[istim%len(stim_colors)])
    plt.legend(leg_items,stim_labels)
    plt.xlabel(x_label)
    plt.axis((x[0],x[-1])+plt.axis()[2:])
    
    # Single plot, average over stims, one curve per cell
    plt.subplot(2,1,2)
    cell_means=np.nanmean(val_means,axis=0)
    cell_sems=np.nanstd(val_means,axis=0)*2./np.sqrt(n_stims)
    leg_items=[]
    for icell in range(n_cells):
        leg_items.append(plt.plot(x,cell_means[:,icell],color=cell_colors[icell%len(cell_colors)],linewidth=linewidth)[0])
        plt.fill_between(x,cell_means[:,icell]-cell_sems[:,icell],cell_means[:,icell]+cell_sems[:,icell],alpha=alpha,facecolor=cell_colors[icell%len(cell_colors)])
    #plt.legend(leg_items,cell_labels)
    plt.xlabel(x_label)
    plt.axis((x[0],x[-1])+plt.axis()[2:])


def compute_PSTHs(spike_list,ep_durs,bin_size=0,margins=[0,0]):
    n_cells=len(spike_list)
    ep_nbins=((ep_durs-np.sum(margins))/bin_size).astype(int)
    bin_edges=[np.arange(nbins+1)*bin_size+margins[0] for nbins in ep_nbins]
    PSTHs=[[]]*len(ep_durs)
    for iep in range(len(ep_durs)):
        PSTHs[iep]=np.zeros([ep_nbins[iep],n_cells])
        for icell in range(n_cells):
            PSTHs[iep][:,icell]=np.histogram(spike_list[icell],bin_edges[iep]+ep_durs[:iep].sum())[0]
    return PSTHs, [0.5*(edges[:-1]+edges[1:]) for edges in bin_edges]


def max_possible_crosscorr(repeated_resps):
    # Repeated_resps : n_trials x time x n_cells
    n=repeated_resps.shape[0]
    assert n>1
    if repeated_resps.ndim<3:
        repeated_resps=repeated_resps.reshape(n,-1,1)
    resps_vars=np.nanvar(repeated_resps,axis=1)
    summed_resp=repeated_resps.sum(axis=0)
    return 1./np.sqrt( 1 - 1./n + ((1-1./n)*resps_vars.sum(axis=0)) / (np.nanvar(summed_resp,axis=0)-resps_vars.sum(axis=0)) ).flatten()


def barplot(dataset,reference_values=[None,None],labels=[None,None,None],factor2_colors=None,single_vals_color=None,single_vals_alphas=None,title='',error_bars=False):
    assert dataset.ndim in [2,3]
    if dataset.ndim==2:
        n1,n_data=dataset.shape
        n2=1
        dataset=dataset.reshape(n1,n2,n_data).copy()        
    else:  
        n1,n2,n_data=dataset.shape        
    if factor2_colors is None:
        factor2_colors='rgbycmk' 
    if labels is None:
        labels=[None,None,None]
    if (single_vals_color is not None) and (single_vals_alphas is None):
        single_vals_alphas=[0.8]*n_data
    
    x_positions=np.linspace(-0.4,0.4,n2+1)
    x_width=x_positions[1]-x_positions[0]
    x_positions+=0.5*x_width
    data_mean=np.nanmean(dataset,axis=2)
    data_sem=np.nanstd(dataset,axis=2)*2./np.sqrt(np.sum(~np.isnan(dataset),axis=2))
    fig=plt.figure()
    leg_items=[]

    for i2 in range(n2): 
       err_bar = data_sem[:,i2] if error_bars else None
       leg_items.append(plt.bar(np.arange(n1)+x_positions[i2], data_mean[:,i2], x_width, yerr=err_bar, color=factor2_colors[i2%len(factor2_colors)], edgecolor=factor2_colors[i2%len(factor2_colors)], linewidth=3))
    if reference_values[0] is not None:
       plt.bar(np.arange(n1), reference_values[0], x_positions[-1]-x_positions[0], color='none', edgecolor='k', linewidth=3)
    if (len(reference_values)>1) and (reference_values[1] is not None):
       for i2 in range(n2):
           plt.bar(np.arange(n1)+x_positions[i2], [reference_values[1][i2]]*n1, x_width, color='none', edgecolor=factor2_colors[i2%len(factor2_colors)], linewidth=3)
#    for i2 in range(n2):              
#       leg_items.append(plt.bar(np.arange(n1)+x_positions[i2], data_mean[:,i2], x_width, color=factor2_colors[i2%len(factor2_colors)], edgecolor=factor2_colors[i2%len(factor2_colors)], linewidth=3))
    if single_vals_color is not None:
       single_vals=dataset.mean(axis=1)
       for ival in range(n_data):
           plt.plot(np.arange(n1), single_vals[:,ival], color=single_vals_color, alpha=single_vals_alphas[ival]) 
    
    plt.axis([-1,n1]+list(plt.axis()[2:]))
    if labels[0] is not None:    
        plt.xticks(np.arange(n1),labels[0])  
        plt.tick_params(labelsize=20)
    if labels[1] is not None:     
        plt.legend(leg_items,labels[1])     
    for side in plt.gca().spines.itervalues():
        side.set_linewidth(3)
    if labels[2] is not None:    
        plt.ylabel(labels[2],fontsize=20)  
    plt.title(title)    
        
    return fig


def lineplot(dataset,labels=[None,None,None],factor2_colors=None,single_vals_alpha=0.5,title=''):
    assert dataset.ndim in [2,3]
    if dataset.ndim==2:
        n1,n_data=dataset.shape
        n2=1
        dataset=dataset.reshape(n1,n2,n_data).copy()        
    else:  
        n1,n2,n_data=dataset.shape        
    if factor2_colors is None:
        factor2_colors='rgbycmk' 
    if labels is None:
        labels=[None,None,None]
    
    data_mean=np.nanmean(dataset,axis=2)
    data_std=np.nanstd(dataset,axis=2)
    fig=plt.figure()
    leg_items=[]


    for i2 in range(n2):              
       leg_items.append(plt.errorbar(range(n1),data_mean[:,i2], yerr=2*data_std[:,i2]*1./np.sqrt(n_data), color=factor2_colors[i2%len(factor2_colors)], linewidth=3))
       if single_vals_alpha is not None:
           plt.plot(dataset[:,i2], color=factor2_colors[i2%len(factor2_colors)], alpha=single_vals_alpha) 
    
    plt.axis([-1,n1]+list(plt.axis()[2:]))
    if labels[0] is not None:    
        plt.xticks(np.arange(n1),labels[0])  
        plt.tick_params(labelsize=20)
    if labels[1] is not None:     
        plt.legend(leg_items,labels[1])     
    for side in plt.gca().spines.itervalues():
        side.set_linewidth(3)
    if labels[2] is not None:    
        plt.ylabel(labels[2],fontsize=20)  
    plt.title(title)    
        
    return fig


#def elphydata_to_mat():
    
def pythondata_to_rfdata(data_file,ep_info_file=None,stim_categories={},file_type='new'):
    
    if file_type=='new':
        ep_info=load_info_table(ep_info_file)
        n_ep=len(ep_info.values()[0])
        if type(stim_categories)==str:
            stim_categories=load_info_table(stim_categories)
        elif len(stim_categories)==0:
            stim_categories={'name':['all'],'fraction':[1],'selection_seed':[0],'stimuli':[','.join(list(set(ep_info['stim_type'])))]}
            
        if data_file.split('.')[-1]=='mat':    
            data_dict=loadmat(data_file)
            data_dict['stimulus']=data_dict['stimulus'].flatten()
            data_dict['responses']=data_dict['responses'].flatten()
        else:
            data_dict=binary_to_dict(data_file,data_file.split('.')[0]+'.txt')
       
        stim={dtype: {} for dtype in stim_categories['name']}
        resps={dtype: {} for dtype in stim_categories['name']}
        reshaped_info={dtype: {} for dtype in stim_categories['name']}
        for icat in range(len(stim_categories['name'])):
            dtype=stim_categories['name'][icat]
            cat_stims=stim_categories['stimuli'][icat].split(',')
            cat_eps=np.array([iep for iep in range(n_ep) if (ep_info['stim_type'][iep] in cat_stims)])
            assert np.all([np.all(ep_info[key][cat_eps]==ep_info[key][cat_eps[0]]) for key in ['stim_npix','theo_nframes']]) # check that various stim properties are the same in a category            
            assert np.all([data_dict['stimulus'][iep].shape==data_dict['stimulus'][cat_eps[0]].shape for iep in cat_eps]) # check that stims all have same size in category
    
            for split_type in set(ep_info['data_splitting']):
                stim[dtype]['raw_'+split_type]=np.array(list(data_dict['stimulus'][cat_eps][ep_info['data_splitting'][cat_eps]==split_type]))
                resps[dtype]['raw_'+split_type]=np.array(list(data_dict['responses'][cat_eps][ep_info['data_splitting'][cat_eps]==split_type]))
                reshaped_info[dtype]['raw_'+split_type]={key: ep_info[key][cat_eps][ep_info['data_splitting'][cat_eps]==split_type] for key in ep_info.keys()}
        return  stim,resps,reshaped_info
    
    else:
        return None



#def merge_data_repeats(stim,resps,stim_ids):
#    stim_repeats=list_repeats(stim_ids)
#    repeated_ids=sorted(stim_repeats.keys())
#    n_rep=np.array([len(stim_repeats[mov_id]) for mov_id in repeated_ids])
#    assert np.all(n_rep==n_rep[0])
#    merged_repeats=itertools.chain(*[stim_repeats[mov_id] for mov_id in repeated_ids])
#    corr_max=max_possible_crosscorr(np.concatenate([np.array([resps[iep] for iep in stim_repeats[mov_id]]) for mov_id in repeated_ids],axis=1))
#    merged_resps=[np.nanmean(np.array([resps[iep] for iep in stim_repeats[mov_id]]),axis=0) for mov_id in merged_repeats]
#    merged_stim=[np.nanmean(np.array([stim[iep] for iep in stim_repeats[mov_id]]),axis=0) for mov_id in merged_repeats]
#    return merged_stim, merged_resps, corr_max
    
    
def merge_data_repeats(stim,resps,stim_ids):
    # stim and resps must already be arrays (same size for all eps) n_eps x n_t x n_pix, n_eps x n_t x n_cells
    stim_repeats=list_repeats(stim_ids)[0]
    repeated_ids=sorted(stim_repeats.keys())
    n_rep=np.array([len(stim_repeats[mov_id]) for mov_id in repeated_ids])
    assert np.all(n_rep==n_rep[0])
    corr_max=max_possible_crosscorr(np.concatenate([resps[stim_repeats[mov_id]] for mov_id in repeated_ids],axis=1))
    merged_resps=np.array([np.nanmean(resps[stim_repeats[mov_id]],axis=0) for mov_id in repeated_ids])
    merged_stim=np.array([np.nanmean(stim[stim_repeats[mov_id]],axis=0) for mov_id in repeated_ids])  
    return merged_stim, merged_resps, corr_max