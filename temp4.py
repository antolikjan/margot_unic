from utils.volterra_kernels import movie_to_BWT

for varname in ['rf_preds','rf_corr','NL_funcs']:
     vars()[varname].update({key: {} for key in set(data_types)-set(vars()[varname].keys())})
for dtype1 in data_types:
    for dtype2 in [dtype1]+list(set(data_types)-set([dtype1])): # data_types but starting with dtype1 (mandatory for NL_funcs computation)
        for varname in ['rf_preds','rf_corr']:
            vars()[varname][dtype1].update({key: {} for key in set(data_types)-set(vars()[varname][dtype1].keys())})
        for rf_type in pm.model_types:
            print 'Computing '+rf_type+' correlation ('+dtype1+' predicts '+dtype2+')'
            rf_preds[dtype1][dtype2][rf_type]={}
            rf_corr[dtype1][dtype2][rf_type]={}
            if rf_type!='HSM':
                if pm.add_nonlinearity:
                    rf_preds[dtype1][dtype2][rf_type+'_NL']={}
                    rf_corr[dtype1][dtype2][rf_type+'_NL']={}
                for dset in ['val']:
                    if stim[dtype2][dset].shape[0]>0:
                        if rf_type=='BWT':
                            stim_BWT=movie_to_BWT(stim[dtype2][dset].reshape([-1]+list(ROIshape[dtype2])), matlab_engine=_matlab_engine).reshape(stim[dtype2][dset].shape)
                            rf_preds[dtype1][dtype2][rf_type][dset]=np.dot(stim_BWT,rfs[dtype1][rf_type][1])
                        else:    
                            rf_preds[dtype1][dtype2][rf_type][dset]=np.dot(stim[dtype2][dset],rfs[dtype1][rf_type][1])
                        if rfs[dtype1][rf_type].has_key(0):
                            rf_preds[dtype1][dtype2][rf_type][dset]+=rfs[dtype1][rf_type][0]
                        if rf_type=='volt2diag':
                            rf_preds[dtype1][dtype2][rf_type][dset]+=np.dot(stim[dtype2][dset]**2,rfs[dtype1][rf_type][2])
                        elif rf_type=='volt2':     
                            rf_preds[dtype1][dtype2][rf_type][dset]+=k2resp(restorek2shape(rfs[dtype1][rf_type][2]),stim[dtype2][dset])
                        elif rf_type=='volt2sig': 
                            rf_preds[dtype1][dtype2][rf_type][dset]+=k2resp(rfs[dtype1][rf_type][2],stim[dtype2][dset]) 
                        rf_corr[dtype1][dtype2][rf_type][dset]=np.array([np.corrcoef(resps[dtype2][dset][:,icell].flatten(),rf_preds[dtype1][dtype2][rf_type][dset][:,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype1]))])      
                        if pm.add_nonlinearity:
                            if (dtype1==dtype2) & (dset=='train'):
                                NL_funcs[dtype1][rf_type]=[estimate_output_nonlinearity(rf_preds[dtype1][dtype1][rf_type]['train'][:,icell],resps[dtype1]['train'][:,icell],n_intervals=15) for icell in range(len(cells_to_keep[dtype1]))]
                            rf_preds[dtype1][dtype2][rf_type+'_NL'][dset]=np.array([NL_funcs[dtype1][rf_type][icell](rf_preds[dtype1][dtype2][rf_type][dset][:,icell]) for icell in range(len(cells_to_keep[dtype1]))]).T
                            rf_corr[dtype1][dtype2][rf_type+'_NL'][dset]=np.array([np.corrcoef(resps[dtype2][dset][:,icell].flatten(),rf_preds[dtype1][dtype2][rf_type+'_NL'][dset][:,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype1]))])
            else:
                for dset in ['val']:
                    if HSM_stim[dtype2][dset].shape[0]>0:
                        rf_preds[dtype1][dtype2]['HSM'][dset]=lscsm[dtype1].response(HSM_stim[dtype2][dset],K[dtype1])
                        rf_corr[dtype1][dtype2]['HSM'][dset]=np.array([np.corrcoef(HSM_resps[dtype2][dset][:,icell].flatten(),rf_preds[dtype1][dtype2]['HSM'][dset][:,icell].flatten())[0,1] for icell in range(len(cells_to_keep[dtype1]))])
            