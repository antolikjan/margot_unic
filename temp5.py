#plt.figure()
#plt.plot([-1,1],[-1,1],'k')
#leg_items=[]
#for dtype in data_types:
#    leg_items.append(plt.plot(rf_corr_withblanks[dtype][dtype]['BWT'][1.]['val'],rf_corr[dtype][dtype]['BWT'][1.]['val'],'o',color=pm.stim_colors[dtype])[0])
#plt.axis([-0.1,0.6,-0.1,0.6])
#plt.legend(leg_items,data_types)
#plt.xlabel('Validation corr (blanks included)')
#plt.ylabel('Validation corr (without blanks)')


#for dtype2 in data_types:
#       figs[global_name[dtype2]+'_corrceiling']=plt.figure()
#       plt.suptitle(dtype2)
#       for irf in range(len(rfs_to_plot)):    
#           plt.subplot(2,len(rfs_to_plot),irf+1)
#           for dtype1 in [dtype2]:#data_types:                   
#               plt.plot(pm.noise_ceiling_vals,[rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=pm.stim_colors[dtype1],linewidth=0.5)
#               fitted_max=rf_corr[dtype1][dtype2][rfs_to_plot[irf]][0][pm.displayed_data]+rf_corr[dtype1][dtype2][rfs_to_plot[irf]]['slope'][pm.displayed_data]*max(pm.noise_ceiling_vals)
#               plt.plot([0,max(pm.noise_ceiling_vals)],np.array([rf_corr[dtype1][dtype2][rfs_to_plot[irf]][0][pm.displayed_data],fitted_max]),color=pm.stim_colors[dtype1])
##                       plt.plot(1./np.array(pm.noise_ceiling_vals),[rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=pm.stim_colors[dtype1],linewidth=0.5)
##                       plt.plot(np.array(pm.noise_ceiling_vals),[rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=pm.stim_colors[dtype1],linewidth=0.5)
##                       plt.plot(np.array(pm.noise_ceiling_vals),[1./rf_corr[dtype1][dtype2][rfs_to_plot[irf]][cv][pm.displayed_data] for cv in pm.noise_ceiling_vals],marker='o',color=pm.stim_colors[dtype1],linewidth=0.5)
#           plt.subplot(2,len(rfs_to_plot),irf+1+len(rfs_to_plot))
#           plt.plot([-0.1,1],[-0.1,1],'k')
#           corr_max=np.nanmax([np.nanmax(rf_corr[dtype1][dtype2][rfs_to_plot[irf]][1][pm.displayed_data]) for dtype1 in data_types]+[np.nanmax(rf_corr[dtype1][dtype2][rfs_to_plot[irf]][0][pm.displayed_data]) for dtype1 in data_types])
#           for dtype1 in data_types:
#               plt.plot(rf_corr[dtype1][dtype2][rfs_to_plot[irf]][1][pm.displayed_data],rf_corr[dtype1][dtype2][rfs_to_plot[irf]][0][pm.displayed_data].flatten(),'o',color=pm.stim_colors[dtype1])
#           plt.axis([-0.1,corr_max,-0.1,corr_max])
           
           
#for dtype2 in data_types:           
#       plt.figure()
#       plt.suptitle(global_name[dtype2]+'_'+rf_type+'_predSTA_mean')
#       leg_items=[]
#       for dtype1 in data_types:
#           vals=np.nanmean(rf_predSTA[dtype1][dtype2][rf_type][cv][pm.displayed_data][::-1,unit_order[dtype2][:pm.n_disp_cells]],axis=1)
#           leg_items.append(plt.plot(range(-predSTA_tau,predSTA_tau+1),vals*1./vals.sum(),color=pm.stim_colors[dtype1])[0])    
#       plt.legend(leg_items,data_types) 
#       plt.ylabel('mean over cells')    
#
data={}
data['HSM_predictions']={dtype1: {dtype2: rf_preds[dtype1][dtype2]['HSM'][1.0]['val'][:,unit_order[dtype2]] for dtype2 in data_types} for dtype1 in data_types}
data['readme']="true_reponses[stim] = responses of the 49 neurons (sorted by reliability) to stimulus stim (averaged over 10 repetitions)\nHSM_predictions[stim1][stim2]=HSM prediction of responses to stim2 when learned on stim1"
data['true_responses']={dtype: resps[dtype]['val'][:,unit_order[dtype]] for dtype in data_types}
from utils.handlebinaryfiles import multiple_pickledump
multiple_pickledump(data,'HSM_predictions.pickle')

#from utils.handlebinaryfiles import multiple_pickleload
#data=multiple_pickleload('HSM_predictions.pickle')