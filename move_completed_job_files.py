import os
import glob

joblist_file='/home/margot/current_jobs.txt'
job_dir='/home/margot/HSM/Explore_params/2817_CXLEFT_TUN21_outputrateadapt_seeds/running_slurm'
done_dir='/home/margot/HSM/Explore_params/2817_CXLEFT_TUN21_outputrateadapt_seeds/done'
bug_dir='/home/margot/HSM/Explore_params/2817_CXLEFT_TUN21_outputrateadapt_seeds/bug'
n_digits=6

log_files=glob.glob(os.path.join(job_dir,'*log.txt'))
os.system('squeue -u margot -t RUNNING > '+joblist_file)
with open(joblist_file,'r') as f:
    lines=f.read().split('\n')
jobnums=[l[1:n_digits+1] for l in lines[1:] if len(l)>0]    

for log_fname in log_files:
    num=log_fname[-n_digits-8:-8]
    with open(log_fname,'r') as f:
        text=f.read()
    if ("Time elapsed" in text):
        if ("Traceback" in text):
            new_dir=bug_dir
        else:
            new_dir=done_dir
        print os.path.basename(log_fname), '>', new_dir    
        related_files=glob.glob(log_fname.split('metaparams')[0]+'*')    
        for fname in related_files:    
            os.rename(fname,os.path.join(new_dir,os.path.basename(fname)))


main_dir='/home/margot/HSM/Explore_params'


os.system('squeue -u margot -t RUNNING > '+joblist_file)
with open(joblist_file,'r') as f:
    lines=f.read().split('\n')
    
for l in lines[1:]:
    if len(l)>0:
        job_num=l[1:n_digits+1]
        matching=[y for x in os.walk(main_dir) for y in glob.glob(os.path.join(x[0],'*'+job_num+'*log.txt'))]
        if len(matching)==0:
            print job_num
    