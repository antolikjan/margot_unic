from elphy_reader import *  # this imports only public symbols
from elphy_reader import _read_string  # import additional private symbol
from scipy import signal

from matplotlib.pyplot import *
import time
import struct

if sys.version_info >= (3,):
    from typing import List, BinaryIO, Any


class ElphyFileMissingData(ElphyFile):
    """
    This class detects missing data when reading en Elphy file for the first
    time, and later on automatically corrects the data it reads before
    returning it.
    """

    def __init__(self, filename, read_data=False, read_spikes=None):
        # First read the file normally
        ElphyFile.__init__(self, filename, read_data, read_spikes)

        # Then detect missing data!
        file_handle = open(filename, 'rb')
        for ep in self.episodes:
            ep._correct_missing_data(file_handle, read_data)

    class Episode(ElphyFile.Episode):

        def __init__(self, ef, episode_num, file_handle, block_end):
            # Read the episode headers normally
            ElphyFile.Episode.__init__(self, ef, episode_num, file_handle, block_end)

            # Additional properties for marking missing data will be set later
            if self._format_option != 1:
                raise Exception('Data is not saved in contiguous mode. '
                                'Missing data detection not implemented.')
            self.nmissing_30khz = self.nmissing_1khz = None
            self.missing_index = self.missing_num = []
            self.missing_index_1khz = self.missing_num_1khz = []

        def _read_rdata(self, file_handle, block_size, read_data):
            # Read data headers normally
            ElphyFile.Episode._read_rdata(self, file_handle, block_size, read_data=False)

            # Then check whether data is missing
            self._check_channels_nsamp()

        def _check_channels_nsamp(self):  # type ElphyFile.Episode -> int, int
            # which are 30kHz or 1kHz channels
            channels_30khz = [ch for ch in self.channels if ch.dxu == 1. / 30]
            channels_1khz = [ch for ch in self.channels if ch.dxu == 1]
            assert np.all(np.array([ch.nbyte for ch in self.channels]) == 2)
            group_sizes = np.array((len(channels_30khz), len(channels_1khz)))  # Number of channels of each type
            assert group_sizes.sum() == self.nbchan

            # expected and actual total number of samples
            theo_nsamp_30khz = np.array([ch.nsamp for ch in channels_30khz])
            theo_nsamp_1khz = np.array([ch.nsamp for ch in channels_1khz])
            assert not np.any(np.diff(theo_nsamp_30khz))
            assert not np.any(np.diff(theo_nsamp_1khz))
            theo_nsamp = np.array((theo_nsamp_30khz[0], theo_nsamp_1khz[0]))  # Theoretical channel lengths
            nbyte = np.array([ch.nbyte for ch in self.channels])
            assert not np.any(np.diff(nbyte))
            actual_nsamp_sum = self.data_size // nbyte[0]  # True sum of all channel lengths

            # Any missing data?
            if actual_nsamp_sum == theo_nsamp.sum():
                # No data missing, we can return
                return 0, 0
            if actual_nsamp_sum > (theo_nsamp * group_sizes).sum():
                raise Exception('Actual episode length > theoretical length ! There must be something wrong...')

            # Channel lengths to test for 30kHz : at most theoretical value,
            # at least length in case type 1kHz is at theoretical value
            tested_nsamp0 = np.arange(int((actual_nsamp_sum - group_sizes[1] * theo_nsamp[1]) / group_sizes[0]), theo_nsamp[0] + 1)
            valid_nsamp0 = tested_nsamp0[(actual_nsamp_sum - group_sizes[0] * tested_nsamp0) % group_sizes[1] == 0]  # Type 0 solutions are the ones whose corresponding type 1 values are integers
            assert len(valid_nsamp0) > 0  # There should be at least one solution !
            valid_nsamp1 = ((actual_nsamp_sum - group_sizes[0] * valid_nsamp0) / group_sizes[1]).astype(int)  # Corresponding solutions for type 1
            if len(valid_nsamp0) > 1:  # If there is more than one solution : take the one for which channel size ratio is closest to its theoretical value
                best_index = np.argmin(np.abs(valid_nsamp0 * 1. / valid_nsamp1 - theo_nsamp[0] * 1. / theo_nsamp[1]))
                valid_nsamp0, valid_nsamp1 = valid_nsamp0[best_index], valid_nsamp1[best_index]
            else:
                valid_nsamp0, valid_nsamp1 = valid_nsamp0[0], valid_nsamp1[0]

            # Store these values
            self.nmissing_30khz = theo_nsamp[0] - valid_nsamp0
            self.nmissing_1khz = theo_nsamp[1] - valid_nsamp1
            for ch in channels_30khz:
                ch._nsamp = valid_nsamp0
                ch._data_size = ch.nsamp * ch.nbyte
                ch.nmissing = self.nmissing_30khz
            for ch in channels_1khz:
                ch._nsamp = valid_nsamp1
                ch._data_size = ch.nsamp * ch.nbyte
                ch.nmissing = self.nmissing_1khz

        def _correct_missing_data(self, file_handle, read_data=False, do_display=False):
            # no missing data?
            if self.nmissing_30khz == 0 and self.nmissing_1khz == 0:
                return

            # get spike times and waves
            spike_times, _, spike_waves = self.get_spikes()
            all_spike_times = np.hstack(spike_times)
            fs = 30000  # signal sampled at 30 kHz
            fsk = fs // 1000  # sampling frequency is kHz
            all_spike_times = np.rint(all_spike_times * fsk).astype(int)  # convert times to signal indices
            all_spike_times = all_spike_times - 1  # make all_spike_times the index at which waveforms start, instead of number of samples before they start
            all_spike_channels = [np.ones(s.shape, dtype=int) * i for i, s in enumerate(spike_times)]
            all_spike_channels = np.hstack(all_spike_channels)
            all_spike_waves = np.vstack(spike_waves)

            # sort spike times
            ord = np.argsort(all_spike_times)
            all_spike_times = all_spike_times[ord]
            all_spike_channels = all_spike_channels[ord]
            all_spike_waves = all_spike_waves[ord, :]  # num x time

            # get data
            x0 = np.array(self.get_data(channels='30kHz'))  # channel x time

            # filter data
            fnyq = fs / 2.  # Nyquist frequency
            fc = 250  # high-pass at 250 Hz
            b, a = signal.butter(4, fc *1./ fnyq, 'high', analog=False, output='ba')
            zi = signal.lfilter_zi(b, a)  # initialize the filter so as to avoid strong initial transient
            zi = zi[np.newaxis, :] * x0[:, :int(fnyq *1./ fc / 2.)].mean(axis=1, keepdims=True)
            x, _ = signal.lfilter(b, a, x0, zi=zi, axis=1)

            # std of data on a running window [TAKES A LONG TIME!]
            nw = all_spike_waves.shape[1]
            box = np.ones((1, nw)) *1./ nw
            xstd = np.sqrt(signal.correlate2d(x ** 2, box, mode='valid')
                           - signal.correlate2d(x, box, mode='valid') ** 2)

            # z-score waveforms
            w = all_spike_waves
            w = (w - w.mean(axis=1, keepdims=True)) *1./ w.std(axis=1, keepdims=True) *1./ nw

            # Detect lags between signal and waveforms
            n_spike = all_spike_times.shape[0]
            t_cur = 0  # current position in signals
            lags = np.zeros(n_spike, dtype=int)
            cur_lag = 0
            for i in range(n_spike):
                t_spike = all_spike_times[i] - cur_lag  # index at which the next spike is supposed to occur
                c = all_spike_channels[i]
                if t_spike + nw > x.shape[1]:
                    # spike goes out of available signal, stop
                    lags[i:] = cur_lag
                    break
                xi = x[c, range(t_cur, t_spike + nw)]  # portion of signal to inspect
                xstdi = xstd[c, range(t_cur, t_spike + 1)]  # corresponding portion of normalizing signal to use
                cross = np.correlate(xi, w[i]) *1./ xstdi
                if cross.max() < .95:
                    # no match -> this means that there is missing data during the
                    # spike, so better not use this spike
                    lags[i] = -1  # indicate a problem
                else:
                    t_match = t_cur + cross.argmax()
                    cur_lag = cur_lag + (t_spike - t_match)
                    lags[i] = cur_lag
                    t_cur = t_match

            # Does the final lag match the number of missing data?
            if lags[-1] != self.nmissing_30khz:
                raise Exception('Final lag does not match the estimated number of missing data. Missing data detection failed.')

            # Remove unmatched spikes
            ok_spikes = (lags >= 0)
            all_spike_times = all_spike_times[ok_spikes]
            all_spike_channels = all_spike_channels[ok_spikes]
            all_spike_waves = all_spike_waves[ok_spikes]
            w = w[ok_spikes, :]
            lags = lags[ok_spikes]
            n_spike = all_spike_times.shape[0]

            # Display lags
            if do_display:
                figure(1)
                clf()
                plot(all_spike_times, lags)
                gca().set_title('Lags')
                gcf().canvas.draw()
                gcf().canvas.flush_events()

            # Precisely detect missing data
            jumps = np.sqrt((np.diff(x0.astype('single'), axis=1) ** 2).sum(axis=0))
            spike_num, = np.diff(lags).nonzero()  # indices of spikes after which we detected missing data
            n_holes = spike_num.shape[0]
            missing_num = np.diff(lags)[spike_num]
            missing_index = np.zeros(n_holes, dtype=int)
            for i in range(n_holes):
                istart = all_spike_times[spike_num[i]] - lags[spike_num[i]]
                istop = all_spike_times[spike_num[i] + 1] - lags[spike_num[i] + 1]
                istart = min(istart + int(nw / 2.), istop - 1)  # we expect the jump to occur after the meaningful part of the first spike, otherwise there would not have been a good correlation
                missing_index[i] = istart + np.argmax(jumps[istart:istop]) + 1

            # Display locations of missing data
            if do_display:
                figure(2)
                window = range(-200, 201)
                x_ = x  # trick to show graphs using either x0 or x
                for i in range(n_holes):
                    j = spike_num[i]
                    clf()

                    plot(window, x_[all_spike_channels[j], missing_index[i] + window])
                    plot(window, x_[all_spike_channels[j + 1], missing_index[i] + window])

                    xj = x_[all_spike_channels[j], all_spike_times[j] - lags[j] + np.arange(nw)]
                    plot(all_spike_times[j] - lags[j] - missing_index[i] + np.arange(nw), w[j] * (xj.std() * nw) + xj.mean());
                    xj = x_[all_spike_channels[j + 1], all_spike_times[j + 1] - lags[j + 1] + np.arange(nw)]
                    plot(all_spike_times[j + 1] - lags[j + 1] - missing_index[i] + np.arange(nw), w[j + 1] * (xj.std() * nw) + xj.mean())

                    gcf().canvas.draw()
                    gcf().canvas.flush_events()
                    time.sleep(2)

            # Missing data in 1kHz channels
            missing_index_1khz = missing_index // 30
            missing_num_1khz = (missing_index + missing_num) // 30 - missing_index // 30
            # (adjust to match actual number of missing samples at 1kHz)
            d = self.nmissing_1khz - missing_num_1khz.sum()
            c1 = self.channels[[c.dxu for c in self.channels].index(1)]
            nactual_1khz = c1.nsamp - self.nmissing_1khz
            if d == 0:
                # perfect!
                pass
            elif d == 1:
                # add one missing sample at the end
                missing_index_1khz = np.concatenate((missing_index_1khz, [nactual_1khz]))
                missing_num_1khz = np.concatenate((missing_num_1khz, [1]))
            elif 0 < d <= n_holes + 1:
                # add one missing sample to some holes, and if necessary one at the end
                idx = np.linspace(0, n_holes, min(d, n_holes), endpoint=False, dtype=int)
                missing_num_1khz[idx] += 1
                if d == n_holes + 1:
                    missing_index_1khz = np.concatenate((missing_index_1khz, [nactual_1khz]))
                    missing_num_1khz = np.concatenate((missing_num_1khz, [1]))
            elif -(missing_num_1khz > 0).sum() <= d < 0:
                # remove one missing sample to some holes
                idx, = (missing_num_1khz > 0).nonzero()
                idx = idx[np.linspace(0, idx.shape[0], -d, endpoint=False, dtype=int)]
                missing_num_1khz[idx] -= 1
            else:
                print('expected', missing_num_1khz.sum(), 'real', self.nmissing_1khz)
                raise Exception('Number of missing samples are too inconsistent between 30kHz and 1kHz trials')

            # Display a summary
            print('Episode', self.episode_num, ':', self.nmissing_30khz, 'samples missing,', n_holes, 'hole(s).')

            # Store information about missing samples
            self.missing_index = missing_index
            self.missing_num = missing_num
            self.missing_index_1khz = missing_index_1khz
            self.missing_num_1khz = missing_num_1khz

            # Now we can read the data if needed
            if read_data:
                for ch in self.channels:
                    ch.data = ch._read_data(file_handle)

    class Channel(ElphyFile.Channel):

        def __init__(self, ep, channel_num, file_handle, format_option):
            ElphyFile.Channel.__init__(self, ep, channel_num, file_handle, format_option)

            # add nmissing property
            self.nmissing = 0

        def _read_data(self, file_handle):
            # First read the data normally
            data = ElphyFile.Channel._read_data(self, file_handle)

            # Then correct it
            return self._correct_data(data)

        def _correct_data(self, data):
            # type: (np.ndarray) -> np.ndarray
            ep = self.episode  # type: ElphyFileMissingData.Episode
            if self.dxu == 1./30:
                missing_index, missing_num = ep.missing_index, ep.missing_num
            elif self.dxu == 1:
                missing_index, missing_num = ep.missing_index_1khz, ep.missing_num_1khz
            else:
                raise Exception('Channel must be either 30kHz, either 1kHz '
                                'for missing data correction to apply')

            # no missing data?
            if len(missing_index) == 0:
                return data

            # allocate vector for corrected data
            data2 = np.zeros(len(data)+missing_num.sum(), dtype=data.dtype)

            # loop over holes
            cur_lag = 0
            missing_index = np.concatenate((np.array([0]), missing_index))
            for i, num in enumerate(missing_num):
                idx = range
                data2[cur_lag+missing_index[i]:cur_lag+missing_index[i+1]] \
                    = data[missing_index[i]:missing_index[i+1]]
                data2[cur_lag+missing_index[i+1]:cur_lag+missing_index[i+1]+num]\
                    = data[missing_index[i+1]]
                cur_lag += num
            data2[cur_lag + missing_index[-1]:] = data[missing_index[-1]:]

            return data2

    def save_corrected_file(self, file_name=None, save_spikes=True):
        # Save the corrected data back to a .DAT file
        if file_name is None:
            file_name = self.file_name[:-4] + '_MISSING_DATA_CORRECTED.DAT'
        print('Save new file', file_name)
        file_in = open(self.file_name, 'rb')
        file_out = open(file_name, 'wb')

        # Copy header
        file_out.write(file_in.read(18))
        offset = 18

        # Cycle over all blocks
        episode_num = 0  # current episode
        ep = None
        while offset < self.file_size:
            block_size = fread(file_in, '< i')
            block_end = offset + block_size
            identity, string_size = _read_string(file_in, return_size=True)
            header_size = struct.calcsize('< i') + string_size
            file_in.seek(offset)
            if identity == 'RDATA':  # data block: need to re-write with corrected data!
                # block header
                out_start = file_out.tell()
                file_out.write(file_in.read(header_size))
                # data header
                file_out.write(file_in.read(15))
                # data
                for ch in ep.channels:
                    data = ch.get_data().astype(elphy_types[ch.Ktype]).tobytes()
                    file_out.write(data)
                file_in.seek(block_end)
                # replace block size
                out_end = file_out.tell()
                file_out.seek(out_start)
                file_out.write(struct.pack('< i', out_end - out_start))
                file_out.seek(out_end)
            elif (not save_spikes) and identity in ['RSPK', 'RspkWave']:
                # do not save spikes and waveforms to save disk space!!
                file_in.seek(block_end)
            else:
                # block can be copied as is
                if identity == 'B_Ep':
                    episode_num += 1
                    print('Saving episode', episode_num)
                    ep = self.episodes[episode_num-1]
                file_out.write(file_in.read(block_size))
            offset = block_end


if __name__ == "__main__":

    # filename = "../users/luc/test_mouse001.DAT"
    # filename = "../users/cdesbois/data/1716A_VEP1.DAT"
    # filename = "../users/tomasz/EXTRACELLULAR + CARB/171128_SLICE6A_ENT_L23_CARB_ONCE_AGAIN.DAT"
    # filename = "../users/tomasz/EXTRACELLULAR + CARB/171128_SLICE6A_ENTORHINAL_L23_EXT.DAT"
    # filename = "C:/Users/THomas/Thomas/PROJECTS/1311_AudioVis/data/behavior/150105 symmetric behavior/150216/15-02-16_M18-2_VDEFVINF1.DAT"
    # filename = '/media/margot/backup_mgt/DATA/raw_data/MANIP_2018/MANIP_2018_03/0318_CXLEFT/0318_CXLEFT_TUN2.DAT'
    # filename = '../users/Margot/0318_CXLEFT_TUN2.DAT'
    # filename = '../users/Margot/0918_CXLEFT_TUN1.DAT'
    # filename = '../users/Margot/0918_CXLEFT_2_TUN3.DAT'  # = worst errors
    # filename = '../users/Gilles Fortin/normo1.DAT'
    filename = '../users/Margot/TEST_MARGOT_TUN_5EP.DAT'

    ef = ElphyFileMissingData(filename, read_data=False, read_spikes=False)
    ef.save_corrected_file(save_spikes=False)

