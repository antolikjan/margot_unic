
import numpy as np
import os

from pyedflib.edfwriter import EdfWriter
from elphy_reader import ElphyFile, elphy_types


def elphy2edf(elphy_file, edf_file=None):
    # Folder -> scan it and reproduce the tree in a new folder
    if os.path.isdir(elphy_file):
        elphy_dir = elphy_file
        edf_dir = edf_file
        if edf_dir is None:
            edf_dir = elphy_dir + ' - edf'
        if not os.path.isdir(edf_dir):
            os.mkdir(edf_dir)
        for file in os.scandir(elphy_dir):  # type: os.DirEntry
            elphy_file = os.path.join(elphy_dir, file.name)
            if file.is_dir() or '.DAT' in file.name:
                edf_file = os.path.join(edf_dir, file.name.replace('.DAT', '.edf'))
                try:
                    elphy2edf(elphy_file, edf_file)
                except:
                    print('problem with file ', elphy_file)
        return

    # Read Elphy file
    elp = ElphyFile(elphy_file, readdata=True)
    if elp.n_episodes > 1:
        raise Exception('Elphy File must have only one episode')
    ep = elp.episodes[0]
    n_channel = ep.nbchan
    if ep.uX == 'ms':
        sample_freqs = [int(1000/ch.Dxu) for ch in ep.channels]
    elif ep.uX == 'sec':
        sample_freqs = [int(1/ch.Dxu) for ch in ep.channels]
    else:
        raise Exception('Unrecognized time unit:', ep.uX)
    ch0 = ep.channels[0]

    # Open edf file
    if edf_file is None:
        edf_file = elphy_file.replace('.DAT', '.edf')
    edf = EdfWriter(edf_file, n_channel)

    # Header info
    for i, ch in enumerate(ep.channels):
        edf.setSamplefrequency(i, sample_freqs[i])

        data_type = elphy_types[ch.Ktype]
        if data_type != 'h':
            raise Exception('Only short data type is currently handled')

        edf.setDigitalMinimum(i, -32768)
        edf.setDigitalMaximum(i, 32767)
        edf.setPhysicalDimension(i, ch.uY)
        edf.setPhysicalMinimum(i, ch.y0u - 32768*ch.Dyu)
        edf.setPhysicalMaximum(i, ch.y0u + 32767*ch.Dyu)

    # Write data per blocks of one second
    n_seconds = 0
    while n_seconds * sample_freqs[0] < ch0.data.shape[0]:
        for i, ch in enumerate(ep.channels):
            nsamp = ch.data.shape[0]
            idx = range(n_seconds*sample_freqs[i], (n_seconds + 1) * sample_freqs[i])
            if idx[-1] < nsamp:
                data = ch.data[idx]
            else:
                # Need to add NaNs to make one second!!!
                data = np.hstack((ch.data[idx[0]:],
                                  np.zeros(idx[-1]+1-nsamp)))
            edf.writePhysicalSamples(data.astype('double'))
            n_seconds += 1

    edf.close()

if __name__ == '__main__':

    # Read Elphy file
    elphy_file = '../users/Gilles Fortin/HoxA2CrePhox2b27Ala'
    elphy2edf(elphy_file)
    print('done')

