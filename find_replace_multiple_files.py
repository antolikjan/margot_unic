import glob

name_pattern='/home/margot/Cluster/HSM/Explore_params/1718_CXRIGHT_TUN2_pix9x9_size/test/*_metaparams'
find_str='array([[ 2, 11],\n       [ 2, 11]])'
replace_str='(2,11,2,11)'
new_file_label=''

files=sorted(glob.glob(name_pattern))

for fname in files:
    with open(fname,'r') as f:
        content=f.read()
    content=content.replace(find_str,replace_str)
    with open(fname+new_file_label,'w') as f:
        f.write(content)
        