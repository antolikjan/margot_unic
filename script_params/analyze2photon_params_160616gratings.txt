### 1/ TO RUN ###
'load' : True,
'preprocess' : True,
'to_plot' : [],
'cells_to_plot' : [],
'to_include' : ['uncorrected', 'corrected', 'neuropil'],


### 2/ PROTOCOL INFOS ###
'protocol' : 'gratings',
'ep_iblank' : 0,
'ep_fblank' : 0,
'n_im' : 1,
'subep_dur' : 5.0,
'subep_iblank' : 1,
'subep_fblank' : 2,


### 3/ NEURONAL DATA PREPROCESSING ###
'respfile' : '/media/partage/eqbrice_rawdata/ANALYSIS/Margot/160616_36-4/data_group1 - signals.mat',
'expcond_seqs' : {'iso 1.2': [54, 108], 'iso 1': [0, 54]},
'include_blanks' : False,
'baseline_mode' : ['min', 3],
'deconv_tau' : 0,
'lowpass_tau' : 0,
'peak_loc' : [0.10000000000000001, 0.69999999999999996],
'peak_width' : 0.3,
'averaging_loc' : [1.8, 3.0],
'resp_measure' : 'average',
'substract_blank' : True,


### 4/ STIMULUS (natural images protocol only) ###
'imagelistfile' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Mice_data/NaturalImagesJan/image_list - 1x1700 - 7x100 - randomized.txt',
'imagepath' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Mice_data/NaturalImagesJan',
'stim_downsplfact' : 15,
'normalize_stim' : True
