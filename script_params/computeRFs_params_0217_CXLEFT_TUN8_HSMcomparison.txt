### 1/ DATA PATHS ###
'data_files' : {'DN':'/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_DN_regval.mat','VH':'/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_vanhateren_regval.mat','VHRP':'/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_vanhaterenRP_regval.mat'},
'HSM_files' : {'DN':'/home/margot/Cluster/HSM/Fitting_results/0217_CXLEFT_TUN8_DN_regval_reliableunits_standardparams_ntau7_prefitted_BEST_metaparams','VH':'/home/margot/Cluster/HSM/Fitting_results/0217_CXLEFT_TUN8_vanhateren_regval_reliableunits_standardparams_ntau7_seed3_BEST_metaparams','VHRP':'/DATA/Margot/ownCloud/MargotUNIC/Data/LSCSM_data/FittingResults/0217_CXLEFT_TUN8_vanhaterenRP_regval_reliableunits_standardparams_ntau7_seed0_BEST_metaparams'},
'rf_path' : '/DATA/Margot/ownCloud/MargotUNIC/Data/ReceptiveFields',
'common_name': '0217_CXLEFT_TUN8_sepstims_regval',

### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : ['volt1','volt2diag','volt2','volt2sig','HSM'],
'to_compute' : ['RFs'],
'load_existing_RFs' : True,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'cells_to_keep' : [ 2, 13, 14, 16, 18, 20, 21, 32, 43, 48, 52, 53, 58, 62, 64, 66, 68, 72, 73, 82],
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [[0, 8], [0, 8]],
'ROI_downsplfact' : 2,
'include_constant' : True,
'reg_frac' : 0.0,
'val_frac' : 0.2,
'rand_seed' : 0,
'RF_tau' : 2,
'RF_Ntau' : 4,
'STA_biases' : [8000],
'volt2eig_nsig' : 3,
'check_HSM_compatibility' : False,


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 20,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : True