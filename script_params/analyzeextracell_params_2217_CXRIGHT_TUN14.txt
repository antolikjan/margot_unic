### 1/ TO RUN ###
'load_resps' : True,
'load_bstim' : True,
'recompute_stim' : True,
'store_bstim' : False,
'to_plot' : [],
'split_by' : {},


### 2/ DATA PATHS ###
'spikes_file' : '/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/2217_CXRIGHT_TUN14_SPK_reformatted.mat',
'basestim_file' : '/media/margot/DATA/Margot/tex_stimuli/natural_movies/tenseconds_tex_800_stretched_c2ds4/naturalmovie800_001.tex',
'frameorder_file' : '/media/margot/DATA/Margot/tex_stimuli/empty_file',
'stiminfo_file' : '/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/2217_CXRIGHT_TUN14_stiminfo.txt',
'vtag_file' : '/home/margot/Bureau/Jonathan/2217_CXRIGHT_TUN14.DAT/2217_CXRIGHT_TUN14_Vtags.mat',


### 3/ PROTOCOL INFOS ###
'protocol' : 'natural_movies',
'ep_dur' : 360000.0,
'sampling_rate' : 30000.0,


### 4/ DATA PREPROCESSING PARAMETERS ###
'bstim_numtype' : 'uint8',
'stim_numtype' : 'float32',
'crop_range' : [[10, 90], [10, 90]],
'bstim_downsplfact' : 1,
'normalize_stim' : True,
'blank_val' : 0.0,
'discard_wrong_t0' : False,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : [],
'cells_tokeep' : 'all',
'corr_threshold' : -1.0,
'rate_threshold' : 0.0,
'RF_valfrac' : 0.0,
'data_rand_seed' : 0,
'ROI' : [[25, 75], [25, 75]],
'ROI_downsplfact' : 5,
'RF_tau' : 2,
'RF_Ntau' : 3,
'STA_bias' : 0.0001,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 8,
'fig_size' : (5, 10),
'onset_dur' : 0.1,
'offset_dur' : 0.1,
'crosscorr_bin' : 0.01,
'crosscorr_max' : 0.3