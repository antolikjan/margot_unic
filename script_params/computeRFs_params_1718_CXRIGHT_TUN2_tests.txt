### 1/ DATA PATHS ###
'data_files' : {'natmov': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov.mat'},
                
'HSM_files' : {},

'rf_path' : '/media/margot/DATA/Margot/ownCloud/ANALYSIS/RF_analysis/1718_CXRIGHT_TUN2',
'common_name' : '',
'label' : '',


### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : [],
'add_nonlinearity' : True,
'to_compute' : [],
'load_existing_RFs' : False,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'cells_to_keep' : [5, 30, 60],
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [[0,9],[0,9]],
'ROI_downsplfact' : 1,
'include_constant' : True,
'fit_mode' : 'theano',
'epoch_size': 1000,
'num_epochs': 10,
'reg_frac' : 0.0,
'val_frac' : 0.0,
'fit_seed' : None,
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 9,
'STA_biases' : range(0,100001,1000),
'volt2eig_nsig' : 5,
'check_HSM_compatibility' : True,


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 20,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : False,
'smooth_plots' : False,
'rf_colors' : {'HSM': (0,0.8,0.8), 'STALR_NL': (1,0,1), 'volt2diag': (0.5,0,0.5), 'volt2sig': (0.8,0,0.4)},
'stim_colors' : {'natmovRAP': (0,0,1), 'natmov': (0,0.6,0), 'natmovRF': (0,1,0), 'natmovRF3': (1,0,0), 'natmovRP':(0.8,0.8,0), 'natmovRSP':(1,0.5,0), 'all':(0,0,0)}