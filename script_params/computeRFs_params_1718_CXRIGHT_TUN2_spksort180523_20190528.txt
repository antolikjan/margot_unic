### 1/ DATA PATHS ###
'data_file' :  '/home/margot/Bureau/temp_data/jan2_mai_2019/1718_CXRIGHT_TUN2_spksort180523.dat', 
'ep_info_file' :  '/home/margot/Bureau/temp_data/jan2_mai_2019/1718_CXRIGHT_TUN2_spksort180523_epinfo.txt',    
'unit_info_file' :  '/home/margot/Bureau/temp_data/jan2_mai_2019/1718_CXRIGHT_TUN2_spksort180523_unitinfo.txt',
'stim_categories_file' :  '/home/margot/Bureau/temp_data/test_stim_groups',       
'rf_path' : '1718_CXRIGHT_TUN2_spksort180523',
'common_name' : '',
'label' : '',


### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : [],
'time_separable' : False,
'add_nonlinearity' : False,
'to_compute' : [],
'standard_stims' : [],
'load_existing_RFs' : True,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'merge_val_repeats' : True,
'cells_to_keep' : range(49),
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [[2,11],[2,11]],
'ROI_downsplfact' : 1,
'include_constant' : True,
'fit_mode' : 'theano',
'epoch_size' : 5,
'num_epochs' : 20,
'reg_frac' : 0.0,
'val_frac' : 0.0,
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 10,
'STA_biases' : range(0,100000,1000)+range(100000,1000000,50000)+range(1000000,10000001,500000),
'volt2eig_nsig' : 5,
'noise_ceiling_vals' : [1.],


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 15,
'n_disp_cells': 49,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : False,
'smooth_plots' : False,
'rf_colors' : {'HSM': 'cyan', 'volt2diag_NL': 'pink', 'BWT_NL':'purple', 'volt1_NL':'kaki'},
'stim_colors' : {'all':'black', 'natmov':'dark_green', 'natmovRF3':'light_green', 'natmovRF':'light_red', 'natmovRAP':'dark_blue', 'natmovRSP':'orange', 'natmovRP':'yellow'},
'channel_depths' : [],
'order_cells_by': 'corr'