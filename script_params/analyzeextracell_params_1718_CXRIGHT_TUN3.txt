### 1/ TO RUN ###
'load_metadata' : True,
'load_resps' : True,
'load_stim' : True,
'to_plot' : [],
'split_by' : {'stim_type': ['densenoise', 'densenoise_densercenter', 'densenoise_halfdensity', 'vanhateren', 'vanhateren_randphase', 'natmov', 'natmov_randframe'], 'pixel_size':[1,6]},


### 2/ DATA PATHS ###
'epinfo_file' : None,
'python_data_file' : None,
'python_stim_file' : None,
'elphy_file' : 'MANIP_2018/MANIP_2018_17/1718_CXRIGHT/1718_CXRIGHT_TUN3.DAT',
'elphy_structure_file': 'elphy_fileinfo_structure_2018.txt',
'spikesorting_file' : None,
'stimbank_file' : 'mixed_stims/mixedstims_tex_120pix_std60_120717/mixedtypes_fr120_0001.tex',
'stimbank_info_file': 'stimbankinfo_mixedtypes_120_sd60_withgratings.txt',
'frameorder_file' : None,
'save_path' : 'SYNCED_FILES/DATA/extracell_data/python_data',


### 3/ PROTOCOL INFOS ###
'protocol' : 'movies',


### 4/ DATA PREPROCESSING PARAMETERS ###
'stim_numtype' : 'float32',
'crop_range' : [[30,90],[30,90]],
'stim_downsplfact' : 3,
'normalize_stim' : True,
'discard_wrong_t0' : True,
'blank_val' : 0.0,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : [],
'errors_to_discard': ['wrong_init_blanks','wrong_dton','missing_samples'],
'cells_tokeep' : [],
'unit_quality_tokeep': ['raw_multiunit'],
'corr_threshold' : -1.0,
'rate_threshold' : 0.0,
'RF_valfrac' : 0.2,
'data_rand_seed' : None,
'ROI' : [],
'ROI_downsplfact' : 1,
'RF_tau' : 0,
'RF_Ntau' : 8,
'STA_bias' : 10000,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 16,
'onset_dur' : 0.8,
'offset_dur' : 0.5,
'crosscorr_bin' : 0.0166666666667,
'crosscorr_max' : 0.6,
'stim_colors' : {'all': 'black', ('natmov',): 'dark_green', ('natmov_randframe3',): 'light_green', ('natmov_randframe',): 'light_red', ('natmov_randallpix',): 'dark_blue', ('natmov_randspatpix',):'orange', ('natmov_randphase',):'yellow'}