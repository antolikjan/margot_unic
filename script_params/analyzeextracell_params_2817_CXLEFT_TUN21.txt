### 1/ TO RUN ###
'load_resps' : True,
'load_bstim' : True,
'recompute_stim' : True,
'store_bstim' : False,
'to_plot' : [],
'split_by' : {'type': ['densenoise', 'vanhateren', 'vanhateren_randphase', 'natmov', 'natmov_randframe'], 'pixel_size':[1,6]},


### 2/ DATA PATHS ###
'spikes_file' : '2817_CXLEFT_TUN21.kwik',
'raw_spikes_file' : '2817_CXLEFT_TUN21_SPK.mat',
'basestim_file' : '2817_CXLEFT_TUN21_stim_ds3.mat',
'frameorder_file' : 'empty_file',
'stiminfo_file' : '2817_CXLEFT_TUN21_stiminfo.txt',
'vtag_file' : '2817_CXLEFT_TUN21_Vtags.mat',


### 3/ PROTOCOL INFOS ###
'protocol' : 'natural_movies',
'ep_dur' : 359999,
'sampling_rate' : 30000,


### 4/ DATA PREPROCESSING PARAMETERS ###
'bstim_numtype' : 'uint8',
'stim_numtype' : 'float32',
'crop_range' : [],
'bstim_downsplfact' : 1,
'normalize_stim' : True,
'blank_val' : 0.0,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : [],
'cells_tokeep' : [ 2,  3,  5,  6,  7,  9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 24, 27, 30],
'unit_quality_tokeep': 'all',
'corr_threshold' : -1.0,
'rate_threshold' : 0.0,
'RF_valfrac' : 0.2,
'data_rand_seed' : None,
'ROI' : [[10, 34], [10, 34]],
'ROI_downsplfact' : 2,
'RF_tau' : 2,
'RF_Ntau' : 4,
'STA_bias' : 8000.0,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 31,
'onset_dur' : 0.8,
'offset_dur' : 0.5,
'crosscorr_bin' : 0.0166666666667,
'crosscorr_max' : 0.6
