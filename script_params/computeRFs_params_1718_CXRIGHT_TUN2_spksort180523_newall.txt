### 1/ DATA PATHS ###
'data_files' : {'natmov': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov.mat',
                'natmovRF': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randframe.mat',
                'natmovRF3': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randframe3.mat',
                'natmovRP': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randphase.mat',
                'natmovRAP': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randallpix.mat',
                'natmovRSP': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_natmov_randspatpix.mat',
                'all': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_all.mat',
                'all_correlated': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_all_correlated.mat',
                'all_decorrelated': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/1718_CXRIGHT_TUN2_all_decorrelated.mat'},
                
'rf_path' : '/media/margot/DATA/Margot/ownCloud/ANALYSIS/RF_analysis/1718_CXRIGHT_TUN2_spksort180523',
'common_name' : '',
'label' : '',


### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : [],
'add_nonlinearity' : False,
'to_compute' : [],
'load_existing_RFs' : True,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'cells_to_keep' : [],
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [[2,11],[2,11]],
'ROI_downsplfact' : 1,
'include_constant' : True,
'fit_mode' : 'theano',
'epoch_size' : 5,
'num_epochs' : 20,
'reg_frac' : 0.0,
'val_frac' : 0.0,
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 10,
'STA_biases' : range(0,100000,1000)+range(100000,1000000,50000)+range(1000000,10000001,500000),
'volt2eig_nsig' : 5,
'noise_ceiling_vals' : [1.],


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 15,
'n_disp_cells' : 49,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : False,
'smooth_plots' : False,
'rf_colors' : {'HSM': (0,0.8,0.8), 'STALR_NL': (1,0,1), 'volt2diag': (0.5,0,0.5), 'volt2sig': (0.8,0,0.4), 'BWT':(1,1,0)},
'stim_colors' : {'natmovRAP': (0,0,1), 'natmov': (0,0.6,0), 'natmovRF': (0,1,0), 'natmovRF3': (1,0,0), 'natmovRP':(0.8,0.8,0), 'natmovRSP':(1,0.5,0), 'all':(0,0,0)},
'channel_depths' : []