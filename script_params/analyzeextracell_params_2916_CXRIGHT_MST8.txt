### 1/ TO RUN ###
'load_resps' : True,
'load_bstim' : True,
'recompute_stim' : True,
'store_bstim' : False,
'to_plot' : ['volt1'],


### 2/ DATA PATHS ###
'spikes_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_MST8_klustafmt.kwik',
'basestim_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_MST8_stim.mat',
'frameorder_file' : '/DATA/Margot/tex_stimuli/empty_file',
'stiminfo_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN15_stiminfo.txt',
'vtag_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_MST8_Vtags.mat',


### 3/ PROTOCOL INFOS ###
'protocol' : 'DN',
'ep_dur' : 1859998,
'sampling_rate' : 30000,
'repeated_eps' : [],


### 4/ STIMULUS PREPROCESSING PARAMETERS ###
'bstim_numtype' : 'float64',
'stim_numtype' : 'float64',
'crop_range' : [],
'bstim_downsplfact' : 1,
'normalize_stim' : True,
'blank_val' : 0.0,


### 5/ DATA PROCESSING/PLOTTING PARAMETERS ###
'eps_tokeep' : [],
'cells_tokeep' : 'all',
'RF_valfrac' : 0.2,
'data_rand_seed' : 0,
'ROI' : [[3,12],[2,11]],
'ROI_downsplfact' : 1,
'RF_tau' : 0,
'RF_Ntau' : 4,
'STA_bias' : 10000.0,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 10