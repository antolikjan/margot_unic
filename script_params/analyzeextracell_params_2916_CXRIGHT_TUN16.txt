### 1/ TO RUN ###
'load_resps' : True,
'load_bstim' : True,
'recompute_stim' : True,
'store_bstim' : False,
'to_plot' : [],
'split_by' : {},


### 2/ DATA PATHS ###
'spikes_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN16_spksortYP.kwik',
'basestim_file' : '/DATA/Margot/natural_movies/tenseconds_tex_800_stretched_c2ds4/naturalmovie800_001.tex',
'frameorder_file' : '/DATA/Margot/natural_movies/composite_movies/tenseconds_tex_800_stretched_Rand1_mixed_randomization_order.txt',
'stiminfo_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN16_stiminfo.txt',
'vtag_file' : '/DATA/Margot/ownCloud/MargotUNIC/Data/Extracellular_elphy_data/2916_CXRIGHT_TUN16_Vtags.mat',


### 3/ PROTOCOL INFOS ###
'protocol' : 'natural_movies',
'ep_dur' : 359998,
'sampling_rate' : 30000,


### 4/ DATA PREPROCESSING PARAMETERS ###
'bstim_numtype' : 'uint8',
'stim_numtype' : 'float32',
'crop_range' : [],
'bstim_downsplfact' : 2,
'normalize_stim' : True,
'blank_val' : 0.0,


### 5/ ANALYSIS PARAMETERS ###
'eps_tokeep' : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149],
'cells_tokeep' : 'all',
'RF_valfrac' : 0.2,
'data_rand_seed' : 0,
'ROI' : [[12, 28], [20, 36]],
'ROI_downsplfact' : 1,
'RF_tau' : 3,
'RF_Ntau' : 1,
'STA_bias' : 10000.0,
'fourier_space' : 'false',
'spike_tol' : 6,
'ncells_per_fig' : 50
