This directory contains parameter files for data_analysis.analyzeExtracellData and data_analysis.computeRFs programs. Each file corresponds to one set of parameter I once used to analyze one data file with one program

Naming convention : programname_"params"_filename_someadditionalinfo

Parameter names correspond to the ones listed at the beginning of the corresponding program. Note that when this program is run a GUI is displayed to allow for parameter editing. Most parameters are documented : a tooltip appears when you run over it with the mouse. The desired parameter file can be loaded by clicking on the top left "load" button. This will set the GUI to the loaded values.
