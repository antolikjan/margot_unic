### 1/ DATA PATHS ###
'data_files' : {'DN': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_DN.mat',
                'DNhd': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_DNhd.mat',
                'DNdc': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_DNdc.mat',
                'natmov': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_natmov.mat',
                'natmovRF': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_natmovRF.mat',
                'vanhaterenRP': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_vanhaterenRP.mat',
                'vanhateren': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_vanhateren.mat',                
                'all': '/media/margot/DATA/Margot/ownCloud/DATA/extracell_data/python_data/0318_CXLEFT_TUN2_allstim.mat'},
                
'HSM_files' : {},

'rf_path' : '/media/margot/DATA/Margot/ownCloud/ANALYSIS/RF_analysis/0318_CXLEFT_TUN2/allcells',
'common_name' : 'allcells',
'label' : '',


### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : [],
'add_nonlinearity' : True,
'to_compute' : [],
'load_existing_RFs' : False,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'cells_to_keep' : [],
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [],
'ROI_downsplfact' : 1,
'include_constant' : True,
'reg_frac' : 0.0,
'val_frac' : 0.0,
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 10,
'STA_biases' : range(0,100001,1000),
'volt2eig_nsig' : 5,
'check_HSM_compatibility' : True,


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 20,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : False,
'smooth_plots' : False,
'rf_colors' : {'HSM': (0,0.8,0.8), 'STALR_NL': (1,0,1), 'volt2diag': (0.5,0,0.5), 'volt2sig': (0.8,0,0.4)},
'stim_colors' : {'DN': (0,0,1), 'natmov': (0,0.6,0), 'natmovRF': (0,1,0), 'vanhateren': (1,0,0), 'vanhaterenRP':(0.8,0.8,0), 'vanhateren6':(1,0.5,0), 'all':(0,0,0)}