### 1/ DATA PATHS ###
'data_files' : {'DN':'/media/margot/DATA/Margot/ownCloud/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_OLD/0217_CXLEFT_TUN8_DN_regval.mat',
		'vanhateren':'/media/margot/DATA/Margot/ownCloud/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_OLD/0217_CXLEFT_TUN8_vanhateren_regval.mat',
		'vanhaterenRP':'/media/margot/DATA/Margot/ownCloud/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_OLD/0217_CXLEFT_TUN8_vanhaterenRP_regval.mat',
		'all':'/media/margot/DATA/Margot/ownCloud/Data/Extracellular_elphy_data/0217_CXLEFT_TUN8_OLD/0217_CXLEFT_TUN8_regval.mat'},

'HSM_files' : {'DN':'/media/margot/DATA/Margot/HSM_exploreparams/0217_CXLEFT_TUN8/0217_CXLEFT_TUN8_allstim_24validcells_sizeexpl/0217_CXLEFT_TUN8_DN_regval_24validcells_refitfromallstim_BEST_metaparams',
	       'vanhateren':'/media/margot/DATA/Margot/HSM_exploreparams/0217_CXLEFT_TUN8/0217_CXLEFT_TUN8_allstim_24validcells_sizeexpl/0217_CXLEFT_TUN8_regval_24validcells_sizeexpl_58_BEST_metaparams',
	       'vanhaterenRP':'/media/margot/DATA/Margot/HSM_exploreparams/0217_CXLEFT_TUN8/0217_CXLEFT_TUN8_allstim_24validcells_sizeexpl/0217_CXLEFT_TUN8_regval_24validcells_sizeexpl_58_BEST_metaparams',
	       'all':'/media/margot/DATA/Margot/HSM_exploreparams/0217_CXLEFT_TUN8/0217_CXLEFT_TUN8_allstim_24validcells_sizeexpl/0217_CXLEFT_TUN8_regval_24validcells_sizeexpl_58_BEST_metaparams'},

'rf_path' : 'Data/ReceptiveFields/0217_CXLEFT_TUN8',
'common_name': '0217_CXLEFT_TUN8_24validcells',
'label' : '',

### 2/ STEPS TO EXECUTE ###
'load_data' : True,
'model_types' : ['volt2diag','STALR'],
'add_nonlinearity' : True,
'to_compute' : [],
'load_existing_RFs' : False,
'to_plot' : [],
'to_save' : [],


### 3/ RF COMPUTING PARAMETERS ###
'cells_to_keep' : [ 2,  6, 10, 13, 16, 18, 20, 21, 28, 32, 35, 43, 46, 48, 52, 53, 58, 62, 64, 66, 67, 68, 73, 82],
'normalize_stim' : False,
'normalize_resps' : False,
'ROI' : [],
'ROI_downsplfact' : 1,
'include_constant' : True,
'reg_frac' : 0.0,
'val_frac' : 0.2,
'rand_seed' : -1,
'RF_tau' : 0,
'RF_Ntau' : 7,
'STA_biases' : range(0,100001,1000),
'volt2eig_nsig' : 5,
'check_HSM_compatibility' : True,


### 4/ PLOTTING PARAMETERS ###
'ncells_per_fig' : 24,
'im_type' : '.svg',
'display_plots' : True,
'invert_corrplot' : False,
'smooth_plots' : False,
'rf_colors' : {'HSM': (0,0.8,0.8), 'STALR_NL': (1,0,1), 'volt2diag': (0.5,0,0.5), 'volt2sig': (0.8,0,0.4)},
'stim_colors' : {'DN': (0,0,1), 'vanhateren': (1,0,0), 'vanhaterenRP':(0.8,0.8,0), 'all': (0,0,0)}