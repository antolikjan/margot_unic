from struct import pack, unpack
from os.path import getsize, splitext
import numpy as np
from collections import OrderedDict
import pickle


def saveMat(mat,fileobj,fmt='<d',close=True,shape_fmt=None):
    """    
    Saves an array/matrix in binary format
    """ 
    mat = np.array(mat)
    shape = np.array((mat.ndim,) + mat.shape).astype(shape_fmt) if shape_fmt else np.array([])
    mat=mat.flatten().astype(fmt) if fmt else mat.flatten()

    if isinstance(fileobj,str):
        f=open(fileobj,'wb')
    else:
        f=fileobj
    try:
        if shape_fmt:
            f.write(shape.tobytes())
        f.write(mat.tobytes())    
        if close:
            f.close() 
    except:
        f.close()    
    return mat.nbytes + shape.nbytes    
        
    
        
def loadVec(fileobj,fmt='d',start=0,stop='end',size_unit='item',close=True,endian='<',keep_fmt=False):
    """
    Load an array/matrix stored in binary format
    """
        
    try:
        f=open(fileobj,'rb')
    except TypeError:
        f=fileobj
        
    itemsize=len(pack(endian+'1'+fmt,1.))
    if size_unit=='item':
        assert getsize(f.name)%itemsize==0
        mult_factor=itemsize
    else:
        assert size_unit=='byte'
        mult_factor=1
    if stop=='end':
        stop=getsize(f.name)/mult_factor    
        
    f.seek(start*mult_factor)
    binary=f.read((stop-start)*mult_factor)
    if close:
        f.close()    
    
    if keep_fmt:
        dtype=fmt
    else:
        dtype=float
        
    return np.array(unpack(endian+str(len(binary)/itemsize)+fmt,binary),dtype=dtype)    



def load_info_table(filename,sep=' '):
    with open(filename,'r') as f:
        lines=f.read().split('\n')
    keys=lines[0].split(sep)
    types=lines[1].split(sep)
    data_strings=[l.split(sep) for l in lines[2:] if len(l)>0]
    info_dict=OrderedDict()
    for ikey in range(len(keys)):
        if types[ikey]=='bool':
            info_dict[keys[ikey]]=np.array([line[ikey] in ['True','true','1'] for line in data_strings])
        elif types[ikey]=='tuple':
            info_dict[keys[ikey]]=np.array([eval(line[ikey]) for line in data_strings])
        else:    
            info_dict[keys[ikey]]=np.array([line[ikey]  for line in data_strings]).astype(types[ikey])
    return info_dict  



def save_info_table(info_dict,file_name='info_table.txt',sep=' '):
    with open(file_name,'w') as f:          
        f.write(sep.join(info_dict.keys())+'\n' )
        f.write(sep.join([str(type(info_dict[key][0])).split("'")[1].split('numpy.')[-1].split('64')[0].split('_')[0] for key in info_dict.keys()])+'\n' )
        for i_stim in range(len(info_dict.values()[0])):
            f.write(sep.join([str(info_dict[key][i_stim]).replace(' ','') for key in info_dict.keys()])+'\n')
    
    
    
def multiple_pickledump(object_dict,file_name):
    object_names=sorted(object_dict.keys())
    with open(file_name,'wb') as f:
        pickle.dump(object_names,f)
        for key in object_names:
            pickle.dump(object_dict[key],f)
     
       
            
def multiple_pickleload(file_name):
    with open(file_name,'r') as f:            
        object_names=pickle.load(f)
        object_dict={key: pickle.load(f) for key in object_names}
    return object_dict    



def clean_dtype(dtype):
    if str(dtype) is 'S2':
        return 'str'
    else:
        return str(dtype)
       


def dict_to_binary(data_dict,file_name,binary_ext='.binary',text_ext='.txt',shape_fmt='<I'):
    types_dict = {'int32': '<i',
                  #'int64': '<l', 
                  'int64': '<i',  # Store to 4 bytes even if originally in 8 bytes
                  'float32': '<f',
                  #'float64': '<q'}
                  'float64': '<f'} # idem
    
    text_name = splitext(file_name)[0] + text_ext
    binary_name = splitext(file_name)[0] + binary_ext    
    info_dict = OrderedDict([(key,[]) for key in ['name','shape','nested','python_type','binary_type','start','n_bytes']])
    info_dict['name']=sorted(data_dict.keys())
    
    with open(binary_name, 'wb') as bfile :
        position=0
        for key in sorted(data_dict.keys()):
            val = np.array(data_dict[key]) if (type(data_dict[key]) in [list, tuple, np.ndarray]) else np.array([data_dict[key]])
            elem = val.flatten()[0] if (len(val.flatten())>0) else None
            info_dict['shape'].append(val.shape)
            info_dict['start'].append(position)
            # if nested :
            if type(elem) in [list, tuple, np.ndarray]:
                info_dict['nested'].append(True)
                info_dict['python_type'].append(clean_dtype(np.array(elem).dtype))
                info_dict['binary_type'].append(types_dict[info_dict['python_type'][-1]])
                for el in val.flatten():
                    position+=saveMat(el,bfile,fmt=info_dict['binary_type'][-1],close=False,shape_fmt=shape_fmt)
            else:
                info_dict['nested'].append(False)
                info_dict['python_type'].append(clean_dtype(val.dtype))
                info_dict['binary_type'].append(types_dict[info_dict['python_type'][-1]])
                position+=saveMat(val,bfile,fmt=info_dict['binary_type'][-1],close=False,shape_fmt=shape_fmt)
            info_dict['n_bytes'].append(position-info_dict['start'][-1])    
                
    save_info_table(info_dict,text_name)



def binary_to_arrays(binary_seq, data_fmt='<d', shape_fmt='<I'):
    s_nbytes=np.array([0]).astype(shape_fmt).nbytes # Size (number of bytes) of a number in shape info binary segments
    d_nbytes=np.array([0]).astype(data_fmt).nbytes # Size (number of bytes) of a number in data binary segments
    data_list=[]
    pos=0 # Cursor that will scan the binary sequence
    
    while pos<len(binary_seq):
        n_dim = np.fromstring(binary_seq[pos : pos+s_nbytes], dtype=shape_fmt)[0]
        pos += s_nbytes
        shape = np.fromstring(binary_seq[pos : pos+s_nbytes*n_dim], dtype=shape_fmt)
        pos += s_nbytes*n_dim
        data_list.append(np.fromstring(binary_seq[pos : int(pos+d_nbytes*np.prod(shape))], dtype=data_fmt).reshape(shape))
        pos += int(d_nbytes*np.prod(shape))
    return data_list    
        
    

def binary_to_dict(binary_file, info_file, sub_part=None, shape_fmt='<I'):
    info_dict = load_info_table(info_file)
    if sub_part:
        ind_list = np.array([np.where(info_dict['name']==key)[0][0] for key in sub_part if key in info_dict['name']])
    else:
        ind_list = np.arange(len(info_dict['name']))
    data_dict={}
    
    with open(binary_file,'rb') as bfile:
        for ind in ind_list:
            bfile.seek(info_dict['start'][ind])
            binary = bfile.read(info_dict['n_bytes'][ind])
            data_arrays = binary_to_arrays(binary, data_fmt=info_dict['binary_type'][ind], shape_fmt=shape_fmt)
            data_arrays = [a.astype(info_dict['python_type'][ind]) for a in data_arrays]
            data_dict[info_dict['name'][ind]] = data_arrays[0] if len(data_arrays)==1 else np.array(data_arrays).reshape(info_dict['shape'][ind])
                        
    return data_dict