import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt
from numpy.fft import fftn, ifftn, rfftn, irfftn, fftshift, ifftshift


#orientations=np.arange(0,180,10)
#periods=[20,100,200,500]
#orientations=[0,45]
#periods=[20]
#npixside=100
#
#stims=gen_grat_seq(periods=periods,oris=orientations,phase_ratios=[0],sidesize=npixside,functype='sin').reshape(len(periods),len(orientations),npixside,npixside)
#fig=plt.figure()
#count=1
#for i in range(len(periods)):
#    for j in range(len(orientations)):
#        fft=np.abs(fftshift(fft2(stims[i,j,:,:])))
#        plt.subplot(len(periods),len(orientations),count)
#        #plt.pcolor(fft,vmin=0,vmax=fft.max(),cmap='Greys')
#        plt.pcolor(-fft,vmin=-fft.max(),vmax=0,cmap='Greys')
#        count+=1
#        

#fig=plt.figure()
#count=1
#for i in range(len(periods)):
#    for j in range(len(orientations)):
#        fft=np.abs(rfft2(stims[i,j,:,:]))
#        plt.subplot(len(periods),len(orientations),count)
#        #plt.pcolor(fft,vmin=0,vmax=fft.max(),cmap='Greys')
#        plt.pcolor(-fft,vmin=-fft.max(),vmax=0,cmap='Greys')
#        plt.axis([0,npixside,0,npixside])
#        count+=1        

def generate_1overF_spectrum(alpha,npixside=[101,101,21],phasevals=None,ellshape=[1,1,1]):
    npixside=np.array(npixside)
    ellshape=np.array(ellshape)
    assert np.all(np.mod(npixside,2)==1)
    nfreqs=np.ceil(npixside*0.5).astype(int)
    zeroind=nfreqs-1
    
    ellshape=np.abs(ellshape)
    ellshape=ellshape*2./ellshape.sum()
    freqs=np.zeros([3]+list(npixside))
    for i in range(3):
        matshape=np.ones(3)
        matshape[i]=-1
        axisnums=np.where(matshape==1)[0]
        posfreqs=np.arange(nfreqs[i])
        fullfreqs=np.concatenate([-posfreqs[::-1],posfreqs[1:]])*1.
        freqs[i,:,:,:]=np.repeat(np.repeat(fullfreqs.reshape(matshape),npixside[axisnums[0]],axis=axisnums[0]),npixside[axisnums[1]],axis=axisnums[1])
    #spectrum_ampl=np.abs(freqsi*freqsj)**-alpha
    #spectrum_ampl=(np.abs(freqsi)+np.abs(freqsj))**(-alpha)
    spectrum_ampl=np.sqrt(((freqs/ellshape.reshape(-1,1,1,1))**2).sum(axis=0))**(-alpha)
    #spectrum_ampl[np.isinf(spectrum_ampl)]=100
    spectrum_ampl[tuple(zeroind)]=0

    if phasevals is None:
        phasevals=rand(*npixside)*2*np.pi
    elif isinstance(phasevals,float) | isinstance(phasevals,int):
        phasevals=np.ones(npixside)*phasevals
    spectrum_phases=circsym_mat(phasevals,coef=-1)
   
    spectrum=spectrum_ampl*np.exp(spectrum_phases*1j)
    
    return spectrum


def restorestim_fromfft(fftval,mode='full'):
    if mode=='full':    
        stim=ifftn(ifftshift(fftval))
    elif mode=='real':
        #stim=irfftn(ifftshift(fftval))
        stim=irfftn(fftval)
    return stim


def randomize_phases(multidim_var):
    var_mean=multidim_var.mean()
    var_fft=fftshift(fftn(multidim_var-var_mean))
    rand_phases=circsym_mat(rand(*var_fft.shape)*2*np.pi,coef=-1)
    modif_fft=np.abs(var_fft)*np.exp(rand_phases*1j)
    return ifftn(ifftshift(modif_fft)).real+var_mean


def flatten_fourier(multidim_var):
    #var_mean=multidim_var.mean()
    var_fft=fftshift(fftn(multidim_var))#-var_mean))
    modif_fft=np.ones(var_fft.shape)*np.abs(var_fft).mean()*np.exp(np.angle(var_fft)*1j)
    return ifftn(ifftshift(modif_fft)).real#+var_mean


#def white_noise(mat_shape):
    


def randomize_stim_phases(stim,in_time=False,renorm=False):
    if in_time:
        rand_stim=randomize_phases(stim)
        if renorm:
            rand_stim=(rand_stim-rand_stim.mean())*stim.std()/rand_stim.std()+stim.mean()
    else:
        assert (len(stim.shape)>1) and stim.shape[0]>1
        rand_stim=np.zeros(stim.shape)
        for iframe in range(stim.shape[0]):
            rand_stim[iframe]=randomize_phases(stim[iframe])
            if renorm:
                rand_stim[iframe]=(rand_stim[iframe]-rand_stim[iframe].mean())*stim[iframe].std()/rand_stim[iframe].std()+stim[iframe].mean()
    return rand_stim    
            
    

def unique_indices(matshape):
    matshape=np.array(matshape)
    #assert all(np.mod(matshape,2)==1)
    assert len(matshape)<=3
    boolmat=np.zeros(matshape+matshape%2-1).astype(bool)
    zeroind=np.floor(np.array(boolmat.shape)*0.5).astype(int)
    boolmat[:zeroind[0]]=True
    if len(matshape)>1:
        boolmat[zeroind[0],:zeroind[1]]=True      
    if len(matshape)>2:
        boolmat[zeroind[0],zeroind[1],:zeroind[2]]=True  
    
    invboolmat=~boolmat.copy()
    invboolmat[tuple(zeroind)]=False       
    
    for dim in range(len(matshape)):
        if matshape[dim]%2==0:
            part_shape=list(boolmat.shape)
            part_shape[dim]=1
            boolmat=np.concatenate([np.zeros(part_shape).astype(bool),boolmat],axis=dim)
            invboolmat=np.concatenate([np.zeros(part_shape).astype(bool),invboolmat],axis=dim)
        
    return boolmat, invboolmat    
     
     
def circsym_mat(inmat,coef=1):
    outmat=inmat.copy()
    boolmat,invboolmat=unique_indices(outmat.shape)   
    outmat[invboolmat]=outmat[boolmat][::-1]*coef
    return outmat
        


#npix=[101,101,31]  
##patterned_phases=generate_grating(sidesize=npix+1,period=20,ori=0,phase_ratio=0,functype='sin').reshape(npix+1,npix+1)[:,:np.int(npix/2)+1]
##patterned_phases=generate_grating(sidesize=npix+1,period=20,ori=0,phase_ratio=0,functype='sin').reshape(npix+1,npix+1)[:,:np.int(npix/2)+1]+generate_grating(sidesize=npix+1,period=20,ori=30,phase_ratio=0,functype='sin').reshape(npix+1,npix+1)[:,:np.int(npix/2)+1]
##patterned_phases=np.zeros((npix*2+1,npix+1))
##patterned_phases=rand(npix+1,np.int(npix/2)+1)
##patterned_phases[patterned_phases<=0.5]=0
##patterned_phases[patterned_phases>0.5]=np.pi
#patterned_phases=None
##ellshape=[1,10]
#ellshape=[1,3,1]
##alphas=arange(0.5,3.5,0.5)
#alphas=[1.5]
#
#for alpha in alphas:
#    spectrum=generate_1overF_spectrum(alpha,npixside=npix,phasevals=patterned_phases,ellshape=ellshape)
#    stim=restorestim_fromfft(spectrum,mode='full')
#play_video(stim,0.1)    
#stimbis=restorestim_fromfft(spectrum[:,int(npix/2):]*2,mode='real') 
    
#test=unique_indices([3,5])    