import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.signal import gaussian
from scipy.ndimage import filters


def downsample(mat,axis=0,factor=1,mode='mean',anti_alias=2):
    """
    Downsamples an array by a given factor along a given dimension by taking the mean over n=factor consecutive pixels
    If factor isn't a divisor of mat.shape[dim]:  the last pixel of the output array is the mean over the remaining pixels of the input array(n<factor) 
    """
    if mode=='mean':
        toadd=factor-mat.shape[axis]%factor
        downmat=mat.copy()
        if toadd!=factor:
            adddims=downmat.shape[:axis]+(toadd,)+downmat.shape[axis+1:]
            downmat=np.concatenate([downmat,np.ones(adddims)*np.nan],axis=axis)
        newlen=int(downmat.shape[axis]/factor)    
        newshape=downmat.shape[:axis]+(newlen,factor)+downmat.shape[axis+1:]    
        return np.nanmean(downmat.reshape(newshape),axis=axis+1)  
    
    elif mode=='blur': # Nyquist period = 2 * factor
        mat=gaussianLowpass(mat,anti_alias*factor,axis=axis,width_fact=6)
        interp_func=interp1d(range(mat.shape[axis]),mat,axis=axis)
        return interp_func(np.arange(factor/2.-0.5,mat.shape[axis],factor))
    
    
def list_repeats(listobj):
    """
    Takes a list of items and returns a dict of repeated items and another dict of nonrepeated items with their occurences in the list
    """
    # Convert NaNs to strings for them to be handled properly by the set function
    strnan_list=list(listobj[:])
    for i in range(len(strnan_list)):
        if (type(strnan_list[i])==float) and (np.isnan(strnan_list[i])):
            strnan_list[i]='numpy_NaN'
            
    diffitems=set(strnan_list)
    repeats={}
    nonrepeated={}
    for item in diffitems:
        occurrences=np.array([i for i, x in enumerate(strnan_list) if x == item])
        if item=='numpy_NaN':
            item=np.nan
        if len(occurrences)>1:
            repeats[item]=occurrences
        else:
            nonrepeated[item]=occurrences
            
    return repeats, nonrepeated   
    
    
def cross_correlation(vec1,vec2,n_jitters):
    assert len(vec1)==len(vec2)
    cross_corr=np.zeros(2*n_jitters-1)
    for jitter in range(n_jitters):
        cut=n_jitters-jitter
        cross_corr[n_jitters-1+jitter]=np.corrcoef(vec1[int(np.ceil(0.5*cut)):len(vec1)-jitter-int(0.5*cut)],vec2[jitter+int(np.ceil(0.5*cut)):len(vec2)-int(0.5*cut)])[0,1]
        if jitter>0:
            cross_corr[n_jitters-1-jitter]=np.corrcoef(vec2[int(np.ceil(0.5*cut)):len(vec2)-jitter-int(0.5*cut)],vec1[jitter+int(np.ceil(0.5*cut)):len(vec1)-int(0.5*cut)])[0,1]
    return cross_corr    


def clean_plot(ax,to_delete=['frame','ticks'],square=True):
# Remove all unwanted features from a plot (frame, ticks or both)
# Set plot shape to square    
    if 'ticks' in to_delete:
        ax.set_xticks([])
        ax.set_yticks([])
    if 'frame' in to_delete:
        ax.set_frame_on(False)
    if square:    
        ax.set_aspect('equal')  
        

def generate_cmap(n_colors,randomize=True):        
    color_map=plt.cm.get_cmap('hsv',n_colors)
    colors=[color_map(i) for i in range(n_colors)]
    if randomize:
        np.random.shuffle(colors)    
    return colors    


def normalize(mat,axis=0,sub_mean=True,div_std=True):  
    mat=np.array(mat)
    mat_mean=np.nanmean(mat,axis=axis)
    mat_std=np.nanstd(mat,axis=axis)
    mat_std[mat_std==0]=10**-15 # Just to get 0 instead of NaN in the output if std is 0
    mat-=mat_mean
    if div_std:
        mat*=1./mat_std
    if not(sub_mean):
        mat+=mat_mean
    return mat     


def gaussianLowpass(signal,cutoff_period,axis=0,width_fact=4):
    # width_fact : length of the kernel compared to width of the gaussian ?
    gaussian_sd=np.sqrt(2.*np.log(2))/(2.*np.pi)*cutoff_period
    kernel=gaussian(int(np.ceil(width_fact*gaussian_sd)),gaussian_sd)
    return filters.convolve1d(signal,kernel*1./kernel.sum(),axis=axis)


def colornames_to_rgb(color_names,color_file='rgb_color_list.txt'):
    assert (isinstance(color_names,dict) or isinstance(color_names,list))
    with open(color_file,'r') as f:
        color_dict=eval('{'+f.read()+'}')
    if isinstance(color_names,dict):
        return {label: color_dict[color_names[label]] for label in color_names.keys()}
    else:
        return [color_dict[name] for name in color_names]