import numpy as np
import matplotlib.pyplot as plt
#from channel_order import *
from matplotlib.gridspec import GridSpec
from utils.various_tools import downsample, clean_plot
from scipy.interpolate import interp1d
#import matlab.engine
from utils.handlebinaryfiles import loadVec
from collections import OrderedDict


kernel_definitions={'0':     lambda stim : np.ones([stim.shape[0],1]),
                    '1':     lambda stim : stim,
                    '1on':    lambda stim : np.maximum(stim,0),
                    '1off':    lambda stim : np.maximum(-stim,0),
                    '2diag': lambda stim : stim**2,
                    '2':     lambda stim : np.array([np.outer(stim[t,:],stim[t,:])[np.triu_indices(stim.shape[1])] for t in range(stim.shape[0])])}

 
# Fonctions to compute and plot different types of RF: STA, STC Volterra       
    
def lsq_kernel(stim,responses,orders=['0','1'],history_ntau=0,pure_STA=False):
# Compute first and (possibly) second order Volterra kernels (least squares)    
    n_bins,n_pix=stim.shape
    if responses.ndim>1:
        n_cells=responses.shape[1]
    else:
        n_cells=1
    
    eff_stim=[kernel_definitions[key](stim) for key in orders]
    if history_ntau>0:
        history=add_temporal_dim(np.concatenate([np.zeros([history_ntau,n_cells]),responses.reshape(n_bins,n_cells)[:-1]]), None, history_ntau) #n_t, n_tau x n_cells
        eff_stim.append(history)
        orders.append('history')
    to_return=np.cumsum([0]+[mat.shape[1] for mat in eff_stim])
    eff_stim=np.concatenate(eff_stim, axis=1)
    stimcond=np.linalg.cond(eff_stim)   
    if stimcond>2:
        print "Warning: stim matrix is ill-conditioned: "+str(stimcond)
    #rf=np.linalg.lstsq(eff_stim,responses)[0]
    rf = np.dot(eff_stim.T,responses)*1./responses.sum(axis=0) if pure_STA else np.dot( np.linalg.pinv(np.dot(eff_stim.T,eff_stim)) , np.dot(eff_stim.T,responses) )
    if len(rf.shape)==1: rf=np.reshape(rf,(rf.shape[0],1))
    
    return {orders[i] : rf[to_return[i]:to_return[i+1],:] for i in range(len(to_return)-1)}    



#def lsq_kernel(stim,responses,order0=True,order1=True,order2=False,history_ntau=0):
## Compute first and (possibly) second order Volterra kernels (least squares)    
#    n_bins,n_pix=stim.shape
#    if responses.ndim>1:
#        n_cells=responses.shape[1]
#    else:
#        n_cells=1
#    
#    toreturn=[0]
#    if order0:
#        firstcol=np.ones((n_bins,1))
#        toreturn.append(1)
#    else:
#        firstcol=np.zeros((n_bins,0))
#    if order1 in ['standard', True]:
#        stim1=stim
#        toreturn.append(n_pix)
#    elif order1=='onoff':
#        stim1=np.concatenate([np.maximum(stim,0),np.maximum(-stim,0)],axis=1)
#        toreturn+=[n_pix,n_pix]
#    else:
#        stim1=np.zeros((n_bins,0))
#    if order2 in ['full',True]:
#        #stim2=np.array([np.reshape(np.outer(stim[t,:],stim[t,:]),(n_pix**2,1)) for t in range(n_bins)]).squeeze()
#        #toreturn.append(n_pix**2)
#        #stim2=np.array([[stim[t,i]*stim[t,j] for i in range(n_pix) for j in np.arange(i,n_pix)] for t in range(n_bins)])
#        stim2=np.array([np.outer(stim[t,:],stim[t,:])[np.triu_indices(n_pix)] for t in range(n_bins)])
#        toreturn.append(stim2.shape[1])
#    elif order2=='diag':
#        stim2=stim**2
#        toreturn.append(n_pix)
#    else:
#        stim2=np.zeros((n_bins,0))
#    if history_ntau>0:
#        history=add_temporal_dim(np.concatenate([np.zeros([history_ntau,n_cells]),responses.reshape(n_bins,n_cells)[:-1]]), None, history_ntau) #n_t, n_tau x n_cells
#        toreturn.append(history_ntau*n_cells)
#    else:
#        history=np.zeros((n_bins,0))
#    toreturn=np.cumsum(toreturn)
#    eff_stim=np.concatenate((firstcol,stim1,stim2,history), axis=1)
#    stimcond=np.linalg.cond(eff_stim)   
#    if stimcond>2:
#        print "Warning: stim matrix is ill-conditioned: "+str(stimcond)
#    #rf=np.linalg.lstsq(eff_stim,responses)[0]
#    rf=np.dot( np.linalg.pinv(np.dot(eff_stim.T,eff_stim)) , np.dot(eff_stim.T,responses) )
#    if len(rf.shape)==1: rf=np.reshape(rf,(rf.shape[0],1))
#    
#    return [rf[toreturn[i]:toreturn[i+1],:] for i in range(len(toreturn)-1)]


        
def diag_kernel2(k2):
# Returns a sorted list of eigenvectors and eigenvalues    
    if len(k2.shape)==3:
        assert k2.shape[0]==k2.shape[1]
        ncells=k2.shape[2]
    else:    
        n_pix2,ncells=k2.shape
        n_pix=int(np.sqrt(n_pix2))
        assert n_pix2==n_pix**2   
        k2=np.reshape(k2,(n_pix,n_pix,ncells))
        
    k2=[(k2[:,:,i]+k2[:,:,i].T)/2. for i in range(ncells)]
    w, v = np.linalg.eig(k2)
    sortorder=np.argsort(-abs(w))
    # Returns: array of eigenvalues, array of eigenvectors, in descending order
    return np.array([w[i,sortorder[i,:]] for i in range(ncells)]), np.array([v[i,:,sortorder[i,:]].T for i in range(ncells)])
  

def nSigEig(k2):
    # TO BE CHANGED
    n_pix,n_pix,ncells=k2.shape
    #eigvals,eigveccs=diag_kernel2(k2)
    return 2*ones(ncells)
    
    
def partial_k2(eigvals,eigvecs,nsig):
    ncells,k2size,_=eigvecs.shape
    k2=np.zeros((k2size,k2size,ncells))
    for icell in range(ncells):
        k2[:,:,icell]=np.dot(eigvecs[icell,:,:nsig[icell]],np.dot(np.diag(eigvals[icell,:nsig[icell]]),eigvecs[icell,:,:nsig[icell]].T))
    return k2   
  
    
# /!\/!\/!\ A rajouter: possibilite de restreindre la region plottee (parametre subRegion)    
def plot_kernel2(k1,k2eigw,k2eigv,ntau=1,k2neig=10,title='',cell_labels=None,bold_cells=None):
# Plot eigenvectors of the second order Volterra kernel ("subunits")   
    ntimedpix,ncells=k1.shape
    assert ntimedpix%ntau==0
    n_pix=ntimedpix/ntau
    stimsize=int(np.sqrt(n_pix))
    assert n_pix==stimsize**2
    k2neig=min(k2neig,ntimedpix)
    if cell_labels is None:
        cell_labels=[str(i) for i in range(ncells)]
    if bold_cells is None:
        bold_cells=np.zeros(ncells).astype(bool)
    fighandles=[]
    
    for iCell in range(ncells):
        if ntau==1:
            nrow=np.floor(np.sqrt(k2neig+1))
            ncol=np.ceil((k2neig+1)/nrow)
        else:
            nrow=k2neig+1
            ncol=ntau
            
        fighandles.append(plt.figure(figsize=(2*ncol,2*nrow)))
        rf=np.reshape(k1[:,iCell],(ntau,stimsize,stimsize))
        rfmax=abs(rf).max()
        for tau in range(ntau):
            ax=plt.subplot(nrow,ncol,tau+1)           
            plt.imshow(-rf[tau,:],vmin=-rfmax,vmax=rfmax,cmap='RdBu',interpolation='nearest')
            clean_plot(ax)
            ax.set_aspect('equal')
        
        rfmax=abs(k2eigv[iCell,:,:]).max()
        for iEig in range(k2neig):
            for tau in range(ntau):
                ax=plt.subplot(nrow,ncol,(iEig+1)*ntau+tau+1)
                rf=np.reshape(k2eigv[iCell,tau*n_pix:(tau+1)*n_pix,iEig],(stimsize,stimsize))
                plt.pcolor(-rf,cmap='RdBu')
                clean_plot(ax)
                ax.set_aspect('equal')
                #if tau==0: ax.set_title(str(int(100*k2eigw[iCell,iEig])*0.01))
                    
        fighandles[iCell].tight_layout()
        fighandles[iCell].suptitle(title+' Cell '+cell_labels[iCell],fontweight=('bold' if bold_cells[iCell] else None))            
                
    return fighandles
    
 
def plot_kernel1(k1,ntau=1,firstcell=1,scalemax='global',smooth=False,griddim=None,bycol=False,subRegion=None,celllabels=None,bold_cells=None,binwidth=None,stimshape='square',title=''):
# Plot the first order Volterra kernel

#    if restoreChOrder:
#        k1=restore_recsite_order(k1)
    ntimedpix,ncells=k1.shape
    assert ntimedpix%ntau==0
    n_pix=ntimedpix/ntau
    if stimshape=='square':
        sqlen=int(np.sqrt(n_pix))
        assert n_pix==sqlen**2
        stimshape=(sqlen,sqlen)
    else:
        assert n_pix==np.prod(stimshape)
    if scalemax=='global':
        #scalemax=np.ones(ncells)*abs(k1).max()/2
        scalemax=np.ones(ncells)*k1.std()*5
    elif scalemax=='bycell':
        #scalemax=abs(k1).max(axis=0)/2  
        scalemax=k1.std(axis=0)*5
    else:
        scalemax=np.ones(ncells)*scalemax
        
    if celllabels is None:
        celllabels=['Cell '+str(iCell+firstcell) for iCell in range(ncells)]
    if bold_cells is None:
        bold_cells=np.zeros(ncells).astype(bool)    
    if binwidth<>None:
        taulabels=[str(-binwidth*itau-0.5*binwidth)+' ms' for itau in range(ntau)]
    else:
        taulabels=['tau='+str(itau) for itau in range(ntau)]
        
     
    if ntau==1:
        if griddim is None:
            nrow=np.floor(np.sqrt(ncells))
            ncol=np.ceil((ncells)/nrow)
        else:
            nrow,ncol=griddim
    else:
        nrow=ncells
        ncol=ntau
    
    if bycol:    
        gs=GridSpec(nrow,ncol)    
        
    fig=plt.figure(figsize=(2*ncol,2*nrow))       
    for iCell in range(ncells):
        #scalemax=abs(k1[:,iCell]).max()
        for tau in range(ntau):
            if bycol:
                ax=plt.subplot(gs[(iCell*ntau+tau)%nrow,int((iCell*ntau+tau)/nrow)])
            else:    
                ax=plt.subplot(nrow,ncol,iCell*ntau+tau+1)

            toplot=-np.reshape(k1[tau*n_pix:(tau+1)*n_pix,iCell],stimshape).copy()
            if subRegion is not None:
                toplot=toplot[subRegion[0]:subRegion[1],subRegion[2]:subRegion[3]]
            if smooth:
                toplot=smoothMat(toplot)
            plt.imshow(toplot,vmin=-scalemax[iCell],vmax=scalemax[iCell],cmap='RdBu',interpolation='nearest')
            clean_plot(ax)
            if tau==0:
                ax.set_ylabel(celllabels[iCell],fontweight=('bold' if bold_cells[iCell] else None),fontsize=8)
            if iCell==0:
                ax.set_title(taulabels[tau])    
    fig.tight_layout()  
    fig.suptitle(title)      
    return fig
    

#def plot_kernel1_recsite(k1,firstcell=1,scalemax=None,smooth=False):
#    convlist=coltorow(np.arange(32),4,8)
#    n_pix,ncells=k1.shape
#    assert ncells==32
#    stimsize=int(np.sqrt(n_pix))
#    assert n_pix==stimsize**2
#    k1=coltorow(restore_recsite_order(k1),4,8)
#    if scalemax is None:
#        scalemax=abs(k1).max()/2
#     
#    nrow=4
#    ncol=8       
#    fig=plt.figure(figsize=(2*ncol,2*nrow))       
#    for iCell in range(ncells):
#        #scalemax=abs(k1[:,iCell]).max()
#        ax=plt.subplot(nrow,ncol,iCell+1)
#        toplot=-np.reshape(k1[:,iCell],(stimsize,stimsize)).copy()
#        if smooth:
#            toplot=smoothMat(toplot)
#        plt.pcolor(toplot,vmin=-scalemax,vmax=scalemax,cmap='RdBu')
#        plt.axis('off')
#        ax.set_aspect('equal')
#        ax.set_title('Cell '+str(int(convlist[iCell]+firstcell)))
#    fig.tight_layout()        
#    return fig    
    
    
def plotk1_multifig(k1,ntau=1,ncellsperfig='all',scalemax='global',smooth=False,griddim=None,bycol=False,subRegion=None,stimshape='square',celllabels=None,bold_cells=None,title=''):
# Call plot_kernel1 several times to split the group of cells between several figures        
    ncells=k1.shape[1]
    if ncellsperfig=='all':
        ncellsperfig=ncells
    nfigs=int(np.ceil(ncells*1./ncellsperfig))
    figs=[]
    for ifig in range(nfigs):
        # /!\/!\/!\/!\/!\/!\/!\/!\
        #figs.append(plot_kernel1_recsite(k1[:,ifig*ncellsperfig:min((ifig+1)*ncellsperfig,ncells)],firstcell=ifig*ncellsperfig+1,scalemax=scalemax,smooth=smooth))
        if celllabels is not None:
            sublabels=celllabels[ifig*ncellsperfig:min((ifig+1)*ncellsperfig,ncells)]
        else:
            sublabels=None
        if bold_cells is not None:
            sub_bold=bold_cells[ifig*ncellsperfig:min((ifig+1)*ncellsperfig,ncells)]
        else:
            sub_bold=None    
        figs.append(plot_kernel1(k1[:,ifig*ncellsperfig:min((ifig+1)*ncellsperfig,ncells)],ntau=ntau,firstcell=ifig*ncellsperfig+1,scalemax=scalemax,smooth=smooth,griddim=griddim,bycol=bycol,subRegion=subRegion,stimshape=stimshape,celllabels=sublabels,bold_cells=sub_bold,title=title+' - fig'+str(ifig+1)+'/'+str(nfigs)))
    return figs    
    
    
def k2stimsize(n_pix,ntau,n_bins,precision=8):
# Compute size occupied in memory by the second order stimulus variable that needs to be created to solve Volterra2, in Mo    
    return (n_pix**2)*ntau * ((n_pix**2)*ntau+1) * 0.5 * n_bins * precision * 10**-6

    
def restorek2shape(k2):
# For each cell, reshape 2nd order Volterra kernel into a 2D matrix (Npixels*Npixels) 
# (lsq_kernel returns it as a vector that contains only the upper trinanglar and diagonal terms)       
    nval,ncells=k2.shape   
    n_pix=np.sqrt(2*nval+0.25)-0.5
    assert abs(n_pix-np.floor(n_pix))<10**-2
    n_pix=int(n_pix)
    k2mat=[np.zeros((n_pix,n_pix))]*ncells
    for iCell in range(ncells):
        k2mat[iCell][np.triu_indices(n_pix)]=k2[:,iCell]
        k2mat[iCell][np.tril_indices(n_pix,-1)]=k2mat[iCell].T[np.tril_indices(n_pix,-1)]
        # /!\ all values of k2 are used twice in k2mat, except those on the diagonal -> divide by 2:
        k2mat[iCell]=k2mat[iCell]*0.5
        np.fill_diagonal(k2mat[iCell],np.diag(k2mat[iCell])*2) # But not the diagonal
    return np.rollaxis(np.array(k2mat),0,3)


def k2resp(k2,stim):
# Compute response of each cell's 2nd order Volterra kernel to the stimulus    
    assert len(k2.shape)==3
    assert k2.shape[0]==k2.shape[1]
    ncells=k2.shape[2]
    assert k2.shape[0]==stim.shape[1]
    n_bins,n_pix=stim.shape
    
    resp=np.zeros((n_bins,ncells))
    for iCell in range(ncells):
        for t in range(n_bins):
            resp[t,iCell]=np.dot(stim[t,:],np.dot(k2[:,:,iCell],stim[t,:].T))
    
    return resp 
            
            
def STACrevcorOLD(stim,resp):
# Compute STA and STC (by reverse correlation)    
    # A refaire avec operations matricielles, autant que possible
    n_bins,n_pix=stim.shape
    n_bins,ncells=resp.shape
    indices=np.array(np.triu_indices(n_pix))
    k1=np.zeros((n_pix,ncells))
    n_pix2=indices.shape[1]
    k2=np.zeros((n_pix2,ncells))
    
    for icell in range(ncells):
        for ipix in range(n_pix):
            stimvar=np.sum(stim[:,ipix]**2)
            if stimvar>0:
                k1[ipix,icell]=np.sum(stim[:,ipix]*resp[:,icell])*1./stimvar
            
        for ipix in range(n_pix2):
            stim2=stim[:,indices[0,ipix]]*stim[:,indices[1,ipix]]
            stimvar=np.sum(stim2**2)
            if stimvar>0:
                k2[ipix,icell]=np.sum(stim2*resp[:,icell])*1./stimvar
            
    return k1,k2   


def STACrevcor(stim,resp,order='both'):
# Compute STA and STC (by reverse correlation)    
    n_bins,n_pix=stim.shape

    if order in [1, 'both']:
        k1=np.dot(np.diag(1./np.diag(np.dot(stim.T,stim))),np.dot(stim.T,resp))
    else:
        k1=None
    if order in [2,'both']:
        stim2=np.array([np.outer(stim[t,:],stim[t,:])[np.triu_indices(n_pix)] for t in range(n_bins)])
        k2=np.dot(np.diag(1./np.diag(np.dot(stim2.T,stim2))),np.dot(stim2.T,resp))
    else:
        k2=None
            
    return k1,k2       
        

def diagterms(k2vec):
    assert len(k2vec.shape)==2
    nval,ncells=k2vec.shape
    n_pix=np.sqrt(2*nval+0.25)-0.5
    assert abs(n_pix-np.floor(n_pix))<10**-2
    indices=np.triu_indices(int(n_pix))
    return k2vec[np.where(indices[0]==indices[1])[0],:]

#def findVoltNpix(veclen,ntau,order=1:
#    if order==2:
#        n_pix=1./ntau*(np.sqrt(2.25+2*veclen)-1.5)
#    elif order==1:
#        
#    assert abs(n_pix-int(n_pix))<0.01
#    return int(n_pix)


def smoothMat(mat,mult=2,avrange=1):
# Smooth 2D array (sliding average)   
    matbig=np.repeat(np.repeat(mat,mult,axis=0),mult,axis=1)
    nrow,ncol=matbig.shape
    matsmooth=np.zeros(matbig.shape)
    for i in range(nrow):
        for j in range(ncol):
            matsmooth[i,j]=matbig[max(i-avrange,0):min(i+avrange+1,nrow),max(j-avrange,0):min(j+avrange+1,ncol)].mean()
    return matsmooth 
    
    
def add_temporal_dim(stim,resp,ntau):
# Reshape stimulus variable so as to include several time bins in the "pixel" dimension
# (i.e. the stimulus at a given point in time is the currently displayed frame + the ntau-1 previous frames)    
    n_bins,n_pix=stim.shape
    n_bins+=1-ntau
    assert n_bins>0
    timed_stim=np.zeros((n_bins,n_pix*ntau),dtype=stim.dtype)
    for tau in range(ntau):
        timed_stim[:,tau*n_pix:(tau+1)*n_pix]=stim[ntau-tau-1:n_bins+ntau-tau-1,:].copy()
    if resp is not None:    
        return timed_stim, resp[ntau-1:,:] 
    else:
        return timed_stim
    
    
def STA_LR_3D(training_inputs,training_set, kernel_shape, laplace_bias):
    """
    This function implements the spike triggered averaging with laplacian regularization.
    It takes the training inputs and responses, and returns the estimated kernels.
    """
    laplace = laplaceBias_3D(*kernel_shape)
    return np.dot(np.dot(np.linalg.pinv(np.dot(training_inputs.T,training_inputs) + laplace_bias*laplace), training_inputs.T), training_set)


def laplaceBias_3D(sizex,sizey=1,sizez=1):
    S = np.zeros((sizex*sizey*sizez,sizex*sizey*sizez))
    for x in xrange(0,sizex):
        for y in xrange(0,sizey):
            for z in xrange(0,sizez):
                norm = np.zeros((sizex,sizey,sizez))
                norm[x,y,z]=4
                if x > 0:
                    norm[x-1,y,z]=-1
                if x < sizex-1:
                    norm[x+1,y,z]=-1   
                if y > 0:
                    norm[x,y-1,z]=-1
                if y < sizey-1:
                    norm[x,y+1,z]=-1
                if z > 0:
                    norm[x,y,z-1]=-1
                if z < sizez-1:
                    norm[x,y,z+1]=-1
                S[x*sizey*sizez+y*sizez+z,:] = norm.flatten()
    return np.dot(S,S.T)
    
    
def reshape_data_forRF(stims,resps,normalize_stim=True,normalize_resp=False,tau=0,n_tau=1,stim_seed=None,sub_data_fracs=[1],ROI=None,downsample_fact=1):
    sub_data_fracs=np.array(sub_data_fracs)
    assert sub_data_fracs.sum()==1
    if not(isinstance(stims,list)):
        stims=[stims]
    if not(isinstance(resps,list)):
        resps=[resps]
    assert len(stims)==len(resps)
    stims=stims[:] 
    resps=resps[:]
    assert ((ROI is None) and (downsample_fact==1)) or (len(stims[0].shape)==3)
    
    if normalize_stim:
        stim_mean=np.concatenate(stims,axis=0).mean()
        stim_std=np.concatenate(stims,axis=0).std()
    if normalize_resp:
        resp_mean=np.concatenate(stims,axis=0).mean()
        resp_std=np.concatenate(resps,axis=0).std()
        
    for isub in range(len(stims)):
        
        if ROI is not None:
            stims[isub]=stims[isub][:,ROI[0,0]:ROI[0,1],ROI[1,0]:ROI[1,1]]
        
        if downsample_fact!=1:
            stims[isub]=downsample(downsample(stims[isub],2,downsample_fact),1,downsample_fact)
        
        if len(stims[isub].shape)==3:
            ROI_shape=np.array(stims[isub].shape[1:])
            stims[isub]=stims[isub].reshape(stims[isub].shape[0],np.prod(stims[isub].shape[1:]))
        else:
            ROI_shape=None
            
        if normalize_stim:
            stims[isub]=(stims[isub]-stim_mean)*1./stim_std
            
        if normalize_resp:
            resps[isub]=(resps[isub]-resp_mean)*1./resp_std    
            
        if tau>0:    
            stims[isub]=stims[isub][:stims[isub].shape[0]-tau]
            resps[isub]=resps[isub][tau:]
            
        elif tau<0:
            stims[isub]=stims[isub][-tau:]
            resps[isub]=resps[isub][:resps[isub].shape[0]+tau]
            
        if n_tau>1:
            if stims[isub].shape[0]>0:
                stims[isub],resps[isub]=add_temporal_dim(stims[isub],resps[isub],n_tau)
            else:
                stims[isub]=np.zeros((0,stims[isub].shape[1]*n_tau))
    
        if (stim_seed is not None) and (stim_seed>=0):
            np.random.seed(stim_seed)
            random_range=np.random.permutation(np.arange(stims[isub].shape[0]))
            stims[isub]=stims[isub][random_range]
            resps[isub]=resps[isub][random_range,:]
    
    if (len(stims)==1) and sub_data_fracs[0]<1:
        sub_n_bins=np.round(np.array(sub_data_fracs)*stim.shape[0]).astype(int)
        if sub_n_bins.sum()>stim.shape[0]:
            sub_n_bins[np.where(sub_n_bins==sub_n_bins.max())[0][0]]+=stim.shape[0]-sub_n_bins.sum()
        elif sub_n_bins.sum()<stim.shape[0]:
            sub_n_bins[np.where(sub_n_bins==sub_n_bins.min())[0][0]]+=stim.shape[0]-sub_n_bins.sum()
        sub_pos=np.cumsum(np.array([0]+list(sub_n_bins)))   
        stims=[stims[0][sub_pos[i]:sub_pos[i+1]] for i in range(len(sub_data_fracs))]    
        resps=[resps[0][sub_pos[i]:sub_pos[i+1]] for i in range(len(sub_data_fracs))] 
    
    return stims, resps, ROI_shape  
    

def estimate_output_nonlinearity(model_preds,real_resps,n_intervals=20):
    # Very basic for now : cuts the range of model responses in n_intervals intervals and computes the mean real response for each
    assert (model_preds.ndim==1) & (real_resps.ndim==1) & (len(model_preds)==len(real_resps)) # Works only for 1d vectors
    interval_edges=np.linspace(model_preds.min(), model_preds.max(),n_intervals+1)
    interval_centers=0.5*(interval_edges[:-1]+interval_edges[1:])
    mean_resps=np.array([real_resps[(model_preds>=interval_edges[n]) & (model_preds<=interval_edges[n+1])].mean() for n in range(n_intervals)])
    valid=~np.isnan(mean_resps)
    return interp1d(interval_centers[valid],mean_resps[valid],fill_value='extrapolate')


def movie_to_BWT_matlab(movie,matlab_engine=None):
    """    
    movie shape : time x npix x npix
    npix has to be a power of 3
    """    
    assert len(movie.shape)==3
    assert movie.shape[1]==movie.shape[2]
    power=np.log(movie.shape[1])/np.log(3)
    assert np.abs(power-int(power))<10**-10
    if matlab_engine is None:
        matlab_engine = matlab.engine.start_matlab()
    return np.array([np.array(matlab_engine.bwt(matlab.double(im))) for im in movie.tolist()])
    
    
#def movie_to_BWT(movie,rectify=True,BWT_file='/media/margot/DATA/Margot/ownCloud/CODE/Python_code/BWT_filter_9pix'):
def movie_to_BWT(movie,rectify=True,BWT_file='BWT_filter_9pix'):
    """    
    /!\ only working for a 9x9 pixel stimulus
    """    
    movie_shape=movie.shape
    assert movie_shape[1]==81 
    BWT_filter=loadVec(BWT_file).reshape(81,81)
    return np.dot(movie.reshape(movie_shape[0],-1),BWT_filter).reshape(movie_shape)
#    BWT=np.dot(movie,BWT_filter).reshape(movie_shape)
#    if rectify:
#        BWT=BWT.reshape(movie_shape+(1,))
#        BWT=np.concatenate(np.abs(BWT),np.abs(-BWT),axis=len(movie_shape))
#    return BWT
    

def movie_to_BWT_inplace(movie,rectify=True,BWT_file='/media/margot/DATA/Margot/ownCloud/CODE/Python_code/BWT_filter_9pix'):
    """    
    /!\ only working for a 9x9 pixel stimulus
    """    
    movie_shape=movie.shape
    assert movie_shape[1]==81 
    BWT_filter=loadVec(BWT_file).reshape(81,81)
    #print movie.reshape(movie_shape[0],-1).reshape(movie_shape).shape
    movie[...]=np.dot(movie.reshape(movie_shape[0],-1),BWT_filter).reshape(movie_shape)

    

#def movie_to_BWT(movie,rectify=True,BWT_file='/media/margot/DATA/Margot/ownCloud/CODE/Python_code/BWT_filter_9pix'):
#    """    
#    /!\ only working for a 9x9 pixel stimulus
#    """    
#    movie_shape=movie.shape
#    movie=movie.reshape(movie_shape[0],-1)
#    assert movie_shape[1]==81 
#    BWT_filter=loadVec(BWT_file).reshape(81,81)
#    return np.dot(movie,BWT_filter).reshape(movie_shape)
    


    
#def reshape_data_forRF(stim,resps,normalize_stim=True,normalize_resp=False,tau=0,n_tau=1,stim_seed=None,sub_data_fracs=[1]):
#    sub_data_fracs=np.array(sub_data_fracs)
#    assert sub_data_fracs.sum()==1
#    if len(stim.shape)==3:
#        stim=stim.reshape(stim.shape[0],-1)
#    
#    if normalize_stim:
#        stim=(stim-stim.mean())*1./stim.std()
#    if normalize_resp:
#        resps=(resps-resps.mean())*1./resps.std()
#   
#    if tau>0:    
#        stim=stim[:stim.shape[0]-tau]
#        resps=resps[tau:]
#        
#    if n_tau>1:
#        stim,resps=add_temporal_dim(stim,resps,n_tau)
#
#    if (stim_seed is not None) and (stim_seed>=0):
#        np.random.seed(stim_seed)
#        random_range=np.random.permutation(np.arange(stim.shape[0]))
#        stim=stim[random_range]
#        resps=resps[random_range,:]
#    
#    sub_n_bins=np.round(np.array(sub_data_fracs)*stim.shape[0]).astype(int)
#    if sub_n_bins.sum()>stim.shape[0]:
#        sub_n_bins[np.where(sub_n_bins==sub_n_bins.max())[0][0]]+=stim.shape[0]-sub_n_bins.sum()
#    elif sub_n_bins.sum()<stim.shape[0]:
#        sub_n_bins[np.where(sub_n_bins==sub_n_bins.min())[0][0]]+=stim.shape[0]-sub_n_bins.sum()
#    sub_pos=np.cumsum(np.array([0]+list(sub_n_bins)))   
#    sub_stims=[stim[sub_pos[i]:sub_pos[i+1]] for i in range(len(sub_data_fracs))]    
#    sub_resps=[resps[sub_pos[i]:sub_pos[i+1]] for i in range(len(sub_data_fracs))] 
#    
#    return sub_stims, sub_resps   


def noise_ceiling_fit(ceiling_vals,corr_vals,fit_type='linear',averaging_size=1):
    averaged_ceiling_vals=np.array(ceiling_vals).reshape(-1,averaging_size).mean(axis=1).reshape(-1,1)
    averaged_corr_vals=np.array(corr_vals).reshape(-1,averaging_size,np.array(corr_vals).shape[1]).mean(axis=1)
    if fit_type=='linear':
        x_vals=averaged_ceiling_vals
        y_vals=averaged_corr_vals
    elif fit_type=='inverse':
        x_vals=averaged_ceiling_vals
        y_vals=1./averaged_corr_vals
    elif fit_type=='square':
        x_vals=np.array([averaged_ceiling_vals.flatten()**val for val in [1,2]]).T
        y_vals=averaged_corr_vals
    
    coeffs=lsq_kernel(x_vals,y_vals,orders=['0','1'])
    # Correction for cells for which some corr values are nan : treat them one by one by removing the nans from the fit
    nan_cells=np.where(np.isnan(coeffs['1'].flatten()))[0]
    for icell in nan_cells:
        valid_inds=np.where(~np.isnan(y_vals))[0]
        if len(valid_inds)>1:
            cell_coeffs=lsq_kernel(x_vals[valid_inds].reshape(-1,1),y_vals[valid_inds,icell],orders=['0','1'])
            coeffs['0'][0,icell],coeffs['1'][0,icell]=(cell_coeffs['0'],cell_coeffs['1'])
    return (coeffs['0'].squeeze(),coeffs['1'].squeeze())     