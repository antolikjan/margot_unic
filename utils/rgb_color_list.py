colors_dict={
'dark_green': (0, 0.5, 0),
'light_red': (1, 0, 0),
'dark_blue': (0, 0, 0.7),
'yellow': (1, 0.9, 0),
'orange': (1, 0.45, 0.1),
'light_blue': (0.2, 0.6, 1),
'pink': (1, 0.2, 0.8),
'brown': (0.6, 0.4, 0.2),
'light_green': (0, 1, 0),
'purple': (0.7, 0, 0.7),
'green_blue': (0, 0.6, 0.6),
'dark_red': (0.6, 0, 0),
'cyan': (0, 1, 1),
'kaki': (0.45, 0.55, 0.2),
'magenta': (0.7, 0, 0.35),
'black': (0, 0, 0),
'light_grey' : (0.2, 0.2, 0.2),
'dark_grey' : (0.7, 0.7, 0.7)
}


