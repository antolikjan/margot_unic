import glob
import os
from filecmp import cmp as compare_files


dir1='/media/margot/DATA/Margot/tex_stimuli/noise/dense_noise_fr120_sq6_std60_NEW'
dir2='/media/margot/DATA/Margot/tex_stimuli/noise/dense_noise_fr120_sq6_std60'
ext='.tex'
list_file='matches.txt'

matches={}
files1=sorted(glob.glob(os.path.join(dir1,'*'+ext)))
files2=sorted(glob.glob(os.path.join(dir2,'*'+ext)))

with open(list_file,'w') as f:
    for f1 in files1:
        identical=False
        count=-1
        while not(identical) and (count<len(files2)-1):
            count+=1
            identical=compare_files(f1,files2[count])
        if identical:
            print os.path.basename(f1),'>',os.path.basename(files2[count])
            matches[os.path.basename(f1)]=os.path.basename(files2[count])
            f.write(os.path.basename(f1)+' '+os.path.basename(files2[count])+'\n')
        else:
            print os.path.basename(f1),'> ?'
            matches[os.path.basename(f1)]=None
            f.write(os.path.basename(f1)+' ?\n')   