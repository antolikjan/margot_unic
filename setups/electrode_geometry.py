import numpy as np

# Electrode 1x64
klusta_geometry={0: (-5, 55), 1: (-5, 65), 2: (-5, 75), 3: (-5, 85), 4: (-5, 95), 5: (-5, 105), 6: (-5, 115), 7: (-5, 125), 8: (-5, 135), 9: (-5, 145), 10: (-5, 155), 11: (-5, 165), 12: (-5, 175), 13: (-5, 185), 14: (-5, 195), 15: (-5, 205), 16: (-5, 215), 17: (-5, 225), 18: (-5, 235), 19: (-5, 245), 20: (-5, 255), 21: (-5, 265), 22: (-5, 275), 23: (-5, 285), 24: (-5, 295), 25: (-5, 305), 26: (-5, 315), 27: (-5, 45), 28: (-5, 35), 29: (-5, 25), 30: (-5, 15), 31: (-5, 5), 32: (5, 0), 33: (5, 10), 34: (5, 20), 35: (5, 30), 36: (5, 310), 37: (5, 300), 38: (5, 290), 39: (5, 280), 40: (5, 270), 41: (5, 260), 42: (5, 250), 43: (5, 240), 44: (5, 230), 45: (5, 220), 46: (5, 210), 47: (5, 200), 48: (5, 190), 49: (5, 180), 50: (5, 170), 51: (5, 160), 52: (5, 150), 53: (5, 140), 54: (5, 130), 55: (5, 120), 56: (5, 110), 57: (5, 100), 58: (5, 90), 59: (5, 80), 60: (5, 70), 61: (5, 60), 62: (5, 50), 63: (5, 40)}
circus_geometry={k:(klusta_geometry[k][0]*3,klusta_geometry[k][1]/5*23) for k in klusta_geometry.keys()}
circus_geometry[31]=(-10,circus_geometry[31][1])
circus_geometry[32]=(9,circus_geometry[32][1])

channels=np.array(sorted(circus_geometry.keys()))
heights=np.array([circus_geometry[ch][1] for ch in channels])
for ind in np.argsort(-heights):
    spaces='         ' if circus_geometry[channels[ind]][0]>0 else ''
    print spaces+str(channels[ind])+': '+str(circus_geometry[channels[ind]])+','