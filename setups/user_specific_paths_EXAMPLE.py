main_path = '/media/margot/DATA/Margot'

code_path = main_path + '/SYNCED_FILES/CODE/Python_code'

data_paths = [ main_path + '/SYNCED_FILES/DATA/extracell_data/python_data',
	       main_path + '/NON_SYNCED_FILES/DATA/extracell_data/python_data',
	       main_path + '/NON_SYNCED_FILES/DATA/extracell_data/raw_data',
	       '/media/margot/backup_mgt/DATA/raw_elphy_files' ]

stim_paths = [ main_path + '/NON_SYNCED_FILES/STIMULI' ]

rf_paths = [ main_path + '/SYNCED_FILES/ANALYSIS/RF_analysis' ]
