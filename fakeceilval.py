import numpy as np
import glob
import os

dirname='/media/margot/DATA/Margot/ownCloud/ANALYSIS/RF_analysis/nom_a_changer'
files=sorted(glob.glob(dirname+'/*'))

for fname in files:
    ceilval=np.float(os.path.basename(fname).split('_')[10][4:])
    new=np.round(ceilval*1./6,1)
    print ceilval, new
    os.rename(fname,fname.replace('_ceil'+str(ceilval)+'_','_ceil'+str(new)+'_'))