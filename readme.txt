 data_analysis
- code to run on raw data files (+/- spike files) to visualize them and convert them to a format used for RF computation
- code to compute RFs

utils
libraries used by different main programs from other directories

lscsm
code to fit HSM (from Jan Antolik but modified)
/!\ it is a git submodule (a nested git module inside the main one which is Python_code)

lscsm_adapt
additional code related to HSM and specific to my project

miscellaneous
... stuff. some files are libraries used by programs in other directories. maybe could be merged with utils, I dont remember why there were two separate directories

pythonUNIC
Library shared by UNIC lab, written and regularly updated by Thomas Deneux. Some of my programs from other directories are using the elphy-related functions
/!\ it is a git submodule (a nested git module inside the main one which is Python_code)

script_params
parameter files for main programs in other directories


sharing, volt_artef, notebooks, demos : not useful

files in the main directory : not absolutely needed but some might be useful and could be put into miscellaneous/utils
