Take only data files that were recorded with the same stimulation protocol (stimulation_protocol_id)

The precise stimulus set and stimulus order differ between data files. To match episodes corresponding to the same 10s stimulus sequence between data files, use the stim_ids vector : a given id always maps to the same sequence. Note that the repeated sequences were not necessarily the same between recording sessions, thus it probably wont be possible to have the same trial-averaged validation data for all files.

To reconstruct a common stimulus, use raw stimuli provided in separate .mat files (one per protocol id): 
** stim ID "i" of "protocol_1" will be stim[i-1] in protocol_1.mat **
Note that blanks are included in the data files, but not in the provided raw stimuli. Use the "stim_ROI" in each data file to determine where the population RF was in each case, and design a global ROI that contains population RFs of all files. Note that all these RFs should be approximately in the center of the stimulus but wont necessarily overlap. Note also that the provided raw stimuli are already spatially downsampled by a factor of 3, while the provided ROIs are defined BEFORE downsampling.

Final remark : you might notice that raw stimuli and versions that are provided in data files differ a bit (even after accounting for downsampling and cropping). This is because the latter have been recomputed using a recorded frame onset signal so as to account for potential display errors (missing frames, frames lasting too long, etc).
