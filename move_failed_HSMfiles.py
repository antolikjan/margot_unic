import glob
import os

running_dir='/home/margot/Cluster/HSM/Explore_params/2817_CXLEFT_TUN21_outputsynadapt_seeds/running_slurm'
new_dir='/home/margot/Cluster/HSM/Explore_params/2817_CXLEFT_TUN21_outputsynadapt_seeds/to_run'
criterion='Traceback'
suffix_len=15

log_files=sorted(glob.glob(running_dir+'/*log.txt'))

for fname in log_files:
    with open(fname,'r') as f:
        text=f.read()
    if criterion in text:
        print fname
        os.rename(fname,new_dir+'/'+os.path.basename(fname))
        os.rename(fname[:-suffix_len],new_dir+'/'+os.path.basename(fname[:-suffix_len]))
