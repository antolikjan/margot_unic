import glob
import os
from shutil import copyfile

#in_dir='/home/margot/Bureau/temp/bestfiles'
in_dir='/home/margot/Bureau/temp/renamedfiles'
out_dir='/home/margot/Bureau/temp/renamedfiles'
patterns=[]
mode='rename'
#mode='copy'

#replace_in_name={'_noiseceilingval':'_ceil'}
#replace_in_name={'_ceil':'_HSM_nx9_ny9_tau0_ntau10_randseed-1_ceil'}
replace_in_name={key: '_normstim0_normresp0_earlystop1_timesep0_const0_' for key in ['_seed0_','_seed1_','_seed2_']}


files_to_convert=glob.glob(os.path.join(in_dir,'*'+'*'.join(patterns)+'*'))

for fname in files_to_convert:        
    new_fname=os.path.basename(fname)   
    for key in replace_in_name.keys():
        new_fname=new_fname.replace(key,replace_in_name[key])
    if mode=='copy':
        copyfile(fname,os.path.join(out_dir,new_fname))
    elif mode=='rename':
        os.rename(fname,os.path.join(out_dir,new_fname))
    else:
        raise NotImplementedError